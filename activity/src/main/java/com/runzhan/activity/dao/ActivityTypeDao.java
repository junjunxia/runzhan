package com.runzhan.activity.dao;

import com.runzhan.activity.po.ActivityType;
import com.runzhan.base.dao.BaseDao;

public interface ActivityTypeDao extends BaseDao<ActivityType> {
	
}