package com.runzhan.activity.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.runzhan.activity.po.Activity;
import com.runzhan.activity.po.ExpertActivityRelation;
import com.runzhan.activity.vo.ActivityHistoryVo;
import com.runzhan.activity.vo.ActivityManageVo;
import com.runzhan.activity.vo.AttendExpertCountVo;
import com.runzhan.base.dao.BaseDao;

public interface ActivityDao extends BaseDao<Activity> {
	
    int deleteByPrimaryKey(Long id);

    int insert(Activity record);

    int insertSelective(Activity record);

    Activity selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Activity record);

    int updateByPrimaryKey(Activity record);

	int getTotal();

	Integer getByTitle(String title);

	List<ActivityManageVo> getActivityList(@Param("expertId") int expertId, 
										@Param("activityName") String activityName);

	int insertExpertActivityRelation(ExpertActivityRelation rl);

	void updateExpertActivityRelationByExpertId(ExpertActivityRelation rl);

	int deleteExpertActivityRelationByActivityId(Long activityId);

	List<ActivityHistoryVo> getActivityHistoryList(int expertId);

	int updateExpertTopic(ExpertActivityRelation rl);

	int getExpertCount(Long activityId);

	List<AttendExpertCountVo> getAttendExpertCountVo(List<Long> list);

}