package com.runzhan.activity.dao;

import java.util.List;

import com.runzhan.activity.po.ActivityClass;
import com.runzhan.base.dao.BaseDao;

public interface ActivityClassDao extends BaseDao<ActivityClass> {
	
    int deleteByPrimaryKey(Long id);

    int insert(ActivityClass record);

    int insertSelective(ActivityClass record);

    ActivityClass selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ActivityClass record);

    int updateByPrimaryKey(ActivityClass record);
    
    List<ActivityClass> list(ActivityClass ac);
}