package com.runzhan.activity.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.runzhan.activity.po.ActivityOut;
import com.runzhan.activity.vo.ActivityHistoryVo;
import com.runzhan.activity.vo.ActivityManageVo;
import com.runzhan.base.dao.BaseDao;

public interface ActivityOutDao extends BaseDao<ActivityOut> {

	long insert(ActivityOut activityOut);

	void updateByPrimaryKeySelective(ActivityOut activityOut);

	int deleteByPrimaryKey(Long id);

	List<ActivityHistoryVo> getActivityOutHistoryList(int expertId);

	List<ActivityManageVo> getActivityOutList(@Param("expertId") int expertId, 
			@Param("activityName") String activityName);

}