package com.runzhan.activity.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.runzhan.activity.po.Activity;
import com.runzhan.activity.po.ActivityOrg;
import com.runzhan.activity.vo.ActivityOrgVo;
import com.runzhan.base.dao.BaseDao;

public interface ActivityOrgMapper extends BaseDao<ActivityOrg>{
    int deleteByPrimaryKey(Integer id);

    int insert(ActivityOrg record);

    int insertSelective(ActivityOrg record);

    ActivityOrg selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ActivityOrg record);

    int updateByPrimaryKey(ActivityOrg record);

	void deleteByActivityId(Long id);

	List<ActivityOrg> list(Activity ac);

	List<ActivityOrgVo> getActivityOrgVoByActivityId(@Param("list") List<Long> list, @Param("sponsor") String sponsor);
}