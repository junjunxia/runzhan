package com.runzhan.activity.po;

public class ExpertActivityRelation {
	
    private Long id;
    private Long expertId;
    private Long activityId;
    private String inviter;
	private String topic;
    private String remarks;
    private Integer inviteStatus = 0;
    private String activityTitle;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getExpertId() {
		return expertId;
	}
	public void setExpertId(Long expertId) {
		this.expertId = expertId;
	}
	public Long getActivityId() {
		return activityId;
	}
	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}
	public String getInviter() {
		return inviter;
	}
	public void setInviter(String inviter) {
		this.inviter = inviter;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Integer getInviteStatus() {
		return inviteStatus;
	}
	public void setInviteStatus(Integer inviteStatus) {
		this.inviteStatus = inviteStatus;
	}
	public String getActivityTitle() {
		return activityTitle;
	}
	public void setActivityTitle(String activityTitle) {
		this.activityTitle = activityTitle;
	}

	public String toString() {
		return "ExpertActivityRelation [id=" + id + ", expertId=" + expertId
				+ ", activityId=" + activityId + ", inviter=" + inviter
				+ ", topic=" + topic + ", remarks=" + remarks
				+ ", inviteStatus=" + inviteStatus + ", activityTitle="
				+ activityTitle + "]";
	}
	
}