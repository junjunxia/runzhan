package com.runzhan.activity.service;

import java.util.List;

import com.runzhan.activity.po.ActivityType;
import com.runzhan.base.service.BaseService;

public interface ActivityTypeService extends BaseService<ActivityType> {

	List<ActivityType> list();

}
