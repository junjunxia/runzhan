package com.runzhan.activity.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.runzhan.activity.dao.ActivityClassDao;
import com.runzhan.activity.po.Activity;
import com.runzhan.activity.po.ActivityClass;
import com.runzhan.activity.po.ActivityOrg;
import com.runzhan.activity.service.ActivityClassService;
import com.runzhan.base.dao.BaseDao;
import com.runzhan.base.service.impl.BaseServiceImpl;
import com.runzhan.base.util.JacksonUtils;

@Service("activityClasservice")
public class ActivityClassServiceImpl extends BaseServiceImpl<ActivityClass> implements ActivityClassService {

	@Autowired
	private ActivityClassDao activityClassDao;
	
	public String page(Map<String, Object> params) {
        List<ActivityClass> list = activityClassDao.page(params);
        int total = activityClassDao.total(params);

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("total", total);
        map.put("rows", list);

        return JacksonUtils.object2json(map);
    }


	protected BaseDao<ActivityClass> getMapper() {
		return activityClassDao;
	}


	public List<ActivityClass> list() {
		return activityClassDao.queryAll();
	}

	public List<ActivityClass> queryAll2(ActivityClass ac) {
		return activityClassDao.list(ac);
	}


	public Map<String, Object> saveActivityClass(ActivityClass user) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		String flag ="0";
		
		if(null==user.getId()){
			int count = activityClassDao.insert(user);
			if(count>0){
				flag ="1";
			}
		}else{
			activityClassDao.updateByPrimaryKeySelective(user);
			flag ="1";
		}
		jsonMap.put("flag", flag);
		return jsonMap;
	}


	public Map<String, Object> delete(ActivityClass ac) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		String flag ="0";
		
			int count = activityClassDao.deleteByPrimaryKey(ac.getId());
			
			if(count>0){
				flag ="1";
			}
		jsonMap.put("flag", flag);
		return jsonMap;
	}

}
