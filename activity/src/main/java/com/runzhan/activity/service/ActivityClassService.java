package com.runzhan.activity.service;

import java.util.List;
import java.util.Map;

import com.runzhan.activity.po.ActivityClass;
import com.runzhan.base.service.BaseService;

public interface ActivityClassService extends BaseService<ActivityClass> {

	List<ActivityClass> list();
	List<ActivityClass> queryAll2(ActivityClass ac);
	Map<String, Object> saveActivityClass(ActivityClass user);
	Map<String, Object> delete(ActivityClass ac);
}
