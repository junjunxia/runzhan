package com.runzhan.activity.service;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import com.runzhan.activity.po.Activity;
import com.runzhan.activity.po.ActivityOrg;
import com.runzhan.activity.po.ExpertActivityRelation;
import com.runzhan.activity.vo.ActivityHistoryVo;
import com.runzhan.activity.vo.ActivityManageVo;
import com.runzhan.activity.vo.ActivityOrgVo;
import com.runzhan.activity.vo.AddActivityOutVo;
import com.runzhan.activity.vo.DeleteActivityVo;
import com.runzhan.base.service.BaseService;


public interface ActivityService extends BaseService<Activity> {

	int getTotal();
	
	@Transactional
	Map<String, Object> delete(Activity user);
	
	Activity get(Long id);
	
	@Transactional
	Map<String, Object> saveActivity(Activity user);
	
	List<ActivityManageVo> getActivityList(int expertId, String activityName);
	
	List<ActivityOrg> orglist(Activity ac);
	
	@Transactional
	Map<String, Object> saveActivityOut(AddActivityOutVo addActivityOutVo);
	
	@Transactional
	Map<String, Object> deleteActivity(DeleteActivityVo vo);

	List<ActivityHistoryVo> getActivityHistoryList(int expertId);

	List<ActivityOrgVo> getActivityOrgVoByActivityId(List<Long> activityIdList, String sponsor);

	Map<String, Object> updateExpertTopic(ExpertActivityRelation rl);

	int getExpertCount(Long activityId);
}
