package com.runzhan.activity.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.runzhan.activity.dao.ActivityTypeDao;
import com.runzhan.activity.po.ActivityType;
import com.runzhan.activity.service.ActivityTypeService;
import com.runzhan.base.dao.BaseDao;
import com.runzhan.base.service.impl.BaseServiceImpl;

@Service("activityTypeService")
public class ActivityTypeServiceImpl extends BaseServiceImpl<ActivityType> implements ActivityTypeService {

	@Autowired
	private ActivityTypeDao activityTypeDao;
	


	protected BaseDao<ActivityType> getMapper() {
		return activityTypeDao;
	}



	public List<ActivityType> list() {
		return activityTypeDao.queryAll();
	}

}
