package com.runzhan.activity.service;

import java.util.List;

import com.runzhan.activity.po.ActivityOut;
import com.runzhan.activity.vo.ActivityManageVo;
import com.runzhan.base.service.BaseService;

public interface ActivityOutService extends BaseService<ActivityOut> {

	List<ActivityManageVo> getActivityOutList(int expertId, String activityName);

}
