package com.runzhan.activity.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.runzhan.activity.constant.ActivityConstant;
import com.runzhan.activity.dao.ActivityDao;
import com.runzhan.activity.dao.ActivityOrgMapper;
import com.runzhan.activity.dao.ActivityOutDao;
import com.runzhan.activity.po.Activity;
import com.runzhan.activity.po.ActivityOrg;
import com.runzhan.activity.po.ActivityOut;
import com.runzhan.activity.po.ExpertActivityRelation;
import com.runzhan.activity.service.ActivityService;
import com.runzhan.activity.vo.ActivityHistoryVo;
import com.runzhan.activity.vo.ActivityManageVo;
import com.runzhan.activity.vo.ActivityOrgVo;
import com.runzhan.activity.vo.AddActivityOutVo;
import com.runzhan.activity.vo.AttendExpertCountVo;
import com.runzhan.activity.vo.DeleteActivityVo;
import com.runzhan.base.dao.BaseDao;
import com.runzhan.base.service.impl.BaseServiceImpl;
import com.runzhan.base.util.JacksonUtils;
import com.runzhan.base.vo.PageBean;

@Service("activityService")
public class ActivityServiceImpl extends BaseServiceImpl<Activity> implements ActivityService {
	
	Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private ActivityDao activityDao;
	@Autowired
	private ActivityOutDao activityOutDao;
	@Autowired
	private ActivityOrgMapper activityOrgMapper;
	
	public String page(Map<String, Object> params) {
        List<Activity> list = activityDao.page(params);
        
        List<Long> activityIds = new ArrayList<Long>();
        Map<Long, Activity> result = new HashMap<Long, Activity>();
        for (Activity a : list) {
        	result.put(a.getId(), a);
        	activityIds.add(a.getId());
        }
        List<AttendExpertCountVo> countList = new ArrayList<AttendExpertCountVo>();
        if (!CollectionUtils.isEmpty(activityIds)) {
        	countList = activityDao.getAttendExpertCountVo(activityIds);
        }
        for (AttendExpertCountVo vo : countList) {
        	if (result.containsKey(vo.getActivityId())) {
        		result.get(vo.getActivityId()).setExpertCount(vo.getCount());
        	}
        }
        
        Map<String, Object> map = new HashMap<String, Object>();
        Collection<Activity> activityList = result.values();
        
        int total = activityDao.total(params);
        int pageSize = Integer.valueOf((String) params.get("pageSize"));
        PageBean<Activity> page = new PageBean<Activity>(new ArrayList<Activity>(activityList), pageSize, total%pageSize==0?total/pageSize:total/pageSize+1, total, pageSize,Integer.parseInt(params.get("pageNum").toString()));
        return JacksonUtils.object2json(page);
    }
	
	public int getTotal() {
		return activityDao.getTotal();
	}


	protected BaseDao<Activity> getMapper() {
		return activityDao;
	}


	public Map<String, Object> delete(Activity user) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		String flag ="0";
		
			int count = activityDao.deleteByPrimaryKey(user.getId());
			
			if(count>0){
				flag ="1";
				activityOrgMapper.deleteByActivityId(user.getId());
			}
		jsonMap.put("flag", flag);
		return jsonMap;
	}


	public Activity get(Long id) {
		return activityDao.selectByPrimaryKey(id);
	}


	@Transactional
	public Map<String, Object> saveActivity(Activity user) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		String flag ="0";
		
		if(null==user.getId()){
			try {
				int count = activityDao.insert(user);
				if(count>0){
					String orgType = user.getOrganizationType();
					if(StringUtils.isNotEmpty(orgType)){
						String[] orgTypes = orgType.split(";");
						String[] orgNames = user.getOrganizationName().split(";");
						if(orgTypes.length>0){
//							Integer id = activityDao.getByTitle(user.getTitle());
							for(int a = 0;a<orgTypes.length;a++){
								activityOrgMapper.insert(new ActivityOrg(user.getId().intValue(),Integer.parseInt(orgTypes[a]),orgNames[a]));
							}
							
						}
					}
					flag ="1";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
		}else{
			activityDao.updateByPrimaryKeySelective(user);
			String orgType = user.getOrganizationType();
			if(StringUtils.isNotEmpty(orgType)){
				activityOrgMapper.deleteByActivityId(user.getId());
				String[] orgTypes = orgType.split(";");
				String[] orgNames = user.getOrganizationName().split(";");
				if(orgTypes.length>0){
//					Integer id = activityDao.getByTitle(user.getTitle());
					for(int a = 0;a<orgTypes.length;a++){
						activityOrgMapper.insert(new ActivityOrg(user.getId().intValue(),Integer.parseInt(orgTypes[a]),orgNames[a]));
					}
					
				}
			}
			flag ="1";
		}
		jsonMap.put("flag", flag);
		return jsonMap;
	}


	public List<ActivityManageVo> getActivityList(int expertId, String activityName) {
		return activityDao.getActivityList(expertId, activityName);
	}


	public List<ActivityOrg> orglist(Activity ac) {
		
		List<ActivityOrg> list = activityOrgMapper.list(ac);
		return list;
	}


	public Map<String, Object> saveActivityOut(AddActivityOutVo addActivityOutVo) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		String flag ="0";
		
		try {
			ActivityOut activityOut = assemActivityOut(addActivityOutVo);
			ExpertActivityRelation rl = assemExpertActivityRelation(addActivityOutVo,activityOut.getId());
			
			String orgType = addActivityOutVo.getOrganizationType();
			String[] orgTypes = null;
			String[] orgNames = null;
			if(StringUtils.isNotEmpty(orgType)){
				orgTypes = orgType.split(";");
				orgNames = addActivityOutVo.getOrganizationName().split(";");
			}
			if(null==addActivityOutVo.getId()){
				activityOutDao.insert(activityOut);
				rl.setActivityId(activityOut.getId());
				activityDao.insertExpertActivityRelation(rl);
				if(null != orgTypes){
					if(orgTypes.length>0){
						Integer activityOutId = Integer.valueOf(String.valueOf(activityOut.getId()));
						for(int a = 0;a<orgTypes.length;a++){
							activityOrgMapper.insert(new ActivityOrg(activityOutId,Integer.parseInt(orgTypes[a]),orgNames[a]));
						}
					}
				}
				flag ="1";
			}else{
				activityOutDao.updateByPrimaryKeySelective(activityOut);
				activityDao.updateExpertActivityRelationByExpertId(rl);
				
				// ��ȫɾ����֯�ṹ�������
				activityOrgMapper.deleteByActivityId(addActivityOutVo.getId());
				
				if(null != orgTypes){
					if(orgTypes.length>0){
						Integer activityOutId = Integer.valueOf(String.valueOf(activityOut.getId()));
						for(int a = 0;a<orgTypes.length;a++){
							activityOrgMapper.insert(new ActivityOrg(activityOutId,Integer.parseInt(orgTypes[a]),orgNames[a]));
						}
					}
				}
				
				flag ="1";
			}
		} catch (Exception e) {
			logger.error("添加活动异常", e);
		}
		
		jsonMap.put("flag", flag);
		return jsonMap;
	}

	private ExpertActivityRelation assemExpertActivityRelation(
			AddActivityOutVo vo, Long activityId) {
		ExpertActivityRelation rl = new ExpertActivityRelation();
		rl.setExpertId(vo.getExpertId());
		rl.setActivityId(activityId);
		rl.setInviter(vo.getInviter());
		rl.setTopic(vo.getTopic());
		rl.setRemarks(vo.getRemarks());
		rl.setActivityTitle(vo.getTitle());
		return rl;
	}

	private ActivityOut assemActivityOut(AddActivityOutVo vo) {
		ActivityOut activityOut = new ActivityOut();
		activityOut.setTitle(vo.getTitle());
		activityOut.setBeginTime(vo.getBeginTime());
		activityOut.setEndTime(vo.getEndTime());
		activityOut.setId(vo.getId());
		activityOut.setVenue(vo.getVenue());
		activityOut.setRemarks(vo.getRemarks());
		return activityOut;
	}


	public Map<String, Object> deleteActivity(DeleteActivityVo vo) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		String flag ="0";
		if (null == vo) {
			logger.error(" deleteActivity--����Ϊ��");
			jsonMap.put("msg", "δѡ���¼��������ѡ��");
			jsonMap.put("flag", flag);
			return jsonMap;
		}
		
		if (ActivityConstant.ACTIVITY_TAG_IN.equals(vo.getActivityTag())) {// �ڲ��
			int count = activityDao.deleteByPrimaryKey(vo.getActivityId());
			if(count == 0){
				logger.error(" deleteActivity--�ڲ��--�ɾ��ʧ��");
				jsonMap.put("msg", "ɾ��ʧ�ܣ�");
				jsonMap.put("flag", flag);
				return jsonMap;
			}
			
			count = activityDao.deleteExpertActivityRelationByActivityId(vo.getActivityId());
			if(count == 0){
				logger.error(" deleteActivity--�ڲ��--ר�һ��ϵɾ��ʧ��");
				jsonMap.put("msg", "ɾ��ʧ�ܣ�");
				jsonMap.put("flag", flag);
				return jsonMap;
			}
			
			activityOrgMapper.deleteByActivityId(vo.getActivityId());
		} else {// �ⲿ�
			int count = activityOutDao.deleteByPrimaryKey(vo.getActivityId());
			if(count == 0){
				logger.error(" deleteActivity--�ⲿ�--�ɾ��ʧ��");
				jsonMap.put("msg", "ɾ��ʧ�ܣ�");
				jsonMap.put("flag", flag);
				return jsonMap;
			}
			count = activityDao.deleteExpertActivityRelationByActivityId(vo.getActivityId());
			if(count == 0){
				logger.error(" deleteActivity--�ڲ��--ר�һ��ϵɾ��ʧ��");
				jsonMap.put("msg", "ɾ��ʧ�ܣ�");
				jsonMap.put("flag", flag);
				return jsonMap;
			}
			
			activityOrgMapper.deleteByActivityId(vo.getActivityId());
		}
		
		flag ="1";
		jsonMap.put("flag", flag);
		return jsonMap;
	}


	public List<ActivityHistoryVo> getActivityHistoryList(int expertId) {
		List<ActivityHistoryVo> activityHistory = new ArrayList<ActivityHistoryVo>();
		List<ActivityHistoryVo> l1 = activityDao.getActivityHistoryList(expertId);
		List<ActivityHistoryVo> l2 = activityOutDao.getActivityOutHistoryList(expertId);

		if (null != l1) {
			activityHistory.addAll(l1);
		}
		if (null != l2) {
			activityHistory.addAll(l2);
		}
		
		

		List<ActivityHistoryVo> activityManageList = new ArrayList<ActivityHistoryVo>();
		if (!CollectionUtils.isEmpty(activityHistory)) {
			List<Long> activityIdList = new ArrayList<Long>(activityHistory.size());
			Map<Long, ActivityHistoryVo> result = new HashMap<Long, ActivityHistoryVo>();
			for (ActivityHistoryVo vo : activityHistory) {
				Long acId = vo.getActivityId();
				activityIdList.add(acId);
				result.put(acId, vo);
			}
			
			List<ActivityOrgVo> activityOrgVoList = getActivityOrgVoByActivityId(activityIdList, null);
			for (ActivityOrgVo vo : activityOrgVoList) {
				if (result.containsKey(vo.getActivityId())) {
					ActivityHistoryVo ac = result.get(vo.getActivityId());
					ac.setSponsor(vo.getSponsor());
					ac.setOrganizer(vo.getOrganizer());
					activityManageList.add(ac);
				}
			}
		}
		return activityHistory;
	}


	public List<ActivityOrgVo> getActivityOrgVoByActivityId(List<Long> activityIdList, String sponsor) {
		return activityOrgMapper.getActivityOrgVoByActivityId(activityIdList, sponsor);
	}


	public Map<String, Object> updateExpertTopic(ExpertActivityRelation rl) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		String flag ="1";
		
		int cnt = activityDao.updateExpertTopic(rl);
		if (cnt == 0) {
			logger.error(" updateExpertTopic--失败--params:"+rl);
			jsonMap.put("msg", "操作失败");
			flag ="0";
		}
		
		jsonMap.put("flag", flag);
		return jsonMap;
	}


	public int getExpertCount(Long activityId) {
		return activityDao.getExpertCount(activityId);
	}

}
