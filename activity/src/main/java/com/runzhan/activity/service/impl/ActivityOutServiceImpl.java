package com.runzhan.activity.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.runzhan.activity.dao.ActivityOutDao;
import com.runzhan.activity.po.ActivityOut;
import com.runzhan.activity.service.ActivityOutService;
import com.runzhan.activity.vo.ActivityManageVo;
import com.runzhan.base.dao.BaseDao;
import com.runzhan.base.service.impl.BaseServiceImpl;

@Service("activityOutService")
public class ActivityOutServiceImpl extends BaseServiceImpl<ActivityOut>
		implements ActivityOutService {

	@Autowired
	private ActivityOutDao activityOutdao;
	

	protected BaseDao<ActivityOut> getMapper() {
		return activityOutdao;
	}
	

	public List<ActivityManageVo> getActivityOutList(int expertId, String activityName) {
		return activityOutdao.getActivityOutList(expertId, activityName);
	}

}
