package com.runzhan.activity.constant;

public interface ActivityConstant {

	// 活动类型标识：（in：内部活动hd_activity；out：外部活动hd_activity_out）
	String ACTIVITY_TAG_IN = "in";
	String ACTIVITY_TAG_OUT = "out";
}
