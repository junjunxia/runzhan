package com.runzhan.activity.vo;

import java.util.Date;

public class ActivityManageVo {
	private Long id;
	private String title;
	private String sponsor;
	private String organizer;
	private String inviter;
	private String topic;
	private Date beginTime;
	private Date endTime;
	private String venue;
	private String remarks;
	private String type;//
	
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getOrganizer() {
		return organizer;
	}
	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}
	public String getInviter() {
		return inviter;
	}
	public void setInviter(String inviter) {
		this.inviter = inviter;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String toString() {
		return "ActivityManageVo [id=" + id + ", title=" + title + ", sponsor="
				+ sponsor + ", organizer=" + organizer + ", inviter=" + inviter
				+ ", topic=" + topic + ", beginTime=" + beginTime + ", venue="
				+ venue + ", remarks=" + remarks + "]";
	}
	
}
