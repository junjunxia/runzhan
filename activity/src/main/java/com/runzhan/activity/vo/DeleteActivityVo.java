package com.runzhan.activity.vo;

public class DeleteActivityVo {

	// 活动ID
	private Long activityId;
	// 活动类型标识：（in：内部活动hd_activity；out：外部活动hd_activity_out）
	private String activityTag;

	public Long getActivityId() {
		return activityId;
	}
	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}
	public String getActivityTag() {
		return activityTag;
	}
	public void setActivityTag(String activityTag) {
		this.activityTag = activityTag;
	}


}
