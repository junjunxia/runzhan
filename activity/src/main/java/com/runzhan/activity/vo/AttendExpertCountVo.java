package com.runzhan.activity.vo;

public class AttendExpertCountVo {
	private Long activityId;
	private int count;
	public Long getActivityId() {
		return activityId;
	}
	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

}
