package com.runzhan.sms.context;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.runzhan.sms.exception.SMSException;
import com.runzhan.sms.service.ChannelManagerService;

@Component
public class ResourceContextLoad {
	
	public static final Logger logger = Logger.getLogger(ResourceContextLoad.class);
	
	@Autowired
	private ChannelManagerService channelManagerService;
	
	@PostConstruct
	public void loadGlobalData() throws SMSException {
		// 加载短信通道信息
		channelManagerService.queryDB();
		// 查询应用名称
//			List<SMSAuthBase> appList = smsSendDao.findAuthInfo();
//			// 应用信息
//			for (SMSAuthBase appInfo : appList) {
//				SysConstant.APP_MAP.put(appInfo.getSapplicationno(),
//						appInfo.getSapplicationname());
//			}
	}

	

}
