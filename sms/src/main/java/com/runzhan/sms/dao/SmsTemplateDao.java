package com.runzhan.sms.dao;

import java.util.List;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.sms.po.SmsTemplate;
import com.runzhan.sms.po.TemplateObjectDict;

public interface SmsTemplateDao extends BaseDao<SmsTemplate> {

	List<TemplateObjectDict> getTemplateObjectDict();

	void updateSmsTemplate(SmsTemplate params);

	void batchDelete(List<Long> list);

	SmsTemplate getById(Long id);

}
