package com.runzhan.sms.dao;

import java.util.List;

import com.runzhan.sms.exception.SMSException;
import com.runzhan.sms.po.SMSAuthBase;
import com.runzhan.sms.po.SMSChannelBase;
import com.runzhan.sms.po.SMSLogBase;


public interface SMSSendDao {

	/**
	 * @Title: findAllSmsChannel
	 * @Description: 查找所有短信通道
	 * @param
	 * @return List<SMSChannel>
	 * @throws SMSException
	 */
	public List<SMSChannelBase> findAllSmsChannel() throws SMSException;

	/**
	 * @Title: updateSmsChWeigth
	 * @Description: 修改短信通道权重
	 * @param SMSChannelBase
	 * @return int
	 */
	public int updateSmsChWeigth(SMSChannelBase sb);

	/**
	 * @Title: findAuthInfo
	 * @Description: 查找所有应用信息
	 * @param
	 * @return List<SMSAuthBase>
	 * @throws SMSException
	 */
	public List<SMSAuthBase> findAuthInfo() throws SMSException;

	/**
	 * @Title: findAppInfo
	 * @Description: 查找所有应用
	 * @param
	 * @return List<SMSAuthBase>
	 * @throws SMSException
	 */
	public List<SMSAuthBase> findAppInfo() throws SMSException;

	/**
	 * @Title: insertSMSSendLogs
	 * @Description: 插入短信发送结果
	 * @param SMSLogBase
	 * @return void
	 * @throws SMSException
	 */
	public void insertSMSSendLogs(final SMSLogBase record) throws Exception;

	/**
	 * @Title: insertBatchSMSSendLogs
	 * @Description: 批量保存短信发送结果
	 * @return: int
	 */
	int insertBatchSMSSendLogs(List<SMSLogBase> list);
}
