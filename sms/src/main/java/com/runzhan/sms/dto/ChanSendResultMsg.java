package com.runzhan.sms.dto;

public class ChanSendResultMsg {
	private String resultCode;
	private String resulstMsg;

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public ChanSendResultMsg() {
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResulstMsg(String msg) {
		this.resulstMsg = msg;
	}

	public String getResulstMsg() {
		return resulstMsg;
	}


	public String toString() {
		return "ChanSendResultMsg [resultCode=" + resultCode + ", resulstMsg="
				+ resulstMsg + "]";
	}
}
