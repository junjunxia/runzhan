package com.runzhan.sms.dto;

import java.io.Serializable;

public class ResponseDto implements Serializable {
	private static final long serialVersionUID = -7473870192544064115L;
	
	private int errCode;
	private String resMsg;
	private int status;
	
	public int getErrCode() {
		return errCode;
	}
	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}
	public String getResMsg() {
		return resMsg;
	}
	public void setResMsg(String resMsg) {
		this.resMsg = resMsg;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public String toString() {
		return "ResponseDto [errCode=" + errCode + ", resMsg=" + resMsg + ", status=" + status + "]";
	}
	
}
