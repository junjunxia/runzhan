package com.runzhan.sms.dto;

import java.io.Serializable;
import java.util.Map;

public class TemplateSmsBean implements Serializable {
	private static final long serialVersionUID = 5441719265326676871L;

	private String[] mobiles;// 接收方
	private int serviceType = 1;
	private Map<String, Object> data; // 短信数据
	private String template; // 短信模板
	
	public String[] getMobiles() {
		return mobiles;
	}
	public void setMobiles(String[] mobiles) {
		this.mobiles = mobiles;
	}
	public int getServiceType() {
		return serviceType;
	}
	public void setServiceType(int serviceType) {
		this.serviceType = serviceType;
	}
	public Map<String, Object> getData() {
		return data;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	
}
