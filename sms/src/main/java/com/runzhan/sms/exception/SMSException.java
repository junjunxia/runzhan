package com.runzhan.sms.exception;

public class SMSException extends Exception {

	private static final long serialVersionUID = 1L;

	private final int errorNum;

	private int outFacadeRetCode;

	private String msgContent = "";

	private Throwable throwable;

	/**
	 * 构函数
	 * 
	 * @param errorNum
	 *            错误
	 * @param msgContent
	 *            错误内容.
	 * @param throwable
	 *            引发异常对象.
	 */
	public SMSException(int errorNum, String msgContent, Throwable throwable) {
		super(throwable);
		this.errorNum = errorNum;
		this.msgContent = msgContent;
		this.throwable = throwable;
	}

	/**
	 * 构函数
	 * 
	 * @param errorNum
	 *            错误
	 * @param msgContent
	 *            错误内容.
	 */
	public SMSException(int errorNum, String msgContent) {
		super();
		this.errorNum = errorNum;
		this.msgContent = msgContent;
	}

	/**
	 * 构函数
	 * 
	 * @param errorNum
	 *            错误
	 * @param msgContent
	 *            错误内容.
	 * @param throwable
	 *            引发异常对象.
	 */
	public SMSException(int errorNum, int outFacadeRetCode, String msgContent,
			Throwable throwable) {
		super(throwable);
		this.errorNum = errorNum;
		this.msgContent = msgContent;
		this.throwable = throwable;
	}

	/**
	 * 构函数
	 * 
	 * @param errorNum
	 *            错误
	 * @param msgContent
	 *            错误内容.
	 */
	public SMSException(int errorNum, int outFacadeRetCode, String msgContent) {
		super();
		this.errorNum = errorNum;
		this.msgContent = msgContent;
	}

	/**
	 * 构函数
	 * 
	 * @param errorNum
	 *            错误
	 * @param cause
	 *            引发异常对象.
	 */
	public SMSException(int errorNum, Throwable cause) {
		super(cause);
		this.errorNum = errorNum;
		this.throwable = cause;
	}

	/**
	 * 重写toString
	 * 
	 * @return 异常信息的字符串
	 */

	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("errorNum:");
		builder.append(errorNum);
		builder.append(";msgContent:");
		builder.append(msgContent);
		builder.append(";throwable:");
		builder.append(throwable);
		return builder.toString();
	}

	/**
	 * 返回异常
	 * 
	 * @return 返回 errorNum
	 */
	public int getErrorNum() {
		return errorNum;
	}

	public int getOutFacadeRetCode() {
		return outFacadeRetCode;
	}

	/**
	 * 返回异常信息
	 * 
	 * @return 返回 msgContent
	 */
	public String getMsgContent() {
		return msgContent;
	}

	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}
}
