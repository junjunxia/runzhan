package com.runzhan.sms.po;

import java.io.Serializable;

/**
 * 统一定义id的entity基类. 基类统一定义id的属性名称、数据类型、列名映射及生成策略. 子类可重载getId()函数重定义id的列名映射和生成策略.
 * 
 * @since version1.0
 */
public abstract class BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	protected int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
