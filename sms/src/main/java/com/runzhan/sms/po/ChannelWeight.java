package com.runzhan.sms.po;

public class ChannelWeight {
	/** 通道名称 */
	private String ssmschannelname;
	/** 通道号码 */
	private String ssmschannelno;
	/** 通道性质：1:业务通道，2:营销通道 */
	private String ismschanneltype;
	/** 权重 */
	private int iweight;

	public String getSsmschannelname() {
		return ssmschannelname;
	}

	public void setSsmschannelname(String ssmschannelname) {
		this.ssmschannelname = ssmschannelname;
	}

	public String getSsmschannelno() {
		return ssmschannelno;
	}

	public void setSsmschannelno(String ssmschannelno) {
		this.ssmschannelno = ssmschannelno;
	}

	public String getIsmschanneltype() {
		return ismschanneltype;
	}

	public void setIsmschanneltype(String ismschanneltype) {
		this.ismschanneltype = ismschanneltype;
	}

	public int getIweight() {
		return iweight;
	}

	public void setIweight(int iweight) {
		this.iweight = iweight;
	}


	public String toString() {
		return "ChannelWeight [ssmschannelname=" + ssmschannelname
				+ ", ssmschannelno=" + ssmschannelno + ", ismschanneltype="
				+ ismschanneltype + ", iweight=" + iweight + "]";
	}
}
