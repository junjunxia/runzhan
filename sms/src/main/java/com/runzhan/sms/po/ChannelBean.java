package com.runzhan.sms.po;


public class ChannelBean {
	/** 通道编号 */
	private String ssmschannelno;
	/** 通道名称 */
	private String ssmschannelname;
	/** 通道性质：1:业务通道，2:营销通道 */
	private int ismschanneltype;
	/** 权重 */
	private int iweight;
	/** 临时权重 */
	private int iTempweight;

	public String getSsmschannelno() {
		return ssmschannelno;
	}

	public void setSsmschannelno(String ssmschannelno) {
		this.ssmschannelno = ssmschannelno;
	}

	public String getSsmschannelname() {
		return ssmschannelname;
	}

	public void setSsmschannelname(String ssmschannelname) {
		this.ssmschannelname = ssmschannelname;
	}

	public int getIsmschanneltype() {
		return ismschanneltype;
	}

	public void setIsmschanneltype(int ismschanneltype) {
		this.ismschanneltype = ismschanneltype;
	}

	public int getIweight() {
		return iweight;
	}

	public void setIweight(int iweight) {
		this.iweight = iweight;
	}

	public int getiTempweight() {
		return iTempweight;
	}

	public void setiTempweight(int iTempweight) {
		this.iTempweight = iTempweight;
	}


	public String toString() {
		return "ChannelBean [ssmschannelno=" + ssmschannelno + ", ssmschannelname=" + ssmschannelname
				+ ", ismschanneltype=" + ismschanneltype + ", iweight=" + iweight + "]";
	}

}
