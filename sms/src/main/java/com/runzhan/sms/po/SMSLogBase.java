package com.runzhan.sms.po;

import java.util.Date;

public class SMSLogBase extends BaseEntity {
	/**  */
	private static final long serialVersionUID = 8347966429016374682L;

	// 短信ID
	private String smsId;

	// 短信内容
	private String smsContent;

	// 手机号码
	private String mobile;

	/*
	 * 1:BUSINESS:业务短信 2:MARKETING:营销短信
	 */
	// 短信业务属性
	private int smsAttr;

	/*
	 * 1:REALTIME:实时 2:NOTREALTIME:准实时 3:TIMING:定时
	 */
	// 发送属性
	private int sendAttr;

	// 短信提交时间
	private Date postDate;

	// 短信发送时间
	private Date sendDate;

	/*
	 * 0:FAULT:发送失败 1:SUCCESS:发送成功
	 */
	// 短信发送状态
	private int sendStatus;

	/*
	 * 0:AUTO:系统自动 1:MANUAL:手工补发
	 */
	// 发送方式
	private int sendMethod;

	// 通道编号
	private String channelNo;

	// 通道名称
	private String channelName;

	/*
	 * 1:BUSINESS:业务通道 2:MARKETING:营销通道
	 */
	// 通道性质
	private int channelType;

	// 短信摘要
	private String summary;

	// 添加人
	private Long createId;
	private String createName;
	// 添加日期
	private Date createTime;
	public String getSmsId() {
		return smsId;
	}
	public void setSmsId(String smsId) {
		this.smsId = smsId;
	}
	public String getSmsContent() {
		return smsContent;
	}
	public void setSmsContent(String smsContent) {
		this.smsContent = smsContent;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public int getSmsAttr() {
		return smsAttr;
	}
	public void setSmsAttr(int smsAttr) {
		this.smsAttr = smsAttr;
	}
	public int getSendAttr() {
		return sendAttr;
	}
	public void setSendAttr(int sendAttr) {
		this.sendAttr = sendAttr;
	}
	public Date getPostDate() {
		return postDate;
	}
	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	public int getSendStatus() {
		return sendStatus;
	}
	public void setSendStatus(int sendStatus) {
		this.sendStatus = sendStatus;
	}
	public int getSendMethod() {
		return sendMethod;
	}
	public void setSendMethod(int sendMethod) {
		this.sendMethod = sendMethod;
	}
	public String getChannelNo() {
		return channelNo;
	}
	public void setChannelNo(String channelNo) {
		this.channelNo = channelNo;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public int getChannelType() {
		return channelType;
	}
	public void setChannelType(int channelType) {
		this.channelType = channelType;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public Long getCreateId() {
		return createId;
	}
	public void setCreateId(Long createId) {
		this.createId = createId;
	}
	public String getCreateName() {
		return createName;
	}
	public void setCreateName(String createName) {
		this.createName = createName;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String toString() {
		return "SMSLogBase [smsId=" + smsId + ", smsContent=" + smsContent + ", mobile=" + mobile + ", smsAttr="
				+ smsAttr + ", sendAttr=" + sendAttr + ", postDate=" + postDate + ", sendDate=" + sendDate
				+ ", sendStatus=" + sendStatus + ", sendMethod=" + sendMethod + ", channelNo=" + channelNo
				+ ", channelName=" + channelName + ", channelType=" + channelType + ", summary=" + summary
				+ ", createId=" + createId + ", createName=" + createName + ", createTime=" + createTime + "]";
	}


}
