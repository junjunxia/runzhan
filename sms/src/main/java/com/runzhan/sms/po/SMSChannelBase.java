package com.runzhan.sms.po;

import java.util.Date;

/// <summary>
/// dx_SMSChannel
/// </summary>
/*
 $.setSsmschannelno();//通道编号 String
 $.setSsmschannelname();//通道名称 String
 $.setSchannelnumber();//通道号码 String
 $.setIsmschanneltype();//通道性质 int
 $.setIstatus();//通道状态 int
 $.setIweight();//通道权重 int
 $.setBistakeover();//是否接管临时通道关闭流量 int
 $.setDclosebegindate();//通道临时关闭开始时间 Date
 $.setDcloseenddate();//通道临时关闭结束时间 Date
 $.setSaddoperator();//添加人 String
 $.setDadddate();//添加日期 Date
 $.setSmodifyman();//修改人 String
 $.setDmodifydate();//修改时间 Date

 $.setSsmschannelno(#.getSsmschannelno());//通道编号 String
 $.setSsmschannelname(#.getSsmschannelname());//通道名称 String
 $.setSchannelnumber(#.getSchannelnumber());//通道号码 String
 $.setIsmschanneltype(#.getIsmschanneltype());//通道性质 int
 $.setIstatus(#.getIstatus());//通道状态 int
 $.setIweight(#.getIweight());//通道权重 int
 $.setBistakeover(#.getBistakeover());//是否接管临时通道关闭流量 int
 $.setDclosebegindate(#.getDclosebegindate());//通道临时关闭开始时间 Date
 $.setDcloseenddate(#.getDcloseenddate());//通道临时关闭结束时间 Date
 $.setSaddoperator(#.getSaddoperator());//添加人 String
 $.setDadddate(#.getDadddate());//添加日期 Date
 $.setSmodifyman(#.getSmodifyman());//修改人 String
 $.setDmodifydate(#.getDmodifydate());//修改时间 Date
 */
/*
 @Entity
 @Table(name = "dx_smschannel")
 @Cache(usage = CacheConcurrencyStrategy.NONE)
 */
public class SMSChannelBase extends BaseEntity {
	/**  */
	private static final long serialVersionUID = 2819337864484949097L;

	// 通道编号
	private String channelNo;
	// 通道名称
	private String channelName;

	/*
	 * 1:BUSINESS:业务通道 2:MARKETING:营销通道
	 */
	// 通道性质
	private int channelType;

	/*
	 * 0:NOTMAYBE:不可用 1:MAYBE:可用(默认)
	 */
	// 通道状态
	private int status;

	// 通道权重
	private int weight;

	// 添加人
	private int createId;
	private String createName;
	// 添加日期
	private Date createTime;

	// 修改人
	private int updateId;
	private String updateName;
	// 修改时间
	private Date updateTime;
	public String getChannelNo() {
		return channelNo;
	}
	public void setChannelNo(String channelNo) {
		this.channelNo = channelNo;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public int getChannelType() {
		return channelType;
	}
	public void setChannelType(int channelType) {
		this.channelType = channelType;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public int getCreateId() {
		return createId;
	}
	public void setCreateId(int createId) {
		this.createId = createId;
	}
	public String getCreateName() {
		return createName;
	}
	public void setCreateName(String createName) {
		this.createName = createName;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public int getUpdateId() {
		return updateId;
	}
	public void setUpdateId(int updateId) {
		this.updateId = updateId;
	}
	public String getUpdateName() {
		return updateName;
	}
	public void setUpdateName(String updateName) {
		this.updateName = updateName;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String toString() {
		return "SMSChannelBase [channelNo=" + channelNo + ", channelName=" + channelName + ", channelType="
				+ channelType + ", status=" + status + ", weight=" + weight + ", createId=" + createId + ", createName="
				+ createName + ", createTime=" + createTime + ", updateId=" + updateId + ", updateName=" + updateName
				+ ", updateTime=" + updateTime + "]";
	}

}
