package com.runzhan.sms.po;

import java.util.Date;

public class SMSAuthBase extends BaseEntity {
	private static final long serialVersionUID = 4863536212928817195L;
	// 应用编号
	private String sapplicationno;

	public String getSapplicationno() {
		return sapplicationno;
	}

	public void setSapplicationno(String sapplicationno) {
		this.sapplicationno = sapplicationno;
	}

	// 应用名称
	private String sapplicationname;

	public String getSapplicationname() {
		return sapplicationname;
	}

	public void setSapplicationname(String sapplicationname) {
		this.sapplicationname = sapplicationname;
	}

	// 用户名
	private String susername;

	public String getSusername() {
		return susername;
	}

	public void setSusername(String susername) {
		this.susername = susername;
	}

	// 密码
	private String spassword;

	public String getSpassword() {
		return spassword;
	}

	public void setSpassword(String spassword) {
		this.spassword = spassword;
	}

	// IP地址
	private String sipaddr;

	public String getSipaddr() {
		return sipaddr;
	}

	public void setSipaddr(String sipaddr) {
		this.sipaddr = sipaddr;
	}

	// 添加人
	private String saddoperator;

	public String getSaddoperator() {
		return saddoperator;
	}

	public void setSaddoperator(String saddoperator) {
		this.saddoperator = saddoperator;
	}

	/*
	     
	      */
	// 添加日期
	private Date dadddate;

	public Date getDadddate() {
		return dadddate;
	}

	public void setDadddate(Date dadddate) {
		this.dadddate = dadddate;
	}

	// 修改人
	private String smodifyman;

	public String getSmodifyman() {
		return smodifyman;
	}

	public void setSmodifyman(String smodifyman) {
		this.smodifyman = smodifyman;
	}

	/*
	     
	      */
	// 修改时间
	private Date dmodifydate;

	public Date getDmodifydate() {
		return dmodifydate;
	}

	public void setDmodifydate(Date dmodifydate) {
		this.dmodifydate = dmodifydate;
	}


	public String toString() {
		return "SMSAuthBase [sapplicationno=" + sapplicationno
				+ ", sapplicationname=" + sapplicationname + ", susername="
				+ susername + ", spassword=" + spassword + ", sipaddr="
				+ sipaddr + ", saddoperator=" + saddoperator + ", dadddate="
				+ dadddate + ", smodifyman=" + smodifyman + ", dmodifydate="
				+ dmodifydate + "]";
	}
}
