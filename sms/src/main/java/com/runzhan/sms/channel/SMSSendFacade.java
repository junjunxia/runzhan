package com.runzhan.sms.channel;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.runzhan.sms.constant.SysConstant;
import com.runzhan.sms.dto.ChanSendResultMsg;
import com.runzhan.sms.exception.SMSException;
import com.runzhan.sms.po.ChannelWeight;
import com.runzhan.sms.service.ChannelWeightManagerService;
import com.runzhan.sms.util.SMSSendMsg;

/**
 * 发送短信服务
 */
@Service
public class SMSSendFacade {
	
	public static final Logger logger = Logger.getLogger(SMSSendFacade.class);
			
	@Autowired
	private ChannelWeightManagerService channelWeightManagerService;

	/**
	 * 根据服务类型发送短信
	 * @param mobile
	 * @param content
	 * @param serviceType
	 * @throws SMSException
	 */
	public SMSSendMsg sendSMSBySrvType(String mobile, String content, int serviceType)
			throws SMSException {
		SMSSendMsg result = new SMSSendMsg();
		ChanSendResultMsg chSendRes = null;
		int resultCode = -1;
		int channelNo = 0;
		ChannelWeight channelWeight = null;
		if (SysConstant.BUSINESS_SMS_CHANNEL == serviceType) {
			channelWeight = channelWeightManagerService.getServiceChannel(true);
		} else {
			channelWeight = channelWeightManagerService.getMarketChannel(true);
		}

		if (channelWeight == null) {
			logger.info("【sendSMSBySrvType】没有该类型的短信通道可用");
			result.setResultCode(1);
			result.setResulstMsg("没有该类型的短信通道可用");
			return result;
		} else {
			channelNo = Integer.valueOf(channelWeight.getSsmschannelno());
			result.setChName(channelWeight.getSsmschannelname());
			result.setChNo(channelWeight.getSsmschannelno());
		}
		try {
			// 发送短信
			chSendRes = send(mobile, content, channelNo, result.getChName());
			if (chSendRes == null)
				return null;
			if (!StringUtils.isBlank(chSendRes.getResultCode())) {
				resultCode = Integer.valueOf(chSendRes.getResultCode());
			}
			logger.debug("【sendSMSBySrvType】短信通道返回状态码resultCode===》" + resultCode);
			// 设置返回结果
			result.setResulstMsg(chSendRes.getResulstMsg());
			result.setResultCode(resultCode);
		} catch (Exception e) {
			logger.error(new Throwable("【sendSMSBySrvType】短信发送失败,手机号码:" + mobile
					+ ";短信内容:" + content), e);
			resultCode = 1;
			result.setResultCode(resultCode);
			result.setResulstMsg("短信通道发送异常");
			result.setChNo(channelWeight.getSsmschannelno());
			result.setChName(channelWeight.getSsmschannelname());
		}
		logger.info(channelWeight.getSsmschannelname() + "【sendSMSBySrvType】发送短信");
		return result;
	}

	private ChanSendResultMsg send(String mobile, String content,
			int channelNo, String channelName) {
		Date reqTime = new Date();
		ChanSendResultMsg chSendRes = null;
		if (SysConstant.YXT_MARK_CHANNEL == channelNo) {// 优讯通营销通道
			chSendRes = YXTChannel.sendSms(mobile, content);
		} else if (SysConstant.YXT_SERVER_CHANNEL == channelNo) {// 优讯通业务通道
			chSendRes = YXTChannel.sendSms(mobile, content);
		} else {// 优讯通业务通道
			chSendRes = YXTChannel.sendSms(mobile, content);
		}
		Date resTime = new Date();
		if (chSendRes != null) {// 单发日志
			// 记录访问日志
			logger.info(channelNo + "," + channelName + "," + reqTime + "mobile-" + mobile + resTime
					+ (chSendRes.getResultCode().equals("1") ? "send fail!"
							: "send success!"));
		}
		return chSendRes;
	}

	
}
