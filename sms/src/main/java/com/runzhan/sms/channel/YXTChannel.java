package com.runzhan.sms.channel;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.params.HttpMethodParams;

import com.alibaba.fastjson.JSON;
import com.runzhan.base.common.spring.ApplicationContextHolder;
import com.runzhan.sms.dto.ChanSendResultMsg;
import com.runzhan.sms.util.PropertyConfig;
import com.runzhan.sms.util.SHA1;
import com.runzhan.sms.util.StringUtil;

/**
 * 
 * 名称: HttpJsonUtil.java 描述: json格式传送 类型: JAVA 最近修改时间:2016年4月19日 上午10:38:09
 */
public class YXTChannel {
	/** 配置文件 */
	public static final PropertyConfig pro = (PropertyConfig) ApplicationContextHolder.getBean(PropertyConfig.class);

	// ############################此部分参数需要修改############################
//	public static String url_submit = "http://new.yxuntong.com/emmpdata/sms/Submit";// 提交短信地址
	public static String url_deliver = "http://new.yxuntong.com/emmpdata/sms/Deliver";// 下行地址
	public static String url_report = "http://new.yxuntong.com/emmpdata/sms/Report";// 状态报告地址
	public static String url_balance = "http://new.yxuntong.com/emmpdata/sms/Balance";// 余额提醒地址
	public static String token = "65dccc653893416f9d5c9c3ae95a9b1c";// 安全验证key
//	public static String phone = "13111111111,13222222222,13333333333";// 手机号码
//	public static String userName = "1003"; // 用户名
//	public static String password = "qazwsx123"; // 密码
//	public static String content = "你好，测试短信"; // 短信内容
	public static String sign = "【润展国际】"; // 短信内容
	
	public static String url_submit = pro.getYxtUrlSubmit();// 提交短信地址
	public static String userName = pro.getYxtUserName(); // 用户名
	public static String password = pro.getYxtPassword(); // 密码

	public static ChanSendResultMsg sendSms(String mobile, String content) {
		ChanSendResultMsg sendMsg = new ChanSendResultMsg();

		String resp = sendJson(userName, password, url_submit, mobile, content, sign, "", "");
		if (StringUtil.isNull(resp)) {
			sendMsg.setResultCode("1");
			sendMsg.setResulstMsg("接口返回结果为空");
			return sendMsg;
		}

		Map<String, String> mapRes = JSON.parseObject(resp, Map.class);
		String result = mapRes.get("result");
		String resulstMsg = mapRes.get("desc");
		sendMsg.setResulstMsg(resulstMsg);
		if (!"0".equals(result)) {
			sendMsg.setResultCode("1");
		} else {
			sendMsg.setResultCode("0");
		}

		return sendMsg;

	}

	/**
	 * 方法描述 发送短信
	 */
	public static String sendJson(String userName, String password, String url_submit, String phones, String content,
			String sign, String subcode, String sendtime) {
		Map<String, String> params = new LinkedHashMap<String, String>();
		String message = "{'account':'" + userName + "','password':'" + MD5Encode(password) + "','phones':'" + phones
				+ "','content':'" + content + "','sign':'" + sign + "'}";
		params.put("message", message);

		String sid = new SHA1().getDigestOfString((token + "&" + message).getBytes());
		params.put("sid", sid);
		System.out.println(params);
		params.put("type", "json");
		String resp = doPost(url_submit, params);
		return resp;
	}

	/**
	 * 方法描述 状态报告
	 */
	public static String getReportJson(String userName, String password, String url_report) throws Exception {
		String message = "{'account':'" + userName + "','password':'" + MD5Encode(password) + "'}";
		String sid = new SHA1().getDigestOfString((token + "&" + message).getBytes());
		Map<String, String> params = new LinkedHashMap<String, String>();
		params.put("message", message);
		params.put("sid", sid);
		params.put("type", "json");
		String resp = doPost(url_report, params);
		return resp;
	}

	/**
	 * 方法描述 查询余额
	 */
	public static String getBalanceJson(String userName, String password, String url_balance) throws Exception {
		String message = "{'account':'" + userName + "','password':'" + MD5Encode(password) + "'}";
		String sid = new SHA1().getDigestOfString((token + "&" + message).getBytes());
		Map<String, String> params = new LinkedHashMap<String, String>();
		params.put("message", message);
		params.put("sid", sid);
		params.put("type", "json");
		String resp = doPost(url_balance, params);
		return resp;
	}

	/**
	 * 方法描述 获取上行
	 */
	public static String getSmsJson(String userName, String password, String url_deliver) throws Exception {
		String message = "{'account':'" + userName + "','password':'" + MD5Encode(password) + "'}";
		String sid = new SHA1().getDigestOfString((token + "&" + message).getBytes());
		Map<String, String> params = new LinkedHashMap<String, String>();
		params.put("message", message);
		params.put("sid", sid);
		params.put("type", "json");
		String resp = doPost(url_deliver, params);
		return resp;
	}

	/**
	 * 执行一个HTTP POST请求，返回请求响应的HTML
	 * 
	 * @param url
	 *            请求的URL地址
	 * @param params
	 *            请求的查询参数,可以为null
	 * @return 返回请求响应的HTML
	 */
	private static String doPost(String url, Map<String, String> params) {
		String response = null;
		HttpClient client = new HttpClient();
		// 设置超时时间
		HttpConnectionManagerParams httpConnectionManagerParams = client.getHttpConnectionManager().getParams();
		httpConnectionManagerParams.setConnectionTimeout(30 * 1000);
		httpConnectionManagerParams.setSoTimeout(30 * 1000);

		PostMethod postMethod = new PostMethod(url);
		postMethod.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "utf-8");

		// 设置Post数据
		if (!params.isEmpty()) {
			int i = 0;
			NameValuePair[] data = new NameValuePair[params.size()];
			for (Entry<String, String> entry : params.entrySet()) {
				data[i] = new NameValuePair(entry.getKey(), entry.getValue());
				i++;
			}

			postMethod.setRequestBody(data);

		}
		try {
			client.executeMethod(postMethod);
			if (postMethod.getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(postMethod.getResponseBodyAsStream()));
				StringBuffer stringBuffer = new StringBuffer();
				String str = null;
				while ((str = reader.readLine()) != null) {
					stringBuffer.append(str);
				}
				response = stringBuffer.toString();
				reader.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			postMethod.releaseConnection();
			client.getHttpConnectionManager().closeIdleConnections(0);
		}
		return response;
	}

	// MD5加密函数
	public static String MD5Encode(String sourceString) {
		String resultString = null;
		try {
			resultString = new String(sourceString);
			MessageDigest md = MessageDigest.getInstance("MD5");
			resultString = byte2hexString(md.digest(resultString.getBytes()));
		} catch (Exception ex) {
		}
		return resultString;
	}

	private static final String byte2hexString(byte[] bytes) {
		StringBuffer bf = new StringBuffer(bytes.length * 2);
		for (int i = 0; i < bytes.length; i++) {
			if ((bytes[i] & 0xff) < 0x10) {
				bf.append("0");
			}
			bf.append(Long.toString(bytes[i] & 0xff, 16));
		}
		return bf.toString();
	}
}
