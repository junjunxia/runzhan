package com.runzhan.sms.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.runzhan.sms.constant.ErrCodeConstant;
import com.runzhan.sms.constant.SysConstant;

public final class CheckParameterUtil {
	private static List<Integer> serTypeList = new ArrayList<Integer>();
	private static List<Integer> chNoList = new ArrayList<Integer>();
	private static int[] DAYS = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30,
			31 };
	private static List<String> paramTypeList = new ArrayList<String>();
	static {
		serTypeList.add(1);
		serTypeList.add(2);

		chNoList.add(2001);
		chNoList.add(2002);
		chNoList.add(1001);
		chNoList.add(3001);
		chNoList.add(4001);
		chNoList.add(4002);
		chNoList.add(5001);
		chNoList.add(5002);

		paramTypeList.add("1");
		paramTypeList.add("2");
		paramTypeList.add("3");
	}

	public static List<Integer> getChNoList() {
		return chNoList;
	}

	public static CheckResult checkSMSContLeng(String SMSContent) {
		if (StringUtils.isBlank(SMSContent) || "null".equals(SMSContent)) {
			return CheckResult.getFailCheckResult(
					ErrCodeConstant.SMS_CONTENT_IS_NULL,
					ErrCodeConstant.SMS_CONTENT_NULL_MSG);
		}
		int length = SMSContent.length();
		if (length > SysConstant.SMS_CONT_LENGTH) {
			return CheckResult.getFailCheckResult(
					ErrCodeConstant.SMS_CONTENT_LENG_LIMIT,
					ErrCodeConstant.SMS_CONTENT_LENG_ERROR_MSG);
		}
		return CheckResult.getSuccessCheckResult(SysConstant.VERIFIED_OK,
				"短信内容校验通过");
	}

	public static CheckResult checkTimeSMSContLeng(String SMSContent) {
		if (StringUtils.isBlank(SMSContent) || "null".equals(SMSContent)) {
			return CheckResult.getFailCheckResult(
					ErrCodeConstant.SMS_CONTENT_IS_NULL,
					ErrCodeConstant.SMS_CONTENT_NULL_MSG);
		}
		int length = SMSContent.length();
		if (length > SysConstant.TIME_SMS_CONT_LENGTH) {
			return CheckResult.getFailCheckResult(
					ErrCodeConstant.SMS_CONTENT_LENG_LIMIT,
					ErrCodeConstant.TIMD_SMS_CONTENT_LENG_ERROR_MSG);
		}
		return CheckResult.getSuccessCheckResult(SysConstant.VERIFIED_OK,
				"短信内容校验通过");
	}

	/**
	 * @Title: checkMobile
	 * @Description: 校验手机内容
	 * @param @param content
	 * @param @return
	 * @return CheckResult
	 * @throws
	 */
	public static CheckResult checkMobile(String content) {
		if (StringUtils.isBlank(content) || "null".equals(content)) {
			return CheckResult.getFailCheckResult(
					ErrCodeConstant.MOBILE_IS_NULL,
					ErrCodeConstant.MOBILE_NULL_MSG);
		}
		if (content.length() != SysConstant.MOBILE_STR_LENGTH) {
			return CheckResult.getFailCheckResult(
					ErrCodeConstant.MOBILE_FORMAT_ERROR,
					ErrCodeConstant.MOBILE_FORMAT_MSG);
		}
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(content);
		if (!isNum.matches()) {
			return CheckResult.getFailCheckResult(
					ErrCodeConstant.MOBILE_FORMAT_ERROR,
					ErrCodeConstant.MOBILE_FORMAT_MSG);
		}

		CheckResult checkResult = CheckResult.getSuccessCheckResult(
				SysConstant.VERIFIED_OK, "手机号码校验通过");
		return checkResult;
	}

	public static CheckResult checkSMSID(String smsID) {
		if (StringUtils.isBlank(smsID) || "null".equals(smsID)) {
			return CheckResult.getFailCheckResult(
					ErrCodeConstant.SMS_ID_IS_NULL,
					ErrCodeConstant.SMS_ID_NULL_MSG);
		}
		return CheckResult.getSuccessCheckResult(SysConstant.VERIFIED_OK,
				"短信ID校验通过");
	}

	public static CheckResult checkServerType(int serverType) {
		if (0 != serverType) {
			Pattern pattern = Pattern.compile("[0-9]*");
			Matcher isNum = pattern.matcher(String.valueOf(serverType));
			if (!isNum.matches()) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.SMS_SERVER_TYPE_NOT_NUM,
						ErrCodeConstant.SMS_SERVER_TYPE_NOT_NUM_MSG);
			}
			if (!serTypeList.contains(Integer.valueOf(serverType))) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.SMS_SERVER_TYPE_ERROR,
						ErrCodeConstant.SMS_SERVER_TYPE_ERROR_MSG);
			}
		} else {
			return CheckResult.getFailCheckResult(
					ErrCodeConstant.SMS_SERVER_TYPE_IS_NULL,
					ErrCodeConstant.SMS_SERVER_TYPE_NULL_MSG);
		}
		return CheckResult.getSuccessCheckResult(SysConstant.VERIFIED_OK,
				"业务类型校验通过");
	}

	public static CheckResult checkChNo(String chNo, int serviceType) {
		if (!StringUtil.isNull(chNo)) {
			Pattern pattern = Pattern.compile("[0-9]*");
			Matcher isNum = pattern.matcher(chNo);
			if (!isNum.matches()) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.CH_NO_NOT_NUM,
						ErrCodeConstant.CH_NO_NOT_NUM_MSG);
			}
			if (!chNoList.contains(Integer.valueOf(chNo))) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.CH_NO_INVALIED,
						ErrCodeConstant.CH_NO_INVALIED_MSG);
			}
			Integer channelTypeByChNo = SysConstant.getChannelTypeByChNo(chNo);
			if (channelTypeByChNo != serviceType) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.CH_NO_TYPE_INVALIED,
						ErrCodeConstant.CH_NO_TYPE_INVALIED_MSG);
			}
		}
		return CheckResult.getSuccessCheckResult(SysConstant.VERIFIED_OK,
				"通道编号校验通过");
	}

	public static void main(String[] args) {
		CheckResult checkSMSContLeng = checkSMSContLeng(" ");
		System.out.println(checkSMSContLeng);
	}

	public static CheckResult checkSMSAbstract(String smsAbstract) {
		if (!StringUtils.isBlank(smsAbstract) && !"null".equals(smsAbstract)) {
			int length = smsAbstract.length();
			if (length > SysConstant.SMS_ABST_CONT_LENGTH) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.SMS_ABSTRACT_CONTENT_LENG_LIMIT,
						ErrCodeConstant.SMS_ABSTRACT_CONTENT_LENG_ERROR_MSG);
			}
		}
		return CheckResult.getSuccessCheckResult(SysConstant.VERIFIED_OK,
				"短信摘要校验通过");
	}

	private static final boolean isGregorianLeapYear(int year) {
		if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) {
			return true;
		} else {
			return false;
		}
	}

	public static CheckResult checkTimeFormat(String date) {
		try {
			int length = date.length();
			if (length != SysConstant.DATE_STR_LENGTH) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR,
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR_MSG);
			}
			int year = Integer.parseInt(date.substring(0, 4));
			if (year <= 0) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR,
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR_MSG);
			}
			int month = Integer.parseInt(date.substring(4, 6));
			if (month <= 0 || month > 12) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR,
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR_MSG);
			}
			int day = Integer.parseInt(date.substring(6, 8));
			if (day <= 0 || day > DAYS[month - 1]) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR,
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR_MSG);
			}
			if (month == 2 && day == 29 && !isGregorianLeapYear(year)) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR,
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR_MSG);
			}
			if (month == 2 && day == 28 && isGregorianLeapYear(year)) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR,
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR_MSG);
			}
			int hour = Integer.parseInt(date.substring(8, 10));
			if (hour < 0 || hour > 23) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR,
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR_MSG);
			}
			int minute = Integer.parseInt(date.substring(10, 12));
			if (minute < 0 || minute > 59) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR,
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR_MSG);
			}
			int second = Integer.parseInt(date.substring(12, 14));
			if (second < 0 || second > 59) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR,
						ErrCodeConstant.TIME_SMS_FORMAT_ERROR_MSG);
			}
		} catch (Exception e) {
			return CheckResult.getFailCheckResult(
					ErrCodeConstant.TIME_SMS_FORMAT_ERROR,
					ErrCodeConstant.TIME_SMS_FORMAT_ERROR_MSG);
		}
		return CheckResult.getSuccessCheckResult(SysConstant.VERIFIED_OK,
				"时间格式校验通过");
	}

	public static CheckResult checkPlanTime(String begDate, String endDate) {
		if (!StringUtils.isBlank(begDate) && !StringUtils.isBlank(endDate)) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
					Locale.CHINA);
			Date begin;
			try {
				begin = sdf.parse(begDate);
				Date end = sdf.parse(endDate);
				boolean flag = begin.after(end);
				if (flag) {
					return CheckResult
							.getFailCheckResult(
									ErrCodeConstant.PLAN_BEGIN_GREATE_THAN_END_DATE_ERROR,
									ErrCodeConstant.PLAN_BEGIN_GREATE_THAN_END_DATE_MSG);
				}
			} catch (ParseException e) {
			}
		}
		return CheckResult.getSuccessCheckResult(SysConstant.VERIFIED_OK,
				"时间大小校验通过");
	}

	public static CheckResult checkParam(String prmType) {
		if (!StringUtils.isBlank(prmType)) {
			Pattern pattern = Pattern.compile("[0-9]*");
			Matcher isNum = pattern.matcher(prmType);
			if (!isNum.matches()) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.CH_REF_TYPE_CODE,
						ErrCodeConstant.CH_REF_TYPE_DIGITAL_MSG);
			}
			if (!paramTypeList.contains(prmType)) {
				return CheckResult.getFailCheckResult(
						ErrCodeConstant.CH_REF_TYPE_CODE,
						ErrCodeConstant.CH_REF_TYPE_NOT_EXT_MSG);
			}
		}
		return CheckResult.getSuccessCheckResult(SysConstant.VERIFIED_OK,
				"通道参数校验通过");
	}
}
