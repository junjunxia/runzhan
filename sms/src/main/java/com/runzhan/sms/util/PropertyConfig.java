package com.runzhan.sms.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertyConfig {
	
	@Value("${yxt.url.submit}")
	private String yxtUrlSubmit;
	
	@Value("${yxt.userName}")
	private String yxtUserName;
	
	@Value("${yxt.password}")
	private String yxtPassword;

	public String getYxtUrlSubmit() {
		return yxtUrlSubmit;
	}

	public void setYxtUrlSubmit(String yxtUrlSubmit) {
		this.yxtUrlSubmit = yxtUrlSubmit;
	}

	public String getYxtUserName() {
		return yxtUserName;
	}

	public void setYxtUserName(String yxtUserName) {
		this.yxtUserName = yxtUserName;
	}

	public String getYxtPassword() {
		return yxtPassword;
	}

	public void setYxtPassword(String yxtPassword) {
		this.yxtPassword = yxtPassword;
	}
	
}
