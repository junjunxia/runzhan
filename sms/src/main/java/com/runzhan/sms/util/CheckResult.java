package com.runzhan.sms.util;

public class CheckResult {

	private int checkCode;
	private String checkMessage;

	public CheckResult(int checkCode, String checkMessage) {
		this.checkCode = checkCode;
		this.checkMessage = checkMessage;
	}

	public static CheckResult getFailCheckResult(int errorCode,
			String check_message) {
		return new CheckResult(errorCode, check_message);
	}

	public static CheckResult getSuccessCheckResult(int sucCode,
			String check_message) {
		return new CheckResult(sucCode, check_message);
	}

	public int getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(int checkCode) {
		this.checkCode = checkCode;
	}

	public String getCheckMessage() {
		return checkMessage;
	}

	public void setCheckMessage(String checkMessage) {
		this.checkMessage = checkMessage;
	}
}
