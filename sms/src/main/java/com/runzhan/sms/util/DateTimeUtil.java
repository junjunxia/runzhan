/** 
 * TimeFormatUtil.java Created on May 21, 2009
 * Copyright 2009@JSHX. 
 * All right reserved. 
 */
package com.runzhan.sms.util;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * 时间格式工具类
 * 
 * @Time 5:14:11 PM
 * @author
 */
public class DateTimeUtil {
	public static final String FORMAT1 = "yyyyMMdd";
	public static final String FORMAT2 = "HHmmss";
	public static final String FORMAT3 = "yyyy-MM-dd HH:mm:ss";
	public static final String FORMAT4 = "yyyyMMDDhhmmss";

	/**
	 * 将时间格式话成 yyyy年MM月dd日 HH点mm分ss秒 的格式
	 * 
	 * @param date
	 *            需要格式化的时间
	 * @return 标准格式的时间-yyyy年MM月dd日 HH点mm分ss秒
	 */
	public static String getFormatDate(Date date) {
		try {
			DateFormat mat = new SimpleDateFormat(FORMAT1);
			return mat.format(date);
		} catch (Exception e) {
			return "";
		}
	}

	public static java.sql.Date getSqlDateFromDateString(String datestring,
			String masktype) {
		Date dateFromDateString = null;
		try {
			dateFromDateString = DateTimeUtil.getDateFromDateString(datestring,
					masktype);
			return new java.sql.Date(dateFromDateString.getTime());
		} catch (ParseException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
			return null;
		}
	}

	public static String getFormatTime(Date date) {
		try {
			DateFormat mat = new SimpleDateFormat(FORMAT2);
			return mat.format(date);
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * 取得日期datestring 格式yyyymmdd
	 * 
	 * @param masktype
	 *            "yyyy-MM-dd" or "yyyyMMdd"
	 * @throws ParseException
	 * */

	public static Date getDateFromDateString(String datestring, String masktype)
			throws ParseException {

		SimpleDateFormat sdf = new SimpleDateFormat(masktype);
		return sdf.parse(datestring);
	}

	/**
	 * 取得系统当前时间
	 * 
	 * @return 系统当前时间
	 */
	public static Date getSystemDateTime() {
		return new Date();
	}

	/**
	 * 取得系统当前时间
	 * 
	 * @return 系统当前时间
	 */
	public static java.sql.Date getSqlSystemDateTime() {
		return new java.sql.Date(0);
	}

	/**
	 * 根据年度和期间得到某期第一天或最后一天
	 * 
	 * @param type
	 *            'first'or'last'
	 * @param year
	 * @param qj
	 * @return 系统当前时间 如 2010 3 first 返回2010-03-01的date形式
	 * @throws ParseException
	 */
	public static String getDateTimeInACurtainYearAndMonth(String year,
			String qj, String type) throws ParseException {
		if (new Integer(qj) < 10) {
			qj = "0" + qj;
		}
		if ("first".equals(type)) {
			return year + "-" + qj + "-01";
		}
		if ("last".equals(type)) {
			return lastDayOfDate(year + "-" + qj);
		}
		return null;
	}

	/**
	 * 取得系统当前日期组成的字符串 默认格式为：yyyy-MM-dd
	 * 
	 * @return 系统当前日期组成的字符串
	 */
	public static String getSystemDateString() {
		return getDateTime(getSystemDateTime(), "yyyy-MM-dd");
	}

	/**
	 * 取得系统当前日期＋时间组成的字符串 默认格式为：yyyy-MM-dd HH:mm:ss
	 * 
	 * @return 系统当前日期＋时间组成的字符串
	 */
	public static String getSystemDateTimeString() {
		return getDateTime(getSystemDateTime(), "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 根据给定的时间（为空时默认使用当前时间）和时间格式 返回时间字符串
	 * 
	 * @param aMask
	 *            时间格式
	 * @param aDate
	 *            时间参数
	 * @return 格式化后的时间字符串
	 * 
	 * @see java.text.SimpleDateFormat
	 */
	public static String getDateTime(Date aDate, String aMask) {
		SimpleDateFormat df = null;
		String returnValue = "";
		if (StringUtil.isNull(aMask)) {
			return "";
		} else if (aDate == null) {
			df = new SimpleDateFormat(aMask);
			returnValue = df.format(getSystemDateTime());
		} else {
			df = new SimpleDateFormat(aMask);
			returnValue = df.format(aDate);
		}
		return (returnValue);
	}

	/**
	 * 返回时间的字符串
	 * 
	 * @param dt
	 * @return
	 */
	public static String getLongStringByDate(Date aDate) {
		if (aDate == null)
			return "";
		return String.valueOf(aDate.getTime());
	}

	/**
	 * 根据 时间格式 时间Long字符串 转换时间格式
	 * 
	 * @param aMask
	 * @param aDateLong
	 * @return
	 */
	public static String getDateStrByLongStr(String aDateLong, String aMask) {
		SimpleDateFormat df = null;
		String returnValue = "";
		if (aDateLong != null && !"".equals(aDateLong)) {
			if (aMask == null || "".equals(aMask))
				aMask = "yyyy-MM-dd HH:mm:ss";
			df = new SimpleDateFormat(aMask);
			returnValue = df.format(new Date(Long.parseLong(aDateLong)));
		}
		return returnValue;
	}

	/**
	 * 获取上一个月字符串
	 * 
	 * @param aDate
	 *            2010-06-01
	 * @return 05
	 */
	@SuppressWarnings("deprecation")
	public static String getPreMonthStr(Date aDate) {
		return getMonthStr(aDate, -1);
	}

	public static Date getDate(Date aDate) {
		Calendar calendar = initCalendar(aDate);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}

	/**
	 * 根据当前时间和参数 获取月份字符
	 * 
	 * @param aDate
	 * @param m
	 * @return
	 */
	public static String getMonthStr(Date aDate, int m) {
		Calendar calendar = initCalendar(aDate);
		int month = calendar.get(Calendar.MONTH);
		month = (month + m % 12 + 13) % 12;
		month = (month == 0) ? 12 : month;
		return (month > 9) ? String.valueOf(month) : "0"
				+ String.valueOf(month);
	}

	/**
	 * 根据当前时间和参数 获取年份字符
	 * 
	 * @param aDate
	 * @param y
	 * @return
	 */
	public static String getYearStr(Date aDate, int y) {
		Calendar calendar = initCalendar(aDate);
		int year = calendar.get(Calendar.YEAR);
		year = year + y;
		return String.valueOf(year);
	}

	/**
	 * 根据当前时间和参数 获取日期字符
	 * 
	 * @param aDate
	 * @param d
	 * @return
	 */
	public static String getDayStr(Date aDate, int d) {
		Calendar calendar = initCalendar(aDate);
		int day = calendar.get(Calendar.DAY_OF_YEAR);
		day = day + d;
		calendar.set(Calendar.DAY_OF_YEAR, day);
		day = calendar.get(Calendar.DAY_OF_MONTH);
		return (day > 9) ? String.valueOf(day) : "0" + String.valueOf(day);
	}

	/**
	 * 根据日历的规则，为给定的时间字段添加或减去指定的月数
	 * 
	 * @param 时间字段
	 *            （默认为当前时间）
	 * @param 为字段添加的月数
	 * @return
	 */
	public static Date getMonth(Date aDate, int amount) {
		Calendar calendar = initCalendar(aDate);
		calendar.add(Calendar.MONTH, amount);
		return calendar.getTime();
	}

	/**
	 * 根据日历的规则，为给定的时间字段添加或减去指定的年数
	 * 
	 * @param 时间字段
	 *            （默认为当前时间）
	 * @param 为字段添加的年数
	 * @return
	 */
	public static Date getYear(Date aDate, int amount) {
		Calendar calendar = initCalendar(aDate);
		calendar.add(Calendar.YEAR, amount);
		return calendar.getTime();
	}

	/**
	 * 根据日历的规则，为给定的时间字段添加或减去指定的天数
	 * 
	 * @param 时间字段
	 *            （默认为当前时间）
	 * @param 为字段添加的天数
	 * @return
	 */
	public static Date getDay(Date aDate, int amount) {
		Calendar calendar = initCalendar(aDate);
		calendar.add(Calendar.DAY_OF_MONTH, amount);
		return calendar.getTime();
	}

	/**
	 * 设置日期
	 * 
	 * @param aDate
	 * @param field
	 * @param value
	 * @return
	 */
	public static Date set(Date aDate, int field, int value) {
		Calendar c = initCalendar(aDate);
		c.set(field, value);
		return c.getTime();
	}

	public static Calendar initCalendar(Date aDate) {
		Calendar calendar = null;
		if (aDate == null) {
			calendar = Calendar.getInstance();
		} else {
			calendar = new GregorianCalendar();
			calendar.setTime(aDate);
		}
		return calendar;
	}

	/**
	 * 取得2个日期之间的天
	 */
	public static List<String> getDayBetweenTwoDate(Date startdate, Date enddate) {
		List<String> list = new ArrayList();

		Calendar cal_start = Calendar.getInstance();
		Calendar cal_end = Calendar.getInstance();
		cal_start.setTime(startdate);
		cal_end.setTime(enddate);
		// cal_end.set(cal_end.DAY_OF_YEAR,
		// cal_end.get(Calendar.DAY_OF_YEAR)+1);
		for (; cal_start.compareTo(cal_end) <= 0; cal_start.set(
				cal_start.DAY_OF_MONTH,
				cal_start.get(Calendar.DAY_OF_MONTH) + 1)) {
			String dd = new SimpleDateFormat("yyyy-MM-dd").format(cal_start
					.getTime());
			String util[] = dd.split("-");
			// System.out.println(util[0]+"年"+util[1]+"月"+util[2]+"日"+"  ;");
			list.add(util[0] + "-" + util[1] + "-" + util[2]);
		}

		return list;
	}

	/**
	 * 取得2个日期之间的月份
	 */
	public static List<String> getMonthBetweenTwoDate(Date startdate,
			Date enddate) {
		List<String> list = new ArrayList();

		Calendar cal_start = Calendar.getInstance();
		Calendar cal_end = Calendar.getInstance();
		cal_start.setTime(startdate);
		cal_start.set(cal_start.DATE, 1);

		String ddd = new SimpleDateFormat("yyyy-MM-dd").format(cal_start
				.getTime());
		cal_end.setTime(enddate);
		// cal_end.set(cal_end.MONTH, cal_end.get(Calendar.MONTH)+1);
		for (; cal_start.compareTo(cal_end) <= 0; cal_start.set(
				cal_start.MONTH, cal_start.get(Calendar.MONTH) + 1)) {
			String dd = new SimpleDateFormat("yyyy-MM-dd").format(cal_start
					.getTime());
			// System.out.println(dd);
			String util[] = dd.split("-");
			list.add(util[0] + "-" + util[1]);
		}

		return list;
	}

	/**
	 * 根据当前时间和参数 获取某个的日期，正数表示加，负数表示倒退
	 * 
	 * @param startdate
	 *            起始时间yyyy-mm-dd
	 * @param y
	 *            年
	 * @param m
	 *            月
	 * @param d
	 *            日
	 * @return yy-mm-dd
	 * @throws ParseException
	 */
	public static String calDate(String startdatestring, int y, int m, int d)
			throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date startdate = sdf.parse(startdatestring);
		Calendar cal_start = Calendar.getInstance();
		cal_start.setTime(startdate);
		if (!StringUtil.isNull(y))
			cal_start.add(cal_start.YEAR, y);
		if (!StringUtil.isNull(m))
			cal_start.add(cal_start.MONTH, m);
		if (!StringUtil.isNull(d))
			cal_start.add(cal_start.DATE, d);
		String dd = new SimpleDateFormat("yyyy-MM-dd").format(cal_start
				.getTime());
		// System.out.println(dd);
		return dd;
	}

	/*
	 * 传月2009－09返回本月最后一天
	 */
	public static String lastDayOfDate(String startdatestring)
			throws ParseException {
		String[] date = startdatestring.split("-");
		String year = date[0];
		int month = Integer.parseInt(date[1]) + 1;
		String datebase = "";
		if (month < 10) {
			datebase = year + "-0" + month + "-01";
		} else {
			datebase = year + "-" + month + "-01";
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date startdate = sdf.parse(datebase);
		Calendar cal_start = Calendar.getInstance();
		cal_start.setTime(startdate);
		cal_start.add(cal_start.DATE, -1);
		String dd = new SimpleDateFormat("yyyy-MM-dd").format(cal_start
				.getTime());

		return dd;
	}

	/**
	 * 取得2个日期之间的年
	 */
	public static List<String> getYearBetweenTwoDate(Date startdate,
			Date enddate) {
		List<String> list = new ArrayList();

		Calendar cal_start = Calendar.getInstance();
		Calendar cal_end = Calendar.getInstance();
		cal_start.setTime(startdate);
		cal_end.setTime(enddate);
		// cal_end.set(cal_end.YEAR, cal_end.get(Calendar.YEAR)+1);
		for (; cal_start.compareTo(cal_end) <= 0; cal_start.set(cal_start.YEAR,
				cal_start.get(Calendar.YEAR) + 1)) {
			String dd = new SimpleDateFormat("yyyy-MM-dd").format(cal_start
					.getTime());
			String util[] = dd.split("-");
			list.add(util[0]);
		}
		return list;
	}

	/**
	 * 取得2个日期之间的年月
	 */
	public static List<String> getYearMonthBetweenTwoDate(Date startdate,
			Date enddate) {
		List<String> list = new ArrayList();

		Calendar cal_start = Calendar.getInstance();
		Calendar cal_end = Calendar.getInstance();
		cal_start.setTime(startdate);
		cal_end.setTime(enddate);
		// cal_end.set(cal_end.YEAR, cal_end.get(Calendar.YEAR)+1);
		for (; cal_start.compareTo(cal_end) <= 0; cal_start.set(
				cal_start.MONTH, cal_start.get(Calendar.MONTH) + 1)) {
			String dd = new SimpleDateFormat("yyyyMM").format(cal_start
					.getTime());

			list.add(dd);
		}
		return list;
	}

	public static Date getDateFromDateStr(String dateStr) {
		try {
			DateFormat mat = new SimpleDateFormat(FORMAT3);
			return mat.parse(dateStr);
		} catch (Exception e) {
			return null;
		}
	}

	public static Date getDateByDateStr(String dateStr) {
		try {
			DateFormat mat = new SimpleDateFormat(FORMAT4);
			return mat.parse(dateStr);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @param args
	 * @throws ParseException
	 * @throws UnsupportedEncodingException
	 */
	public static void main(String[] args) throws ParseException,
			UnsupportedEncodingException {
		// SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// 2014-11-27 10:00:00 b
		// end 2014-11-27 17:00:00
		// 1417031643953

//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//		long time = getDateFromDateStr("2014-11-27 17:00:00").getTime();
//
//		Date dateFromDateStr = getDateFromDateStr("2014-11-27 14:23:20");
//		System.out.println(String.valueOf(time >= System.currentTimeMillis()));
		
		/*
		 * SimpleDateFormat sdf=new SimpleDateFormat("yyyyMM");
		 * getYearMonthBetweenTwoDate(sdf.parse("201005"), sdf.parse("201006"));
		 */
		// System.out.println(URLEncoder.encode("%E2%88%9A","utf-8"));
		// System.out.println(URLDecoder.decode("%25E2%2588%259A","utf-8"));
		// URLDecoder.decode(s, enc)
		/*
		 * Date dt = new SimpleDateFormat("yyyy-MM-dd").parse("2009-3-3"); Date
		 * dt1 = new SimpleDateFormat().parse("2009-3"); String s = new
		 * SimpleDateFormat("yyyy-MM-dd").format(getDay(dt, -6));
		 * System.out.println(s); System.out.println(dt1);
		 */
		// System.out.println(DateTimeUtil.getReturnDate(new Date())+"-28");
		// System.out.println(d);
		// System.out.println(getDayStr(new Date(), 0));
		// getDateList("201005","201009");\
		/*
		 * System.out.println(DateTimeUtil.getDateTime(new Date(), "yyyyMMdd")+
		 * DateTimeUtil.getLongStringByDate(new Date()));
		 */
		/*
		 * Set<String> pzidset=new HashSet<String>(); pzidset.add("aaa");
		 * pzidset.add("bbb"); pzidset.add("ccc");
		 * 
		 * String pzids=""; Iterator<String> it=pzidset.iterator();
		 * for(;it.hasNext();){ pzids=pzids+" '"+it.next()+"' ,"; }
		 * System.out.println("in ("+pzids.substring(0,
		 * pzids.lastIndexOf(","))+")");
		 */
		// System.out.println(getDateTimeInACurtainYearAndMonth("2010", "2",
		// "first"));
		// System.out.println("CWGLS3201A"+DateTimeUtil.getDateTime(new Date(),
		// "yyyyMMddhhmmss"));
	}

	/**
	 * 取得当前年的前一年的最后一天,或前一月的最后一天,或前一天
	 * 
	 * @param flag
	 *            y,m,d
	 */
	public static String getDateForLastYearOrMonthOrDay(String flag) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		String returnResults = "";

		if ("y".equals(flag)) {
			cal.set(cal.MONTH, 0);
			cal.set(cal.DATE, 1);
			cal.add(cal.DATE, -1);
			returnResults = sdf.format(cal.getTime());
		} else if ("m".equals(flag)) {
			// cal.set(cal.MONTH,-1);
			cal.set(cal.DATE, 1);
			cal.add(cal.DATE, -1);
			returnResults = sdf.format(cal.getTime());
		} else {
			cal.add(cal.DATE, -1);
			returnResults = sdf.format(cal.getTime());
		}
		return returnResults;
	}

	public static boolean isSameDay(Date fDate, Date oDate) {
		return 0 == daysOfTwo(fDate, oDate);
	}

	// 计算相差天数
	public static int daysOfTwo(Date fDate, Date oDate) {
		int compare = fDate.compareTo(oDate);

		if (0 == compare) {
			return 0;
		} else if (1 == compare) {
			return -daysOfTwo0(oDate, fDate);
		} else {
			return daysOfTwo0(fDate, oDate);
		}
	}

	private static int daysOfTwo0(Date previousDate, Date nextDate) {
		Calendar previousCalendar = initCalendar(previousDate);
		Calendar nextCalendar = initCalendar(nextDate);
		if (previousCalendar.get(Calendar.YEAR) == nextCalendar
				.get(Calendar.YEAR)) {
			return daysOfTwo01(previousCalendar, nextCalendar);
		} else {
			return daysOfTwo02(previousCalendar, nextCalendar);
		}
	}

	private static int daysOfTwo01(Calendar previousCalendar,
			Calendar nextCalendar) {
		int day1 = previousCalendar.get(Calendar.DAY_OF_YEAR);
		int day2 = nextCalendar.get(Calendar.DAY_OF_YEAR);
		return day2 - day1;
	}

	private static int daysOfTwo02(Calendar previousCalendar,
			Calendar nextCalendar) {
		int sum = 0;
		int year1 = previousCalendar.get(Calendar.YEAR);
		int year2 = nextCalendar.get(Calendar.YEAR);

		sum += daysOfTwo01(previousCalendar, lastDay(year1));
		for (int i = year1 + 1; i < year2; i++)
			sum += (daysOfTwo01(firstDay(i), lastDay(i)) + 1);
		sum += (daysOfTwo01(firstDay(year2), nextCalendar) + 1);

		return sum;
	}

	private static Calendar firstDay(int year) {
		return specifiedDay(year, 1, 1);
	}

	private static Calendar lastDay(int year) {
		return specifiedDay(year, 12, 31);
	}

	private static Calendar specifiedDay(int year, int month, int date) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month - 1);
		calendar.set(Calendar.DATE, date);
		return calendar;
	}

	public static String timestampToString(Timestamp timestamp)
			throws ParseException {
		// static final SimpleDateFormat df = new
		// SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// 2014-11-27 10:00:00 b
		// end 2014-11-27 17:00:00
		// 1417031643953

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		long time = getDateFromDateStr("2014-11-27 10:00:00").getTime();

		return sdf.format(timestamp);
	}

	public static long getTime(long currTime, String beginTimeStr) {
		String[] timeArr = beginTimeStr.split(":");
		if (timeArr.length == 3) {
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(currTime);
			c.clear(Calendar.HOUR_OF_DAY);
			c.clear(Calendar.MINUTE);
			c.clear(Calendar.SECOND);

			c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(timeArr[0]));
			c.set(Calendar.MINUTE, Integer.valueOf(timeArr[1]));
			c.set(Calendar.SECOND, Integer.valueOf(timeArr[2]));
			long lTime = c.getTimeInMillis();
			return lTime;
		}
		return 0L;
	}
	
}