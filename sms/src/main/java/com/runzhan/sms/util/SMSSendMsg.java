package com.runzhan.sms.util;


public class SMSSendMsg {
	private static final String SUCCESS_CODE = "1";
	private static final String FAIL_CODE = "0";

	private String resultCode;
	private String resulstMsg;
	private String chNo;
	private String chName;

	public String getChNo() {
		return chNo;
	}

	public void setChNo(String chNo) {
		this.chNo = chNo;
	}

	public String getChName() {
		return chName;
	}

	public void setChName(String chName) {
		this.chName = chName;
	}

	public SMSSendMsg() {
	}

	// 转换通道接口返回码(通道：0是成功)
	public void setResultCode(int resultCode) {
		if (resultCode == 0) {
			this.resultCode = SUCCESS_CODE;
		} else {
			this.resultCode = FAIL_CODE;
		}
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResulstMsg(String msg) {
		this.resulstMsg = msg;
	}

	public String getResulstMsg() {
		return resulstMsg;
	}

	public static String getSMSErrByCode(String code) {
		if (code.equals("-1")) {
			return "参数为空。信息、电话号码等有空指针，登陆失败";
		}
		if (code.equals("-2")) {
			return "电话号码个数超过100";
		}
		if (code.equals("-10")) {
			return "申请缓存空间失败";
		}
		if (code.equals("-12")) {
			return "有异常电话号码";
		}
		if (code.equals("-13")) {
			return "电话号码个数与实际个数不相等";
		}
		if (code.equals("-14")) {
			return "实际号码个数超过100";
		}
		if (code.equals("-101")) {
			return "发送消息等待超时";
		}
		if (code.equals("-102")) {
			return "发送或接收消息失败";
		}
		if (code.equals("-103")) {
			return "接收消息超时";
		}
		if (code.equals("-200")) {
			return "其他错误";
		}
		if (code.equals("-999")) {
			return "web服务器内部错误";
		}
		return "未知错误";
	}

	/**
	 * 
	 * @author thuang
	 * @Description: 玄武短信接口
	 * @date 2014-8-8 上午9:23:56
	 * 
	 * @param code
	 * @return
	 */
	public static String getXwErrByCode(String code) {
		if (code.equals("-1")) {
			return "帐号无效";
		}
		if (code.equals("-2")) {
			return "参数：无效";
		}
		if (code.equals("-3")) {
			return "连接不上服务器";
		}
		if (code.equals("-5")) {
			return "无效的短信数据，号码格式不对";
		}
		if (code.equals("-6")) {
			return "用户名密码错误";
		}
		if (code.equals("-7")) {
			return "旧密码不正确";
		}
		if (code.equals("-9")) {
			return "资金账户不存在";
		}
		if (code.equals("-11")) {
			return "包号码数量超过最大限制";
		}
		if (code.equals("-12")) {
			return "余额不足";//
		}
		if (code.equals("-13")) {
			return "账号没有发送权限";
		}
		if (code.equals("-99")) {
			return "系统内部错误";
		}
		if (code.equals("-100")) {
			return "其它错误";
		}
		return "未知错误";
	}

	/**
	 * 易美短信通道错误码
	 * 
	 * @param code
	 *            错误码
	 * @return
	 */
	public static String getEmayErrByCode(String code) {
		if (code.equals("-1")) {
			return "系统异常";
		}
		if (code.equals("-101")) {
			return "命令不被支持";
		}
		if (code.equals("-102")) {
			return "用户信息删除失败";
		}
		if (code.equals("-103")) {
			return "用户信息更新失败";
		}
		if (code.equals("-104")) {
			return "指令超出请求限制";
		}
		if (code.equals("-117")) {
			return "发送短信失败";
		}
		if (code.equals("-9016")) {
			return "发送短信包大小超出范围";
		}
		if (code.equals("-9017")) {
			return "发送短信内容格式错误";
		}
		if (code.equals("-9020")) {
			return "发送短信手机号格式错误";
		}
		if (code.equals("-9021")) {
			return "发送短信定时时间格式错误";
		}
		if (code.equals("-9025")) {
			return "超时";
		}
		if (code.equals("-9000")) {
			return "数据格式错误,数据超出数据库允许范围";
		}
		return "未知错误";
	}


	public String toString() {
		return "SMSSendMsg [resultCode=" + resultCode + ", resulstMsg="
				+ resulstMsg + ", chNo=" + chNo + ", chName=" + chName + "]";
	}
}
