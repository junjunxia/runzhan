package com.runzhan.sms.constant;

/**
 * @author xm.L 错误码常量
 */
public final class ErrCodeConstant {

	private ErrCodeConstant() {

	}

	/** 短信发送成功 */
	public final static int SMS_SEND_SUCCESS = 1;
	/** 短信发送失败 */
	public final static int SMS_SEND_FAIL = 0;

	/** 发送短信系统异常错误码 */
	public final int SMS_SYS_ERROR = 201000;
	/** 发送短信系统异常 */
	public final String SMS_SYS_ERROR_MSG = "系统异常";

	/** 发送连接失败错误码 */
	public final int SMS_SEND_CONNECT_ERROR = 202001;
	/** 发送连接失败 */
	public final String SMS_SEND_CONNECT_ERROR_MSG = "发送连接失败";

	/** 发送失败错误码 */
	public final int SMS_SEND_ERROR = 202002;
	public final String SMS_SEND_ERROR_MSG = "发送失败";

	/** 发送短信内容被拦截错误码 */
	public final int SMS_CONTENT_INTERCEP_ERROR = 202003;
	public final String SMS_CONTENT_INTERCEP_ERROR_MSG = "内容被拦截";

	/** 短信内容超过限定长度错误码 */
	public final static int SMS_CONTENT_LENG_LIMIT = 302001;
	/** 定时短信内容超过限定长度140个字 */
	public final static String TIMD_SMS_CONTENT_LENG_ERROR_MSG = "定时短信内容超过限定长度140个字";
	/** 实时短信内容超过限定长度140个字 */
	public final static String SMS_CONTENT_LENG_ERROR_MSG = "实时短信内容超过限定长度140个字";

	/** 短信内容是空 的错误码 */
	public final static int SMS_CONTENT_IS_NULL = 302002;
	/** 短信内容是空 */
	public final static String SMS_CONTENT_NULL_MSG = "短信内容是空";

	/** 手机号码格式错误码 */
	public final static int MOBILE_IS_NULL = 302003;
	/** 手机号码是空 */
	public final static String MOBILE_NULL_MSG = "手机号码是空";

	/** 手机号码非法错误码 */
	public final static int MOBILE_FORMAT_ERROR = 302004;
	/** 手机号码非法 */
	public final static String MOBILE_FORMAT_MSG = "手机号码格式错误";

	/** 短信ID是空的错误码 */
	public final static int SMS_ID_IS_NULL = 302005;
	/** 短信ID是空 */
	public final static String SMS_ID_NULL_MSG = "短信ID是空";

	/** 业务类型是空的错误码 */
	public final static int SMS_SERVER_TYPE_IS_NULL = 302006;
	/** 业务类型是空 */
	public final static String SMS_SERVER_TYPE_NULL_MSG = "业务类型不能为空";

	/** 业务类型不是数字的错误码 */
	public final static int SMS_SERVER_TYPE_NOT_NUM = 302007;
	/** 业务类型必须是数字 */
	public final static String SMS_SERVER_TYPE_NOT_NUM_MSG = "业务类型必须是数字";

	/** 短信批次ID为空的错误码 */
	public final static int SMS_BATCH_IS_NULL = 302009;
	/** 短信批次ID为空 */
	public final static String SMS_BATCH_NULL_MSG = "短信批次ID是空";

	/** 短信数组为空的错误码 */
	public final static int SMS_ARR_IS_NULL = 302010;
	/** 短信数组为空 */
	public final static String SMS_ARR_NULL_MSG = "短信数组是空";

	/** 短信发送摘要超过限定长度20个字 错误码 */
	public final static int SMS_ABSTRACT_CONTENT_LENG_LIMIT = 302008;
	/** 短信发送摘要超过限定长度20个字 */
	public final static String SMS_ABSTRACT_CONTENT_LENG_ERROR_MSG = "短信发送摘要超过限定长度";

	/** 通道刷新错误错误码 */
	public final static int CH_REF_CODE = 302011;
	/** 通道刷新错误 */
	public final static String CH_REF_ERROR_MSG = "通道刷新错误";
	/** 刷新类型不存在错误码 */
	public final static int CH_REF_TYPE_CODE = 302020;
	/** 刷新参数类型不存在 */
	public final static String CH_REF_TYPE_NOT_EXT_MSG = "刷新参数类型不存在";
	/** 刷新参数必须是数字 */
	public final static String CH_REF_TYPE_DIGITAL_MSG = "刷新参数必须是数字";

	/** SMSId同一个应用不唯一错误码 */
	public final static int SMS_ID_ONLY_CODE = 302021;
	/** SMSId同一个应用必须唯一 */
	public final static String SMS_ID_ONLY_MSG = "同一个应用下SMSID必须唯一";

	/** 批量提交短信条数超限错误码 */
	public final static int BATCH_SUBMIT_LIMIT_ERROR = 302012;
	/** 批量提交短信条数超限 */
	public final static String BATCH_SUBMIT_LIMIT_MSG = "批量提交短信条数超限";

	/** 短信设定发送开始时间错误码 */
	public final static int PLAN_BEGIN_DATE_ERROR = 302013;
	/** 短信设定发送开始时间 */
	public final static String PLAN_BEGIN_DATE_MSG = "短信设定发送开始时间是空";

	/** 短信设定发送结束时间错误码 */
	public final static int PLAN_END_DATE_ERROR = 302014;
	/** 短信设定发送结束时间 */
	public final static String PLAN_END_DATE_MSG = "短信设定发送结束时间是空";

	/** 短信设定开始时间不能大于结束时间错误码 */
	public final static int PLAN_BEGIN_GREATE_THAN_END_DATE_ERROR = 302015;
	/** 短信设定开始时间不能大于结束时间 */
	public final static String PLAN_BEGIN_GREATE_THAN_END_DATE_MSG = "短信设定开始时间不能大于结束时间";

	/** 业务类型不是有效值的错误码 */
	public final static int SMS_SERVER_TYPE_ERROR = 302016;
	/** 业务类型不是有效值 */
	public final static String SMS_SERVER_TYPE_ERROR_MSG = "业务类型不是有效值";

	/** 通道编号不是数字的错误码 */
	public final static int CH_NO_NOT_NUM = 302017;
	/** 通道编号不是数字 */
	public final static String CH_NO_NOT_NUM_MSG = "通道编号不是数字";

	/** 通道编号不是有效值的错误码 */
	public final static int CH_NO_INVALIED = 302018;
	/** 通道编号不是有效值 */
	public final static String CH_NO_INVALIED_MSG = "通道编号不是有效值";

	/** 通道编号与业务类型不一致错误码 */
	public final static int CH_NO_TYPE_INVALIED = 302022;
	/** 通道编号与业务类型必须一致 */
	public final static String CH_NO_TYPE_INVALIED_MSG = "通道编号与业务类型必须一致";

	/** 短信时间格式错误码 */
	public final static int TIME_SMS_FORMAT_ERROR = 302019;
	/** 短信时间格式提醒 */
	public final static String TIME_SMS_FORMAT_ERROR_MSG = "时间格式错误";
}
