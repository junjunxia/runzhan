package com.runzhan.sms.constant;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author xm.L 各种常量
 */
public final class SysConstant {
	private static Map<String, String> chMap = new LinkedHashMap<String, String>();
	private static Map<String, Integer> chTypeMap = new LinkedHashMap<String, Integer>();

	static {
		chMap.put("1001", "优讯通业务通道");
		chMap.put("1002", "优讯通营销通道");
		
		chTypeMap.put("1001", 1);
		chTypeMap.put("1002", 2);
	}

	private SysConstant() {

	}
	
	public static String getChName(String chNo) {
		return chMap.get(chNo);
	}

	public static Integer getChannelTypeByChNo(String chNo) {
		return chTypeMap.get(chNo);
	}
	
	/** 1：全部刷新(默认) */
	public final static int REF_PARAM_ALL = 1;
	/** 2：通道刷新 */
	public final static int REF_PARAM_CH = 2;
	/** 3：参数刷新 */
	public final static int REF_PARAM = 3;

	/** 实时发送 */
	public final static int SMS_SEND_ATTR_IMMED = 1;
	/** 准实时发送 */
	public final int SMS_SEND_ATTR_QUASI_IMMED = 2;
	/** 定时发送 */
	public static final int SMS_SEND_ATTR_TIMER = 3;

	/** 系统自动发送 */
	public static final int SMS_SEND_TYPE_SYS = 0;
	/** 手工补发发送 */
	public static final int SMS_SEND_TYPE_ARTIF = 1;

	/** 数据抓取未锁定状态 */
	public final static int DATE_UNLOCK = 0;
	/** 数据抓取已锁定状态 */
	public final static int DATE_LOCK = 1;

	/** 业务通道 */
	public final static int BUSINESS_SMS_CHANNEL = 1;
	/** 营销通道 */
	public final static int MARKET_SMS_CHANNEL = 2;

	/** 日期字符串长度 */
	public final static int DATE_STR_LENGTH = 14;

	/** 手机号码字符串长度 */
	public final static int MOBILE_STR_LENGTH = 11;

	/** 实时短信内容长度 */
	public final static int SMS_CONT_LENGTH = 200;
	/** 定时短信内容长度 */
	public final static int TIME_SMS_CONT_LENGTH = 200;
	/** 短信摘要长度 */
	public final static int SMS_ABST_CONT_LENGTH = 20;

	/** 优讯通业务通道 */
	public final static int YXT_SERVER_CHANNEL = 1001;
	/** 优讯通营销通道 */
	public final static int YXT_MARK_CHANNEL = 1002;

	/** 校验通过 */
	public static final int VERIFIED_OK = 1;
	/** 校验失败 */
	public static final int VER_ERROR = 0;

}
