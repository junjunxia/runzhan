package com.runzhan.sms.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.runzhan.sms.constant.SysConstant;
import com.runzhan.sms.dao.SMSSendDao;
import com.runzhan.sms.exception.SMSException;
import com.runzhan.sms.po.ChannelBean;
import com.runzhan.sms.po.ChannelWeight;
import com.runzhan.sms.po.SMSChannelBase;

@Service
public class ChannelManagerService {
	
	public static final Logger logger = Logger.getLogger(ChannelManagerService.class);
	

	@Autowired
	private SMSSendDao dao;
	@Autowired
	private ChannelWeightManagerService channelWeightManagerService;
	
	/** 业务通道 */
	private final Map<String, ChannelBean> serviceChannelMap = new ConcurrentHashMap<String, ChannelBean>();
	/** 营销通道 */
	private final Map<String, ChannelBean> maeketChannelMap = new ConcurrentHashMap<String, ChannelBean>();
	
	
//	public ChannelBean getChannelBean (int serviceType) {
//		if (SysConstant.BUSINESS_SMS_CHANNEL == serviceType) {
//			
//		} else {
//			
//		}
//		
//	}

	// 加载短信通道信息
	public void queryDB() {
		// 查询数据库通道
		// 与缓存列表进行比对，如果缓存有，权重没有改变，那么不修改缓存通道对象，否则修改缓存通道对象
		// 如果缓存没有，DB有，那么加入缓存列表。
		// 如果缓存有，DB没有，那么删除缓存对象
		// 根据标识决定是否更新权重对象

		List<SMSChannelBase> smsChannelList = null;
		try {
			smsChannelList = dao.findAllSmsChannel();
		} catch (SMSException e) {
			logger.error("查询短信通道异常" , e);
			return;
		}
		if (smsChannelList == null || smsChannelList.size() == 0) {
			logger.info("查询短信通道--没有可用通道");
			return;
		}

		int serviceFlag = 0;
		int marketFlag = 0;
		for (SMSChannelBase record : smsChannelList) {
			String ssmschannelno = record.getChannelNo();
			int ismschanneltype = record.getChannelType();
			int newiweight = record.getWeight();
			// 业务通道
			if (SysConstant.BUSINESS_SMS_CHANNEL == ismschanneltype) {
				serviceFlag = 1;
				ChannelBean chB = new ChannelBean();
				chB.setIsmschanneltype(ismschanneltype);
				chB.setiTempweight(newiweight);
				chB.setIweight(newiweight);
				chB.setSsmschannelname(record.getChannelName());
				chB.setSsmschannelno(record.getChannelNo());
				// 新增，放入业务缓存，做权重变化标志
				serviceChannelMap.put(ssmschannelno, chB);
			} else if (SysConstant.MARKET_SMS_CHANNEL == ismschanneltype) {// 营销通道
				marketFlag = 1;
				ChannelBean chB = new ChannelBean();
				chB.setIsmschanneltype(ismschanneltype);
				chB.setiTempweight(newiweight);
				chB.setIweight(newiweight);
				chB.setSsmschannelname(record.getChannelName());
				chB.setSsmschannelno(record.getChannelNo());
				// 新增，放入业务缓存，做权重变化标志
				maeketChannelMap.put(ssmschannelno, chB);
			}
		}
		
		// 根据业务标志刷新全部业务权重
		if (serviceFlag != 0) {
			Map<String, ChannelWeight> srvWeightMap = new LinkedHashMap<String, ChannelWeight>();
			for (Map.Entry<String, ChannelBean> entry : serviceChannelMap
					.entrySet()) {
				String channelno = entry.getKey();
				ChannelBean value = entry.getValue();
				ChannelWeight chwgh = new ChannelWeight();
				chwgh.setIsmschanneltype(String.valueOf(value
						.getIsmschanneltype()));
				chwgh.setIweight(value.getIweight());
				chwgh.setSsmschannelname(value.getSsmschannelname());
				chwgh.setSsmschannelno(channelno);
				srvWeightMap.put(channelno, chwgh);
			}
			channelWeightManagerService.setServiceWeightMap(srvWeightMap);
		}
		// 根据营销标志刷新全部营销权重
		if (marketFlag != 0) {
			Map<String, ChannelWeight> mrtWeightMap = new LinkedHashMap<String, ChannelWeight>();
			for (Map.Entry<String, ChannelBean> entry : maeketChannelMap
					.entrySet()) {
				String channelno = entry.getKey();
				ChannelBean value = entry.getValue();
				ChannelWeight chwgh = new ChannelWeight();
				chwgh.setIsmschanneltype(String.valueOf(value
						.getIsmschanneltype()));
				chwgh.setIweight(value.getIweight());
				chwgh.setSsmschannelname(value.getSsmschannelname());
				chwgh.setSsmschannelno(channelno);
				mrtWeightMap.put(channelno, chwgh);
			}
			channelWeightManagerService.setMaeketWeightMap(mrtWeightMap);
		}
		
	}
}
