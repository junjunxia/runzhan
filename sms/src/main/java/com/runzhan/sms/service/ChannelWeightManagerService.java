package com.runzhan.sms.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.stereotype.Component;

import com.runzhan.sms.po.ChannelWeight;

@Component
public class ChannelWeightManagerService {
	
	private Map<String, ChannelWeight> serviceWeightMap = new LinkedHashMap<String, ChannelWeight>();
	private Map<String, ChannelWeight> maeketWeightMap = new LinkedHashMap<String, ChannelWeight>();
	private final List<ChannelWeight> serviceChannelWeights = new ArrayList<ChannelWeight>();
	private final List<ChannelWeight> marketChannelWeights = new ArrayList<ChannelWeight>();
	private int serviceIndex = 0;
	private int marketIndex = 0;
	private final Lock ServiceLock = new ReentrantLock();
	private final Lock MaeketLock = new ReentrantLock();
	
	
	public void setServiceWeightMap(Map<String, ChannelWeight> serviceWeightMap) {
		this.serviceWeightMap = serviceWeightMap;
		// 生成业务权重
		setServiceChannelWeight();
	}
	private void setServiceChannelWeight() {
		serviceChannelWeights.clear();
		for (ChannelWeight chWeight : serviceWeightMap.values()) {
			int iweight = chWeight.getIweight();
			for (int i = 0; i < iweight; i++) {
				serviceChannelWeights.add(chWeight);
			}
		}
		serviceIndex = 0;
	}
	

	public void setMaeketWeightMap(Map<String, ChannelWeight> maeketWeightMap) {
		this.maeketWeightMap = maeketWeightMap;
		// 生成营销权重
		this.setMaeketChannelWeight();
	}
	private void setMaeketChannelWeight() {
		marketChannelWeights.clear();
		for (ChannelWeight chWeight : maeketWeightMap.values()) {
			int iweight = chWeight.getIweight();
			for (int i = 0; i < iweight; i++) {
				marketChannelWeights.add(chWeight);
			}
		}
		marketIndex = 0;
	}

	public ChannelWeight getServiceChannel(boolean status) {
		ChannelWeight channelWeight = null;
		try {
			ServiceLock.lock();// 得到锁
			if (serviceChannelWeights.size() == 0) {
				// 重新生成权重
//				setServiceChannelWeight();
			}
			if (serviceChannelWeights.size() > 0) {
				channelWeight = serviceChannelWeights.get(serviceIndex);
				if (status)
					serviceIndex++;
				if (serviceIndex >= serviceChannelWeights.size()) {
					serviceIndex = 0;
				}
			}
		} finally {
			ServiceLock.unlock();// 释放锁
		}
		return channelWeight;
	}

	public ChannelWeight getMarketChannel(boolean status) {
		ChannelWeight channelWeight = null;
		try {
			MaeketLock.lock();// 得到锁
			if (marketChannelWeights.size() == 0) {
				// 重新生成权重
//				this.setMaeketChannelWeight();
			}
			if (marketChannelWeights.size() > 0) {
				channelWeight = marketChannelWeights.get(marketIndex);
				if (status)
					marketIndex++;
				if (marketIndex >= marketChannelWeights.size()) {
					marketIndex = 0;
				}
			}
		} finally {
			MaeketLock.unlock();// 释放锁
		}
		return channelWeight;
	}
	
}
