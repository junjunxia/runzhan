package com.runzhan.sms.service.impl;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.base.service.impl.BaseServiceImpl;
import com.runzhan.base.util.JacksonUtils;
import com.runzhan.sms.constant.SysConstant;
import com.runzhan.sms.dao.SmsTemplateDao;
import com.runzhan.sms.dto.RealTimeSMSBean;
import com.runzhan.sms.dto.ResponseDto;
import com.runzhan.sms.dto.TemplateSmsBean;
import com.runzhan.sms.po.SmsTemplate;
import com.runzhan.sms.po.TemplateObjectDict;
import com.runzhan.sms.service.SMSSendService;
import com.runzhan.sms.service.SmsTemplateService;
import com.runzhan.sms.util.BeanUtils;

@Service("smsTemplateService")
public class SmsTemplateServiceImpl extends BaseServiceImpl<SmsTemplate>
		implements SmsTemplateService {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private VelocityEngine velocityEngine;          //模板解析 
	@Autowired
	private SmsTemplateDao smsTemplateDao;
	@Autowired
	private SMSSendService smsSendService;
	

	protected BaseDao<SmsTemplate> getMapper() {
		return smsTemplateDao;
	}
	

	public String page(Map<String, Object> params) {
        List<SmsTemplate> list = smsTemplateDao.page(params);
        int total = smsTemplateDao.total(params);
        
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("total", total);
        map.put("rows", list);
        int pageSize = Integer.valueOf((String) params.get("pageSize"));
        map.put("pages", total%pageSize==0?total/pageSize:total/pageSize+1);
        map.put("pageSize", pageSize);
        map.put("pageNum", params.get("pageNum"));
        return JacksonUtils.object2json(map);
	}
	

	public List<TemplateObjectDict> getTemplateObjectDict() {
		return smsTemplateDao.getTemplateObjectDict();
	}

	public void updateSmsTemplate(SmsTemplate params) {
		smsTemplateDao.updateSmsTemplate(params);
	}

	public void batchDelete(List<Long> smsIdList) {
		smsTemplateDao.batchDelete(smsIdList);
	}

	public void sendTemplateSMS(TemplateSmsBean templateSmsBean) {
		if (null == templateSmsBean) {
            logger.error(" 发送模板短信 : templateSmsBean is null \n" );  
			return;
		}
		
		String[] mobiles = templateSmsBean.getMobiles();
		if (null == mobiles || mobiles.length == 0) {
            logger.error(" 发送模板短信 : 手机号为空  \n" );  
			return;
		}
		
		String smsContent = getMessage(templateSmsBean);
		int serviceType = SysConstant.BUSINESS_SMS_CHANNEL;
		for (String mobile : mobiles) {
			RealTimeSMSBean realTimeSMSBean = assemRealTimeSMSBean(mobile, smsContent, serviceType);
			
			ResponseDto dto = smsSendService.sendSMS(realTimeSMSBean);
			
			logger.debug("发送短信：RealTimeSMSBean"+realTimeSMSBean+"；ResponseDto："+dto);
		}
		
		
	}
	
	private RealTimeSMSBean assemRealTimeSMSBean(String mobile, String smsContent, int serviceType) {
		RealTimeSMSBean realTimeSMSBean = new RealTimeSMSBean();
		realTimeSMSBean.setSmsID(BeanUtils.getUUID());
		realTimeSMSBean.setServiceType(serviceType);
		realTimeSMSBean.setsMobile(mobile);
		realTimeSMSBean.setSmsContent(smsContent);
		return realTimeSMSBean;
	}

	private String getMessage(TemplateSmsBean templateSmsBean) {
		if (null == templateSmsBean) {
            logger.error(" 组装模板短信 : templateSmsBean is null \n" );  
			return null;
		}
		
        StringWriter writer = null;  
        try {  
  
            writer = new StringWriter();  
            VelocityContext context = new VelocityContext(templateSmsBean.getData());  
  
            velocityEngine.evaluate(context, writer, "", templateSmsBean.getTemplate());  
  
            return writer.toString();  
        } catch (VelocityException e) {  
            logger.error(" VelocityException : " + templateSmsBean.getTemplate() + "\n" + e);  
        } finally {  
            if (writer != null) {  
                try {  
                    writer.close();  
                } catch (IOException e) {  
                	logger.error("###StringWriter close error ... ");  
                }  
            }  
        }  
        return null;  
    }

	public SmsTemplate getById(Long id) {
		return smsTemplateDao.getById(id);
	}


	public List<SmsTemplate> getSmsTemplateList() {
		return smsTemplateDao.queryAll();
	}  	
}
