package com.runzhan.sms.service;

import com.runzhan.sms.dto.RealTimeSMSBean;
import com.runzhan.sms.dto.ResponseDto;

public interface SMSSendService {

	ResponseDto sendSMS(RealTimeSMSBean realTimeSMSBean);

}
