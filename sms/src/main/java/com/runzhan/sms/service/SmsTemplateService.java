package com.runzhan.sms.service;

import java.util.List;
import java.util.Map;

import com.runzhan.base.service.BaseService;
import com.runzhan.sms.dto.TemplateSmsBean;
import com.runzhan.sms.po.SmsTemplate;
import com.runzhan.sms.po.TemplateObjectDict;

public interface SmsTemplateService extends BaseService<SmsTemplate> {

	String page(Map<String, Object> params);
	
	List<TemplateObjectDict> getTemplateObjectDict();

	void updateSmsTemplate(SmsTemplate params);

	void batchDelete(List<Long> smsIdList);

	void sendTemplateSMS(TemplateSmsBean templateSmsBean);
	SmsTemplate getById(Long id);

	List<SmsTemplate> getSmsTemplateList();
}
