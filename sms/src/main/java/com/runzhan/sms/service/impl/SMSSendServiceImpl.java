package com.runzhan.sms.service.impl;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.runzhan.sms.channel.SMSSendFacade;
import com.runzhan.sms.constant.ErrCodeConstant;
import com.runzhan.sms.constant.SysConstant;
import com.runzhan.sms.dao.SMSSendDao;
import com.runzhan.sms.dto.RealTimeSMSBean;
import com.runzhan.sms.dto.ResponseDto;
import com.runzhan.sms.po.SMSLogBase;
import com.runzhan.sms.service.SMSSendService;
import com.runzhan.sms.util.CheckParameterUtil;
import com.runzhan.sms.util.CheckResult;
import com.runzhan.sms.util.SMSSendMsg;
import com.runzhan.sms.util.StringUtil;

/**
 * 短信service
 */
@Service
public class SMSSendServiceImpl implements SMSSendService {
	
	
	public static final Logger logger = Logger.getLogger(SMSSendServiceImpl.class);

	@Autowired
	private SMSSendDao smsSendDao;
	@Autowired
	private SMSSendFacade smsSendFacade;
	

	public ResponseDto sendSMS(RealTimeSMSBean realTimeSMSBean) {
		logger.info("RealTimeSMSBean:"+realTimeSMSBean);
		// 校验参数
		ResponseDto responseDto = checkSms(realTimeSMSBean);
		if (responseDto == null || ErrCodeConstant.SMS_SEND_FAIL == responseDto.getStatus()){
			return responseDto;
		}

		// 拼接短信接口入参
		SMSLogBase smsInfo = assemblyParam(realTimeSMSBean);
		SMSSendMsg sendSMS = null;
		try {
			// 发送实时短信
			sendSMS = send(smsInfo);
			if (ErrCodeConstant.SMS_SEND_SUCCESS == Integer.valueOf(sendSMS
					.getResultCode())) {
				responseDto.setResMsg("发送短信成功");
				responseDto.setStatus(ErrCodeConstant.SMS_SEND_SUCCESS);
			} else {
				responseDto.setErrCode(Integer.valueOf(sendSMS.getResultCode()));
				responseDto.setResMsg(sendSMS.getResulstMsg());
				responseDto.setStatus(ErrCodeConstant.SMS_SEND_FAIL);
			}
		} catch (Exception e) {
			logger.error(realTimeSMSBean + ",responseDto:" + responseDto,e);
			responseDto.setErrCode(ErrCodeConstant.SMS_SEND_FAIL);
			responseDto.setResMsg("发送短信失败");
			responseDto.setStatus(ErrCodeConstant.SMS_SEND_FAIL);
		}
		logger.info("RealTimeSMSBean:"+realTimeSMSBean+",responseDto:" + responseDto);
		return responseDto;
	}

	private ResponseDto checkSms(RealTimeSMSBean realTimeSMSBean) {
		ResponseDto responseDto = new ResponseDto();
		String smsContent = realTimeSMSBean.getSmsContent();
		// 校验短信内容
		CheckResult checkSMSContLeng = CheckParameterUtil
				.checkSMSContLeng(smsContent);
		int smsContLegCheckCode = checkSMSContLeng.getCheckCode();
		if (SysConstant.VERIFIED_OK != smsContLegCheckCode) {
			responseDto.setErrCode(checkSMSContLeng.getCheckCode());
			responseDto.setResMsg("短信发送失败," + checkSMSContLeng.getCheckMessage());
			responseDto.setStatus(ErrCodeConstant.SMS_SEND_FAIL);
			return responseDto;
		}
		// 校验手机号码格式
		CheckResult checkMobile = CheckParameterUtil.checkMobile(realTimeSMSBean.getsMobile());
		int sMobileCheckCode = checkMobile.getCheckCode();
		if (SysConstant.VERIFIED_OK != sMobileCheckCode) {
			responseDto.setErrCode(sMobileCheckCode);
			responseDto.setResMsg("短信发送失败," + checkMobile.getCheckMessage());
			responseDto.setStatus(ErrCodeConstant.SMS_SEND_FAIL);
			return responseDto;
		}
		// 校验短信ID
		CheckResult checkSMSID = CheckParameterUtil.checkSMSID(realTimeSMSBean
				.getSmsID());
		int checkSMSIDCheckCode = checkSMSID.getCheckCode();
		if (SysConstant.VERIFIED_OK != checkSMSIDCheckCode) {
			responseDto.setErrCode(checkSMSIDCheckCode);
			responseDto.setResMsg("短信发送失败," + checkSMSID.getCheckMessage());
			responseDto.setStatus(ErrCodeConstant.SMS_SEND_FAIL);
			return responseDto;
		}
		// 校验业务类型
		CheckResult checkServerType = CheckParameterUtil
				.checkServerType(realTimeSMSBean.getServiceType());
		int checkServerTypeCheckCode = checkServerType.getCheckCode();
		if (SysConstant.VERIFIED_OK != checkServerTypeCheckCode) {
			responseDto.setErrCode(checkServerTypeCheckCode);
			responseDto.setResMsg("短信发送失败," + checkServerType.getCheckMessage());
			responseDto.setStatus(ErrCodeConstant.SMS_SEND_FAIL);
			return responseDto;
		}
		// 校验短信摘要长度
		CheckResult checkSMSAb = CheckParameterUtil
				.checkSMSAbstract(realTimeSMSBean.getSmsAbstract());
		int checkSMSAbCheckCode = checkSMSAb.getCheckCode();
		if (SysConstant.VERIFIED_OK != checkSMSAbCheckCode) {
			responseDto.setErrCode(checkSMSAbCheckCode);
			responseDto.setResMsg("短信发送失败," + checkSMSAb.getCheckMessage());
			responseDto.setStatus(ErrCodeConstant.SMS_SEND_FAIL);
			return responseDto;
		}
		// 校验通道编号
		CheckResult checkChNo = CheckParameterUtil.checkChNo(
				realTimeSMSBean.getChannelNo(),
				realTimeSMSBean.getServiceType());
		int checkChNoCheckCode = checkChNo.getCheckCode();
		if (SysConstant.VERIFIED_OK != checkChNoCheckCode) {
			responseDto.setErrCode(checkChNoCheckCode);
			responseDto.setResMsg("短信发送失败," + checkChNo.getCheckMessage());
			responseDto.setStatus(ErrCodeConstant.SMS_SEND_FAIL);
			return responseDto;
		}
		responseDto.setResMsg("发送短信成功");
		responseDto.setStatus(ErrCodeConstant.SMS_SEND_SUCCESS);
		return responseDto;
	}
	
	/**
	 * 拼装发送接口参数
	 * 
	 * @param realTimeSMSBean
	 * @return SMSLogBase
	 */
	private SMSLogBase assemblyParam(RealTimeSMSBean realTimeSMSBean) {
		String smsId = realTimeSMSBean.getSmsID();
		int serviceType = realTimeSMSBean.getServiceType();
		String channelNo = realTimeSMSBean.getChannelNo();
		String smsAbstract = realTimeSMSBean.getSmsAbstract();

		SMSLogBase smsInfo = new SMSLogBase();
		if (!StringUtil.isNull(channelNo)) {
			smsInfo.setChannelNo(channelNo);

			String chName = SysConstant.getChName(channelNo);
			if (null != chName && !"null".equals(chName) && !"".equals(chName)) {
				smsInfo.setChannelName(chName);
			}

			Integer channelType = SysConstant.getChannelTypeByChNo(channelNo);
			if (null != channelType) {
				smsInfo.setChannelType(channelType);
			}
		}
		smsInfo.setSmsAttr(serviceType);
		smsInfo.setMobile(realTimeSMSBean.getsMobile());
		smsInfo.setSummary(smsAbstract);
		smsInfo.setSmsContent(realTimeSMSBean.getSmsContent());
		smsInfo.setSmsId(smsId);
		
		Date date = new Date();
		smsInfo.setPostDate(date);
		smsInfo.setSendDate(date);
		smsInfo.setSendAttr(SysConstant.SMS_SEND_ATTR_IMMED);
		smsInfo.setSendMethod(SysConstant.SMS_SEND_TYPE_ARTIF);
		return smsInfo;
	}

	/**
	 * @Title: send
	 * @Description: 发送实时短信
	 * @param SMSLogBase
	 * @return SMSSendMsg
	 * @throws SMSException
	 */
	private SMSSendMsg send(SMSLogBase smsInfo) throws Exception {
		SMSSendMsg sendSMS = null;
		String smobile = smsInfo.getMobile();
		String ssmscontent = smsInfo.getSmsContent();
		int ismschanneltype = smsInfo.getSmsAttr();

		// 发送短信
		sendSMS = smsSendFacade.sendSMSBySrvType(smobile, ssmscontent, ismschanneltype);
		if (null == sendSMS) {
			sendSMS = new SMSSendMsg();
			sendSMS.setResultCode(1);
			sendSMS.setResulstMsg("发送短信--返回为空");
			return sendSMS;
		}
		if (null != sendSMS.getChName()
				&& !"null".equals(sendSMS.getChName())
				&& !"".equals(sendSMS.getChName())) {
			smsInfo.setChannelName(sendSMS.getChName());
		}
		if (null != sendSMS.getChNo() && !"null".equals(sendSMS.getChNo())
				&& !"".equals(sendSMS.getChNo())) {
			smsInfo.setChannelNo(sendSMS.getChNo());
			Integer channelType = SysConstant.getChannelTypeByChNo(sendSMS
					.getChNo());
			if (null != channelType) {
				smsInfo.setChannelType(channelType);
			}
		}

		smsInfo.setSendStatus(Integer.parseInt(sendSMS.getResultCode()));
		// 发送结果,保存发送结果到数据库
		smsSendDao.insertSMSSendLogs(smsInfo);

		return sendSMS;
	}

}
