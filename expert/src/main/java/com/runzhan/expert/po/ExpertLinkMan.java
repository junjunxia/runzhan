package com.runzhan.expert.po;

public class ExpertLinkMan {
    private Integer id;

    private String name;

    private String title;

    private String unitname;

    private String department;

    private String mobile;
    private String phone;

    private String fax;

    private String email;

    private String unitAddr;

    private Integer expertId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUnitname() {
        return unitname;
    }

    public void setUnitname(String unitname) {
        this.unitname = unitname;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUnitAddr() {
        return unitAddr;
    }

    public void setUnitAddr(String unitAddr) {
        this.unitAddr = unitAddr;
    }

    public Integer getExpertId() {
        return expertId;
    }

    public void setExpertId(Integer expertId) {
        this.expertId = expertId;
    }

	public ExpertLinkMan(String name, String title, String unitname, String department, String mobile, String phone,
			String fax, String email, String unitAddr) {
		super();
		this.name = name;
		this.title = title;
		this.unitname = unitname;
		this.department = department;
		this.mobile = mobile;
		this.phone = phone;
		this.fax = fax;
		this.email = email;
		this.unitAddr = unitAddr;
	}

	public ExpertLinkMan() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    
}