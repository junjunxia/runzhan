package com.runzhan.expert.po;

import java.util.Date;

public class ActivityOut {
    private Long id;
    private String title;
    private String venue;
    private Date beginTime;
    private Date endTime;
    private String remarks;
    
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public ActivityOut(String title, String venue, Date beginTime, Date endTime, String remarks) {
		super();
		this.title = title;
		this.venue = venue;
		this.beginTime = beginTime;
		this.endTime = endTime;
		this.remarks = remarks;
	}
	public ActivityOut() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}