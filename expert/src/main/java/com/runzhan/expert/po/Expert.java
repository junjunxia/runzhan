package com.runzhan.expert.po;

import java.util.Date;

public class Expert {
    private Integer id;

    private String name;

    private String unitname;

    private String email;

    private String department;
    private String title;

    private String mobile;

    private String phone;

    private String fax;

    private String unitAddr;

    private String zipCode;

    //行业领域
    private String domainName;

    private String topic;

    private String socialCircle;
    
    private String socialTitle;

    private Long createId;
 
    private String createName;
    
    private Date createTime;
    //所属领域
    private Long domainId;
    
    private String domainIdName;
    
    public Long getCreateId() {
		return createId;
	}
	public void setCreateId(Long createId) {
		this.createId = createId;
	}
	public String getCreateName() {
		return createName;
	}
	public void setCreateName(String createName) {
		this.createName = createName;
	}

	private int inviteCount = 0;
	private int attendCount = 0;
    public int getInviteCount() {
		return inviteCount;
	}
	public void setInviteCount(int inviteCount) {
		this.inviteCount = inviteCount;
	}
	public int getAttendCount() {
		return attendCount;
	}
	public void setAttendCount(int attendCount) {
		this.attendCount = attendCount;
	}

	public String getSocialTitle() {
		return socialTitle;
	}

	public void setSocialTitle(String socialTitle) {
		this.socialTitle = socialTitle;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	private Integer score;

    private String image;

    private String resume;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnitname() {
        return unitname;
    }

    public void setUnitname(String unitname) {
        this.unitname = unitname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getUnitAddr() {
        return unitAddr;
    }

    public void setUnitAddr(String unitAddr) {
        this.unitAddr = unitAddr;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }


    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getSocialCircle() {
        return socialCircle;
    }

    public void setSocialCircle(String socialCircle) {
        this.socialCircle = socialCircle;
    }

   

    public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }
	public Expert(String name, String unitname, String email, String department, String title, String mobile,
			String phone, String fax, String unitAddr, String zipCode, String domainName, String topic,
			String socialCircle, String socialTitle, String resume) {
		super();
		this.name = name;
		this.unitname = unitname;
		this.email = email;
		this.department = department;
		this.title = title;
		this.mobile = mobile;
		this.phone = phone;
		this.fax = fax;
		this.unitAddr = unitAddr;
		this.zipCode = zipCode;
		this.domainName = domainName;
		this.topic = topic;
		this.socialCircle = socialCircle;
		this.socialTitle = socialTitle;
		this.resume = resume;
	}
	public Expert() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getDomainId() {
		return domainId;
	}
	public void setDomainId(Long domainId) {
		this.domainId = domainId;
	}
	public String getDomainIdName() {
		return domainIdName;
	}
	public void setDomainIdName(String domainIdName) {
		this.domainIdName = domainIdName;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
    
    
}