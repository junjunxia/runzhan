package com.runzhan.expert.po;

public class ActivityOrg {
    private Integer id;
    private Integer activityId;

    private Integer orgType;

    private String name;

    public Integer getId() {
        return id;
    }

    
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public Integer getOrgType() {
        return orgType;
    }

    public void setOrgType(Integer orgType) {
        this.orgType = orgType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
	public ActivityOrg() {
		super();
	}

	public ActivityOrg(Integer activityId, Integer orgType, String name) {
		super();
		this.activityId = activityId;
		this.orgType = orgType;
		this.name = name;
	}
    
    
}