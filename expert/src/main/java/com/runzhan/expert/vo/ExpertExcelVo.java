package com.runzhan.expert.vo;

import java.util.Date;

import com.runzhan.expert.po.Expert;
import com.runzhan.expert.po.ExpertLinkMan;
public class ExpertExcelVo {
	private Expert expert;
	private ExpertLinkMan linkMan;
	
	    private Long id;

	    private String name;

	    private String topic;
	    
	    private String inviter;
	    
	    private Date beginTime;

	    private Date endTime;

	    private String zhuban;
	    
	    private String chenban;


	    private String remarks;
	
	
	
	
	
	

	public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getTopic() {
			return topic;
		}
		public void setTopic(String topic) {
			this.topic = topic;
		}
		public String getInviter() {
			return inviter;
		}
		public void setInviter(String inviter) {
			this.inviter = inviter;
		}
		public Date getBeginTime() {
			return beginTime;
		}
		public void setBeginTime(Date beginTime) {
			this.beginTime = beginTime;
		}
		public Date getEndTime() {
			return endTime;
		}
		public void setEndTime(Date endTime) {
			this.endTime = endTime;
		}
		public String getZhuban() {
			return zhuban;
		}
		public void setZhuban(String zhuban) {
			this.zhuban = zhuban;
		}
		public String getChenban() {
			return chenban;
		}
		public void setChenban(String chenban) {
			this.chenban = chenban;
		}
		public String getRemarks() {
			return remarks;
		}
		public void setRemarks(String remarks) {
			this.remarks = remarks;
		}
	public Expert getExpert() {
		return expert;
	}
	public void setExpert(Expert expert) {
		this.expert = expert;
	}
	public ExpertLinkMan getLinkMan() {
		return linkMan;
	}
	public void setLinkMan(ExpertLinkMan linkMan) {
		this.linkMan = linkMan;
	}

	private String errorMsg;//失败消息
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
}
