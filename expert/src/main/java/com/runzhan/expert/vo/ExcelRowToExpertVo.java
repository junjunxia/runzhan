package com.runzhan.expert.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;

import com.runzhan.base.util.ExcelRowToObj;
import com.runzhan.base.util.ExcelUtil;
import com.runzhan.expert.po.Expert;
import com.runzhan.expert.po.ExpertLinkMan;

/*
 *@Description:Excel行转为实体类
 *@Since:2017年2月22日
 *@Version:1.1.0
 */
public class ExcelRowToExpertVo implements ExcelRowToObj<ExpertExcelVo> {
	private List<String> excelErrorMsgs = new ArrayList<String>();

	private Integer rowNumber;
	
	SimpleDateFormat sdf =   new SimpleDateFormat("yyyyMMdd");
	

	public ExpertExcelVo excelRowToObj(Row row) {
		rowNumber = row.getRowNum() + 1;
//		memberId = ExcelUtil.getCellValue(row.getCell(0));
//		auditResult = ExcelUtil.getCellValue(row.getCell(1));
		ExpertExcelVo vo = new ExpertExcelVo();
		Expert expert  = new Expert();
		expert.setName(ExcelUtil.getCellValue(row.getCell(1))==null?"":ExcelUtil.getCellValue(row.getCell(1)).trim());
		expert.setUnitname(ExcelUtil.getCellValue(row.getCell(2))==null?"":ExcelUtil.getCellValue(row.getCell(2)).trim());		
		expert.setDepartment(ExcelUtil.getCellValue(row.getCell(3))==null?"":ExcelUtil.getCellValue(row.getCell(3)));
		expert.setTitle(ExcelUtil.getCellValue(row.getCell(4))==null?"":ExcelUtil.getCellValue(row.getCell(4)));
		expert.setMobile(ExcelUtil.getCellValue(row.getCell(5))==null?"":ExcelUtil.getCellValue(row.getCell(5)).trim()); 
		expert.setPhone(ExcelUtil.getCellValue(row.getCell(6))==null?"":ExcelUtil.getCellValue(row.getCell(6)));
		expert.setEmail(ExcelUtil.getCellValue(row.getCell(7))==null?"":ExcelUtil.getCellValue(row.getCell(7)));
		expert.setFax(ExcelUtil.getCellValue(row.getCell(8))==null?"":ExcelUtil.getCellValue(row.getCell(8))); 
		expert.setUnitAddr(ExcelUtil.getCellValue(row.getCell(9))==null?"":ExcelUtil.getCellValue(row.getCell(9))); 
		expert.setZipCode(ExcelUtil.getCellValue(row.getCell(10))==null?"":ExcelUtil.getCellValue(row.getCell(10)));
		expert.setDomainName(ExcelUtil.getCellValue(row.getCell(11))==null?"":ExcelUtil.getCellValue(row.getCell(11))); 
		expert.setSocialTitle(ExcelUtil.getCellValue(row.getCell(12))==null?"":ExcelUtil.getCellValue(row.getCell(12))); 
		expert.setResume(ExcelUtil.getCellValue(row.getCell(13))==null?"":ExcelUtil.getCellValue(row.getCell(13)));
		
		vo.setName(ExcelUtil.getCellValue(row.getCell(14))==null?"":ExcelUtil.getCellValue(row.getCell(14)));
		vo.setTopic(ExcelUtil.getCellValue(row.getCell(16))==null?"":ExcelUtil.getCellValue(row.getCell(16)));
		String date=ExcelUtil.getCellValue(row.getCell(15))==null?"":ExcelUtil.getCellValue(row.getCell(15));
		if(StringUtils.isNotBlank(date)){
			try {
				vo.setBeginTime(sdf.parse(date));
				vo.setEndTime(sdf.parse(date));
			} catch (ParseException e) {
				e.printStackTrace();
				vo.setBeginTime(null);
				vo.setEndTime(null);
			}
		}
		vo.setZhuban(ExcelUtil.getCellValue(row.getCell(17))==null?"":ExcelUtil.getCellValue(row.getCell(17)));
		vo.setChenban(ExcelUtil.getCellValue(row.getCell(18))==null?"":ExcelUtil.getCellValue(row.getCell(18)));
		vo.setRemarks(ExcelUtil.getCellValue(row.getCell(20))==null?"":ExcelUtil.getCellValue(row.getCell(20)));
		vo.setInviter(ExcelUtil.getCellValue(row.getCell(19))==null?"":ExcelUtil.getCellValue(row.getCell(19)));
		
		ExpertLinkMan linkman = new ExpertLinkMan();
		linkman.setName(ExcelUtil.getCellValue(row.getCell(21))); 		
		linkman.setUnitname(ExcelUtil.getCellValue(row.getCell(22)));
		linkman.setDepartment(ExcelUtil.getCellValue(row.getCell(23))); 
		linkman.setTitle(ExcelUtil.getCellValue(row.getCell(24))); 
		linkman.setMobile(ExcelUtil.getCellValue(row.getCell(25))); 
		linkman.setPhone(ExcelUtil.getCellValue(row.getCell(26))); 
		linkman.setFax(ExcelUtil.getCellValue(row.getCell(27)));		
		linkman.setUnitAddr(ExcelUtil.getCellValue(row.getCell(28)));
		linkman.setEmail(ExcelUtil.getCellValue(row.getCell(29)));
		vo.setExpert(expert);
		vo.setLinkMan(linkman);
		return vo;
	}
	public List<String> getExcelErrorMsgs() {
		return excelErrorMsgs;
	}

}
