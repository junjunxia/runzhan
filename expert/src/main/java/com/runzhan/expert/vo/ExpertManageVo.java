package com.runzhan.expert.vo;

public class ExpertManageVo {

	private Integer id;

	private Integer expertId;

    private String name;
    private String unitname;

    private String department;

    private String email;
    
    private String mobile;

    private String topic;
    
    private String activityTitle;
    
    private Integer inviteStatus;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getExpertId() {
		return expertId;
	}

	public void setExpertId(Integer expertId) {
		this.expertId = expertId;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnitname() {
		return unitname;
	}

	public void setUnitname(String unitname) {
		this.unitname = unitname;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getActivityTitle() {
		return activityTitle;
	}

	public void setActivityTitle(String activityTitle) {
		this.activityTitle = activityTitle;
	}

	public Integer getInviteStatus() {
		return inviteStatus;
	}

	public void setInviteStatus(Integer inviteStatus) {
		this.inviteStatus = inviteStatus;
	}


	public String toString() {
		return "ExpertManageVo [id=" + id + ", expertId=" + expertId
				+ ", name=" + name + ", unitname=" + unitname + ", department="
				+ department + ", email=" + email + ", mobile=" + mobile
				+ ", topic=" + topic + ", activityTitle=" + activityTitle
				+ ", inviteStatus=" + inviteStatus + "]";
	}
    
}
