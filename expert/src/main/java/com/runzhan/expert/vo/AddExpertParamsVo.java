package com.runzhan.expert.vo;

import java.util.List;

public class AddExpertParamsVo {
	
	private Long activityId;
	private String activityTitle;
	private String expertName;
    private String domainName;
    private List<Long> expertIdList;
    
	public List<Long> getExpertIdList() {
		return expertIdList;
	}
	public void setExpertIdList(List<Long> expertIdList) {
		this.expertIdList = expertIdList;
	}
	public Long getActivityId() {
		return activityId;
	}
	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}
	public String getExpertName() {
		return expertName;
	}
	public void setExpertName(String expertName) {
		this.expertName = expertName;
	}

	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public String getActivityTitle() {
		return activityTitle;
	}
	public void setActivityTitle(String activityTitle) {
		this.activityTitle = activityTitle;
	}

	public String toString() {
		return "AddExpertParamsVo [activityId=" + activityId + ", activityTitle=" + activityTitle + ", expertName="
				+ expertName + ", domainName=" + domainName + ", expertIdList=" + expertIdList + "]";
	}
	

}
