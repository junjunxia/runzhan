package com.runzhan.expert.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.expert.po.ExpertLinkMan;

public interface ExpertLinkManMapper extends BaseDao<ExpertLinkMan> {
    int deleteByPrimaryKey(Integer id);

    int insert(ExpertLinkMan record);

    int insertSelective(ExpertLinkMan record);

    ExpertLinkMan selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ExpertLinkMan record);

    int updateByPrimaryKey(ExpertLinkMan record);

    
	void deleteByExId(Integer id);

	List<ExpertLinkMan> getByParams(@Param("expertId") int expertId, @Param("name") String name);
}