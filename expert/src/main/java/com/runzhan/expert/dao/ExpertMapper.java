package com.runzhan.expert.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.expert.po.ActivityOrg;
import com.runzhan.expert.po.ActivityOut;
import com.runzhan.expert.po.Expert;
import com.runzhan.expert.po.ExpertActivityRelation;
import com.runzhan.expert.po.JobDomain;
import com.runzhan.expert.vo.AddExpertParamsVo;
import com.runzhan.expert.vo.ExpertInfoVo;
import com.runzhan.expert.vo.ExpertManageVo;
import com.runzhan.expert.vo.InviteExpertCountVo;
import org.apache.poi.ss.formula.functions.T;

public interface ExpertMapper extends BaseDao<Expert> {
    int deleteByPrimaryKey(Integer id);

    int insert(Expert record);

    int insertSelective(Expert record);

    Expert selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Expert record);

    int updateByPrimaryKeyWithBLOBs(Expert record);

    int updateByPrimaryKey(Expert record);

	Integer getByMobile(String mobile);

	List<ExpertManageVo> getExpertManageList(@Param("activityId") Long activityId
            , @Param("expertName") String expertName);

	List<Expert> getExpertListBy(AddExpertParamsVo params);

	void batchSaveExpertActivityRelation(List<ExpertActivityRelation> list);

	List<JobDomain> domainList();

	void saveExpertActivityRelation(ExpertActivityRelation rl);

	List<ExpertInfoVo> getExpertInfoList(List<Long> list);

	List<InviteExpertCountVo> getExpertCountVo(List<Integer> list);

	List<JobDomain> domainSerach(JobDomain name);

	void insertAvtivityOut(ActivityOut out);

	
	void insertActivityExperRl(ExpertActivityRelation rl);

	void insertActivityOrg(ActivityOrg activityOrg);

	List<T> quertExport(@Param("params")Map<String, Object> paraMap);
	
	Expert queryExportByName(String name);
}