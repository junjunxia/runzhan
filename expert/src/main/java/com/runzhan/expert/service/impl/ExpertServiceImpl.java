package com.runzhan.expert.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.alibaba.fastjson.JSON;
import com.runzhan.base.dao.BaseDao;
import com.runzhan.base.service.impl.BaseServiceImpl;
import com.runzhan.base.util.JacksonUtils;
import com.runzhan.base.vo.PageBean;
import com.runzhan.expert.dao.ExpertLinkManMapper;
import com.runzhan.expert.dao.ExpertMapper;
import com.runzhan.expert.po.ActivityOrg;
import com.runzhan.expert.po.ActivityOut;
import com.runzhan.expert.po.Expert;
import com.runzhan.expert.po.ExpertActivityRelation;
import com.runzhan.expert.po.ExpertLinkMan;
import com.runzhan.expert.po.JobDomain;
import com.runzhan.expert.service.ExpertService;
import com.runzhan.expert.vo.AddExpertParamsVo;
import com.runzhan.expert.vo.ExpertExcelVo;
import com.runzhan.expert.vo.ExpertInfoVo;
import com.runzhan.expert.vo.ExpertManageVo;
import com.runzhan.expert.vo.InviteExpertCountVo;
@Service("expertService")
public class ExpertServiceImpl extends BaseServiceImpl<Expert> implements ExpertService {
	Logger log = Logger.getLogger(this.getClass());
	@Autowired
	private  ExpertMapper  expertDao;
	@Autowired
	private  ExpertLinkManMapper  expertLinkManDao;

	protected BaseDao<Expert> getMapper() {
 		return expertDao;
	}

	public String page(Map<String, Object> params) {
		params.put("nickname",params.get("name"));
        List<Expert> list = expertDao.page(params);
        List<Integer> expertIds = new ArrayList<Integer>();
        Map<Integer, Expert> result = new HashMap<Integer, Expert>();
        for (Expert ep : list) {
        	expertIds.add(ep.getId());
        	result.put(ep.getId(), ep);
        }
        List<InviteExpertCountVo> inviteExpertCountList = new ArrayList<InviteExpertCountVo>();
        if (!CollectionUtils.isEmpty(expertIds)) {
        	inviteExpertCountList = expertDao.getExpertCountVo(expertIds);
        }
        
        for (InviteExpertCountVo vo : inviteExpertCountList) {
        	if (result.containsKey(vo.getExpertId())) {
        		Expert ep = result.get(vo.getExpertId());
        		ep.setAttendCount(vo.getAttendCount());
        		ep.setInviteCount(vo.getInviteCount());
        	}
        }
        Collection<Expert> expertList= result.values();
        
        int total = expertDao.total(params);
        int pageSize = Integer.valueOf((String) params.get("pageSize"));
        PageBean<Expert> page = new PageBean<Expert>(new ArrayList<Expert>(expertList), pageSize, total%pageSize==0?total/pageSize:total/pageSize+1, total, pageSize,Integer.parseInt(params.get("pageNum").toString()));
        return JacksonUtils.object2json(page);
    }

    public List<Map<String,Object>> quertExport(Map<String, Object> params){
		List results = expertDao.quertExport(params);

		return results;
	}

	public Map<String, Object> saveExpert(Expert user) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		String flag ="0";
		try {
			if(null==user.getId()){
				 expertDao.insert(user);
				if(user.getId()>0){
					flag ="1";
				   jsonMap.put("expertId", user.getId());
				}
			}else{
				expertDao.updateByPrimaryKeySelective(user);
				flag ="1";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		jsonMap.put("flag", flag);
		return jsonMap;
	}

	public Map<String, Object> saveExpertLinkMan(ExpertLinkMan user) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		String flag ="0";
		try {
		if(null==user.getId()){
			if(StringUtils.isEmpty(user.getName())){
				jsonMap.put("flag", 1);
				return jsonMap;
			}
			int count = expertLinkManDao.insert(user);
			if(count>0){
				flag ="1";
			}
		}else{
			expertLinkManDao.updateByPrimaryKey(user);
			flag ="1";
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		jsonMap.put("flag", flag);
		return jsonMap;
	}

	public Map<String, Object> delete(Expert user) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		String flag ="0";
		
			int count = expertDao.deleteByPrimaryKey(user.getId());
			
			if(count>0){
				flag ="1";
				expertLinkManDao.deleteByExId(user.getId());
			}
		jsonMap.put("flag", flag);
		return jsonMap;
	}

	public Map<String, Object> batchDelete(List<Integer> deletIDS) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		
		String flag ="0";
		if(null == deletIDS || deletIDS.size() ==0 )
		{
			jsonMap.put("msg", "请选择专家");
	    }
		else
		{

	        for (Integer id : deletIDS) 
	        {
	        	int count = expertDao.deleteByPrimaryKey(id);
				if(count>0){
					flag ="1";
					expertLinkManDao.deleteByExId(id);
				}
	        }

		}
		
		jsonMap.put("flag", flag);
		return jsonMap;
	}
	
	public Expert get(Integer id) {
		return expertDao.selectByPrimaryKey(id);
	}


	public List<ExpertManageVo> getExpertManageList(Long activityId,
			String expertName) {
		return expertDao.getExpertManageList(activityId, expertName);
	}


	public List<Expert> getExpertListBy(AddExpertParamsVo params) {
		return expertDao.getExpertListBy(params);
	}


	public Map<String, Object> saveInviterExpert(AddExpertParamsVo params,
			List<Long> expertIdList) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		String flag ="1";
		flag = checkInviterExpertParams(params, expertIdList);
		if ("0".equals(flag)) {
			jsonMap.put("msg", "参数为空");
			jsonMap.put("flag", flag);
			return jsonMap;
		}
		
		for (Long expertId : expertIdList) {
			ExpertActivityRelation rl = new ExpertActivityRelation();
			rl.setActivityId(params.getActivityId());
			rl.setActivityTitle(params.getActivityTitle());
			rl.setExpertId(expertId);
			rl.setInviteStatus(0);
			expertDao.saveExpertActivityRelation(rl);
		}
		
		
//		List<ExpertActivityRelation> list = new ArrayList<ExpertActivityRelation>(expertIdList.size());
//		assemSaveParams(list, expertIdList, params);
//		expertDao.batchSaveExpertActivityRelation(list);
		jsonMap.put("flag", flag);
		return jsonMap;
	}

	private void assemSaveParams(List<ExpertActivityRelation> list,
			List<Long> expertIdList, AddExpertParamsVo vo) {
		for (Long expertId : expertIdList) {
			ExpertActivityRelation rl = new ExpertActivityRelation();
			rl.setActivityId(vo.getActivityId());
			rl.setActivityTitle(vo.getActivityTitle());
			rl.setExpertId(expertId);
			rl.setInviteStatus(0);
			list.add(rl);
		}
		
	}

	private String checkInviterExpertParams(AddExpertParamsVo params,
			List<Long> expertIdList) {
		String flag ="1";
		if (null == params || null == params.getActivityId() || null == expertIdList){
			return "0";
		}
		if (CollectionUtils.isEmpty(expertIdList)) {
			return "0";
		}
		return flag;
	}


	public List<JobDomain> domainList() {
		return expertDao.domainList();
	}


	public List<ExpertInfoVo> getExpertInfoList(List<Long> expertIds) {
		return expertDao.getExpertInfoList(expertIds);
	}


	public List<JobDomain> domainSerach(JobDomain domainName) {
		return expertDao.domainSerach(domainName);
	}

	public String checkImportParams(ExpertExcelVo expertExcelVo)
	{   
		
		String errorMessage = "";
		Expert expert  = expertExcelVo.getExpert();
		//判断专家基础信息
		String expertName = expert.getName();
		if(StringUtils.isEmpty(expertName))
		{
			errorMessage+="专家姓名不允许为空，";
		}
		else
		{
			if(expertName.length()>20)
			{
				errorMessage+="专家姓名不允许超过20位，";
			}
			//检查专家是否存在
			if(null != expertDao.queryExportByName(expertName))
			{
				errorMessage+="专家信息已经存在，";
			}
		}
		//判断专家基础信息
		if(!StringUtils.isEmpty(expert.getMobile()) && expert.getMobile().length()>11)
		{
			errorMessage+="专家联系人手机号不允许超过11位，";
		}
		
		
		return errorMessage;
	}

	public Map<String, List<ExpertExcelVo>> batchAddExpert(List<ExpertExcelVo> toImportData) {
		Map<String,List<ExpertExcelVo>> resultMap=new HashMap<String,List<ExpertExcelVo>>();
		List<ExpertExcelVo> successData=new ArrayList<ExpertExcelVo>();
		List<ExpertExcelVo> failData=new ArrayList<ExpertExcelVo>();
		
		for (ExpertExcelVo expertInfo : toImportData) {
			log.debug("[excel导入]Execl导入-----开始"
					+ JSON.toJSONString(expertInfo));
			
			//参数校验
			String errorMessage = checkImportParams(expertInfo);
			if(!StringUtils.isEmpty(errorMessage))
			{
				expertInfo.setErrorMsg(errorMessage.substring(0,errorMessage.length()-1));
				failData.add(expertInfo);
				continue;
			}
			
			Map<String, Object> params = new HashMap<String, Object>();
			try {
				Expert expert  = expertInfo.getExpert();
				expertDao.insert(expert);
				if(expert.getId()>0){
//					Integer id = expertDao.getByMobile(expertInfo.getExpert().getMobile());
					if(null !=expertInfo.getLinkMan()){
						expertInfo.getLinkMan().setExpertId(expert.getId());
						expertLinkManDao.insert(expertInfo.getLinkMan());
					}
					
					ActivityOut out = new ActivityOut(expertInfo.getName(), null, expertInfo.getBeginTime(), expertInfo.getEndTime(), expertInfo.getRemarks());
					expertDao.insertAvtivityOut(out);
					ExpertActivityRelation rl = assemExpertActivityRelation(expertInfo,out.getId(),expert);
					expertDao.insertActivityExperRl(rl);
					expertDao.insertActivityOrg(new ActivityOrg(out.getId().intValue(),1,expertInfo.getZhuban()));
					expertDao.insertActivityOrg(new ActivityOrg(out.getId().intValue(),2,expertInfo.getChenban()));
					
					successData.add(expertInfo);
				}
				
			} catch (Exception e) {
				log.error(
						"[excel导入]插入异常="
								+ params.toString(), e);
				expertInfo.setErrorMsg("插入异常");
				failData.add(expertInfo);
			}
		}
		resultMap.put("successData", successData);
		resultMap.put("failData", failData);
		log.debug("[excel导入]插入结束-----成功"+successData.size()+"条,失败"+failData.size()+"条");
		return resultMap;
	}
	
	private ExpertActivityRelation assemExpertActivityRelation(
			ExpertExcelVo expertInfo, Long activityId,Expert expert) {
		ExpertActivityRelation rl = new ExpertActivityRelation();
		rl.setExpertId(Long.valueOf(expert.getId()));
		rl.setActivityId(activityId);
		rl.setInviter(expertInfo.getInviter());
		rl.setTopic(expertInfo.getTopic());
		rl.setRemarks(expertInfo.getRemarks());
		rl.setActivityTitle(expertInfo.getName());
		rl.setInviteStatus(1);
		return rl;
	}

}
