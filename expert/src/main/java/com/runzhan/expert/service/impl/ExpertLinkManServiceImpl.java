package com.runzhan.expert.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.base.service.impl.BaseServiceImpl;
import com.runzhan.expert.dao.ExpertLinkManMapper;
import com.runzhan.expert.po.ExpertLinkMan;
import com.runzhan.expert.service.ExpertLinkManService;

@Service("expertLinkManService")
public class ExpertLinkManServiceImpl extends BaseServiceImpl<ExpertLinkMan>implements ExpertLinkManService {

	@Autowired
	private ExpertLinkManMapper mapper;
	

	protected BaseDao<ExpertLinkMan> getMapper() {
		return mapper;
	}

	public List<ExpertLinkMan> getByParams(int expertId, String expertLinkManName) {
		return mapper.getByParams(expertId, expertLinkManName);
	}


	public Map<String, Object> delete(Integer id) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		String flag ="0";
		
		int count = mapper.deleteByPrimaryKey(id);
		if (count > 0) {
			flag ="1";
		}
		
		jsonMap.put("flag", flag);
		return jsonMap;
	}

}
