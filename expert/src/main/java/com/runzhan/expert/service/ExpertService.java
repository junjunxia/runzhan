package com.runzhan.expert.service;

import java.util.List;
import java.util.Map;

import com.runzhan.expert.po.Expert;
import com.runzhan.expert.po.ExpertLinkMan;
import com.runzhan.expert.po.JobDomain;
import com.runzhan.expert.vo.AddExpertParamsVo;
import com.runzhan.expert.vo.ExpertExcelVo;
import com.runzhan.expert.vo.ExpertInfoVo;
import com.runzhan.expert.vo.ExpertManageVo;

public interface ExpertService {

	Map<String, Object> saveExpert(Expert user);

	Map<String, Object> saveExpertLinkMan(ExpertLinkMan user);

	Map<String, Object> delete(Expert user);
	
	Map<String, Object> batchDelete(List<Integer> deletIDS);
	
	Expert get(Integer id);

	List<ExpertManageVo> getExpertManageList(Long activityId, String expertName);

	List<Expert> getExpertListBy(AddExpertParamsVo params);

	Map<String, Object> saveInviterExpert(AddExpertParamsVo params,
                                          List<Long> expertIdList);

	List<JobDomain> domainList();

	List<ExpertInfoVo> getExpertInfoList(List<Long> expertIds);

	List<JobDomain> domainSerach(JobDomain domainName);

	Map<String, List<ExpertExcelVo>> batchAddExpert(List<ExpertExcelVo> toImportData);

	List<Map<String,Object>> quertExport(Map<String, Object> params);

}
