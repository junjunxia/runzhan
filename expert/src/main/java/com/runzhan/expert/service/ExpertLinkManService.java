package com.runzhan.expert.service;

import java.util.List;
import java.util.Map;

import com.runzhan.expert.po.ExpertLinkMan;

public interface ExpertLinkManService {

	List<ExpertLinkMan> getByParams(int expertId, String expertLinkManName);

	Map<String, Object> delete(Integer id);
}
