package com.runzhan.base.util;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.StringUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * 导出EXCEL
 * @author lijianjun
 *
 */
public class ExcelWriter {
	

	/**
	 * @param sheetName sheet名称
	 * @param subTitle 二级标题
	 * @param data Excel数据集合
	 * @param title 列名集合
	 * @return
	 */
	public static HSSFWorkbook write2003Xls(String sheetName,String subTitle,List<List<String>> datas,List<String> coloms){
		
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet(sheetName);
		int rowIndex = 0;
		//起始行，起始列，结束行，结束列  
		HSSFRow TOP = sheet.createRow(rowIndex++);
		HSSFRow SED = sheet.createRow(rowIndex++);
		HSSFRow THR = sheet.createRow(rowIndex++);
		//增加标题
		HSSFCell toptitle = TOP.createCell(0);
		toptitle.setCellValue(sheetName);
		toptitle.setCellStyle(getXSSFCellStyle(workbook,1));
		
		//增加副标题
		HSSFCell subCell = SED.createCell(0);
		subCell.setCellValue(subTitle);
		subCell.setCellStyle(getXSSFCellStyle(workbook,3));
		
		//增加列名
		if(coloms!=null&& coloms.size()>0){
			for(int i=0;i<coloms.size();i++){
				HSSFCell coltitle = THR.createCell(i);
				
				coltitle.setCellValue(coloms.get(i) == null ? "" : coloms.get(i));
				coltitle.setCellStyle(getXSSFCellStyle(workbook,2));
			}
		
			// 四个参数分别是：起始行，起始列，结束行，结束列
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, coloms.size()-1));
		}
		
		//填充数据
		if(datas!=null){
			for(int i=0;i<datas.size();i++){
				HSSFRow rowCell = sheet.createRow(rowIndex++);
				List<String> data = datas.get(i);
				for(int j=0;j<data.size();j++){
					HSSFCell cellM = rowCell.createCell(j);
					cellM.setCellValue(data.get(j) == null ? "" : data.get(j));
				}
			}
		}
		
		//增加脚底
		HSSFRow footer = sheet.createRow(rowIndex++);
		if(coloms!= null){
			for(int i=0;i<coloms.size();i++){
				HSSFCell coltitle = footer.createCell(i);
				if(i==(coloms.size()-1)){
					coltitle.setCellValue("admin");
				}
				coltitle.setCellStyle(getXSSFCellStyle(workbook,4));
			}
		}
		
		return workbook;
	}
	
	/**
	 * 创建单表单Excel对象
	 * @param rowCellList Excel数据集合
	 * @param columnNameList 列名集合
	 * @param sheetName sheet名称
	 * @return
	 */
	public static XSSFWorkbook write2007Xls(String sheetName,List<String> subTitle,List<List<String>> datas,List<String> coloms) {
		
		XSSFWorkbook workbook = new XSSFWorkbook(); 
		XSSFSheet sheet = workbook.createSheet(sheetName);
		int rowIndex = 0;


		if(subTitle!=null&&subTitle.size()>0){
			//SEDCOND
			XSSFRow SED = sheet.createRow(rowIndex++);
			//增加副标题
			for(int i=0;i<subTitle.size();i++){
				XSSFCell subCell = SED.createCell(i);

				subCell.setCellValue(subTitle.get(i) == null ? "" : subTitle.get(i));
				subCell.setCellStyle(getXSSFCellStyle(workbook,3));
			}
		}
		
		XSSFRow THR = sheet.createRow(rowIndex++);
		
		XSSFCellStyle style = getXSSFCellStyle(workbook,5);
		
		//增加列名
		if(coloms!= null && coloms.size()>0){
			for(int i=0;i<coloms.size();i++){
				XSSFCell coltitle = THR.createCell(i);
				
				coltitle.setCellValue(coloms.get(i) == null ? "" : coloms.get(i));
				coltitle.setCellStyle(getXSSFCellStyle(workbook,2));
			}
		
			// 四个参数分别是：起始行，起始列，结束行，结束列  (合并第一行标题)
			//sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, coloms.size()-1));
		}
		
		//填充数据
		if(datas!=null){
			for(int i=0;i<datas.size();i++){
				XSSFRow rowCell = sheet.createRow(rowIndex++);
				List<String> data = datas.get(i);
				for(int j=0;j<data.size();j++){
					XSSFCell cellM = rowCell.createCell(j);
					cellM.setCellValue(data.get(j) == null ? "" : data.get(j));
					cellM.setCellType(XSSFCell.CELL_TYPE_STRING);
					cellM.setCellStyle(style);
				}
			}
		}
		
		return workbook;
	}
	
	/**
	 * EXCEL模板导出
	 * @param coloms
	 * @return
	 */
	public static XSSFWorkbook template(List<Map<String,String>> coloms) {
		XSSFWorkbook workbook = new XSSFWorkbook(); 
		XSSFSheet sheet = workbook.createSheet("通用模板");
		int rowIndex = 0;
		
		if(coloms!= null && coloms.size()>0){
			/** 创建单元格字体样式 */
			XSSFFont font = workbook.createFont();
			font.setFontName("宋体");// 宋体
			font.setFontHeightInPoints((short) 10);// 字号
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 加粗
			
			XSSFFont font_M = workbook.createFont();
			font_M.setFontName("宋体");// 宋体
			font_M.setFontHeightInPoints((short) 10);// 字号
			font_M.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 加粗
			font_M.setColor(HSSFColor.RED.index);

			/** 创建单元格样式 */
			//可选项样式
			XSSFCellStyle style = workbook.createCellStyle();
			style.setBottomBorderColor(HSSFColor.BLACK.index);// 设置边框颜色
			style.setBorderTop(HSSFCellStyle.BORDER_THIN);
			style.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			//style.setFillBackgroundColor(HSSFColor.YELLOW.index);// 设置背景色
			style.setFillForegroundColor(HSSFColor.YELLOW.index);
			style.setFont(font);// 将字体应用到样式
			
			//必输项样式
			XSSFCellStyle style_M = workbook.createCellStyle();
			style_M.setBottomBorderColor(HSSFColor.BLACK.index);// 设置边框颜色
			style_M.setBorderTop(HSSFCellStyle.BORDER_THIN);
			style_M.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style_M.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style_M.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			//style.setFillBackgroundColor(HSSFColor.YELLOW.index);// 设置背景色
			style_M.setFillForegroundColor(HSSFColor.YELLOW.index);
			style_M.setFont(font_M);// 将字体应用到样式
			
			XSSFRow rowCell = sheet.createRow(rowIndex++);
			XSSFComment comment = null;
			for(int i=0;i<coloms.size();i++){
				Map<String,String> colomMap = coloms.get(i);
				XSSFCell cell = rowCell.createCell(i);
				String isReqField = (String)colomMap.get("isReqField");
				String attrName = (String)colomMap.get("attrName");
				String attrDesc = (String)colomMap.get("attrDesc");
				
				if(isReqField != null && "1".equals(isReqField)){
					cell.setCellStyle(style_M);
					attrName += "*";
				}else{
					cell.setCellStyle(style);
				}
				//设值
				cell.setCellValue(attrName);
				//添加备注
				if(StringUtils.isNotEmpty(attrDesc)){
					//设置批注样式
					XSSFDrawing patr = sheet.createDrawingPatriarch();
					XSSFClientAnchor anchor = new XSSFClientAnchor(0,0,0,0,(short)3,3,(short)5,6);
					comment = patr.createCellComment(anchor);
					comment.setString(new XSSFRichTextString(attrDesc));
					comment.setAuthor("system");
					//设置单元格批注
					cell.setCellComment(comment);
				}
			}
		}
		
		return workbook;
	}
	
	/**
	 * 合并单元格
	 * @param sheet 要合并单元格的excel 的sheet
     * @param cellLine  要合并的列  
     * @param startRow  要合并列的开始行  
     * @param endRow    要合并列的结束行  
	 * @param workBook
	 */
	public static void addMergedRegion(HSSFSheet sheet, int cellLine,int startRow, int endRow, HSSFWorkbook workBook) {

		HSSFCellStyle style = workBook.createCellStyle(); // 样式对象

		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);// 垂直
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 水平
		// 获取第一行的数据,以便后面进行比较
		String s_will = sheet.getRow(startRow).getCell(cellLine)
				.getStringCellValue();

		int count = 0;
		boolean flag = false;
		for (int i = 1; i <= endRow; i++) {
			String s_current = sheet.getRow(i).getCell(0).getStringCellValue();
			if (s_will.equals(s_current)) {
				s_will = s_current;
				if (flag) {
					// 四个参数分别是：起始行，起始列，结束行，结束列   
					sheet.addMergedRegion(new CellRangeAddress(startRow - count, startRow, cellLine, cellLine));
					HSSFRow row = sheet.getRow(startRow - count);
					String cellValueTemp = sheet.getRow(startRow - count)
							.getCell(0).getStringCellValue();
					HSSFCell cell = row.createCell(0);
					cell.setCellValue(cellValueTemp); // 跨单元格显示的数据
					cell.setCellStyle(style); // 样式
					count = 0;
					flag = false;
				}
				startRow = i;
				count++;
			} else {
				flag = true;
				s_will = s_current;
			}
			// 由于上面循环中合并的单元放在有下一次相同单元格的时候做的，所以最后如果几行有相同单元格则要运行下面的合并单元格。
			if (i == endRow && count > 0) {
				sheet.addMergedRegion(new CellRangeAddress(endRow - count,
						endRow, cellLine, cellLine));
				String cellValueTemp = sheet.getRow(startRow - count)
						.getCell(0).getStringCellValue();
				HSSFRow row = sheet.getRow(startRow - count);
				HSSFCell cell = row.createCell(0);
				cell.setCellValue(cellValueTemp); // 跨单元格显示的数据
				cell.setCellStyle(style); // 样式
			}
		}
	}

	
	/**
	 * 创建样式
	 * @param workBook
	 * @return
	 */
	public static HSSFCellStyle getXSSFCellStyle(HSSFWorkbook workBook,int flag){
		//创建样式对象
		HSSFCellStyle style = workBook.createCellStyle();
		//标题样式
		if(flag == 1){
			HSSFFont font = workBook.createFont();          // 创建字体对象
			font.setFontHeightInPoints((short) 16);         // 设置字体大小
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);   // 设置粗体
			font.setFontName("宋体");                       // 设置为黑体字
			//将字体加入到样式对象中
			style.setFont(font);
			// 设置对齐方式
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER_SELECTION);   // 水平居中
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);  // 垂直居中
		}
		//抬头样式
		else if(flag == 2){
			// 设置字体
			HSSFFont font = workBook.createFont();          // 创建字体对象
			font.setFontHeightInPoints((short) 12);         // 设置字体大小
			font.setFontName("宋体");                       // 设置为黑体字
			//将字体加入到样式对象中
			style.setFont(font);
			// 设置对齐方式
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER_SELECTION);   // 水平居中
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);  // 垂直居中
			//设置背景色
			style.setFillPattern(XSSFCellStyle.FINE_DOTS);
			style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
			style.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
			// 设置边框
			style.setTopBorderColor(HSSFColor.BLACK.index);  
			style.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);       // 设置为红色
			style.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);    // 底部边框双线
			style.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);      // 左边边框
			style.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);     // 右边边框
			// 格式化日期
			style.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));
		}
		//副标题样式
		else if(flag == 3){
			// 设置字体
			HSSFFont font = workBook.createFont();          // 创建字体对象
			font.setFontHeightInPoints((short) 13);         // 设置字体大小
			font.setFontName("宋体");                       // 设置为黑体字
			// 设置对齐方式
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT);   // 水平居中
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);  // 垂直居中
			
		}
		//脚底样式
		else if(flag == 4){
			//设置字体
			HSSFFont font = workBook.createFont();          // 创建字体对象
			font.setFontHeightInPoints((short) 13);         // 设置字体大小
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);   // 设置粗体
			font.setFontName("宋体");                       // 设置为黑体字
			// 设置对齐方式
			style.setAlignment(HSSFCellStyle.ALIGN_RIGHT);   // 水平居中
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);  // 垂直居中
		}
		return style;
	}
	
	/**
	 * 创建抬头样式
	 * @param workBook
	 * @return
	 */
	public static XSSFCellStyle getXSSFCellStyle(XSSFWorkbook workBook,int flag){
		//创建样式对象
		XSSFCellStyle style = workBook.createCellStyle();
		//标题样式
		if(flag == 1){
			XSSFFont font = workBook.createFont();          // 创建字体对象
			font.setFontHeightInPoints((short) 16);         // 设置字体大小
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);   // 设置粗体
			font.setFontName("宋体");                       // 设置为黑体字
			//将字体加入到样式对象中
			style.setFont(font);
			// 设置对齐方式
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER_SELECTION);   // 水平居中
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);  // 垂直居中
		}
		//抬头样式
		else if(flag == 2){
			// 设置字体
			XSSFFont font = workBook.createFont();          // 创建字体对象
			font.setFontHeightInPoints((short) 12);         // 设置字体大小
			font.setFontName("宋体");                       // 设置为黑体字
			//将字体加入到样式对象中
			style.setFont(font);
			// 设置对齐方式
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER_SELECTION);   // 水平居中
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);  // 垂直居中
			//设置背景色
			style.setFillPattern(XSSFCellStyle.FINE_DOTS);
			style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
			style.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
			// 设置边框
			style.setTopBorderColor(HSSFColor.BLACK.index);  
			style.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);       // 设置为红色
			style.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);    // 底部边框双线
			style.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);      // 左边边框
			style.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);     // 右边边框
			// 格式化日期
			style.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));
		}
		//副标题样式
		else if(flag == 3){
			// 设置字体
			XSSFFont font = workBook.createFont();          // 创建字体对象
			font.setFontHeightInPoints((short) 13);         // 设置字体大小
			font.setFontName("宋体");                       // 设置为黑体字
			// 设置对齐方式
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT);   // 水平居中
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);  // 垂直居中
			
		}
		//脚底样式
		else if(flag == 4){
			//设置字体
			XSSFFont font = workBook.createFont();          // 创建字体对象
			font.setFontHeightInPoints((short) 13);         // 设置字体大小
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);   // 设置粗体
			font.setFontName("宋体");                       // 设置为黑体字
			// 设置对齐方式
			style.setAlignment(HSSFCellStyle.ALIGN_RIGHT);   // 水平居中
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);  // 垂直居中
		}else if(flag == 5){
			// 设置边框
			style.setTopBorderColor(HSSFColor.BLACK.index);  
			style.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);    // 底部边框双线
			style.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);      // 左边边框
			style.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);     // 右边边框
		}
		
		
		return style;
	}
	
	/**
	 * @param workBook
	 * @param singleSheetList
	 */
	public static void listConvertSingleSheetExcel(Workbook workBook, List<List<String>> singleSheetList, int rowIndex) {
		if(workBook instanceof XSSFWorkbook) {
			if(singleSheetList != null && singleSheetList.size() > 0) {
				XSSFSheet sheet = (XSSFSheet)workBook.getSheetAt(0);
				for(int i = 0; i < singleSheetList.size(); i++) {
					XSSFRow row = sheet.createRow(i + rowIndex);
					List<String> rowList = singleSheetList.get(i);
					for(int j = 0; j < rowList.size(); j++ ) {
						XSSFCell cell = row.createCell(j);
						cell.setCellValue(rowList.get(j));
					}
				}
			}
		}
		if(workBook instanceof HSSFWorkbook) {
			if(singleSheetList != null && singleSheetList.size() > 0) {
				HSSFSheet sheet = (HSSFSheet)workBook.getSheetAt(0);
				for(int i = 0; i < singleSheetList.size(); i++) {
					HSSFRow row = sheet.createRow(i + rowIndex);
					List<String> rowList = singleSheetList.get(i);
					for(int j = 0; j < rowList.size(); j++ ) {
						HSSFCell cell = row.createCell(j);
						cell.setCellValue(rowList.get(j));
					}
				}
			}
		}
	}
	
	
	
	/**
	 * 创建XSSFWorkbook
	 * @param allSheetList 所有表单数据List<List<List<String>> allSheetList，最内层list集合表示一行数据，第二层list集合表示每个表单数据集合
	 * @param titleList:所有表单的title集合List<String> tileList，从做到右的表单
	 * @param sheetColumnList：所有表单的列名集合List<List<String> sheetColumnList，从左到右的表单，内层list表示每个表单的列名集合
	 * @return
	 */
	public static XSSFWorkbook createXSSFWorkbook(List<List<List<String>>> allSheetList,
			List<List<String>> sheetColumnList){
		XSSFWorkbook workbook = setXSSFWorkbookColumn(sheetColumnList);
		for(int i=0;i<allSheetList.size();i++){
			XSSFSheet sheet = workbook.getSheetAt(i);
			List<List<String>> sheetDataList = allSheetList.get(i);//当前表单的数据
			for(int j=0;j<sheetDataList.size();j++){
				XSSFRow row = sheet.createRow(j+1);//创建当前行
				List<String> rowList = sheetDataList.get(j);//当前行数据
				for(int k=0;k<rowList.size();k++){
					XSSFCell cell = row.createCell(k);
					cell.setCellValue(rowList.get(k));
				}
			}
		}
		return workbook;
	}
	
	/**
	 * 设置列和title列
	 * @param titleList
	 * @param sheetColumnList
	 * @return
	 */
	public static XSSFWorkbook setXSSFWorkbookColumn(List<List<String>> sheetColumnList){
		//实例化Workbook对象
		XSSFWorkbook workBook = new XSSFWorkbook();
		for(int i=0;i<sheetColumnList.size();i++){//遍历title名
			//构建工作薄
			XSSFSheet sheet = workBook.createSheet();//创建表单
			int colLenth = sheetColumnList.get(i).size();//当前表单中列的数量
			//设置当前表单的列
			XSSFRow colRow = sheet.createRow(0);//创建标题行
			for(int j=0;j<colLenth;j++){
				sheet.setColumnWidth(j, 5000);
				XSSFCell colCell = colRow.createCell(j);
				colCell.setCellValue(sheetColumnList.get(i).get(j));
				colCell.setCellStyle(getXSSFCellStyle(workBook,1));
			}
		}
		return workBook;
	}
	
	public static String getFileName(){
		return SIMPLEDATEFORMAT.format(new Date())+Math.round((Math.random()*10))  + ".xlsx";
	}
	public static String getFileShortName(){
		return SIMPLEDATEFORMAT.format(new Date()) + Math.round((Math.random() * 10));
	}
	public static final DateFormat SIMPLEDATEFORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
	public static String getCurrentDate(){
		SimpleDateFormat sf  =new SimpleDateFormat("yyyy-MM-dd");
		return sf.format(new Date());
	}
}
