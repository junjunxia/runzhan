package com.runzhan.base.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.base.service.BaseService;

/**
 * BaseService实现
 * @author dk
 */

@Service
@Transactional
public abstract class BaseServiceImpl<T> implements BaseService<T> {

	protected abstract BaseDao<T> getMapper();


	public void save(T t) {
		getMapper().save(t);
	}


	public void delete(Serializable... ids) {
		for (Serializable id : ids) {
			getMapper().delete(id);
		}
	}


	public void update(T t) {
		getMapper().update(t);
	}


	@Transactional(readOnly = true)
	public List<T> query(Map<String, Object> paraMap) {
		return getMapper().query(paraMap);
	}


	@Transactional(readOnly = true)
	public int getCount(Map<String, Object> paraMap) {
		return getMapper().getCount(paraMap);
	}


	@Transactional(readOnly = true)
	public T find(Serializable id) {
		return getMapper().find(id);
	}


	@Transactional(readOnly = true)
	public List<T> queryAll() {
		return getMapper().queryAll();
	}

}
