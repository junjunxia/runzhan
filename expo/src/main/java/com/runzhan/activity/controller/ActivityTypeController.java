package com.runzhan.activity.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.runzhan.activity.po.ActivityType;
import com.runzhan.activity.service.ActivityTypeService;
import com.runzhan.expo.controller.BaseController;

@RestController
@RequestMapping("/activityType")
public class ActivityTypeController extends BaseController {
	Logger logger = Logger.getLogger(this.getClass());
	@Autowired
	private ActivityTypeService activityService;
	@RequestMapping(value = "/list")
	@ResponseBody
	public List<ActivityType> list() {
		return activityService.list();
	}
	

}
