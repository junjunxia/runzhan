package com.runzhan.activity.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.runzhan.activity.po.Activity;
import com.runzhan.activity.po.ActivityOrg;
import com.runzhan.activity.service.ActivityOutService;
import com.runzhan.activity.service.ActivityService;
import com.runzhan.activity.vo.ActivityManageVo;
import com.runzhan.activity.vo.ActivityOrgVo;
import com.runzhan.activity.vo.AddActivityOutVo;
import com.runzhan.activity.vo.DeleteActivityVo;
import com.runzhan.expert.po.Expert;
import com.runzhan.expert.service.ExpertService;
import com.runzhan.expo.controller.BaseController;

@RestController
@RequestMapping("/activity")
public class ActivityController extends BaseController {
	Logger logger = Logger.getLogger(this.getClass());
	@Autowired
	private ActivityService activityService;
	@Autowired
	private ExpertService expertService;
	@Autowired
	private ActivityOutService activityOutService;
	
	@RequestMapping(value = "/list")
	public ModelAndView list(String activityClassId) {
		ModelAndView mv = new ModelAndView();
		String pjax = request.getHeader("X-PJAX");
		
		if(StringUtils.isNotEmpty(activityClassId)){
			mv.addObject("activityClassId", activityClassId);
		}
		if (StringUtils.isNotBlank(pjax) && "true".equals(pjax)) {
			mv.setViewName("activity/activity-list");
		} else {
			mv.addObject("include", "activity/activity-list.jsp");
			mv.setViewName("index");
		}
		
		return mv;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> delete(Activity ac) {
		
		Map<String, Object> jsonMap = null;
		try {
			jsonMap  =activityService.delete(ac);
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
		}
		return jsonMap;
	}
	
	
	@RequestMapping(value = "/editUI")
	public ModelAndView editUI(Long id) {
		ModelAndView mv = new ModelAndView();
		String pjax = request.getHeader("X-PJAX");
		if (StringUtils.isNotBlank(pjax) && "true".equals(pjax)) {
			mv.setViewName("activity/editActivity-wizard");
		} else {
			mv.addObject("include", "activity/editActivity-wizard.jsp");
			mv.setViewName("index");
		}
		if(null!=id){
			mv.addObject("activity", activityService.get(id));
		}
		return mv;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> save(Activity user) {
		
		Map<String, Object> jsonMap = null;
		try {
			if(null==user.getId()){
				user.setCreateId(loginUser.getUserId());
				user.setCreateName(loginUser.getLoginName());
			}
			jsonMap  =activityService.saveActivity(user);
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
			logger.error("保存活动失败！", e);
		}
		return jsonMap;
	}
	
	/** 活动管理页面
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/toActivityManagerList")
	public ModelAndView toActivityManagerList(Expert user,String activityName,String sponsor) {
		ModelAndView mv = new ModelAndView();
       String pjax = request.getHeader("X-PJAX");
		
		if (StringUtils.isNotBlank(pjax) && "true".equals(pjax)) {
			mv.setViewName("expert/activityManager-list");
		} else {
			mv.addObject("include", "expert/activityManager-list.jsp");
			mv.setViewName("index");
		}
		if (null == user) {
			logger.error("Expert：参数为空！");
			mv.setViewName("404");
			return mv;
		}
		
		int expertId = user.getId();
		
		Expert expert = expertService.get(expertId);
		mv.addObject("expert", expert);
		
		return mv;
	}
	
	
	@RequestMapping(value = "/toActivityManagerListAjax", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> toActivityManagerListAjax(Expert user,String activityName,String sponsor) {
		
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		try {
			int expertId = user.getId();
			List<ActivityManageVo> list = activityService.getActivityList(expertId, activityName);
			List<ActivityManageVo> list2 = activityOutService.getActivityOutList(expertId, activityName);
			if (null != list) {
				list.addAll(list2);
			} else {
				list = list2;
			}
			
			List<ActivityManageVo> activityManageList = new ArrayList<ActivityManageVo>();
			if (!CollectionUtils.isEmpty(list)) {
				List<Long> activityIdList = new ArrayList<Long>(list.size());
				Map<Long, ActivityManageVo> result = new HashMap<Long, ActivityManageVo>();
				for (ActivityManageVo vo : list) {
					Long acId = vo.getId();
					activityIdList.add(acId);
					result.put(acId, vo);
				}
				
				List<ActivityOrgVo> activityOrgVoList = activityService.getActivityOrgVoByActivityId(activityIdList, sponsor);
				for (ActivityOrgVo vo : activityOrgVoList) {
					if (result.containsKey(vo.getActivityId())) {
						ActivityManageVo ac = result.get(vo.getActivityId());
						ac.setSponsor(vo.getSponsor());
						ac.setOrganizer(vo.getOrganizer());
						activityManageList.add(ac);
					}
				}
			}
//			jsonMap.put("activityName", activityName);
//			jsonMap.put("sponsor", sponsor);
			jsonMap.put("activityManageList", activityManageList);
			jsonMap.put("flag", "1");
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
			logger.error("保存活动失败！", e);
		}
		return jsonMap;
	}
	
	@RequestMapping(value = "/saveActivityOut", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> saveActivityOut(AddActivityOutVo addActivityOutVo) {
		
		Map<String, Object> jsonMap = null;
		try {
			jsonMap  = activityService.saveActivityOut(addActivityOutVo);
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
			logger.error("保存活动失败！", e);
		}
		return jsonMap;
	}
	
	@RequestMapping(value = "/orglist")
	@ResponseBody
	public List<ActivityOrg> orglist(Activity ac) {
		return activityService.orglist(ac);
	}
	
	
	@RequestMapping(value = "/deleteActivity", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> deleteActivity(DeleteActivityVo vo) {
		Map<String, Object> jsonMap = null;
		try {
			jsonMap  = activityService.deleteActivity(vo);
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
			logger.error("保存活动失败！", e);
		}
		return jsonMap;
	}
	
}
