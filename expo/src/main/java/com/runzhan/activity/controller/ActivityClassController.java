package com.runzhan.activity.controller;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.runzhan.activity.po.ActivityClass;
import com.runzhan.activity.service.ActivityClassService;
import com.runzhan.expo.controller.BaseController;

@RestController
@RequestMapping("/activityClass")
public class ActivityClassController extends BaseController {
	Logger logger = Logger.getLogger(this.getClass());
	@Autowired
	private ActivityClassService activityService;
	@RequestMapping(value = "/list")
	public ModelAndView list(ActivityClass ac) {
		ModelAndView mv = new ModelAndView();
		String pjax = request.getHeader("X-PJAX");
		
		if (StringUtils.isNotBlank(pjax) && "true".equals(pjax)) {
			mv.setViewName("activity/activityClass-list");
		} else {
			mv.addObject("include", "activity/activityClass-list.jsp");
			mv.setViewName("index");
		}
		mv.addObject("activityClass", activityService.queryAll2(ac));
		return mv;
	}
	
	@RequestMapping(value = "/listAjax")
	@ResponseBody
	public List<ActivityClass> listAjax() {
		return activityService.list();
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> save(ActivityClass user) {
		
		Map<String, Object> jsonMap = null;
		try {
			if(null==user.getId()){
				user.setCreateId(loginUser.getUserId());
				user.setCreateName(loginUser.getLoginName());
			}
			jsonMap  =activityService.saveActivityClass(user);
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
			logger.error("保存活动分类失败！", e);
		}
		return jsonMap;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> delete(ActivityClass ac) {
		
		Map<String, Object> jsonMap = null;
		try {
			jsonMap  =activityService.delete(ac);
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
		}
		return jsonMap;
	}
}
