package com.runzhan.common.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.reflect.MethodUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.runzhan.base.common.spring.ApplicationContextHolder;
import com.runzhan.base.util.JacksonUtils;
import com.runzhan.common.exception.RunzhanException;

/**
 * @ClassName: BeanMethodInvocation
 * @Description: 
 */
@Service
public class BeanMethodInvocation {
    private static final Logger logger = Logger.getLogger(BeanMethodInvocation.class);
    
    public static Object execute(String targetBeanName, String methodName, Object params) {
        if (StringUtils.isEmpty(targetBeanName)) {
            logger.error("target bean name must not be null or empty!");
            throw new RunzhanException("target.bean.name.must.not.be.null.or.empty!");
        }
        
        Object targetBean = ApplicationContextHolder.getBean(targetBeanName);
        if (null == targetBean) {
            logger.error("target bean is null with bean name:[{}]" + targetBeanName);
            throw new RunzhanException("target.bean.is.null.with.bean.name:[{}]" + new String[]{targetBeanName});
        }
        
        Class<?> paramsClass = null;
        if (params instanceof Map) paramsClass = Map.class;
        else if (params instanceof List) paramsClass = List.class;
        else if (params instanceof Set) paramsClass = Set.class;
        else paramsClass = params.getClass();

        Method method = MethodUtils.getAccessibleMethod(targetBean.getClass(), methodName, paramsClass);
        if (method == null) {
            logger.error("can't find method: [{}] on target: [{}], and params is: {}" + new Object[]{methodName, targetBean.getClass(), JacksonUtils.object2json(params)});
            throw new RunzhanException("can't find method: [{"+ methodName+"}] on bean: [{"+ targetBean.getClass() +"}]");
        }
        Object methodResult = null;
        String errorMsg = String.format("invoke method : target: {%s}, method: {%s}", targetBean, methodName);
        try {
            methodResult = method.invoke(targetBean, params);
        } catch (IllegalArgumentException e) {
            logger.error(errorMsg, e);
            throw new RunzhanException(e.getLocalizedMessage());
        } catch (IllegalAccessException e) {
            logger.error(errorMsg, e);
            throw new RunzhanException(e.getLocalizedMessage());
        } catch (InvocationTargetException e) {
            logger.error(errorMsg, e);
            throw new RunzhanException(e.getLocalizedMessage());
        } catch (Throwable tx) {
            logger.error(errorMsg, tx);
            throw new RunzhanException(tx.getLocalizedMessage());
        }
        return methodResult;
    }
    
}
