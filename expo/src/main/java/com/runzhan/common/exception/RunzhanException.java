package com.runzhan.common.exception;

/**
 * RunzhanException RuntimeException
 * 
 * @author  dk
 * @since   Admui v1.0.0
 */
public class RunzhanException extends RuntimeException {

	private static final long serialVersionUID = 5447628653259133057L;

	public RunzhanException() {
		super();
	}

	public RunzhanException(String message) {
		super(message);
	}

	public RunzhanException(String message, Throwable cause) {
		super(message, cause);
	}

	public RunzhanException(Throwable cause) {
		super(cause);
	}

}
