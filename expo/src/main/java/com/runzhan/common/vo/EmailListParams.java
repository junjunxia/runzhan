package com.runzhan.common.vo;

import java.util.List;

public class EmailListParams {

	private List<Long> emailIdList ;

	public List<Long> getEmailIdList() {
		return emailIdList;
	}

	public void setEmailIdList(List<Long> emailIdList) {
		this.emailIdList = emailIdList;
	}

	
	
	
}
