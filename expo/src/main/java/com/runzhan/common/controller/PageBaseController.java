package com.runzhan.common.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.runzhan.base.util.JacksonUtils;
import com.runzhan.common.service.BeanMethodInvocation;

/**
 * @ClassName: PageBaseController
 * @Description: 
 */
@Controller
public class PageBaseController {
	private static final Logger logger = Logger.getLogger(PageBaseController.class);
	
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/process", method = RequestMethod.POST)
    @ResponseBody
    public String page(HttpServletRequest request) {
        Map<String, String[]> paramsMap = request.getParameterMap();
        logger.debug("received a request, beanName is : [{}], methodName is : [{}], other params is {}" 
            + new Object[]{paramsMap.get("bean"), paramsMap.get("method"), JacksonUtils.object2json(paramsMap)});
        
        String jsonResult = StringUtils.EMPTY;
        
        Map<String, Object> map = new HashMap<String, Object>();
        Set<Entry<String, String[]>> entrySet = paramsMap.entrySet();
        for (Entry<String, String[]> entry : entrySet) {
            map.put(entry.getKey(), entry.getValue()[0]);
        }
        
       String beanName = (String) map.get("bean");
       if (StringUtils.isEmpty(beanName)) {
           logger.error("beanName must not be null or empty");
           return jsonResult;
       }
       
       String methodName = (String) map.get("method");
       if (StringUtils.isEmpty(methodName)) {
           logger.error("methodName must not be null or empty");
           return jsonResult;
       }
       
        try {
            jsonResult = (String) BeanMethodInvocation.execute(beanName + "Service", methodName, map);
        } catch (Throwable tx) {
            logger.error("invoke method : targetBean: {}, method: {} , with error: \n\tparams is : {}; \n\texception is [{}]",tx);
        }

        return jsonResult;
    }
    
}