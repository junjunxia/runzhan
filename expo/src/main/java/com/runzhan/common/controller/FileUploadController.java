package com.runzhan.common.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.runzhan.base.util.CompressTools;
import com.runzhan.base.util.FileTools;
import com.runzhan.expo.controller.BaseController;
import com.runzhan.upload.IUploader;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 知识产权声明:本文件自创建起,其内容的知识产权即归属于原作者,任何他人不可擅自复制或模仿.
 * 创建者: wu   创建时间: 2016/11/25
 * 类说明:
 * 更新记录：
 */
@Controller
@RequestMapping("/file")
public class FileUploadController extends BaseController  {

    //这个是日志用的对象,我用的是logBack
	Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private IUploader uploader;

    @ResponseBody
    @RequestMapping(value = "/portrait", method = {RequestMethod.POST})
    public Map<String,Object> upload(HttpServletRequest request) throws Exception {
    	
    	Map<String,Object> result = new HashMap<String,Object>();
        Integer x = Integer.parseInt(request.getParameter("x"));
        Integer y = Integer.parseInt(request.getParameter("y"));
        Integer w = Integer.parseInt(request.getParameter("w"));
        Integer h = Integer.parseInt(request.getParameter("h"));
        String scaleWidthString = request.getParameter("sw");
        int swIndex = scaleWidthString.indexOf("px");
        Integer sw = Integer.parseInt(scaleWidthString.substring(0, swIndex));
        String scaleHeightString = request.getParameter("sh");
        int shIndex = scaleHeightString.indexOf("px");
        Integer sh = Integer.parseInt(scaleHeightString.substring(0, shIndex));


        //获取用户ID用于指向对应文件夹
        long userID = loginUser.getUserId();
        //获取文件路径
        String filePath = FileTools.getPortraitPath(userID);

        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
                request.getSession().getServletContext());

        String path;
        //检查form中是否有enctype="multipart/form-data"
        if (multipartResolver.isMultipart(request)) {
            //将request变成多部分request
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            //获取multiRequest 中所有的文件名
            Iterator iterator = multiRequest.getFileNames();
            while (iterator.hasNext()) {
                //一次遍历所有文件
                MultipartFile multipartFile = multiRequest.getFile(iterator.next().toString());
                if (multipartFile != null) {
                    String[] allowSuffix = {".jpg",".JPG",".png",".PNG"};
                    if (!FileTools.checkSuffix(multipartFile.getOriginalFilename(), allowSuffix)) {
                        throw new RuntimeException("文件后缀名不符合要求！");
                    }
                    path = filePath + FileTools.getPortraitFileName(multipartFile.getOriginalFilename());
                    //存入硬盘
                    multipartFile.transferTo(new File(path));
                    //图片截取
                    if (FileTools.imgCut(path, x, y, w, h, sw, sh)) {
                        CompressTools compressTools = new CompressTools();
                        if (compressTools.simpleCompress(new File(path))) {
                        	result.put("flag", 1);
//                        	result.put("url", FileTools.filePathToSRC(path, FileTools.IMG));
                        	result.put("url", uploader.upload(new FileInputStream(path), 
                        			"runzhan", multipartFile.getOriginalFilename()));
                        	result.put("msg", "上传成功");
                        	
                        	try {
								new File(path).deleteOnExit();
							} catch (Exception e) {
								logger.error("删除图片异常", e);
							}
                            return result;
                        } else {
                        	result.put("flag",0);
                        	result.put("msg", "图片压缩失败！请重新上传");
                        	return result;
//                            return JsonResult.error("图片压缩失败！请重新上传！");
                        }
                    } else {
//                        return JsonResult.error("图片压缩失败！请重新上传！");
                        result.put("flag",0);
                    	result.put("msg", "图片压缩失败！请重新上传");
                    	return result;
                    }
                }
            }
        }
//        return JsonResult.error("图片获取失败！请重新上传！");
        result.put("flag",0);
    	result.put("msg", "图片获取失败！请重新上传！");
    	return result;
    }
}
