package com.runzhan.expert.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.runzhan.expo.util.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.runzhan.activity.po.Activity;
import com.runzhan.activity.po.ExpertActivityRelation;
import com.runzhan.activity.service.ActivityService;
import com.runzhan.activity.vo.ActivityHistoryVo;
import com.runzhan.base.util.ExcelUtil;
import com.runzhan.expert.po.Expert;
import com.runzhan.expert.po.ExpertLinkMan;
import com.runzhan.expert.po.JobDomain;
import com.runzhan.expert.service.ExpertLinkManService;
import com.runzhan.expert.service.ExpertService;
import com.runzhan.expert.vo.AddExpertParamsVo;
import com.runzhan.expert.vo.ExcelRowToExpertVo;
import com.runzhan.expert.vo.ExpertExcelVo;
import com.runzhan.expert.vo.ExpertManageVo;
import com.runzhan.expo.controller.BaseController;

@RestController
@RequestMapping("/expert")
public class ExpertController extends BaseController {
	Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private ExpertService expertService;
	@Autowired
	private ExpertLinkManService expertLinkManService;
	@Autowired
	private ActivityService activityService;

	@RequestMapping(value = "/list")
	public ModelAndView list() {
		ModelAndView mv = new ModelAndView();
		String pjax = request.getHeader("X-PJAX");

		if (StringUtils.isNotBlank(pjax) && "true".equals(pjax)) {
			mv.setViewName("expert/expert-list");
		} else {
			mv.addObject("include", "expert/expert-list.jsp");
			mv.setViewName("index");
		}

		return mv;
	}

	@RequestMapping(value = "/add")
	public ModelAndView add() {
		ModelAndView mv = new ModelAndView();
		String pjax = request.getHeader("X-PJAX");

		if (StringUtils.isNotBlank(pjax) && "true".equals(pjax)) {
			mv.setViewName("expert/addExpert-wizard");
		} else {
			mv.addObject("include", "expert/addExpert-wizard.jsp");
			mv.setViewName("index");
		}

		return mv;
	}

	@RequestMapping(value = "/editUI")
	public ModelAndView editUI(Expert user) {
		ModelAndView mv = new ModelAndView();
		String pjax = request.getHeader("X-PJAX");

		if (StringUtils.isNotBlank(pjax) && "true".equals(pjax)) {
			mv.setViewName("expert/editExpert-wizard");
		} else {
			mv.addObject("include", "expert/editExpert-wizard.jsp");
			mv.setViewName("index");
		}
		mv.addObject("expert", expertService.get(user.getId()));
		return mv;
	}
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> save(Expert user) {
		
		Map<String, Object> jsonMap = null;
		try {
			user.setCreateId(loginUser.getUserId());
			user.setCreateName(loginUser.getLoginName());
			jsonMap  =expertService.saveExpert(user);
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
			logger.error("保存用户失败！", e);
		}
		return jsonMap;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> delete(Expert user) {
		
		Map<String, Object> jsonMap = null;
		try {
			jsonMap  =expertService.delete(user);
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
		}
		return jsonMap;
	}
	
	/**
	 * 批量删除专家
	 * @return
	 */
	@RequestMapping(value = "/batchDelete", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> batchDelete(String ids) {
		String[] deleteIDS = ids.split(","); 
		List<Integer> deletIDSList = new ArrayList();
		for(int i=0;i<deleteIDS.length;i++)
		{
			deletIDSList.add(Integer.parseInt(deleteIDS[i]));
		}
		Map<String, Object> jsonMap = null;
		try {
			jsonMap  =expertService.batchDelete(deletIDSList);
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
		}
		return jsonMap;
	}
	
	@RequestMapping(value = "/saveExpertLinkMan", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> saveExpertLinkMan(ExpertLinkMan user) {
		
		Map<String, Object> jsonMap = null;
		try {
			jsonMap  =expertService.saveExpertLinkMan(user);
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
			logger.error("保存用户联系人失败！", e);
		}
		return jsonMap;
	}
	
	/** 专家联系人页面
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/toExpertLinkManList")
	public ModelAndView toExpertLinkList(Expert user) {
		ModelAndView mv = new ModelAndView();
		 String pjax = request.getHeader("X-PJAX");
			
			if (StringUtils.isNotBlank(pjax) && "true".equals(pjax)) {
				mv.setViewName("expert/expertLinkMan-list");
			} else {
				mv.addObject("include", "expert/expertLinkMan-list.jsp");
				mv.setViewName("index");
			}
		if (null == user) {
			logger.error("Expert：参数为空！");
			mv.setViewName("404");
			return mv;
		}
		
		int expertId = user.getId();
//		String expertLinkManName = user.getName();
		
		Expert expert = expertService.get(expertId);
//		List<ExpertLinkMan> expertLinkManList = expertLinkManService.getByParams(expertId, expertLinkManName);
		
		mv.addObject("expert", expert);
//		mv.addObject("expertLinkManList", expertLinkManList);
		
		return mv;
	}
	
	/** 删除专家联系人
	 * @param toExpertLinkManListAjax
	 * @return
	 */
	@RequestMapping(value = "/toExpertLinkManListAjax")
	@ResponseBody
	public Map<String, Object> toExpertLinkManListAjax(Expert user) {
		
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		try {
			int expertId = user.getId();
			String expertLinkManName = user.getName();
			List<ExpertLinkMan> expertLinkManList = expertLinkManService.getByParams(expertId, expertLinkManName);
			
			jsonMap.put("expertLinkManList", expertLinkManList);
			jsonMap.put("flag", "1");
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
		}
		return jsonMap;
	}
	
	/** 删除专家联系人
	 * @param expertLinkManId
	 * @return
	 */
	@RequestMapping(value = "/deleteExpertLinkMan")
	@ResponseBody
	public Map<String, Object> deleteExpertLinkMan(Integer expertLinkManId) {
		
		Map<String, Object> jsonMap = null;
		try {
			jsonMap  = expertLinkManService.delete(expertLinkManId);
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
		}
		return jsonMap;
	}
	
	
	/** 专家详情
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/detail")
	public ModelAndView detail(Expert user) {
		ModelAndView mv = new ModelAndView();
		 String pjax = request.getHeader("X-PJAX");
			
			if (StringUtils.isNotBlank(pjax) && "true".equals(pjax)) {
				mv.setViewName("expert/expertDetail");
			} else {
				mv.addObject("include", "expert/expertDetail.jsp");
				mv.setViewName("index");
			}
		int expertId = user.getId();
		Expert expert = expertService.get(expertId);
		List<ExpertLinkMan> expertLinkManList = expertLinkManService.getByParams(expertId,null);
		//	专家活动
		List<ActivityHistoryVo> activityHistoryList = activityService.getActivityHistoryList(expertId);
		
		mv.addObject("expert", expert);
		mv.addObject("expertLinkManList", expertLinkManList);
		mv.addObject("activityHistoryList", activityHistoryList);
		return mv;
	}
	
	/** 专家管理页面
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/toExpertManagerList")
	public ModelAndView toExpertManagerList(Long activityId, String expertName) {
		ModelAndView mv = new ModelAndView();
		 String pjax = request.getHeader("X-PJAX");
			
		if (StringUtils.isNotBlank(pjax) && "true".equals(pjax)) {
			mv.setViewName("expert/expertManager-list");
		} else {
			mv.addObject("include", "expert/expertManager-list.jsp");
			mv.setViewName("index");
		}
		
		Activity activity = activityService.get(activityId);
		int cnt = activityService.getExpertCount(activityId);
		activity.setExpertCount(cnt);
//		List<ExpertManageVo> expertManageList = expertService.getExpertManageList(activityId, expertName);
		
		mv.addObject("activity", activity);
		mv.addObject("activityId", activityId);
		mv.addObject("expertName", expertName);
//		mv.addObject("expertManageList", expertManageList);
		return mv;
	}
	
	
	@RequestMapping(value = "/toExpertManagerListAjax")
	@ResponseBody
	public Map<String, Object> toExpertManagerListAjax(Long activityId, String expertName) {
		
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		try {
			List<ExpertManageVo> expertManageList = expertService.getExpertManageList(activityId, expertName);
			jsonMap.put("expertManageList", expertManageList);
			jsonMap.put("flag", "1");
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
		}
		return jsonMap;
	}
	
	/** 更新专家主题
	 * @param updateExpertTopic
	 * @return
	 */
	@RequestMapping(value = "/updateExpertTopic")
	@ResponseBody
	public Map<String, Object> updateExpertTopic(ExpertActivityRelation rl) {
		
		Map<String, Object> jsonMap = null;
		try {
			jsonMap  = activityService.updateExpertTopic(rl);
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
		}
		return jsonMap;
	}
	
	
	
	/** 添加专家页面
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/toAddExpert")
	public ModelAndView toAddExpert(AddExpertParamsVo params) {
		ModelAndView mv = new ModelAndView();
		 String pjax = request.getHeader("X-PJAX");
			
		if (StringUtils.isNotBlank(pjax) && "true".equals(pjax)) {
			mv.setViewName("expert/addExpert");
		} else {
			mv.addObject("include", "expert/addExpert.jsp");
			mv.setViewName("index");
		}
//		List<Expert> expertList = expertService.getExpertListBy(params);
//		mv.addObject("expertList", expertList);
		mv.addObject("activityInfo", params);
		return mv;
	}
	
	
	@RequestMapping(value = "/toAddExpertListAjax")
	@ResponseBody
	public Map<String, Object> toAddExpertListAjax(AddExpertParamsVo params) {
		
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		try {
			List<Expert> expertList = expertService.getExpertListBy(params);
			jsonMap.put("expertList", expertList);
			jsonMap.put("flag", "1");
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
		}
		return jsonMap;
	}
	
	/** 保存邀请的专家
	 * @param saveInviterExpert
	 * @return
	 */
	@RequestMapping(value = "/saveInviterExpert")
	@ResponseBody
	public Map<String, Object> saveInviterExpert(AddExpertParamsVo params) {
		
		Map<String, Object> jsonMap = null;
		try {
			jsonMap  = expertService.saveInviterExpert(params, params.getExpertIdList());
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
		}
		return jsonMap;
	}
	
	@RequestMapping(value = "/domainList")
	@ResponseBody
	public List<JobDomain> domainList() {
		return expertService.domainList();
	}
	
	@RequestMapping(value = "/domainSerach")
	@ResponseBody
	public List<JobDomain> domainSerach(JobDomain domainName) {
		return expertService.domainSerach(domainName);
	}
	
	

	/**
	 * 
	 * @return
	 * @Description:结果
	 */
	@RequestMapping(value = "/importExpert")
	public ModelAndView impInfo(
			HttpServletRequest request,
			@RequestParam(value = "upFile", required = true) MultipartFile upFile) {
	
		Map<String, Object> model = new HashMap<String, Object>();
		List<ExpertExcelVo> toImportData = new ArrayList<ExpertExcelVo>();
		Map<String, List<ExpertExcelVo>> resultData=new HashMap<String, List<ExpertExcelVo>>();
		String fileName = upFile.getOriginalFilename();
		String result = "1";
		InputStream in = null;
		Long userId = loginUser.getUserId();
		String loginName = loginUser.getLoginName();
		try {
			
			in = upFile.getInputStream();
			// excel转为bean
			ExcelRowToExpertVo excelBlack = new ExcelRowToExpertVo();
			toImportData = ExcelUtil.readExcel(in, fileName, excelBlack);
			List<String> excelErrorMsgs = excelBlack.getExcelErrorMsgs();// 获取校验不通过数据
			if (CollectionUtils.isNotEmpty(excelErrorMsgs)) {
				model.put("result", "0");
				model.put("excelErrorMsgs", excelErrorMsgs);
				logger.debug("结束结果导入：/importExpert|导入失败");
				return new ModelAndView("/expert/importExpertReuslt", model);
			}
			else
			{
				if (null != toImportData && !toImportData.isEmpty())
				{
					for(int i=0;i<toImportData.size();i++)
					{
						Expert exper = toImportData.get(i).getExpert();
						exper.setCreateId(userId);
						exper.setCreateName(loginName);
						toImportData.get(i).setExpert(exper);

					}
					resultData = expertService
							.batchAddExpert(toImportData);
				}
			}
		} catch (Exception e) {
			result = "0";
			logger.error("信审管理", e);
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
					logger.error("流关闭失败", e);
				}
			}
		}
		model.put("result", result);
		model.put("successData", resultData.get("successData"));
		model.put("failData", resultData.get("failData"));
		return new ModelAndView("/expert/importExpertReuslt", model);
	}

	@RequestMapping(value = "/download")
	public void download(HttpServletRequest request, HttpServletResponse response) {
		String ids = request.getParameter("ids");
		Map<String,Object> params = new HashMap<String, Object>();
		if(!StringUtil.isEmpty(ids)){
			String[] idArray = ids.split(",");
			params.put("ids",Arrays.asList(idArray));
		}

		String name = request.getParameter("name");
		String unitname = request.getParameter("unitname");
		String socialCircle = request.getParameter("socialCircle");
		String domainName = request.getParameter("domainName");

		if(!StringUtil.isEmpty(name)){
			params.put("name",name);
		}
		if(!StringUtil.isEmpty(unitname)){
			params.put("unitname",unitname);
		}
		if(!StringUtil.isEmpty(socialCircle)){
			params.put("socialCircle",socialCircle);
		}
		if(!StringUtil.isEmpty(domainName)){
			params.put("domainName",domainName);
		}

		String title = "专家列表";
		List<String> subtitle = null;
		List<List<String>> datas = new ArrayList<List<String>>();
		List<String> columnNameList = null;

		String[] headersFrist = { "第一纬度", "", "", "", "","","","","","","","","","",
				"第二纬度","","","","","","",
				"第三纬度","","","","","","","",""};

		String[] headersSec = { "序号", "专家姓名", "单位名称", "部门", "职务","手机号码","联系电话","邮箱","传真",
				"单位地址","邮编","所属领域","社会头衔","履历","活动名称","活动时间","演讲题目",
				"主办单位","承办单位","邀请人","备注","联系人","单位名称","部门","职务","手机号码","联系电话","传真","单位地址","邮编"};

		columnNameList = Arrays.asList(headersSec);
		subtitle = Arrays.asList(headersFrist);


		List<Map<String,Object>> results = expertService.quertExport(params);
		if(results!=null){
			for(Map<String,Object> map:results){
				List<String> row = new ArrayList<String>();

				row.add(map.get("id")==null ? "" : map.get("id").toString());
				row.add(map.get("name")==null ? "" : map.get("name").toString());
				row.add(map.get("unitname")==null ? "" : map.get("unitname").toString());
				row.add(map.get("department")==null ? "" : map.get("department").toString());
				row.add(map.get("title")==null ? "" : map.get("title").toString());
				row.add(map.get("mobile")==null ? "" : map.get("mobile").toString());
				row.add(map.get("phone")==null ? "" : map.get("phone").toString());
				row.add(map.get("email")==null ? "" : map.get("email").toString());
				row.add(map.get("fax")==null ? "" : map.get("fax").toString());
				row.add(map.get("unitAddr")==null ? "" : map.get("unitAddr").toString());
				row.add(map.get("zipCode")==null ? "" : map.get("zipCode").toString());
				row.add(map.get("domainName")==null ? "" : map.get("domainName").toString());
				row.add(map.get("socialTitle")==null ? "" : map.get("socialTitle").toString());
				row.add(map.get("resume")==null ? "" : map.get("resume").toString());
				row.add(map.get("activityTitle")==null ? "" : map.get("activityTitle").toString());
				row.add(map.get("beginTime")==null ? "" : map.get("beginTime").toString());
				row.add(map.get("actopic")==null ? "" : map.get("actopic").toString());
				row.add(map.get("fanme")==null ? "" : map.get("fanme").toString());
				row.add(map.get("sname")==null ? "" : map.get("sname").toString());
				row.add(map.get("inviter")==null ? "" : map.get("inviter").toString());
				row.add(map.get("remarks")==null ? "" : map.get("remarks").toString());
				row.add(map.get("linkname")==null ? "" : map.get("linkname").toString());
				row.add(map.get("linkunitname")==null ? "" : map.get("linkunitname").toString());
				row.add(map.get("linkdepartment")==null ? "" : map.get("linkdepartment").toString());
				row.add(map.get("linktitle")==null ? "" : map.get("linktitle").toString());
				row.add(map.get("linkmoblie")==null ? "" : map.get("linkmoblie").toString());
				row.add(map.get("linkphone")==null ? "" : map.get("linkphone").toString());
				row.add(map.get("linkfax")==null ? "" : map.get("linkfax").toString());
				row.add(map.get("linkaddr")==null ? "" : map.get("linkaddr").toString());
				row.add(map.get("linkemail")==null ? "" : map.get("linkemail").toString());

				datas.add(row);
			}
		}

		baseDownload(request,response,title,subtitle,datas,columnNameList);


	}
}
