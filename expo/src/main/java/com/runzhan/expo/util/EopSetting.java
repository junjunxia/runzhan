package com.runzhan.expo.util;

import java.io.InputStream;
import java.util.Properties;

import com.qiniu.common.Zone;

public class EopSetting {
	
	public static String VERSION="";
	/*
	 * EOP虚拟目录
	 */
	//数据库类型
	public static String DBTYPE ="1" ; //1是mysql 2为oracle 3为sqlserver
	
	public static String  FILE_STORE_PREFIX ="fs:"; //本地文件存储前缀
	
	public static String INSTALL_LOCK ="NO"; //是否已经安装
	
	public static boolean IS_DEMO_SITE=false; //是否是演示站
	
	public static String PRODUCT="b2c";
	
	public static final String DEMO_SITE_TIP="为保证示例站点完整性，禁用此功能，请下载war包试用完整功能。";
	
	public static String  THUMBNAILCREATOR ="javaimageio";
	
	public static boolean USE_QINIU = false;									// 是否使用七牛
	public static String QINIU_AK;												// 七牛 AccessKey
	public static String QINIU_SK;												// 七牛SecretKey
	public static Zone   QINIU_ZONE;											// 七牛Zone
	public static String QINIU_BUCKET;											// 七牛Bucket
	public static String QINIU_BASEURL = "http://om6sbk5jb.btk.clouddn.com";	// 七牛BaseUrl
	
	/*
	 * 从配置文件中读取相关配置<br/>
	 * 如果没有相关配置则使用默认
	 */
 
	 static{
		 try {
			init("");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
	
	
	public static void init()  throws Exception{

//		String path  = com.runzhan.common.util.StringUtil.getRootPath();
	 
		 InputStream input = null;
	        try {
	        	/**
	             /dbconfig.properties  绝对路径, 取到的文件是classpath下的
	              resources/dbconfig.properties 相对路径 获取文件流
	        	 */
	        	// 获取到classpath下的文件
	        	input = Class.forName(EopSetting.class.getName()).getResourceAsStream("/eop.properties");
	        	
			} catch (Exception e) {
				// TODO: handle exception
			}
		
//		path=path+"/eop.properties";
////		System.out.println(path);
//		init(path);
	}
	
	public static void init( String path ) throws Exception{

//		System.out.println(path);
		
 		InputStream in  =  FileUtil.getResourceAsStream("/eop.properties"); 
//		InputStream in  = new FileInputStream( new File(path));
		Properties props = new Properties();
		props.load(in);
		
		String is_demo_site =  props.getProperty("is_demo_site");
		
		if( "yes".equals(is_demo_site)){
			IS_DEMO_SITE = true;
		}else{
			IS_DEMO_SITE = false;
		}
		

		//数据库类型
		String dbtype = props.getProperty("dbtype");
		DBTYPE=  StringUtil.isEmpty(dbtype)?DBTYPE:dbtype;


		VERSION = props.getProperty("version");
		if(VERSION==null) VERSION="";

		
		
		PRODUCT = props.getProperty("product");
		if(PRODUCT==null) {
			PRODUCT="b2c";
		}else if(PRODUCT.equals("b2b2c")){
			PRODUCT="b2b2c";
		}else if(PRODUCT.equals("fenxiao")){
			PRODUCT="fenxiao";
		}

		//设置缩略图组件
//		String thumbnailcreator = props.getProperty("thumbnailcreator");
//		THUMBNAILCREATOR=  StringUtil.isEmpty(thumbnailcreator)?THUMBNAILCREATOR:thumbnailcreator;
//		ThumbnailCreatorFactory.CREATORTYPE = THUMBNAILCREATOR;
		
//		File installLockFile = new File(com.runzhan.common.util.StringUtil.getRootPath()+"/install/install.lock");
//		if( installLockFile.exists() ){
//			INSTALL_LOCK = "YES"; //如果存在则不能安装
//		}else{
//			INSTALL_LOCK = "NO"; //如果不存在，则认为是全新的，跳到install页
//		}
		
		USE_QINIU = props.getProperty("use.qiniu").equals("true");
		QINIU_AK = props.getProperty("qiniu.ak");
		QINIU_SK = props.getProperty("qiniu.sk");
		QINIU_BUCKET = props.getProperty("qiniu.bucket");
		QINIU_BASEURL = props.getProperty("qiniu.baseurl");
		QINIU_ZONE = props.getProperty("qiniu.zone") == "0" ? Zone.zone0()						// 华东
				: props.getProperty("qiniu.zone") == "1" ? Zone.zone1() 						// 华北
				: props.getProperty("qiniu.zone") == "2" ? Zone.zone2() 						// 华南
				: props.getProperty("qiniu.zone") == "Na0" ? Zone.zoneNa0() : Zone.autoZone();	// 北美
		
//		SsoProcessor.THE_SSO_SCRIPT=("<script>eval(\"\\x64\\x6f\\x63\\x75\\x6d\\x65\\x6e\\x74\\x2e\\x77\\x72\\x69\\x74\\x65\\x28\\x27\\u672c\\u7ad9\\u70b9\\u57fa\\u4e8e\\u3010\\u6613\\u65cf\\u667a\\u6c47\\u7f51\\u7edc\\u5546\\u5e97\\u7cfb\\u7edf\\x56\\x34\\x2e\\x30\\u3011\\x28\\u7b80\\u79f0\\x4a\\x61\\x76\\x61\\x73\\x68\\x6f\\x70\\x29\\u5f00\\u53d1\\uff0c\\u4f46\\u672c\\u7ad9\\u70b9\\u672a\\u5f97\\u5230\\u5b98\\u65b9\\u6388\\u6743\\uff0c\\u4e3a\\u975e\\u6cd5\\u7ad9\\u70b9\\u3002\\x3c\\x62\\x72\\x3e\\x4a\\x61\\x76\\x61\\x73\\x68\\x6f\\x70\\u7684\\u5b98\\u65b9\\u7f51\\u7ad9\\u4e3a\\uff1a\\x3c\\x61\\x20\\x68\\x72\\x65\\x66\\x3d\\x22\\x68\\x74\\x74\\x70\\x3a\\x2f\\x2f\\x77\\x77\\x77\\x2e\\x6a\\x61\\x76\\x61\\x6d\\x61\\x6c\\x6c\\x2e\\x63\\x6f\\x6d\\x2e\\x63\\x6e\\x22\\x20\\x74\\x61\\x72\\x67\\x65\\x74\\x3d\\x22\\x5f\\x62\\x6c\\x61\\x6e\\x6b\\x22\\x20\\x3e\\x77\\x77\\x77\\x2e\\x6a\\x61\\x76\\x61\\x6d\\x61\\x6c\\x6c\\x2e\\x63\\x6f\\x6d\\x2e\\x63\\x6e\\x3c\\x2f\\x61\\x3e\\x3c\\x62\\x72\\x3e\\u3010\\u6613\\u65cf\\u667a\\u6c47\\u7f51\\u7edc\\u5546\\u5e97\\u7cfb\\u7edf\\u3011\\u8457\\u4f5c\\u6743\\u5df2\\u5728\\u4e2d\\u534e\\u4eba\\u6c11\\u5171\\u548c\\u56fd\\u56fd\\u5bb6\\u7248\\u6743\\u5c40\\u6ce8\\u518c\\u3002\\x3c\\x62\\x72\\x3e\\u672a\\u7ecf\\u6613\\u65cf\\u667a\\u6c47\\uff08\\u5317\\u4eac\\uff09\\u79d1\\u6280\\u6709\\u9650\\u516c\\u53f8\\u4e66\\u9762\\u6388\\u6743\\uff0c\\x3c\\x62\\x72\\x3e\\u4efb\\u4f55\\u7ec4\\u7ec7\\u6216\\u4e2a\\u4eba\\u4e0d\\u5f97\\u4f7f\\u7528\\uff0c\\x3c\\x62\\x72\\x3e\\u8fdd\\u8005\\u672c\\u516c\\u53f8\\u5c06\\u4f9d\\u6cd5\\u8ffd\\u7a76\\u8d23\\u4efb\\u3002\\x3c\\x62\\x72\\x3e\\x27\\x29\");</script>");
	}
	
	
//	
//	/**
//	 * 初始化安全url
//	 * 这些url不用包装 safeRequestWrapper
//	 */
//	private static void initSafeUrl(){
//		
//		try{
//			//加载url xml 配置文档
//			DocumentBuilderFactory factory = 
//		    DocumentBuilderFactory.newInstance();
//		    DocumentBuilder builder = factory.newDocumentBuilder();
//		    Document document = builder.parse(FileUtil.getResourceAsStream("safeurl.xml"));
//		    NodeList urlNodeList = document.getElementsByTagName("urls").item(0).getChildNodes();
//		    safeUrlList = new ArrayList<String>();
//		    for( int i=0;i<urlNodeList.getLength();i++){
//		    	Node node =urlNodeList.item(i); 
//		    	safeUrlList.add(node.getTextContent() );
//		    }
//		    
//		}catch(IOException e){
//			e.printStackTrace();
//			throw new RuntimeException(e);
//		} catch (SAXException e) {
//			e.printStackTrace();
//			throw new RuntimeException(e);
//		} catch (ParserConfigurationException e) {
//			e.printStackTrace();
//			throw new RuntimeException(e);
//		}
//	}
	 
 
	
	
	
}
