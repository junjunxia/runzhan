package com.runzhan.expo.util;

import java.util.Map;


/**
 * 系统设置
 * @author kingapex
 *
 */
public class SystemSetting {
	
	
	private static String static_server_domain; //静态服务器域名 
	private static String static_server_path;	//静态服务器路径
	private static int backend_pagesize; //后台分页数
	private static String  default_img_url; //默认图片路径
	private static String context_path="/"; //虚拟目录 
	private static String global_auth_key="123456";//加密密钥
	private static int static_page_open=0; //是否开启静态页生成
	private static int lucene=0; //是开启lucene索引
	private static int sms_reg_open=0; //是否开启短信注册
	private static int wap_open=0; //是否开启wap站点
	private static String wap_folder; //wap模板目录 
	private static String wap_domain;//wap站点域名
	private  static int test_mode=0; //是否是测试模式
	
	//是否启用文件集群
	private static boolean fs_cluster = false;
	
	//系统设置中的分组
	public static final String setting_key="system"; 
	
	//系统缓存设置
	public static String cache="ehCache";	
	
	
	//系统设置的默认初始化
	static{
		 //TODO
	}
	
	
	/**
	 * 加载系统设置
	 * 由数据库中加载
	 */
	public static void load(){
		//TODO
	}
	 
	
	public static int getBackend_pagesize() {
		 
		return backend_pagesize;
	}
	 
	public static String getStatic_server_domain() {
		return static_server_domain;
	}
	 
	public static String getStatic_server_path() {
		return static_server_path;
	}


	public static String getDefault_img_url() {
		return default_img_url;
	}

	public static String getContext_path() {
		return context_path;
	}


	public static String getGlobal_auth_key() {
		return global_auth_key;
	}


	public static int getStatic_page_open() {
		return static_page_open;
	}


	public static int getLucene() {
		return lucene;
	}


	public static int getSms_reg_open() {
		return sms_reg_open;
	}


 
	public static int getWap_open() {
		return wap_open;
 
 
	}
 

	public static String getWap_folder() {
		return wap_folder;
	}


	public static String getWap_domain() {
		
		return wap_domain;
	}

 
	
	
	public static boolean getFs_cluster() {
		return fs_cluster;
	}


	public static int getTest_mode() {
		return test_mode;
	}
	
	public static void setTest_mode(int _test_mode){
		test_mode =_test_mode;
	}
	
	
 
}
