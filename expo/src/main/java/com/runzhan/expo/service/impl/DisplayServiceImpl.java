package com.runzhan.expo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.runzhan.base.service.impl.BaseServiceImpl;
import com.runzhan.base.dao.BaseDao;
import com.runzhan.expo.dao.DisplayDao;
import com.runzhan.expo.entity.Display;
import com.runzhan.expo.service.DisplayService;

/**
 * 显示设置Service实现
 * @author dk
 */

@Service
@Transactional
public class DisplayServiceImpl extends BaseServiceImpl<Display> implements DisplayService {

	@Autowired
	private DisplayDao mapper;


	protected BaseDao<Display> getMapper() {
		return mapper;
	}


	@Transactional(readOnly = true)
	public Display queryDefault() {
		return mapper.queryDefault();
	}


	@Transactional(readOnly = true)
	public Display queryGlobal() {
		return mapper.queryGlobal();
	}


	public void save(Display display) {
		// 系统设置
		if (display.getUser() == null || display.getUser().getUserId() == null) {
			this.updateGlobal(display);
		} else { // 个人设置
			if (mapper.queryByUser(display.getUser().getUserId()) == null) {
				super.save(display);
			} else {
				mapper.update(display);
			}
		}
	}


	@Transactional(readOnly = true)
	public Display queryByUser(Long userId) {
		Display display = mapper.queryByUser(userId);
		if (display == null) {
			display = mapper.queryGlobal();
		}
		return display;
	}


	public void resetUserDisplay(Long userId) {
		mapper.delete(userId);
	}


	public void updateGlobal(Display display) {
		mapper.updateGlobal(display);
	}
	

	public void resetGlobal() {
		Display defaultDisplay = mapper.queryDefault();
		mapper.updateGlobal(defaultDisplay);
	}

}
