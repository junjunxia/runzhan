package com.runzhan.expo.service.impl;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.socket.TextMessage;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.runzhan.base.service.impl.BaseServiceImpl;
import com.runzhan.base.dao.BaseDao;
import com.runzhan.expo.dao.MessageDao;
import com.runzhan.expo.entity.Message;
import com.runzhan.expo.service.MessageService;
import com.runzhan.expo.vo.MessageVo;

/**
 * 消息Service实现
 * @author dk
 */

@Service
@Transactional
public class MessageServiceImpl extends BaseServiceImpl<Message> implements MessageService {

	Logger logger = Logger.getLogger(this.getClass());
	@Autowired
	private MessageDao mapper;


	protected BaseDao<Message> getMapper() {
		return mapper;
	}


	public void sendMsg(Message msg) {
		if (msg != null && msg.getUserId() != 0 && StringUtils.isNotBlank(msg.getTitle())) {
			// 保存入库
			mapper.save(msg);
			// websocket发送消息
			try {
				ObjectMapper mapper = new ObjectMapper();  
		        String json =  mapper.writeValueAsString(msg);  
			} catch (IOException e) {
				logger.error("系统消息发送失败！", e);
			}
		}
	}


	public void read(Long messageId) {
		mapper.updateById(messageId);
	}


	@Transactional(readOnly = true)
	public List<Message> queryByUser(MessageVo msgVo) {
		return mapper.queryByUser(msgVo);
	}
	

	@Transactional(readOnly = true)
	public int getCountByUser(MessageVo msgVo) {
		return mapper.getCountByUser(msgVo);
	}
	
}
