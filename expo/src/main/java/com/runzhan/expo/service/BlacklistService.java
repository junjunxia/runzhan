package com.runzhan.expo.service;

import com.runzhan.base.service.BaseService;
import com.runzhan.expo.entity.Blacklist;


/**
 * 黑名单Service
 * @author dk
 */
public interface BlacklistService extends BaseService<Blacklist> {

	Blacklist queryByIp(String ip);
	
}
