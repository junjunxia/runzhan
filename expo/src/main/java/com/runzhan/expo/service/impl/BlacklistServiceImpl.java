package com.runzhan.expo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.runzhan.base.service.impl.BaseServiceImpl;
import com.runzhan.base.dao.BaseDao;
import com.runzhan.expo.dao.BlacklistDao;
import com.runzhan.expo.entity.Blacklist;
import com.runzhan.expo.service.BlacklistService;

/**
 * 黑名单Service实现
 * @author dk
 */

@Service
@Transactional
public class BlacklistServiceImpl extends BaseServiceImpl<Blacklist> implements BlacklistService {

	@Autowired
	private BlacklistDao mapper;


	protected BaseDao<Blacklist> getMapper() {
		return mapper;
	}


	public Blacklist queryByIp(String ip) {
		return mapper.queryByIp(ip);
	}

}
