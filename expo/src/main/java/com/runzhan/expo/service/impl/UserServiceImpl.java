package com.runzhan.expo.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.runzhan.base.service.impl.BaseServiceImpl;
import com.runzhan.common.exception.DuplicateLoginNameException;
import com.runzhan.base.dao.BaseDao;
import com.runzhan.expo.dao.UserDao;
import com.runzhan.expo.entity.Role;
import com.runzhan.expo.entity.User;
import com.runzhan.expo.entity.UserRole;
import com.runzhan.expo.enums.UserState;
import com.runzhan.expo.service.MessageService;
import com.runzhan.expo.service.UserService;
import com.runzhan.expo.util.Md5Utils;

/**
 * 用户Service实现
 * @author dk
 */

@Service
@Transactional
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

	@Autowired
	MessageService messageService;
	@Autowired
	private UserDao mapper;
	

	protected BaseDao<User> getMapper() {
		return mapper;
	}


	public boolean login(User user) {
		
		if (user == null || StringUtils.isBlank(user.getLoginName()) || StringUtils.isEmpty(user.getPassword())) {
			return false;
		}
		
		User dbUser = mapper.findByLoginName(user.getLoginName());
		if (dbUser != null && user.getPassword().equals(dbUser.getPassword()) && UserState.NORMAL.equals(dbUser.getState())) {
			user.setUserId(dbUser.getUserId());
			user.setLoginCount(dbUser.getLoginCount() + 1);
			user.setLastLoginTime(dbUser.getLastLoginTime());
			user.setRoles(dbUser.getRoles());
			updateLoginInfo(user);
			return true;
		}
		
		return false;
	}
	

	public boolean checkPassword(User user) {
		
		if (user == null || StringUtils.isBlank(user.getLoginName()) || StringUtils.isEmpty(user.getPassword())) {
			return false;
		}
		
		User dbUser = mapper.findByLoginName(user.getLoginName());
		if (dbUser != null && Md5Utils.hash(user.getPassword()).equals(dbUser.getPassword())) {
			return true;
		}
		
		return false;
	}
	
	/**
     * <p>更新用户登录信息</p>
     *
     * @param  user  待更新用户
     */
	@Transactional
	private void updateLoginInfo(User user) {
		mapper.updateLoginInfo(user);
	}


	@Transactional(readOnly = true)
	public int getTotal() {
		return mapper.getTotal();
	}


	public void saveUser(User user) throws DuplicateLoginNameException {
		if (user != null) {
			if (StringUtils.isNotBlank(user.getPassword())) {
				user.setPassword(Md5Utils.hash(user.getPassword()));
			}
			if (user.getState() == null) {
				user.setState(UserState.NORMAL);
			}
			
			if (user.getUserId() == null) {
				if (mapper.findByLoginName(user.getLoginName()) != null) {
					throw new DuplicateLoginNameException("用户名已存在！");
				}
				mapper.save(user);
			} else {
				mapper.update(user);
				mapper.deleteUserRole(user.getUserId());
			}
			saveUserRole(user);
		}
		
	}


	public void saveUserRole(User user) {
		if (user.getRoleIds() != null && user.getRoleIds().length > 0) {
			UserRole userRole = new UserRole();
			for (Long roleId : user.getRoleIds()) {
				userRole.setUserId(user.getUserId());
				userRole.setRoleId(roleId);
				mapper.saveUserRole(userRole);
			}
		}
	}


	@Transactional(readOnly = true)
	public List<Role> queryRole(Long userId) {
		return mapper.queryRole(userId);
	}


	public void deleteUser(Long[] userId) {
		if (userId != null && userId.length > 0) {
			for (Long id : userId) {
				mapper.deleteUserRole(id);
				mapper.delete(id); // 逻辑删除
			}
		}
	}


	public void forbid(Long[] userId) {
		if (userId != null && userId.length > 0) {
			for (Long id : userId) {
				mapper.forbid(id);
			}
		}
	}

}
