package com.runzhan.expo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.runzhan.base.service.impl.BaseServiceImpl;
import com.runzhan.base.dao.BaseDao;
import com.runzhan.expo.dao.EmployeeDao;
import com.runzhan.expo.entity.Employee;
import com.runzhan.expo.service.EmployeeService;
import com.runzhan.expo.vo.EmployeeVo;

/**
 * 员工Service实现
 * @author dk
 */

@Service
@Transactional
public class EmployeeServiceImpl extends BaseServiceImpl<Employee> implements EmployeeService {

	@Autowired
	private EmployeeDao mapper;
	

	protected BaseDao<Employee> getMapper() {
		return mapper;
	}


	public Integer getTotal() {
		return mapper.getTotal();
	}


	public List<Employee> query(EmployeeVo employee) {
		return mapper.query(employee);
	}


	public Integer getCount(EmployeeVo employee) {
		return mapper.getCount(employee);
	}

}
