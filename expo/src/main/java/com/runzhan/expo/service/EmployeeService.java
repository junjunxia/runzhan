package com.runzhan.expo.service;

import java.util.List;

import com.runzhan.base.service.BaseService;
import com.runzhan.expo.entity.Employee;
import com.runzhan.expo.vo.EmployeeVo;

/** 
 * 员工Service
 * @author dk
 */
public interface EmployeeService extends BaseService<Employee>{
	
	Integer getTotal();
	
	Integer getCount(EmployeeVo employee);
	
	List<Employee> query(EmployeeVo employee);
	
}
