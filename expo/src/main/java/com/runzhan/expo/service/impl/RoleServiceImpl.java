package com.runzhan.expo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.base.service.impl.BaseServiceImpl;
import com.runzhan.expo.dao.RoleDao;
import com.runzhan.expo.entity.Menu;
import com.runzhan.expo.entity.MenuRole;
import com.runzhan.expo.entity.Role;
import com.runzhan.expo.entity.User;
import com.runzhan.expo.service.RoleService;

/**
 * 角色Service实现
 * @author dk
 */

@Service
@Transactional
public class RoleServiceImpl extends BaseServiceImpl<Role> implements RoleService {

	@Autowired
	private RoleDao mapper;
	

	protected BaseDao<Role> getMapper() {
		return mapper;
	}


	@Transactional(readOnly = true)
	public List<Role> queryRole(Menu menu) {
		return mapper.queryRole(menu);
	}


	@Transactional(readOnly = true)
	public List<Role> queryRoleUserCount() {
		return mapper.queryRoleUserCount();
	}


	public void saveMenuRole(MenuRole menuRole) {
		mapper.saveMenuRole(menuRole);
	}


	public void save(Role role) {
		if (role != null) {
			if (role.getRoleId() == null) {
				mapper.save(role);
			} else {
				mapper.update(role);
				mapper.deleteMenuRole(role.getRoleId());
			}
			MenuRole menuRole = new MenuRole();
			for (Long menuId : role.getRoleAuth()) {
				menuRole.setMenuId(menuId);
				menuRole.setRoleId(role.getRoleId());
				saveMenuRole(menuRole);
			}
		}
	}


	public void deleteMenuRole(Menu menu) {
		mapper.deleteMenuRoleByMenu(menu);
	}
	

	public void deleteRole(Long roleId) {
		mapper.deleteUserRole(roleId);
		mapper.deleteMenuRole(roleId);
		mapper.delete(roleId);
	}


	@Transactional(readOnly = true)
	public List<User> queryUser(Long roleId) {
		return mapper.queryUser(roleId);
	}
}
