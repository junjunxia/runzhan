package com.runzhan.expo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.runzhan.base.service.impl.BaseServiceImpl;
import com.runzhan.base.dao.BaseDao;
import com.runzhan.expo.dao.LogDao;
import com.runzhan.expo.entity.Log;
import com.runzhan.expo.entity.LogConfig;
import com.runzhan.expo.service.LogService;
import com.runzhan.expo.vo.LogVo;

/**
 * 日志ServiceImpl
 * @author dk
 */

@Service
@Transactional
public class LogServiceImpl extends BaseServiceImpl<Log> implements LogService {

	@Autowired
	private LogDao mapper;


	protected BaseDao<Log> getMapper() {
		return mapper;
	}


	public void saveConfig(LogConfig logConfig) {
		if (logConfig != null) {
			if (logConfig.getConfigId() == null) {
				mapper.saveConfig(logConfig);
			} else {
				mapper.updateConfig(logConfig);
			}
		}
		
	}


	@Transactional(readOnly = true)
	public List<LogConfig> queryAllConfig() {
		return mapper.queryAllConfig();
	}


	@Transactional(readOnly = true)
	public List<Log> query(LogVo logVo) {
		return mapper.query(logVo);
	}


	@Transactional(readOnly = true)
	public int getCount(LogVo logVo) {
		return mapper.getCount(logVo);
	}


	public void deleteConfig(Long configId) {
		mapper.deleteConfig(configId);
	}
	
}
