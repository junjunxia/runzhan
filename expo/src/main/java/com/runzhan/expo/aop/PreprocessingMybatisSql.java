package com.runzhan.expo.aop;

import java.sql.Connection;
import java.util.Date;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.executor.statement.RoutingStatementHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.factory.DefaultObjectFactory;
import org.apache.ibatis.reflection.factory.ObjectFactory;
import org.apache.ibatis.reflection.wrapper.DefaultObjectWrapperFactory;
import org.apache.ibatis.reflection.wrapper.ObjectWrapperFactory;
import org.apache.log4j.Logger;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.runzhan.expo.entity.User;

/**
 * @ClassName: PreprocessingMybatisSql
 * @Description:
 */
@Intercepts({@Signature(method = "prepare", type = StatementHandler.class, args = {Connection.class})})
public class PreprocessingMybatisSql implements Interceptor {
	Logger logger = Logger.getLogger(this.getClass());
	
	private static final ObjectFactory DEFAULT_OBJECT_FACTORY = new DefaultObjectFactory();
    private static final ObjectWrapperFactory DEFAULT_OBJECT_WRAPPER_FACTORY =
            new DefaultObjectWrapperFactory();

    /*
     * (non Javadoc)
     * 
     * @Title: intercept
     * 
     * @Description:
     * 
     * @param invocation
     * 
     * @return
     * 
     * @throws Throwable
     * 
     * @see org.apache.ibatis.plugin.Interceptor#intercept(org.apache.ibatis.plugin.Invocation)
     */

    public Object intercept(Invocation invocation) throws Throwable {
        StatementHandler statementHandler = (StatementHandler) invocation.getTarget();
        MetaObject metaStatementHandler =
                MetaObject.forObject(statementHandler, DEFAULT_OBJECT_FACTORY,
                        DEFAULT_OBJECT_WRAPPER_FACTORY);

        MappedStatement mappedStatement =
                (MappedStatement) metaStatementHandler.getValue("delegate.mappedStatement");
        SqlCommandType sqlCommandType = mappedStatement.getSqlCommandType();
//        String id = StringUtils.substringAfterLast(mappedStatement.getId(), "."); //method's name

        BoundSql boundSql = ((RoutingStatementHandler) invocation.getTarget()).getBoundSql();
        
        if (sqlCommandType.equals(SqlCommandType.UPDATE)) {
        	User user = null;
            try {
                HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();  
                HttpSession session = request.getSession(false);
                // 记录操作用户
                if (session != null && session.getAttribute("loginUser") != null) {
                	user = (User)session.getAttribute("loginUser");
                }
            } catch (Exception ex) {
            	logger.error("can't get current user, may be not authenticated!", ex);
            }
            
            boundSql.setAdditionalParameter("updateId", user == null ? 0L : user.getUserId());
            boundSql.setAdditionalParameter("updateName", user == null ? "unknown" : user.getLoginName());
            boundSql.setAdditionalParameter("updateTime", new Date());
        } else if (sqlCommandType.equals(SqlCommandType.INSERT)) {
        	User user = null;
            try {
                HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();  
                HttpSession session = request.getSession(false);
                // 记录操作用户
                if (session != null && session.getAttribute("loginUser") != null) {
                	user = (User)session.getAttribute("loginUser");
                }
            } catch (Exception ex) {
            	logger.error("can't get current user, may be not authenticated!", ex);
            }
            
            boundSql.setAdditionalParameter("createId", user == null ? 0L : user.getUserId());
            boundSql.setAdditionalParameter("createName", user == null ? "unknown" : user.getLoginName());
            boundSql.setAdditionalParameter("createTime", new Date());
        }
        return invocation.proceed();
    }

    /*
     * (non Javadoc)
     * 
     * @Title: plugin
     * 
     * @Description:
     * 
     * @param target
     * 
     * @return
     * 
     * @see org.apache.ibatis.plugin.Interceptor#plugin(java.lang.Object)
     */

    public Object plugin(Object target) {
        if (target instanceof StatementHandler) {
            return Plugin.wrap(target, this);
        } else {
            return target;
        }
    }

    /*
     * (non Javadoc)
     * 
     * @Title: setProperties
     * 
     * @Description:
     * 
     * @param properties
     * 
     * @see org.apache.ibatis.plugin.Interceptor#setProperties(java.util.Properties)
     */

    public void setProperties(Properties properties) {}


}
