package com.runzhan.expo.dao;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.expo.entity.Display;

/** 
 * DisplayMapper
 * @author dk
 */
public interface DisplayDao extends BaseDao<Display> {

	Display queryDefault();
	
	Display queryGlobal();
	
	Display queryByUser(Long userId);
	
	void updateGlobal(Display display);
	
}
