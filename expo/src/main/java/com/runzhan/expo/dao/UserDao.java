package com.runzhan.expo.dao;

import java.util.List;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.expo.entity.Role;
import com.runzhan.expo.entity.User;
import com.runzhan.expo.entity.UserRole;

/** 
 * UserMapper
 * @author dk
 */
public interface UserDao extends BaseDao<User> {

	User findByLoginName(String loginName);
	
	void updateLoginInfo(User user);
	
	int getTotal();
	
	void saveUserRole(UserRole userRole);
	
	void deleteUserRole(Long userId);
	
	List<Role> queryRole(Long userId);
	
	void forbid(Long userId);
}
