package com.runzhan.expo.dao;

import java.util.List;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.expo.entity.Employee;
import com.runzhan.expo.vo.EmployeeVo;

/** 
 * EmployeeMapper
 * @author dk
 */
public interface EmployeeDao extends BaseDao<Employee> {

	Integer getTotal();
	
	Integer getCount(EmployeeVo employee);
	
	List<Employee> query(EmployeeVo employee);
}
