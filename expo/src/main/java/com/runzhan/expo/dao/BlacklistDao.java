package com.runzhan.expo.dao;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.expo.entity.Blacklist;

/** 
 * BlacklistMapper
 * @author dk
 */
public interface BlacklistDao extends BaseDao<Blacklist> {

	Blacklist queryByIp(String ip);
	
}
