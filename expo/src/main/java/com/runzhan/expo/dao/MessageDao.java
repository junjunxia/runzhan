package com.runzhan.expo.dao;

import java.util.List;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.expo.entity.Message;
import com.runzhan.expo.vo.MessageVo;

/** 
 * MessageMapper
 * @author dk
 */
public interface MessageDao extends BaseDao<Message> {

	void updateById(Long messageId);
	
	List<Message> queryByUser(MessageVo msgVo);
	
	int getCountByUser(MessageVo msgVo);
}
