package com.runzhan.expo.dao;

import java.util.List;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.expo.entity.Menu;
import com.runzhan.expo.entity.MenuRole;
import com.runzhan.expo.entity.Role;
import com.runzhan.expo.entity.User;

/** 
 * RoleMapper
 * @author dk
 */
public interface RoleDao extends BaseDao<Role> {

	List<Role> queryRole(Menu menu);
	
	List<Role> queryRoleUserCount();
	
	void saveMenuRole(MenuRole menuRole);
	
	void deleteMenuRole(Long roleId);
	
	void deleteMenuRoleByMenu(Menu menu);
	
	void deleteUserRole(Long roleId);
	
	List<User> queryUser(Long roleId);
}
