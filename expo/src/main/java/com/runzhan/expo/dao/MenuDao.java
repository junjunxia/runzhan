package com.runzhan.expo.dao;

import java.util.List;
import java.util.Map;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.expo.entity.Menu;
import com.runzhan.expo.entity.Role;
import com.runzhan.expo.entity.User;

/** 
 * MenuMapper
 * @author dk
 */
public interface MenuDao extends BaseDao<Menu> {
	
	List<Menu> queryByUser(User user);
	
	List<Menu> queryByRole(Role role);
	
	void saveMenuRole(Map<String, Object> paraMap);
	
	void deleteMenuRole(Menu menu);
	
	String generateFirstLayer();
	
	void deleteMenu(Menu menu);
	
	List<Menu> queryTop();
	
	void updateTopOrder(Menu menu);
	
	List<Long> queryTopChildrenIds(Menu topMenu);
}
