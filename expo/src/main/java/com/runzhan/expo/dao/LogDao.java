package com.runzhan.expo.dao;

import java.util.List;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.expo.entity.Log;
import com.runzhan.expo.entity.LogConfig;
import com.runzhan.expo.vo.LogVo;

/** 
 * LogMapper
 * @author dk
 */
public interface LogDao extends BaseDao<Log> {

	void saveConfig(LogConfig logConfig);
	
	void deleteConfig(Long configId);
	
	void updateConfig(LogConfig logConfig);
	
	List<LogConfig> queryAllConfig();
	
	List<Log> query(LogVo logVo);
	
	int getCount(LogVo logVo);
}
