package com.runzhan.email.service;

import java.util.List;

import com.runzhan.email.vo.SendEmailParamsVo;
import com.runzhan.expert.vo.ExpertInfoVo;

public interface EmailWebService {

	void sendEmailToExpertList(List<ExpertInfoVo> expertInfoList, SendEmailParamsVo params);

}
