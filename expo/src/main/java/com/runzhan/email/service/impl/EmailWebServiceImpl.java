package com.runzhan.email.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.runzhan.base.util.BeanMapUtils;
import com.runzhan.email.po.MailBean;
import com.runzhan.email.service.EmailWebService;
import com.runzhan.email.util.MailUtil;
import com.runzhan.email.vo.SendEmailParamsVo;
import com.runzhan.expert.vo.ExpertInfoVo;

@Service
public class EmailWebServiceImpl implements EmailWebService {
	Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private MailUtil mailUtil;

	@Async

	public void sendEmailToExpertList(List<ExpertInfoVo> expertInfoList, SendEmailParamsVo params) {
		for (ExpertInfoVo vo : expertInfoList) {
			if (!StringUtils.isBlank(vo.getEmail())) {
				try {
					MailBean mailBean = new MailBean();
					mailBean.setSubject(params.getTitle());
					mailBean.setTemplate(params.getContent());
					mailBean.setToEmails(new String[]{vo.getEmail()});
					
					Map<String, Object> data = BeanMapUtils.objectToMap(vo);
					mailBean.setData(data);
					
					mailUtil.send(mailBean);
				} catch (Exception e) {
					logger.error(" 发送短信 sendEmailToExpertList Exception--params: "+params, e);
				}
			}
		}
	}

}
