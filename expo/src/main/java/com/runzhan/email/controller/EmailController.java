package com.runzhan.email.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.runzhan.common.vo.EmailListParams;
import com.runzhan.email.po.EmailTemplate;
import com.runzhan.email.po.TemplateObjectDict;
import com.runzhan.email.service.EmailTemplateService;
import com.runzhan.email.service.EmailWebService;
import com.runzhan.email.vo.SendEmailParamsVo;
import com.runzhan.expert.service.ExpertService;
import com.runzhan.expert.vo.ExpertInfoVo;
import com.runzhan.expo.controller.BaseController;

@RestController
@RequestMapping("/email")
public class EmailController extends BaseController {
	Logger logger = Logger.getLogger(this.getClass());
	@Autowired
	private EmailTemplateService emailTemplateService;
	@Autowired
	private EmailWebService emailWebService;
	@Autowired
	private ExpertService expertService;
	
	@RequestMapping(value = "/emailTemplate-list")
	public ModelAndView list() {
		ModelAndView mv = new ModelAndView();
		String pjax = request.getHeader("X-PJAX");
		
		if (StringUtils.isNotBlank(pjax) && "true".equals(pjax)) {
			mv.setViewName("email/emailTemplate-list");
		} else {
			mv.addObject("include", "email/emailTemplate-list.jsp");
			mv.setViewName("index");
		}
		
		return mv;
	}
	
	
	@RequestMapping(value = "/editEmailTemplate")
	public ModelAndView toAddEmailTemplate(Long id) {
		ModelAndView mv = new ModelAndView();
		String pjax = request.getHeader("X-PJAX");
		
		if (StringUtils.isNotBlank(pjax) && "true".equals(pjax)) {
			mv.setViewName("email/add-emailTemplate");
		} else {
			mv.addObject("include", "email/add-emailTemplate.jsp");
			mv.setViewName("index");
		}
		if(null!=id && id>0){
			mv.addObject("emailTemplate", emailTemplateService.getById(id));
		}
		List<TemplateObjectDict> objectDictList = emailTemplateService.getTemplateObjectDict();
		mv.addObject("objectDictList", objectDictList);

		return mv;
	}
	
	@RequestMapping(value = "/saveEmailTemplate", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> saveEmailTemplate(EmailTemplate params) {
		
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		try {
			if (null == params) {
				jsonMap.put("msg", "参数为空！");
				jsonMap.put("flag", "0");
				return jsonMap;
			}
			if (null == params.getId()) {
				emailTemplateService.save(params);
			} else {
				emailTemplateService.updateEmailTemplate(params);
			}
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
			logger.error("保存邮件模板失败！", e);
		}
		
		jsonMap.put("flag", "1");
		return jsonMap;
	}
	
	@RequestMapping(value = "/deleteEmailTemplate", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> deleteEmailTemplate(EmailListParams emailParams) {
		
		Map<String, Object> jsonMap  = new HashMap<String, Object>();
		try {
			if (null == emailParams || CollectionUtils.isEmpty(emailParams.getEmailIdList())) {
				jsonMap.put("msg", "参数为空！");
				jsonMap.put("flag", "0");
				return jsonMap;
			}
			emailTemplateService.batchDelete(emailParams.getEmailIdList());
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
			logger.error("删除邮件模板失败！", e);
		}
		
		jsonMap.put("flag", "1");
		return jsonMap;
	}
	
	@RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> sendEmail(SendEmailParamsVo params) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		try {
			if (null == params) {
				jsonMap.put("msg", "参数为空！");
				jsonMap.put("flag", "0");
				return jsonMap;
			}
			
			List<Long> expertIds = params.getExpertIds();
			if (!CollectionUtils.isEmpty(expertIds)) {
				List<ExpertInfoVo> expertInfoList = expertService.getExpertInfoList(expertIds);
				if (null != expertInfoList && !CollectionUtils.isEmpty(expertInfoList)) {
					emailWebService.sendEmailToExpertList(expertInfoList, params);
				}
			}
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
			logger.error("发送邮件失败！", e);
		}
		
		jsonMap.put("flag", "1");
		return jsonMap;
	}
	
	@RequestMapping(value = "/getEmailTemplateList")
	@ResponseBody
	public List<EmailTemplate> getEmailTemplateList() {
		return emailTemplateService.getEmailTemplateList();
	}

}
