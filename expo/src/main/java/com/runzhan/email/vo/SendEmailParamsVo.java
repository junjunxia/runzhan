package com.runzhan.email.vo;

import java.util.ArrayList;
import java.util.List;

public class SendEmailParamsVo {
	
	private List<Long> expertIds = new ArrayList<Long>();
	private String title;
	private String content;
	public List<Long> getExpertIds() {
		return expertIds;
	}
	public void setExpertIds(List<Long> expertIds) {
		this.expertIds = expertIds;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public String toString() {
		return "SendEmailParamsVo [expertIds=" + expertIds + ", title=" + title
				+ ", content=" + content + "]";
	}
	
}
