package com.runzhan.upload;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import com.runzhan.expo.util.DateUtil;
import com.runzhan.expo.util.EopSetting;
import com.runzhan.expo.util.FileUtil;
import com.runzhan.expo.util.StaticResourcesUtil;
import com.runzhan.expo.util.StringUtil;
import com.runzhan.expo.util.SystemSetting;

/**
 * 上传文件到七牛
 *
 */
@Component
public class QiniuUploader implements IUploader {
	
	protected Logger logger = Logger.getLogger(getClass());
	
	private static final String QINIU_AK = EopSetting.QINIU_AK;
	private static final String QINIU_SK = EopSetting.QINIU_SK;
	private static final Zone   QINIU_ZONE = EopSetting.QINIU_ZONE;
	private static final String QINIU_BUCKET = EopSetting.QINIU_BUCKET;

	/**
	 * 上传图片到七牛
	 */

	public String upload(InputStream stream, String subFolder, String fileName) {
		
		// 参数校验
		checkParams(stream, subFolder, fileName);
		// 重命名文件
		fileName = DateUtil.toString(new Date(), "mmss") + StringUtil.getRandStr(4) + "." + FileUtil.getFileExt(fileName);
		// 七牛存放路径
		String path = "attachment/" + (subFolder != null ? subFolder : "") + "/" + this.getTimePath() + fileName;
		// fs:路径
//		String fsPath = EopSetting.FILE_STORE_PREFIX + "/" + path;
		String fsPath = EopSetting.QINIU_BASEURL + "/" + path;

		// 上传文件到七牛
		upload(stream, path);

		return fsPath;
	}
	
	
	/**
	 * 删除七牛图片
	 */

	public void deleteFile(String filePath) {
		
		// TODO 暂时没有地方调用此方法, 后面如果有需求再添加 - 20170331 by Tao
		FileUtil.delete(filePath);
		FileUtil.delete(StaticResourcesUtil.getThumbPath(filePath, "_thumbnail"));
	}

	/**
	 * 上传缩略图到七牛
	 * 
	 * @param stream
	 * @param fileFileName
	 * @param subFolder
	 * @param width
	 * @param height
	 * @return
	 */
	/*public String[] upload(InputStream stream, String fileFileName, String subFolder, int width, int height) {
		
		// TODO 暂时没有地方调用此方法, 后面如果有需求再添加 - 20170331 by Tao
		
		BufferedImage image = ImageIO.read(stream);
		BufferedImage newImage = Scalr.resize(image, Method.QUALITY, Mode.AUTOMATIC, width, height, Scalr.OP_ANTIALIAS);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ImageIO.write(newImage, "jpg", os);
		InputStream is = new ByteArrayInputStream(os.toByteArray());
		
		
		if (stream == null || fileFileName == null) {
			throw new IllegalArgumentException("file or filename object is null");
		}
		if (subFolder == null) {
			throw new IllegalArgumentException("subFolder is null");
		}
		String fileName = null;
		String filePath = "";
		String[] path = new String[2];
		if (!FileUtil.isAllowUpImg(fileFileName)) {
			throw new IllegalArgumentException("不被允许的上传文件类型");
		}
		String ext = FileUtil.getFileExt(fileFileName);
		fileName = DateUtil.toString(new Date(), "yyyyMMddHHmmss") + StringUtil.getRandStr(4) + "." + ext;
		String static_server_path = SystemSetting.getStatic_server_path();

		filePath = static_server_path + "/attachment/";
		if (subFolder != null) {
			filePath += subFolder + "/";
		}

		path[0] = static_server_path + "/attachment/" + (subFolder == null ? "" : subFolder) + "/" + fileName;
		filePath += fileName;
		FileUtil.createFile(stream, filePath);
		String thumbName = StaticResourcesUtil.getThumbPath(filePath, "_thumbnail");

		IThumbnailCreator thumbnailCreator = ThumbnailCreatorFactory.getCreator(filePath, thumbName);
		thumbnailCreator.resize(width, height);
		path[1] = StaticResourcesUtil.getThumbPath(path[0], "_thumbnail");

		return path;
	}*/

	/**
	 * 创建缩略图
	 * @param url
	 * @param endfix
	 * @param width
	 * @param height
	 * @return
	 */
	public String createThumbs(String url, String endfix, int width, int height) {
		
		String ext = FileUtil.getFileExt(url);
		BufferedImage image = null;
		try {
			image = ImageIO.read(new URL(url));
		} catch (MalformedURLException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		InputStream stream = resize(image, ext, width, height);
		String path = url.replaceAll(EopSetting.QINIU_BASEURL + "/", "").replaceAll("." + ext, endfix + "." + ext);
		upload(stream, path);
		return EopSetting.QINIU_BASEURL + "/" + path;
	}
	
	private void upload(InputStream stream, String key) {
		
		// 构造一个带指定Zone对象的配置类, 其他参数参考类
		Configuration cfg = new Configuration(QINIU_ZONE);
		UploadManager uploadManager = new UploadManager(cfg);
		// 生成上传凭证，然后准备上传
		String token = Auth.create(QINIU_AK, QINIU_SK).uploadToken(QINIU_BUCKET);
		try {
			// 默认不指定key的情况下，以文件内容的hash值作为文件名
			Response response = uploadManager.put(stream, key, token, null, null);
			// 解析上传成功的结果
			DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
			logger.debug("七牛上传图成功: [key: " + putRet.key + ", hash: " + putRet.hash + "]");
		} catch (QiniuException ex) {
			logger.error("七牛上传图片失败: " + ex.error());
		}
	}
	
	/**
	 * 参数校验
	 * @param stream
	 * @param subFolder
	 * @param fileName
	 */
	private void checkParams(InputStream stream, String subFolder, String fileName) {
		
		if (stream == null) {
			throw new IllegalArgumentException("file or filename object is null");
		}
		if (subFolder == null) {
			throw new IllegalArgumentException("subFolder is null");
		}
		if (!FileUtil.isAllowUpImg(fileName)) {
			throw new IllegalArgumentException("不被允许的上传文件类型");
		}
	}
	
	/**
	 * Resize图片大小
	 * @param bufferedImage
	 * @param formatName
	 * @param width
	 * @param height
	 * @return InputStream
	 */
	private InputStream resize(BufferedImage bi, String formatName, int w, int h) {
		
		BufferedImage image = Scalr.resize(bi, Method.QUALITY, Mode.AUTOMATIC, w, h, Scalr.OP_ANTIALIAS);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ImageIO.write(image, formatName, os);
		} catch (IOException e) {
			logger.error("Convert BufferedImage To Stream Error: " + e.getMessage());
		}
		return  new ByteArrayInputStream(os.toByteArray());
	}

	/**
	 * 生成时间路径
	 * @return
	 */
	private String getTimePath() {
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		int month = now.get(Calendar.MONTH) + 1;
		int date = now.get(Calendar.DAY_OF_MONTH);
		int minute = now.get(Calendar.HOUR_OF_DAY);
		String filePath = "";
		if (year != 0) {
			filePath += year + "/";
			;
		}
		if (month != 0) {
			filePath += month + "/";
			;
		}
		if (date != 0) {
			filePath += date + "/";
			;
		}
		if (minute != 0) {
			filePath += minute + "/";
			;
		}
		return filePath;
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		QiniuUploader up = new QiniuUploader();
		
		String s = up.upload(new FileInputStream("C:\\Users\\ZhuZheng\\Pictures\\Screenshots\\u10.png"), "runzhan", "u10.png");
		
//		String path = up.createThumbs("http://om6sbk5jb.bkt.clouddn.com/attachment/store/2017/3/31/16/36077341.jpg", "_samll", 20, 10);
		System.out.println(s);
	}
}
