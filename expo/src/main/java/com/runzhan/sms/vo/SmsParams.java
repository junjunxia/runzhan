package com.runzhan.sms.vo;

import java.util.List;

public class SmsParams {
	List<Long> smsIdList;

	public List<Long> getSmsIdList() {
		return smsIdList;
	}

	public void setSmsIdList(List<Long> smsIdList) {
		this.smsIdList = smsIdList;
	}
	

}
