package com.runzhan.sms.vo;

import java.util.ArrayList;
import java.util.List;

public class SendSmsParamsVo {
	
	private List<Long> expertIds = new ArrayList<Long>();
	private String content;
	public List<Long> getExpertIds() {
		return expertIds;
	}
	public void setExpertIds(List<Long> expertIds) {
		this.expertIds = expertIds;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
