package com.runzhan.sms.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.runzhan.sms.po.SmsTemplate;
import com.runzhan.sms.po.TemplateObjectDict;
import com.runzhan.expert.service.ExpertService;
import com.runzhan.expert.vo.ExpertInfoVo;
import com.runzhan.expo.controller.BaseController;
import com.runzhan.sms.service.SMSSendService;
import com.runzhan.sms.service.SmsTemplateService;
import com.runzhan.sms.service.SmsWebService;
import com.runzhan.sms.vo.SendSmsParamsVo;
import com.runzhan.sms.vo.SmsParams;

@RestController
@RequestMapping("/sms")
public class SmsController extends BaseController {
	Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private SMSSendService smsService;
	@Autowired
	private SmsTemplateService smsTemplateService;
	@Autowired
	private ExpertService expertService;
	@Autowired
	private SmsWebService smsWebService;
	
	
	@RequestMapping(value = "/smsTemplateList")
	public ModelAndView list() {
		ModelAndView mv = new ModelAndView();
		String pjax = request.getHeader("X-PJAX");
		
		if (StringUtils.isNotBlank(pjax) && "true".equals(pjax)) {
			mv.setViewName("sms/smsTemplate-list");
		} else {
			mv.addObject("include", "sms/smsTemplate-list.jsp");
			mv.setViewName("index");
		}
		
		return mv;
	}
	
	@RequestMapping(value = "/editSmsTemplate")
	public ModelAndView toAddsmsTemplate(Long id) {
		ModelAndView mv = new ModelAndView();
		String pjax = request.getHeader("X-PJAX");
		
		if (StringUtils.isNotBlank(pjax) && "true".equals(pjax)) {
			mv.setViewName("sms/edit-smsTemplate");
		} else {
			mv.addObject("include", "sms/edit-smsTemplate.jsp");
			mv.setViewName("index");
		}
		if(null!=id && id>0){
			mv.addObject("smsTemplate", smsTemplateService.getById(id));
		}
		List<TemplateObjectDict> objectDictList = smsTemplateService.getTemplateObjectDict();
		mv.addObject("objectDictList", objectDictList);

		return mv;
	}
	
	@RequestMapping(value = "/saveSmsTemplate", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> saveSmsTemplate(SmsTemplate params) {
		
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		try {
			if (null == params) {
				jsonMap.put("msg", "参数为空！");
				jsonMap.put("flag", "0");
				return jsonMap;
			}
			if (null == params.getId()) {
				smsTemplateService.save(params);
			} else {
				smsTemplateService.updateSmsTemplate(params);
			}
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
			logger.error("保存短信模板失败！", e);
		}
		
		jsonMap.put("flag", "1");
		return jsonMap;
	}
	
	@RequestMapping(value = "/deleteSmsTemplate", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> saveSmsTemplate(SmsParams params) {
		
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		try {
			if (null == params || CollectionUtils.isEmpty(params.getSmsIdList())) {
				jsonMap.put("msg", "参数为空！");
				jsonMap.put("flag", "0");
				return jsonMap;
			}
			smsTemplateService.batchDelete(params.getSmsIdList());
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
			logger.error("删除邮件模板失败！", e);
		}
		
		jsonMap.put("flag", "1");
		return jsonMap;
	}
	
	@RequestMapping(value = "/sendSms", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> sendSms(SendSmsParamsVo params) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		try {
			if (null == params) {
				jsonMap.put("msg", "参数为空！");
				jsonMap.put("flag", "0");
				return jsonMap;
			}
			
			List<Long> expertIds = params.getExpertIds();
			if (!CollectionUtils.isEmpty(expertIds)) {
				List<ExpertInfoVo> expertInfoList = expertService.getExpertInfoList(expertIds);
				if (null != expertInfoList && !CollectionUtils.isEmpty(expertInfoList)) {
					smsWebService.sendSmsToExpertList(expertInfoList, params);
				}
			}
		}catch (Exception e) {
			jsonMap.put("msg", "服务器繁忙，请稍后再试！");
			jsonMap.put("flag", "0");
			logger.error("发送邮件失败！", e);
		}
		
		jsonMap.put("flag", "1");
		return jsonMap;
	}
	
	@RequestMapping(value = "/getSmsTemplateList")
	@ResponseBody
	public List<SmsTemplate> getSmsTemplateList() {
		return smsTemplateService.getSmsTemplateList();
	}
	
}
