package com.runzhan.sms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.runzhan.base.util.BeanMapUtils;
import com.runzhan.expert.vo.ExpertInfoVo;
import com.runzhan.sms.dto.TemplateSmsBean;
import com.runzhan.sms.service.SmsTemplateService;
import com.runzhan.sms.service.SmsWebService;
import com.runzhan.sms.vo.SendSmsParamsVo;

@Service
public class SmsWebServiceImpl implements SmsWebService {
	
	Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private SmsTemplateService smsTemplateService;
	
	@Async

	public void sendSmsToExpertList(List<ExpertInfoVo> expertInfoList,
			SendSmsParamsVo params) {
		for (ExpertInfoVo vo : expertInfoList) {
			if (!StringUtils.isBlank(vo.getMobile())) {
				try {
					TemplateSmsBean templateSmsBean = new TemplateSmsBean();
					templateSmsBean.setTemplate(params.getContent());
					templateSmsBean.setMobiles(new String[]{vo.getMobile()});
					
					Map<String, Object> data = BeanMapUtils.objectToMap(vo);
					templateSmsBean.setData(data);
					
					smsTemplateService.sendTemplateSMS(templateSmsBean);
				} catch (Exception e) {
					logger.error(" 发送邮件 sendSmsToExpertList Exception--params: "+params, e);
				}
			}
		}
	}

}
