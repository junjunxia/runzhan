package com.runzhan.sms.service;

import java.util.List;

import com.runzhan.expert.vo.ExpertInfoVo;
import com.runzhan.sms.vo.SendSmsParamsVo;

public interface SmsWebService {

	void sendSmsToExpertList(List<ExpertInfoVo> expertInfoList, SendSmsParamsVo params);

}
