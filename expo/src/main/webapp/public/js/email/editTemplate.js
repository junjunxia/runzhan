$(function() {
//	showActivityType();
	if(!ChkUtil.isNull(contents)){
		setTimeout(function(){
			$('#summernote').code(contents);
		},1000);
	}
	
});


function check() {
	var name=$("#name").val();
	if(ChkUtil.isNull(name)){
		toastr.error("模板名称不能为空!");
		return false;
	}
	var userType=$("#userType").val();
	if(ChkUtil.isNull(userType)||userType=='0'){
		toastr.error("请选择用户类型!");
		return false;
	}
	var title=$("#title").val();
	if(ChkUtil.isNull(title)){
		toastr.error("邮件标题不能为空!");
		return false;
	}
	var remard=$('#summernote').code();
	if(ChkUtil.isNull(remard)){
		toastr.error("邮件模板内容不能为空!");
		return false;
	}
	
	
	return true;
}

function save(event) {
	// 阻止冒泡
	ChkUtil.stopBubbleEvent(event);
	if (!check()) {
		return;
	}
	$("#content").val($('#summernote').code());
	$.ajax({
		url : ctx + "/email/saveEmailTemplate",
		method :'post',
		data : sy.serialieObject($("#activityAccountForm")),
		async : false,
		dataType : 'json',
		timeout : 60000,
		success : function(data) {
			if(data.flag==1){
				window.location.href='emailTemplate-list';
			}
		}
	});
	window.close();
}

function showActivityType(){
		$("#activityTypeId").empty();
		var url = ctx + "/activityType/list";/* 后台url名 */
		var data = {
			"Time" : new Date().getMilliseconds()
		};/* 参数，可以什么都不写，但为了每次获取不同的数据，习惯上要传一个"时间戳"，后面还可以加你自己的数据，但必须是键值对类型的，如果有多个，用“，”隔开 */
		$.get(url, data, function(result) {/* 回调函数，其中data是从后台返回的html数据 */
			var reasons = eval(result);
			$("#activityTypeId").append("<option value='0'>请选择活动类型</option>");
			for (var i = 0; i < reasons.length; i++) {
				var op = "<option value=" + reasons[i].id + ">"
						+ reasons[i].name + "</option>";
				$("#activityTypeId").append(op);
			}
			var classId= $("#activityTypeId").attr("data-value");
			if(!ChkUtil.isNull(classId)){
				$("#activityTypeId option[value='" + classId + "']").attr("selected",
				"selected");
			}
		});
}


