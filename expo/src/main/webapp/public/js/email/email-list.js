/**
 * Admui v1.0.0 (http://www.admui.com/)
 * Copyright 2015-2017 Admui Team
 * Licensed under the Admui License 1.0 (http://www.admui.com/about/#license)
 */
//(function (document, window, $) {
//    'use strict';



$(function() {
	createEmailTemplateList($('#mailboxTable'), $('#srchForm'));
});

function tj_query() {
//	$('#mailboxTable').empty();
	createEmailTemplateList($('#mailboxTable'), $('#srchForm'));
}

function setUserType(type){
	$("#userType").val(type); 
	$("#name").val('');
	tj_query();
}

function add_emailTemplate() {
	$('#addLabelForm').modal('show');
}


function select_all(obj){
	
	if($(obj).is(':checked')) {
	    // do something
		$(":checkbox[name='emailIdList']").prop('checked',true);
	}else{
		$(":checkbox[name='emailIdList']").prop('checked',false);
	}
}

function format_userType_color(value){
	if (value == 1) {
		return "red-600";
	} else if (value == 2) {
		return "orange-600";
	} else if (value == 3) {
		return "cyan-600";
	} else if (value == 4) {
		return "green-600";
	} else {
		return "";
	}
}

function format_userType_name(value){
	if (value == 1) {
		return "专家";
	} else if (value == 2) {
		return "企业";
	} else if (value == 3) {
		return "媒体";
	} else if (value == 4) {
		return "观众";
	} else {
		return "--";
	}
}


 function createEmailTemplateList(t,srchForm) {//t为table对象
	 $.ajax({
 		url : ctx + "/process",
 		method :'post',
 		data : sy.serialieObject(srchForm),
 		async : false,
 		dataType : 'json',
 		timeout : 60000,
 		success : function(data) {
 			var $wrap = t;
 			var $tbody = $('<tbody></tbody>');

	        $.each(data.rows, function (i, item) {
	        	item.userTypeColor = format_userType_color(item.userType);
	        	item.userTypeName = format_userType_name(item.userType);
	        	//<a data-pjax href="editEmailTemplate?id=' + item.id + '" target="_blank"></a>
	        	$tbody.append('<tr id="' + item.id + '" data-mailbox="slidePanel" >' +
	                '<td class="cell-60">' +
	                '<span class="checkbox-custom checkbox-primary checkbox-lg">' +
	                '<input type="checkbox" class="mailbox-checkbox selectable-item" name="emailIdList" value="' + item.id + '"/>' +
	                '<label for="' + item.id + '"></label>' +
	                '</span>' +
	                '</td>' +
	                '<td>' +
	                '<a data-pjax href="editEmailTemplate?id=' + item.id + '" target="_blank"><div class="content">' +
	                '<div class="title">' + item.name + '</div>' +
	                '<div class="abstract">' + item.title + '</div>' +
	                '</div></a>' +
	                '</td>' +
	                '<td class="cell-130">' +
	                '<div class="identity"><i class="wb-medium-point ' + item.userTypeColor
	            + '" aria-hidden="true"></i>' + item.userTypeName + '</div>' +
	                '</td>' +
	                '</tr>');
	        });
	        $wrap.empty().append($tbody);
 			
	        $('#addlabelForm').modal({
                show: false
            });

            $('#addMailForm').modal({
                show: false
            });
 		}
     });
};

function check(){
	var flag = false;
	$(":checkbox[name='emailIdList']").each(
			function(){
		     if($(this).get(0).checked){
		    	 flag=true;
				}
		   });
	if(!flag){
	   toastr.error("请选择邮件模板!");
	}
	return flag;
}

function deleteEmail(){
	if(!check()){
		return;
	}
	$.ajax({
 		url : ctx + "/email/deleteEmailTemplate",
 		method :'post',
 		data : sy.serialieObject($("#deleteForm")),
 		async : false,
 		dataType : 'json',
 		timeout : 60000,
 		success : function(data) {
 			if(data.flag==1){
 				$('input:checkbox[name=emailIdList]:checked').each(
 						function(){
 							$(this).parent().parent().parent().remove();
 					   });
 				$(":checkbox").prop('checked',false);
 			}
 		}
 	});
}


