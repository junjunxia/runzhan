$(function() {
	if(!ChkUtil.isNull(contents)){
		setTimeout(function(){
			$('#summernote').code(contents);
		},1000);
	}
	
});


function check() {
	var name=$("#name").val();
	if(ChkUtil.isNull(name)){
		toastr.error("模板名称不能为空!");
		return false;
	}
	var userType=$("#userType").val();
	if(ChkUtil.isNull(userType)||userType=='0'){
		toastr.error("请选择用户类型!");
		return false;
	}
	/*var title=$("#title").val();
	if(ChkUtil.isNull(title)){
		toastr.error("邮件标题不能为空!");
		return false;
	}*/
	var remard=$('#summernote').code();
	if(ChkUtil.isNull(remard)){
		toastr.error("邮件模板内容不能为空!");
		return false;
	}
	
	
	return true;
}

function save(event) {
	// 阻止冒泡
	ChkUtil.stopBubbleEvent(event);
	if (!check()) {
		return;
	}
	$("#content").val($('#summernote').code());
	$.ajax({
		url : ctx + "/sms/saveSmsTemplate",
		method :'post',
		data : sy.serialieObject($("#activityAccountForm")),
		async : false,
		dataType : 'json',
		timeout : 60000,
		success : function(data) {
			if(data.flag==1){
				window.location.href='smsTemplateList';
			}
		}
	});
	window.close();
}


