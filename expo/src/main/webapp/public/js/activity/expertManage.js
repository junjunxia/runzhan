$(function() {
	showActivityExpertList();
});


function edit_emailTemplate(id,inviteStatus,topic) {
	$('#addLabelForm').modal('show');
	if('null'!=topic &&!ChkUtil.isNull(topic)){
		$("#topic").val(topic);
	}
	if(inviteStatus>0){
		$(":radio[name='inviteStatus'][value='" + inviteStatus + "']").prop("checked", "checked");
	}
	
	$("#id").val(id);
}

function clear(){
	$("#topic").val('');
	$("#inviteStatus").val('');
	$("#id").val('');
}

function save(){
	$.ajax({
		url : ctx + "/expert/updateExpertTopic",
		method :'post',
		data : sy.serialieObject($("#activityClassForm")),
		async : false,
		dataType : 'json',
		timeout : 60000,
		success : function(data) {
			if(data.flag==1){
//				$("#seniorSearchForm").submit();
				$('#addLabelForm').modal('hide');
				showActivityExpertList();
				$(":checkbox").attr('checked',false);
			}
		}
	});
}



function showActivityExpertList(){
	$.ajax({
		url : ctx + "/expert/toExpertManagerListAjax",
		method :'post',
		data : sy.serialieObject($("#seniorSearchForm")),
		dataType : 'json',
		success : function(data) {
			$("#content").html('');
			if(data.flag==1){
				var html='';
				for (var i = 0; i < data.expertManageList.length; i++) {
					html+=' <tr id="'+data.expertManageList[i].id+'">';
					html+='<th><span class="checkbox-custom checkbox-primary checkbox-lg inline-block vertical-align-bottom" ><input type="checkbox" class="mailbox-checkbox selectable-all" name="expertIds" value="'+data.expertManageList[i].id+'_'+data.expertManageList[i].name+'"><label for="select_all"></label></span></th>';
					html+='<th>'+data.expertManageList[i].name+'</th>';
					if(null==data.expertManageList[i].unitname){
						html+='<th></th>';
					}else{
						if(data.expertManageList[i].unitname.length<25){
							html+='<th>'+(data.expertManageList[i].unitname)+'</th>';
						}else{
							html+='<th>'+(data.expertManageList[i].unitname.substr(0,25))+'...</th>';
						}
					}
					html+='<th>'+data.expertManageList[i].department+'</th>';
					//html+='<th>'+$("#activitytitle").val()+'</th>';
					html+='<th>'+(data.expertManageList[i].topic==null?'':data.expertManageList[i].topic)+'</th>';
					if(data.expertManageList[i].inviteStatus==0){
						html+='<th>未邀请</th>';
					}else if(data.expertManageList[i].inviteStatus==1){
						html+='<th>已邀请</th>';
					}else if(data.expertManageList[i].inviteStatus==2){
						html+='<th>已拒绝</th>';
					}
					html+='<th><div class="item-actions">';
					html+='<span class="btn btn-pure btn-icon btn-edit" data-toggle="modal" data-target="#roleForm" onclick="edit_emailTemplate(\''+data.expertManageList[i].id+'\',\''+data.expertManageList[i].inviteStatus+'\',\''+data.expertManageList[i].topic+'\')"><i class="icon wb-edit" aria-hidden="true"></i></span>';
					html+='</div></th>';
					html+='</tr>';
				}
				$("#content").html(html);
			}
		}
	});
}
