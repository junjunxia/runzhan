$(function() {
	var srchForm = $("#searchForm");
	showActivityClass();
	createActivityDataList($("#activitylist"),srchForm);
});

function tj_query() {
	$("#pageNum").val(1);
	var srchForm = $("#searchForm");
	createActivityDataList($("#activitylist"),srchForm);
}

function tj_querySimple() {
	$("#pageNum").val(1);
	$("#activityTitleValue").val($("#activityTitleInput").val());
	var srchForm = $("#searchForm");
	createActivityDataList($("#activitylist"),srchForm);
}

function change_pageSize() {
	$("#pageSize").val($("#pageSizeSelect").val());
	$("#pageNum").val(1);
	var srchForm = $("#searchForm");
	createActivityDataList($("#activitylist"),srchForm);
}


function deleteActivity(id){
	
	alertify.theme('bootstrap')
    .confirm('你确定要删除吗？', function(){
    	$.ajax({
    		url : ctx + "/activity/delete",
    		method :'post',
    		data : {
    		  id:id
    		},
    		dataType : 'json',
    		timeout : 60000,
    		success : function(data) {
    			if(data.flag==1){
    				$("#"+id).remove();
    			}
    		}});
    });
}
function showActivityClass(){
	$("#activityClassId").empty();
	var url = ctx + "/activityClass/listAjax";/* 后台url名 */
	var data = {
		"Time" : new Date().getMilliseconds()
	};/* 参数，可以什么都不写，但为了每次获取不同的数据，习惯上要传一个"时间戳"，后面还可以加你自己的数据，但必须是键值对类型的，如果有多个，用“，”隔开 */
	$.get(url, data, function(result) {/* 回调函数，其中data是从后台返回的html数据 */
		var reasons = eval(result);
		$("#activityClassId").append("<option value='0'>请选择活动分类</option>");
		for (var i = 0; i < reasons.length; i++) {
			var op = "<option value=" + reasons[i].id + ">"
					+ reasons[i].name + "</option>";
			$("#activityClassId").append(op);
		}
		var classId= $("#activityClassId").attr("data-value");
		if(undefined !=classId && !ChkUtil.isNull(classId)){
			$("#activityClassId option[value='" + classId + "']").attr("selected",
			"selected");
		}
	});
}


function createActivityDataList(t,srchForm) {//t为table对象
	$.ajax({
		url : ctx + "/process",
		method :'post',
		data : sy.serialieObject(srchForm),
		async : false,
		dataType : 'json',
		timeout : 60000,
		success : function(data) {
			t.empty();
			$.each(data.list, function(index, row){
				t.append("<li class='list-group-item' id='"+row.id+"'>"
	                +"<div class='media'>"
	                    +"<div class='media-left'>"
	                        +"<div>"
	                        	+'<a data-pjax href="'+ctx+'/activity/editUI?id='+row.id+'"  target="_blank" style="color: black;text-decoration:none;">'
	                                +' <img style="width:120px;height:120px;" src="'+(ChkUtil.isNull(row.activityImage)?(ctx+'/public/images/portraits/default.jpg'):row.activityImage)+'" alt="...">'
	                                +"<i class='avatar avatar-busy'></i>"
	                            +"</a>"
	                        +"</div>"
	                    +"</div>"
	                    +"<div class='media-body'>"
	                        +"<h4 class='media-heading'>"
	                            +"<font size='5'>"+row.title+"</font>&nbsp;&nbsp;（活动ID:"+row.id+"）"
	                        +"</h4>"
	                        +"<p>"
	                    		+"<i class='icon wb-time' aria-hidden='true'></i>截至时间："+new Date(row.endTime).format("yyyy-MM-dd hh:mm")
	                        	+"&nbsp;&nbsp;<i class='icon wb-map' aria-hidden='true'></i>"+row.address
	                        +"</p>"
	                        +"<div>"
	                        	+"<i class='icon wb-time' aria-hidden='true'></i>报名时间："+new Date(row.beginTime).format("yyyy-MM-dd hh:mm")
	                        	+"&nbsp;&nbsp;-&nbsp;&nbsp;"
	                        	+new Date(row.endTime).format("yyyy-MM-dd hh:mm")
	                        	+"&nbsp;&nbsp;<i class='icon wb-small-point' aria-hidden='true'></i>发布者："+row.createName
	                        	+"&nbsp;&nbsp;<i class='icon wb-small-point' aria-hidden='true'></i>参会人数："+row.expertCount
	                        +"</div>"
	                        +'<div  class="padding-1" style="padding-top:10px" role="group">'
	                          /*  +'<a  data-pjax href="editUI?id='+row.id+'" target="_blank"><button type="button" class="btn btn-outline btn-default">'
	                            	+'<i class="icon topmenu-icon wb-table" aria-hidden="true"></i>活动信息'
	                           +"</button></a>"
	                            +"<button type='button' class='btn btn-outline btn-default'>"
	                            	+'<i class="icon topmenu-icon wb-table" aria-hidden="true"></i>专家管理'
	                            +"</button>"
	                            +"<button type='button' class='btn btn-outline btn-default' onclick='deleteActivity("+row.id+");'>"
	                            	+"<i class='icon wb-trash' aria-hidden='true'></i>删除"
	                            +"</button>"*/
	                            +'<a class="text-action" data-pjax href="'+ctx+'/activity/editUI?id='+row.id+'" target="_blank">'
	            				+'<i class="icon wb-order" aria-hidden="true"></i>活动信息</a>'
	            				+' <a class="text-action"  data-pjax href="'+ctx+'/expert/toExpertManagerList?activityId='+row.id+'" target="_blank">'
	            				+' <i class="icon wb-users" aria-hidden="true"></i>专家管理</a>'
	            				+' <a class="text-action" href="javascript:deleteActivity('+row.id+');">'
	            				+' <i class="icon wb-trash" aria-hidden="true"></i>删除</a>'
	                        +"</div>"
	                    +"</div>"
	                +"</div>"
	            +"</li>");
			});
			
			pg = new showPages('pg',data.pageNum ,data.pages,data.total,data.pageSize);
            pg.printHtml();
		}
	});
};


function goPage(pageNumber) {
    $("#pageNum").val(pageNumber);
    var srchForm = $("#searchForm");
    createActivityDataList($("#activitylist"),srchForm);
}