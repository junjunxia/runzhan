$(function() {
	showActivityType();
	showActivityClass();
	showActivityOrg();
	if(!ChkUtil.isNull(remarks)){
		setTimeout(function(){
			$('#summernote').code(remarks);
		},1000);
	}
	
});

function readFile(){
	var image = $("#stationImg").val();
	if (null!=image && ""!=image) {
		var file = document.querySelector("input[type=file]").files[0];
		if (file.size>300*1024) {
			toastr.error("图片不要超过300k!");
			$("#stationImg").val('');
			  return;
		}
		 // 创建一个FileReader对象
		  var reader = new FileReader();
		  // 绑定load事件
		  reader.onloadend = function(e) {
		      $("#stationImgStr").val(this.result.replace('data:image/jpeg;base64,','').replace('data:image/png;base64,',''));
		 };
		  // 读取File对象的数据
		 reader.readAsDataURL(file);
	}
	
}

function check() {
	var name=$("#name").val();
	if(ChkUtil.isNull(name)){
		toastr.error("活动标题不能为空!");
		return false;
	}
	var activityClassId=$("#activityClassId").val();
	if(ChkUtil.isNull(activityClassId)||activityClassId=='0'){
		toastr.error("请选择活动分类!");
		return false;
	}
	var activityTypeId=$("#activityTypeId").val();
	if(ChkUtil.isNull(activityTypeId)||activityTypeId=='0'){
		toastr.error("请选择活动类型!");
		return false;
	}
	var beginTime=$("#beginTime").val();
	if(ChkUtil.isNull(beginTime)){
		toastr.error("开始时间不能为空!");
		return false;
	}
	var endTime=$("#endTime").val();
	if(ChkUtil.isNull(endTime)){
		toastr.error("结束时间不能为空!");
		return false;
	}
	var city=$("#city").val();
	if(ChkUtil.isNull(city)){
		toastr.error("举办城市不能为空!");
		return false;
	}
	var venue=$("#venue").val();
	if(ChkUtil.isNull(venue)){
		toastr.error("举办地点不能为空!");
		return false;
	}
	var address=$("#address").val();
	if(ChkUtil.isNull(address)){
		toastr.error("详细地址不能为空!");
		return false;
	}
	var orgNames = $("input[data-name='organizationName']");
	if(orgNames.size()>0){
		for(var a=0;a<orgNames.size();a++){
			if(ChkUtil.isNull($(orgNames[a]).val())){
				toastr.error("组织机构名称不能为空!");
				return false; 
			}
		}
	}
	if(ChkUtil.isNull(id)){
		var stationImg=$("#stationImgStr").val();
		if(ChkUtil.isNull(stationImg)){
			toastr.error("活动封面没有上传!");
			return false;
		}
	}
	var remard=$('#summernote').code();
	if(ChkUtil.isNull(remard)){
		toastr.error("活动简介不能为空!");
		return false;
	}
	
	
	return true;
}

function save(event) {
	// 阻止冒泡
	ChkUtil.stopBubbleEvent(event);
	if (!check()) {
		return;
	}
	$("#remarks").val($('#summernote').code());
	var orgNames = $("input[data-name='organizationName']");
	var names = '';
	if(orgNames.size()>0){
		for(var a=0;a<orgNames.size();a++){
			names+=$(orgNames[a]).val()+";";
		}
	}
	var orgtypes = $("select[data-type='organizationType']");
	var types = '';
	if(orgtypes.size()>0){
		for(var j=0;j<orgtypes.size();j++){
			types+=$(orgtypes[j]).val()+";";
		}
	}
	$("#organizationType").val(types);
	$("#organizationName").val(names);
	$.ajax({
		url : ctx + "/activity/save",
		method :'post',
		data : sy.serialieObject($("#activityAccountForm")),
		async : false,
		dataType : 'json',
		timeout : 60000,
		success : function(data) {
			if(data.flag==1){
				window.location.href='list';
			}
		}
	});
	window.close();
}

function showActivityType(){
		$("#activityTypeId").empty();
		var url = ctx + "/activityType/list";/* 后台url名 */
		var data = {
			"Time" : new Date().getMilliseconds()
		};/* 参数，可以什么都不写，但为了每次获取不同的数据，习惯上要传一个"时间戳"，后面还可以加你自己的数据，但必须是键值对类型的，如果有多个，用“，”隔开 */
		$.get(url, data, function(result) {/* 回调函数，其中data是从后台返回的html数据 */
			var reasons = eval(result);
			$("#activityTypeId").append("<option value='0'>请选择活动类型</option>");
			for (var i = 0; i < reasons.length; i++) {
				var op = "<option value=" + reasons[i].id + ">"
						+ reasons[i].name + "</option>";
				$("#activityTypeId").append(op);
			}
			var classId= $("#activityTypeId").attr("data-value");
			if(!ChkUtil.isNull(classId)){
				$("#activityTypeId option[value='" + classId + "']").attr("selected",
				"selected");
			}
		});
}

function showActivityClass(){
	$("#activityClassId").empty();
	var url = ctx + "/activityClass/listAjax";/* 后台url名 */
	var data = {
		"Time" : new Date().getMilliseconds()
	};/* 参数，可以什么都不写，但为了每次获取不同的数据，习惯上要传一个"时间戳"，后面还可以加你自己的数据，但必须是键值对类型的，如果有多个，用“，”隔开 */
	$.get(url, data, function(result) {/* 回调函数，其中data是从后台返回的html数据 */
		var reasons = eval(result);
		$("#activityClassId").append("<option value='0'>请选择活动分类</option>");
		for (var i = 0; i < reasons.length; i++) {
			var op = "<option value=" + reasons[i].id + ">"
					+ reasons[i].name + "</option>";
			$("#activityClassId").append(op);
		}
		var classId= $("#activityClassId").attr("data-value");
		if(!ChkUtil.isNull(classId)){
			$("#activityClassId option[value='" + classId + "']").attr("selected",
			"selected");
		}
	});
}
function deleteSelect(obj){
	$(obj).parent().remove();
}

function addSelect(){
	var html='';
	html+='<div class="form-group">';
	html+='<label class="col-sm-3 control-label"></label>';
	html+='<div class="col-sm-3">';
	html+='<select data-type="organizationType"   class="form-control">';
	html+='<option value="1" selected>主办方</option>';
	html+='<option value="2" >承办方</option>';
	html+='</select>';
	html+='</div>';
	html+='<div class="col-sm-5">';
	html+='<input type="text" class="form-control" data-name="organizationName" >';
	html+='</div>';
	html+='<button type="button" class="btn btn-pure btn-default btn-sm icon fa-trash-o pull-right delete-submenu" onclick="deleteSelect(this)"></button>';
	html+='</div>';
	$(html).insertBefore($("#insertIndex"));
}

function showActivityOrg(){
	if(ChkUtil.isNull(id)){
		return;
	}
	
	var url = ctx + "/activity/orglist";/* 后台url名 */
	var data = {
			id:id
	};/* 参数，可以什么都不写，但为了每次获取不同的数据，习惯上要传一个"时间戳"，后面还可以加你自己的数据，但必须是键值对类型的，如果有多个，用“，”隔开 */
	$.get(url, data, function(result) {/* 回调函数，其中data是从后台返回的html数据 */
		var reasons = eval(result);
		var html='';
		if(reasons.length>0){
			$("#insertIndex").prev().remove();
			$("#insertIndex").prev().remove();
		}
		for (var i = 0; i < reasons.length; i++) {
			html+='<div class="form-group">';
			html+='<label class="col-sm-3 control-label"></label>';
			html+='<div class="col-sm-3">';
			html+='<select data-type="organizationType"   class="form-control">';
			html+='<option value="1"';
			if(reasons[i].orgType==1){
				html+="selected";
			}
			html+='>主办方</option>';
			html+='<option value="2"';
			if(reasons[i].orgType==2){
				html+="selected";
			}
			html+='>承办方</option>';
			html+='</select>';
			html+='</div>';
			html+='<div class="col-sm-5">';
			html+='<input type="text" class="form-control" data-name="organizationName" value='+(undefined==reasons[i].name?'':reasons[i].name)+' >';
			html+='</div>';
			html+='<button type="button" class="btn btn-pure btn-default btn-sm icon fa-trash-o pull-right delete-submenu" onclick="deleteSelect(this)"></button>';
			html+='</div>';
		}
		$(html).insertBefore($("#insertIndex"));
	});
	
}

