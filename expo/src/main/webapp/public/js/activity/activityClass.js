function edit_emailTemplate(id,name) {
	$('#addLabelForm').modal('show');
	if(id>0){
		$("#name").val(name);
		$("#id").val(id);
	}
}

function clear(){
	$("#name").val('');
	$("#id").val('');
}

function save(){
	if(ChkUtil.isNull($("#name").val())){
		return;
	}
	$.ajax({
		url : ctx + "/activityClass/save",
		method :'post',
		data : sy.serialieObject($("#activityClassForm")),
		async : false,
		dataType : 'json',
		timeout : 60000,
		success : function(data) {
			if(data.flag==1){
				window.location.href='list';
			}
		}
	});
}
function deleteActivity(id){
	
	alertify.theme('bootstrap')
    .confirm('你确定要删除吗？', function(){
    	$.ajax({
    		url : ctx + "/activityClass/delete",
    		method :'post',
    		data : {
    		  id:id
    		},
    		dataType : 'json',
    		timeout : 60000,
    		success : function(data) {
    			if(data.flag==1){
    				$("#"+id).remove();
    			}
    		}});
    });
}