$(function() {
//	 $('.rating').raty({
//		 click: function(score, evt) {
//			    alert(score);
//			  }
//	 });
//	showExpertDomain();
	showToInviteExpertList();
	
});



function showExpertDomain(){
	$("#domainId").empty();
	var url = ctx + "/expert/domainList";/* 后台url名 */
	var data = {
		"Time" : new Date().getMilliseconds()
	};/* 参数，可以什么都不写，但为了每次获取不同的数据，习惯上要传一个"时间戳"，后面还可以加你自己的数据，但必须是键值对类型的，如果有多个，用“，”隔开 */
	$.get(url, data, function(result) {/* 回调函数，其中data是从后台返回的html数据 */
		var reasons = eval(result);
		$("#domainId").append("<option value='0'>请选择所属领域</option>");
		for (var i = 0; i < reasons.length; i++) {
			var op = "<option value=" + reasons[i].id + ">"
					+ reasons[i].name + "</option>";
			$("#domainId").append(op);
		}
		var classId= $("#domainId").attr("data-domainId");
		if(!ChkUtil.isNull(classId)){
			$("#domainId option[value='" + classId + "']").attr("selected",
			"selected");
		}
	});
}


function checkAll(obj){
	
	if($(obj).is(':checked')) {
	    // do something
		$(":checkbox[name='expertIdList']").prop('checked',true);
	}else{
		$(":checkbox[name='expertIdList']").prop('checked',false);
	}
}

function check(){
	var flag = false;
	$(":checkbox[name='expertIdList']").each(
			function(){
		     if($(this).get(0).checked){
		    	 flag=true;
				}
		   });
	if(!flag){
		toastr.error("请选择专家!");
	}
	     return flag;
}

function invite(){
	
	if(!check()){
		return;
	}
     $.ajax({
 		url : ctx + "/expert/saveInviterExpert",
 		method :'post',
 		data : sy.serialieObject($("#activityForm")),
 		async : false,
 		dataType : 'json',
 		timeout : 60000,
 		success : function(data) {
 			if(data.flag==1){
 				toastr.success("邀请成功");
 				$(":checkbox[name='expertIdList']").each(
 						function(){
 					     if($(this).get(0).checked){
 					    	$(this).parent().parent().parent().remove();
 							}
 					     
 					   });
 				$(":checkbox").attr('checked',false);
// 				$("#seniorSearchForm").submit();
 			}
 		}
 	});
     
}


function showToInviteExpertList(){
	$.ajax({
		url : ctx + "/expert/toAddExpertListAjax",
		method :'post',
		data : sy.serialieObject($("#seniorSearchForm")),
		dataType : 'json',
		success : function(data) {
			$("#content").html('');
			if(data.flag==1){
				
				for (var i = 0; i < data.expertList.length; i++) {
					var html='';
					html+=' <tr id="'+data.expertList[i].id+'">';
					html+='<th><span class="checkbox-custom checkbox-primary user-select-all" ><input type="checkbox" class="mailbox-checkbox selectable-all"  name="expertIdList" value="'+data.expertList[i].id+'"><label for="select_all"></label></span></th>';
					html+='<th>'+(null==data.expertList[i].name?'':data.expertList[i].name)+'</th>';
					if(null==data.expertList[i].unitname){
						html+='<th></th>';
					}else{
						if(data.expertList[i].unitname.length<15){
							html+='<th>'+(data.expertList[i].unitname)+'</th>';
						}else{
							html+='<th>'+(data.expertList[i].unitname.substr(0,15))+'...</th>';
						}
					}
					html+='<th>'+(null==data.expertList[i].department?'':data.expertList[i].department)+'</th>';
					//html+='<th>'+(null==data.expertList[i].title?'':data.expertList[i].title)+'</th>';
					if(null==data.expertList[i].title){
						html+='<th></th>';
					}else{
						if(data.expertList[i].title.length<15){
							html+='<th>'+(data.expertList[i].title)+'</th>';
						}else{
							html+='<th>'+(data.expertList[i].title.substr(0,15))+'...</th>';
						}
					}					
					
					//html+='<th>'+(null==data.expertList[i].domainName?'':data.expertList[i].domainName)+'</th>';
					if(null==data.expertList[i].domainName){
						html+='<th></th>';
					}else{
						if(data.expertList[i].domainName.length<15){
							html+='<th>'+(data.expertList[i].domainName)+'</th>';
						}else{
							html+='<th>'+(data.expertList[i].domainName.substr(0,15))+'...</th>';
						}
					}
					//html+='<th>'+(null==data.expertList[i].socialCircle?'':data.expertList[i].socialCircle)+'</th>';
					html+='<th><div class="rating" id="rating'+i+'"  data-score="'+data.expertList[i].score+'"  data-plugin="rating" ></th>';
					html+='</tr>';
					$("#content").append(html);
					$.fn.raty.defaults.path = ctx+'/public/vendor/raty/images';
					$('#rating'+i).raty({score:data.expertList[i].score});
				}
				
			}
		}
	});
}
