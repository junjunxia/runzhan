$(function() {
//	 $('.rating').raty({
//		 click: function(score, evt) {
//			    alert(score);
//			  }
//	 });
	
});

function readFile(){
	var image = $("#stationImg").val();
	if (null!=image && ""!=image) {
		var file = document.querySelector("input[type=file]").files[0];
		if (file.size>50*1024) {
			toastr.error("图片不要超过50k!");
			$("#stationImg").val('');
			  return;
		}
		 // 创建一个FileReader对象
		  var reader = new FileReader();
		  // 绑定load事件
		  reader.onloadend = function(e) {
		      $("#stationImgStr").val(this.result.replace('data:image/jpeg;base64,','').replace('data:image/png;base64,',''));
		 };
		  // 读取File对象的数据
		 reader.readAsDataURL(file);
	}
	
}

function check() {
	return true;
}

function save(event) {
	// 阻止冒泡
	ChkUtil.stopBubbleEvent(event);
	if (!check()) {
		return;
	}
	$("#resume").val($('#summernote').code());
	$.ajax({
		url : ctx + "/expert/save",
		method :'post',
		data : sy.serialieObject($("#exampleAccountForm")),
		async : false,
		dataType : 'json',
		timeout : 60000,
		success : function(data) {
			if(data.flag==1){
				$("#expertId").val(data.expertId);
				$.ajax({
					url : ctx + "/expert/saveExpertLinkMan",
					method :'post',
					data : sy.serialieObject($("#exampleContactForm")),
					async : false,
					dataType : 'json',
					timeout : 60000,
					success : function(data) {
						if(data.flag==1){
							toastr.success("提交成功");
							window.location.href='list';
						}
					}
				});
			}
		}
	});
	window.close();
}