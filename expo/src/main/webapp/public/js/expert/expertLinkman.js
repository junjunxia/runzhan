$(function() {
	showExpertLinkManList();
});
function edit_emailTemplate(id,name,unitname,department,title,mobile,phone,fax,unitAddr,email,expertId) {
	$('#addLabelForm').modal('show');
	$("#activityClassForm")[0].reset();
	if('null'!=name &&!ChkUtil.isNull(name)){
		$("#name").val(name);
	}
	if('null'!=unitname &&!ChkUtil.isNull(unitname)){
		$("#unitname").val(unitname);
	}
	if('null'!=department &&!ChkUtil.isNull(department)){
		$("#department").val(department);
	}
	if('null'!=title &&!ChkUtil.isNull(title)){
		$("#title").val(title);
	}
	if('null'!=mobile &&!ChkUtil.isNull(mobile)){
		$("#mobile").val(mobile);
	}
	if('null'!=phone &&!ChkUtil.isNull(phone)){
		$("#phone").val(phone);
	}
	if('null'!=fax &&!ChkUtil.isNull(fax)){
		$("#fax").val(fax);
	}
	if('null'!=unitAddr &&!ChkUtil.isNull(unitAddr)){
		$("#unitAddr").val(unitAddr);
	}
	if('null'!=email &&!ChkUtil.isNull(email)){
		$("#email").val(email);
	}
	$("#expertId").val(expertId);
	$("#id").val(id);
}
function add_emailTemplate(expertId) {
	$('#addLabelForm').modal('show');
	$("#activityClassForm")[0].reset();
	$("#id").val('');
	$("#expertId").val(expertId);
}

function clear(){
	$("#name").val('');
	$("#id").val('');
}

function save(id){
	if(ChkUtil.isNull($("#name").val())){
		return;
	}
	$.ajax({
		url : ctx + "/expert/saveExpertLinkMan",
		method :'post',
		data : sy.serialieObject($("#activityClassForm")),
		async : false,
		dataType : 'json',
		timeout : 60000,
		success : function(data) {
			if(data.flag==1){
				$("#id").val('');
				$("#activityClassForm")[0].reset();
				$('#addLabelForm').modal('hide');
				showExpertLinkManList();
			}
			else
			{
				toastr.success("提交失败");
			}
		}
	});
}
function deleteActivity(id){
	
	alertify.theme('bootstrap')
    .confirm('你确定要删除吗？', function(){
    	$.ajax({
    		url : ctx + "/expert/deleteExpertLinkMan",
    		method :'post',
    		data : {
    		expertLinkManId:id
    		},
    		dataType : 'json',
    		timeout : 60000,
    		success : function(data) {
    			if(data.flag==1){
    				$("#"+id).remove();
    			}
    		}});
    });
}



function showExpertLinkManList(){
	$.ajax({
		url : ctx + "/expert/toExpertLinkManListAjax",
		method :'post',
		data : sy.serialieObject($("#seniorSearchForm")),
		dataType : 'json',
		success : function(data) {
			$("#content").html('');
			if(data.flag==1){
				var html='';
				for (var i = 0; i < data.expertLinkManList.length; i++) {
					html+=' <tr id="'+data.expertLinkManList[i].id+'">';
					html+='<th>'+(null==data.expertLinkManList[i].name?'':data.expertLinkManList[i].name)+'</th>';
					html+='<th>'+(null==data.expertLinkManList[i].unitname?'':data.expertLinkManList[i].unitname)+'</th>';
					html+='<th>'+(null==data.expertLinkManList[i].department?'':data.expertLinkManList[i].department)+'</th>';
					html+='<th>'+(null==data.expertLinkManList[i].title?'':data.expertLinkManList[i].title)+'</th>';
					html+='<th>'+(null==data.expertLinkManList[i].mobile?'':data.expertLinkManList[i].mobile)+'</th>';
					html+='<th>'+(null==data.expertLinkManList[i].phone?'':data.expertLinkManList[i].phone)+'</th>';
					html+='<th>'+(null==data.expertLinkManList[i].fax?'':data.expertLinkManList[i].fax)+'</th>';
					html+='<th>'+(null==data.expertLinkManList[i].unitAddr?'':data.expertLinkManList[i].unitAddr)+'</th>';
					html+='<th>'+(null==data.expertLinkManList[i].email?'':data.expertLinkManList[i].email)+'</th>';
					html+='<th><div class="item-actions">';
					html+='<span class="btn btn-pure btn-icon btn-edit" data-toggle="modal" data-target="#roleForm" onclick="edit_emailTemplate(\''+data.expertLinkManList[i].id+'\',\''+data.expertLinkManList[i].name+'\',\''+data.expertLinkManList[i].unitname+'\',\''+data.expertLinkManList[i].department+'\',\''+data.expertLinkManList[i].title+'\',\''+data.expertLinkManList[i].mobile+'\',\''+data.expertLinkManList[i].phone+'\',\''+data.expertLinkManList[i].fax+'\',\''+data.expertLinkManList[i].unitAddr+'\',\''+data.expertLinkManList[i].email+'\',\''+$("#expertIds").val()+'\')"><i class="icon wb-edit" aria-hidden="true"></i></span>';
					html+='<span class="btn btn-pure btn-icon" data-tag="list-delete" onclick="deleteActivity('+data.expertLinkManList[i].id+')"><i class="icon wb-close" aria-hidden="true"></i></span>';
					html+='</div></th>';
					html+='</tr>';
				}
				$("#content").html(html);
			}
		}
	});
}