	$(function() {
	var srchForm = $("#searchForm");
//	$.fn.raty.defaults.path = ctx+'/public/vendor/raty/images';
	createExpertDataList($("#expertlist"),srchForm);
	//showExpertDomain();
});

function showModExpertDomain(){
	$('select[name="domainId"]').empty();
	var url = ctx + "/expert/domainList";/* 后台url名 */
	var data = {
		"Time" : new Date().getMilliseconds()
	};/* 参数，可以什么都不写，但为了每次获取不同的数据，习惯上要传一个"时间戳"，后面还可以加你自己的数据，但必须是键值对类型的，如果有多个，用“，”隔开 */
	$.get(url, data, function(result) {/* 回调函数，其中data是从后台返回的html数据 */
		var reasons = eval(result);
		$('select[name="domainId"]').append("<option value='0'>请选择所属行业</option>");
		for (var i = 0; i < reasons.length; i++) 
		{
			var op = "<option value=" + reasons[i].id + ">"
					+ reasons[i].name + "</option>";
			//$("#domainId").append(op);
			$('select[name="domainId"]').append(op);			
		}
		var domainId= $("#domainId").attr("data-value");
		if(undefined !=domainId && !ChkUtil.isNull(domainId)){
			$("#domainId option[value='" + domainId + "']").attr("selected",
			"selected");
		}
	});
}
	
function showExpertDomain(){
	$('select[name="domainId"]').empty();
	var url = ctx + "/expert/domainList";/* 后台url名 */
	var data = {
		"Time" : new Date().getMilliseconds()
	};/* 参数，可以什么都不写，但为了每次获取不同的数据，习惯上要传一个"时间戳"，后面还可以加你自己的数据，但必须是键值对类型的，如果有多个，用“，”隔开 */
	$.get(url, data, function(result) {/* 回调函数，其中data是从后台返回的html数据 */
		var reasons = eval(result);
		$('select[name="domainId"]').append("<option value='0'>请选择所属行业</option>");
		for (var i = 0; i < reasons.length; i++) 
		{
			var op = "<option value=" + reasons[i].id + ">"
					+ reasons[i].name + "</option>";
			//$("#domainId").append(op);
			$('select[name="domainId"]').append(op);			
		}
	});
}

function tj_query() {
	$("#pageNum").val(1);
	var srchForm = $("#searchForm");
	createExpertDataList($("#expertlist"),srchForm);
}


function goPage(pageNumber) {
    $("#pageNum").val(pageNumber);
    var srchForm = $("#searchForm");
	createExpertDataList($("#expertlist"),srchForm);
}

function quickQuery() {
    $("#pageNum").val(1);
    var srchForm = $("#searchForm");
    $('#quickey').val($('#search').val());
    createExpertDataList($("#expertlist"),srchForm);
}
function createExpertDataList(t,srchForm) {//t为table对象
	t.html('');
	$.ajax({
		url : ctx + "/process",
		method :'post',
		data : sy.serialieObject(srchForm),
		async : false,
		dataType : 'json',
		timeout : 60000,
		success : function(data) {
			$.each(data.list, function(index, row){
				t.append(
						'<li class="list-group-item" id="'+row.id+'">'
                        +'<div class="media">'
                          +'<div class="media-left">'
                             +'<div width="20" data-order="false">'
                             +'<span class="checkbox-custom checkbox-primary user-select-all">'
                             +'<input type="checkbox" class="user-checkbox selectable-all" name="expertIds" value="'+row.id+'_'+row.name+'">'
                             +' <label></label>'
                             +'</span>'
                             +'</div>'
                             +'<div class="avatar avatar-online" style="width:60px;">'
                             +'<a data-pjax href="'+ctx+'/expert/detail?id='+row.id+'" target="_blank" style=\'color: black;text-decoration:none;\'>'
               /*  if(ChkUtil.isNull()){
                	 +' <img src="'+ctx+'/public/images/portraits/13.jpg" alt="...">'
                 }else{
                	 +' <img src="'+ctx+row.image+'" alt="...">'
                 }   */         
			   +' <img src="'+(null==row.image||ChkUtil.isNull(row.image)?(ctx+'/public/images/portraits/default.jpg'):row.image)+'" alt="...">'
				//+'<i class="avatar avatar-busy"></i>'
				+'</a>'
				+' </div>'
				+'</div>'
				+' <div class="media-body">'
				+'<a data-pjax href="'+ctx+'/expert/detail?id='+row.id+'"  target="_blank" style=\'color: black;text-decoration:none;\'>'
				+'<h4 class="media-heading">'
				+'<font size="5">'+row.name+'&nbsp;&nbsp;</font>'
				+''+row.title+'&nbsp;&nbsp; <span id="rating'+row.id+'"  data-score="'+row.score+'" data-plugin="rating" class="rating" style="cursor:pointer"></span>'
				+'</h4>'
				+' </a>'
				+' <h5><i class="icon wb-home" aria-hidden="true"></i>'+(null==row.unitname||ChkUtil.isNull(row.unitname)?'':row.unitname)+'</h5>'
				+' <div ><i class="icon wb-small-point" aria-hidden="true"></i>受邀次数: '+row.inviteCount
				+ ' &nbsp;&nbsp;<i class="icon wb-small-point" aria-hidden="true"></i>参会次数:'+row.attendCount
				+ ' &nbsp;&nbsp;<i class="icon wb-small-point" aria-hidden="true"></i>发布人:'+row.createName
				+ ' &nbsp;&nbsp;<i class="icon wb-small-point" aria-hidden="true"></i>发布时间:'+row.createTime+'</div>'
				+' <div class="padding-1" style="padding-top:10px">'
			/*	+' <a class="text-action" href="javascript:;" onclick="sendPersonalEmail(\''+row.id+'_'+row.name+'\')">'
				+'<i class="icon wb-envelope" aria-hidden="true"></i>发送邮件</a>'
				+'<a class="text-action" href="javascript:;" onclick="sendPersonalSms(\''+row.id+'_'+row.name+'\')">'
				+'<i class="icon icon-color wb-mobile" aria-hidden="true"></i>发送短信</a>'*/
				+'<a class="text-action" data-pjax href="'+ctx+'/expert/editUI?id='+row.id+'" target="_blank">'
				+'<i class="icon wb-pencil" aria-hidden="true"></i>编辑信息</a>'
				+' <a class="text-action"  data-pjax href="'+ctx+'/activity/toActivityManagerList?id='+row.id+'" target="_blank">'
				+' <i class="icon topmenu-icon wb-table" aria-hidden="true"></i>活动管理</a>'
				+'<a class="text-action"  data-pjax href="'+ctx+'/expert/toExpertLinkManList?id='+row.id+'" target="_blank">'
				+' <i class="icon wb-user-add" aria-hidden="true"></i>联系人管理</a>'
				+' <a class="text-action" name="deleteid" href="javascript:deleteExpert('+row.id+');">'
				+' <i class="icon wb-trash" aria-hidden="true"></i>删除</a>'
				+'</div>'
				+' </div>'
				+' <div class="media-right">'
				+'<div style="width: 300px;">'				
				/*+'<h5>所属领域：'+(null==row.domainId||ChkUtil.isNull(row.domainId)?'':row.domainId)+'</h5>'*/
				+'<h5>所属行业：'+(null==row.domainIdName||ChkUtil.isNull(row.domainIdName)?'':row.domainIdName)+'</h5>'
				+'<h5>行业领域：'+(null==row.domainName||ChkUtil.isNull(row.domainName)?'':row.domainName)+'</h5>'
				+'</div>'
				+'</div>'
				+' </div>'
				+'</li>'
	            +"</li>");
				$.fn.raty.defaults.path = ctx+'/public/vendor/raty/images';
				$('#rating'+row.id).raty({score:row.score});
			});
			
			if (roleType != 1 )
			{
				var divs = document.getElementsByName('deleteid');
				for (var i=0;i<divs.length;i++) 
				{
					divs[i].style.display = 'none';
				}
			}

			$("#datatotal").html("总条目数："+data.total+"&nbsp;&nbsp;条&nbsp;&nbsp;");
			pg = new showPages('pg',data.pageNum ,data.pages,data.total,data.pageSize);
            pg.printHtml();
			
		}
	});
};

function deleteExpert(id){
	
	alertify.theme('bootstrap')
    .confirm('你确定要删除吗？', function(){
    	$.ajax({
    		url : ctx + "/expert/delete",
    		method :'post',
    		data : {
    		  id:id
    		},
    		dataType : 'json',
    		timeout : 60000,
    		success : function(data) {
    			if(data.flag==1){
    				$("#"+id).remove();
    			}
    		}});
    });
}
