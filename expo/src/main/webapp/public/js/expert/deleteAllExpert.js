function deleteAllExpert() {
	if(!check()){
		return;
	}
	
	//获取对应的IDS
	var ids = '';
	$('input:checkbox[name=expertIds]:checked').each(
			function(){
				ids+=$(this).val().split('_')[0]+',';
		   });
	ids=ids.substring(0,ids.length-1);	
	alertify.theme('bootstrap')
    .confirm('你确定要删除吗？', function(){
    	$.ajax({
    		url : ctx + "/expert/batchDelete",
    		method :'post',
    		data : {
    		 ids:ids
    		},
    		dataType : 'json',
    		timeout : 60000,
    		success : function(data) {
    			var deletIDS = ids.split(',');
    			for(var i = 0;i < deletIDS.length; i++) 
    			{
    				$("#"+deletIDS[i]).remove();
    			}  
    		}});
    });
}

function check(){
	var flag = false;
	$(":checkbox[name='expertIds']").each(
			function(){
		     if($(this).get(0).checked){
		    	 flag=true;
				}
		   });
	if(!flag){
	   toastr.error("请选择专家!");
	}
	return flag;
}

