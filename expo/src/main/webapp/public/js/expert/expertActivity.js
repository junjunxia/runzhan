$(function() {
	showActivityList();
});

function edit_emailTemplate(id,title,inviter,topic,beginTime,endTime,venue,remarks,expertId) {
	$('#addLabelForm').modal('show');
	$("#activityForm")[0].reset();
	if('null'!=title &&!ChkUtil.isNull(title)){
		$("#title").val(title);
	}
//	$("#title").attr('disabled',true);
	if('null'!=inviter &&!ChkUtil.isNull(inviter)){
		$("#inviter").val(inviter);
	}
	if('null'!=topic &&!ChkUtil.isNull(topic)){
		$("#topic").val(topic);
	}
	$("#beginTime").val(beginTime);
	$("#endTime").val(endTime);
	if('null'!=venue &&!ChkUtil.isNull(venue)){
		$("#venue").val(venue);
	}
	if('null'!=remarks &&!ChkUtil.isNull(remarks)){
		$("#remarks").val(remarks);
	}
	$("#expertId").val(expertId);
	$("#id").val(id);
	showActivityOrg(id);
}
function add_emailTemplate(expertId) {
	$('#addLabelForm').modal('show');
//	$("#title").attr('disabled',false);
	$("#activityForm")[0].reset();
	$("input[data-name='organizationName']").val('');
	$("#expertId").val(expertId);
	$("#id").val('');
}

function showActivityOrg(id){
	$("#insertIndex").prev().remove();
	$("#insertIndex").prev().remove();
	var url = ctx + "/activity/orglist";/* 后台url名 */
	var data = {
			id:id
	};/* 参数，可以什么都不写，但为了每次获取不同的数据，习惯上要传一个"时间戳"，后面还可以加你自己的数据，但必须是键值对类型的，如果有多个，用“，”隔开 */
	$.get(url, data, function(result) {/* 回调函数，其中data是从后台返回的html数据 */
		var reasons = eval(result);
		var html='';
		for (var i = 0; i < reasons.length; i++) {
			html+='<div class="form-group">';
			html+='<label class="col-sm-2 control-label">';
			if(i==0){
				html+='组织机构';	
			}
			html+='</label>';
			html+='<div class="col-sm-3">';
			html+='<select data-type="organizationType"   class="form-control">';
			html+='<option value="1"';
			if(reasons[i].orgType==1){
				html+="selected";
			}
			html+='>主办方</option>';
			html+='<option value="2"';
			if(reasons[i].orgType==2){
				html+="selected";
			}
			html+='>承办方</option>';
			html+='</select>';
			html+='</div>';
			html+='<div class="col-sm-5">';
			html+='<input type="text" class="form-control" data-name="organizationName" value='+reasons[i].name+' >';
			html+='</div>';
			html+='<button type="button" class="btn btn-pure btn-default btn-sm icon fa-trash-o pull-right delete-submenu" onclick="deleteSelect(this)"></button>';
			html+='</div>';
		}
		$(html).insertBefore($("#insertIndex"));
	});
}


function clear(){
	$("#name").val('');
	$("#id").val('');
}

function save(id){
	if(ChkUtil.isNull($("#title").val())){
		toastr.error("活动名称不能为空!");
		return;
	}
	var orgNames = $("input[data-name='organizationName']");
	var names = '';
	if(orgNames.size()>0){
		for(var a=0;a<orgNames.size();a++){
			names+=$(orgNames[a]).val()+";"
		}
	}
	var orgtypes = $("select[data-type='organizationType']");
	var types = '';
	if(orgtypes.size()>0){
		for(var j=0;j<orgtypes.size();j++){
			types+=$(orgtypes[j]).val()+";"
		}
	}
	$("#organizationType").val(types);
	$("#organizationName").val(names);
	$.ajax({
		url : ctx + "/activity/saveActivityOut",
		method :'post',
		data : sy.serialieObject($("#activityForm")),
		async : false,
		dataType : 'json',
		timeout : 60000,
		success : function(data) {
			if(data.flag==1){
//				window.location.href=ctx+'/activity/toActivityManagerList?id='+id;
				$("#activityForm")[0].reset();
				$('#addLabelForm').modal('hide');
				$("#title").val('');
				$("#inviter").val('');
				$("#topic").val('');
				$("#beginTime").val('');
				$("#endTime").val('');
				$("#venue").val('');
				$("#remarks").val('');
				$("#id").val('');
				showActivityList();
			}
		}
	});
}
function deleteActivity(id,type){
	
	alertify.theme('bootstrap')
    .confirm('你确定要删除吗？', function(){
    	$.ajax({
    		url : ctx + "/activity/deleteActivity",
    		method :'post',
    		data : {
    		activityId:id,
    		activityTag:type
    		},
    		dataType : 'json',
    		timeout : 60000,
    		success : function(data) {
    			if(data.flag==1){
    				$("#"+id).remove();
    			}
    		}});
    });
}


function deleteSelect(obj){
	$(obj).parent().remove();
}

function addSelect(){
	var html='';
	html+='<div class="form-group">';
	html+='<label class="col-sm-2 control-label"></label>';
	html+='<div class="col-sm-3">';
	html+='<select data-type="organizationType"   class="form-control">';
	html+='<option value="1" selected>主办方</option>';
	html+='<option value="2" >承办方</option>';
	html+='</select>';
	html+='</div>';
	html+='<div class="col-sm-5">';
	html+='<input type="text" class="form-control" data-name="organizationName" >';
	html+='</div>';
	html+='<button type="button" class="btn btn-pure btn-default btn-sm icon fa-trash-o pull-right delete-submenu" onclick="deleteSelect(this)"></button>';
	html+='</div>';
	$(html).insertBefore($("#insertIndex"));
}





function showActivityList(){
	$.ajax({
		url : ctx + "/activity/toActivityManagerListAjax",
		method :'post',
		data : sy.serialieObject($("#activityFormquery")),
		dataType : 'json',
		success : function(data) {
			$("#content").html('');
			if(data.flag==1){
				var html='';
				for (var i = 0; i < data.activityManageList.length; i++) {
					html+=' <tr id="'+data.activityManageList[i].id+'">';
					html+='<th>'+(i+1)+'</th>';
					if(data.activityManageList[i].title.length>10){
						html+='<th>'+data.activityManageList[i].title.substr(0,20)+'.....</th>';
					}else{
						html+='<th>'+data.activityManageList[i].title+'</th>';
					}
					
					html+='<th>'+data.activityManageList[i].sponsor+'</th>';
					html+='<th>'+data.activityManageList[i].organizer+'</th>';
					html+='<th>'+(null==data.activityManageList[i].inviter?'':data.activityManageList[i].inviter)+'</th>';
					html+='<th>'+data.activityManageList[i].topic+'</th>';
					html+='<th>'+ChkUtil.formatDate2(data.activityManageList[i].beginTime)+'</th>';
					html+='<th>'+(null==data.activityManageList[i].venue?'':data.activityManageList[i].venue)+'</th>';
//					html+='<th>'+(null==data.activityManageList[i].remarks?'':data.activityManageList[i].remarks)+'</th>';
					html+='<th><div class="item-actions">';
					html+='<span class="btn btn-pure btn-icon btn-edit" data-toggle="modal" data-target="#roleForm" onclick="edit_emailTemplate(\''+data.activityManageList[i].id+'\',\''+data.activityManageList[i].title+'\',\''+data.activityManageList[i].inviter+'\',\''+data.activityManageList[i].topic+'\',\''+ChkUtil.formatDate2(data.activityManageList[i].beginTime)+'\',\''+ChkUtil.formatDate2(data.activityManageList[i].endTime)+'\',\''+data.activityManageList[i].venue+'\',\''+data.activityManageList[i].remarks+'\',\''+$("#expertIds").val()+'\')"><i class="icon wb-edit" aria-hidden="true"></i></span>';
					html+='<span class="btn btn-pure btn-icon" data-tag="list-delete" onclick="deleteActivity('+data.activityManageList[i].id+',\''+data.activityManageList[i].type+'\')"><i class="icon wb-close" aria-hidden="true"></i></span>';
					html+='</div></th>';
					html+='</tr>';
				}
				$("#content").html(html);
			}
		}
	});
}