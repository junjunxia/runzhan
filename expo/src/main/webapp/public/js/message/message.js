$(function() {
	$("#emailTemplatesList").bind("change",function(){
        //获取被选中的option的值
//        var miaoshu = $(this).val();
        //获取自定义属性的值
        var title = $(this).find("option:selected").attr("data-title");
        var content = $(this).find("option:selected").attr("data-content");
        $("#title").val(title);
        $("#emailContent").val(content);
        
    });
	$("#smsTemplatesList").bind("change",function(){
        //获取被选中的option的值
//        var miaoshu = $(this).val();
        //获取自定义属性的值
        var content = $(this).find("option:selected").attr("data-content");
        $("#smsContent").val(content);
    });
});

function showSendEmailDialog() {
	
	if(!check()){
		return;
	}
	showEmailTemplate();
	var names = '';
	$('input:checkbox[name=expertIds]:checked').each(
				function(){
					names+=$(this).val().split('_')[1]+';';
			   });
	$("#emailRecivers").val(names);
	$('#emailDialog').modal('show');
	var ids = '';
	$('input:checkbox[name=expertIds]:checked').each(
			function(){
				ids+=$(this).val().split('_')[0]+',';
		   });
   $("#emailExpertIds").val(ids);
}


function sendPersonalEmail(idName) {
	
	showEmailTemplate();
	var idnames = idName.split('_');
	$("#emailRecivers").val(idnames[1]);
	$('#emailDialog').modal('show');
	$("#emailExpertIds").val(idnames[0]);
}

function sendPersonalSms(idName){

	showSmsTemplate();
	var idnames = idName.split('_');
	$("#smsRecivers").val(idnames[1]);
	$('#smsDialog').modal('show');
	$("#smsExpertIds").val(idnames[0]);
}
function showSendSmsDialog() {
	if(!check()){
		return;
	}
	showSmsTemplate();
	var names = '';
	$('input:checkbox[name=expertIds]:checked').each(
				function(){
					names+=$(this).val().split('_')[1]+';';
			   });
	$("#smsRecivers").val(names);
	$('#smsDialog').modal('show');
	var ids = '';
	$('input:checkbox[name=expertIds]:checked').each(
			function(){
				ids+=$(this).val().split('_')[0]+',';
		   });
   $("#smsExpertIds").val(ids);
}


function showEmailTemplate(){
	$("#emailTemplatesList").empty();
	var url = ctx + "/email/getEmailTemplateList";/* 后台url名 */
	var data = {
		"Time" : new Date().getMilliseconds()
	};/* 参数，可以什么都不写，但为了每次获取不同的数据，习惯上要传一个"时间戳"，后面还可以加你自己的数据，但必须是键值对类型的，如果有多个，用“，”隔开 */
	$.get(url, data, function(result) {/* 回调函数，其中data是从后台返回的html数据 */
		var reasons = eval(result);
		$("#emailTemplatesList").append("<option value='0'>请选择模板</option>");
		for (var i = 0; i < reasons.length; i++) {
			var op = "<option value=" + reasons[i].name + " data-title='"+ reasons[i].title+"'  data-content='"+ reasons[i].content+"'>"
					+ reasons[i].name + "</option>";
			$("#emailTemplatesList").append(op);
		}
	});
}
function showSmsTemplate(){
	$("#smsTemplatesList").empty();
	var url = ctx + "/sms/getSmsTemplateList";/* 后台url名 */
	var data = {
		"Time" : new Date().getMilliseconds()
	};/* 参数，可以什么都不写，但为了每次获取不同的数据，习惯上要传一个"时间戳"，后面还可以加你自己的数据，但必须是键值对类型的，如果有多个，用“，”隔开 */
	$.get(url, data, function(result) {/* 回调函数，其中data是从后台返回的html数据 */
		var reasons = eval(result);
		$("#smsTemplatesList").append("<option value='0'>请选择模板</option>");
		for (var i = 0; i < reasons.length; i++) {
			var op = "<option value=" + reasons[i].name + " data-content='"+ reasons[i].content+"'>"
					+ reasons[i].name + "</option>";
			$("#smsTemplatesList").append(op);
		}
	});
}
function check(){
	var flag = false;
	$(":checkbox[name='expertIds']").each(
			function(){
		     if($(this).get(0).checked){
		    	 flag=true;
				}
		   });
	if(!flag){
	   toastr.error("请选择专家!");
	}
	return flag;
}

function select_all(obj){
	
	if($(obj).is(':checked')) {
	    // do something
		$(":checkbox[name='expertIds']").prop('checked',true);
	}else{
		$(":checkbox[name='expertIds']").prop('checked',false);
	}
}

function checkContent(id){
	var flag = false;
	var name=$(id).val()
	if(ChkUtil.isNull(name)||name==0){
		 toastr.error("请选择模板!");
		 return flag;
	}
	return true;
}



function sendEmail(event) {
	// 阻止冒泡
	ChkUtil.stopBubbleEvent(event);
	if (!checkContent('#emailTemplatesList')) {
		return;
	}
	$('#emailDialog').modal('hide');
	$.ajax({
		url : ctx + "/email/sendEmail",
		method :'post',
		data : sy.serialieObject($("#emailForm")),
		async : false,
		dataType : 'json',
		timeout : 60000,
		success : function(data) {
			
			if(data.flag==1){
				
				toastr.error("发送成功!");
				$("#emailForm")[0].reset();
				
			}
			
		}
	});
//	window.close();
}

function sendSms(event) {
	// 阻止冒泡
	ChkUtil.stopBubbleEvent(event);
	if (!checkContent('#smsTemplatesList')) {
		return;
	}
	$('#smsDialog').modal('hide');
	$.ajax({
		url : ctx + "/sms/sendSms",
		method :'post',
		data : sy.serialieObject($("#smsForm")),
		async : false,
		dataType : 'json',
		timeout : 60000,
		success : function(data) {
			if(data.flag==1){
				toastr.error("发送成功!");
				
				$("#smsForm")[0].reset();
			}
		}
	});
//	window.close();
}
