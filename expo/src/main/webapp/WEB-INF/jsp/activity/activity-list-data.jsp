<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<c:forEach items="${data.rows}" var="activity" varStatus="status">
  	<li class="list-group-item">
          <div class="media">
              <div class="media-left">
                  <div class="avatar avatar-online">
                  	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
                       <img src="${ctx}/public/images/portraits/13.jpg" alt="...">
                       <i class="avatar avatar-busy"></i>
                      </a>
                  </div>
              </div>
              <div class="media-body">
                  <h4 class="media-heading">
                   <font size="5">${activity.title}</font>&nbsp;&nbsp;（活动ID:${activity.id}）
                  </h4>
                  <p>
                 		截至时间：<fmt:formatDate value="${activity.endtime}" pattern="yyyy-MM-dd hh:mm"/>
                  	&nbsp;&nbsp;${activity.address}
                  </p>
                  <div>
                  	报名时间：<fmt:formatDate value="${activity.begintime}" pattern="yyyy-MM-dd hh:mm"/>
                  	&nbsp;&nbsp;-&nbsp;&nbsp;
                  	<fmt:formatDate value="${activity.endtime}" pattern="yyyy-MM-dd hh:mm"/>
                  	&nbsp;&nbsp;发布者：
                  	&nbsp;&nbsp;参会人数：
                  </div>
                  
                  <div class="btn-group" role="group">
             <button type="button" class="btn btn-outline btn-default">
<!-- 				                                        <i class="icon wb-pencil" aria-hidden="true"></i> -->
                	活动信息
             </button>
             <button type="button" class="btn btn-outline btn-default">
<!-- 				                                        <i class="icon wb-trash" aria-hidden="true"></i> -->
                	专家管理
             </button>
             <button type="button" class="btn btn-outline btn-default">
<!-- 				                                        <i class="icon wb-user-add" aria-hidden="true"></i> -->
                	<i class="icon wb-trash" aria-hidden="true"></i>删除
             </button>
         </div>
              </div>
          </div>
      </li>
</c:forEach>