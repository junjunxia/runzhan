<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../includes/taglib.jsp"%>
<jsp:include page="../common/listUtil.jsp"></jsp:include>
<title>编辑活动</title>

<%-- <link rel="stylesheet"
	href="${ctx}/public/vendor/jquery-wizard/jquery-wizard.css"> --%>
<link rel="stylesheet"
	href="${ctx}/public/vendor/formvalidation/formValidation.css">
<link rel="stylesheet"
	href="${ctx}/public/css/examples/forms/validation.css">
<link rel="stylesheet"
	href="${ctx}/public/vendor/summernote/summernote.css">
	<link rel="stylesheet" href="${ctx}/public/vendor/alertify-js/alertify.css">
			<!-- 文件上传新增 -->
<%-- 	 <link rel="stylesheet" href="${ctx}/public/vendor/bootstrap/bootstrap.css"> --%>
    <link rel="stylesheet" href="${ctx}/public/uploadFile/css/fileinput/file-input.min.css">
    <link rel="stylesheet" href="${ctx}/public/uploadFile/css/jcrop/jquery.Jcrop.min.css">
    <link rel="stylesheet" href="${ctx}/public/uploadFile/css/sco/scojs.css">
    <link rel="stylesheet" href="${ctx}/public/uploadFile/css/sco/sco.message.css">
    <!-- 文件上传新增结束 -->
    
    <!-- 文件上传新增 -->
<!-- <script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap/bootstrap.min.js"></script> -->
<!--图片裁剪插件-->
<script type="text/javascript" src="${ctx}/public/uploadFile/js/jcrop/jquery.Jcrop.min.js"></script>
<!--文件上传插件-->
<script type="text/javascript" src="${ctx}/public/uploadFile/js/fileinput/file-input.min.js"></script>
<!--fileinput中文化-->
<script type="text/javascript" src="${ctx}/public/uploadFile/js/fileinput/zh.js"></script>
<!--模态窗口优化插件,sco还有很多其他优化组件-->
<script type="text/javascript" src="${ctx}/public/uploadFile/js/sco/sco.modal.js"></script>
<!--腾讯的模板插件,使用起来类似JSTL和EL表达式-->
<!-- <script type="text/javascript" src="../js/template/template.js"></script> -->
<script type="text/javascript" src="${ctx}/public/uploadFile/js/portraitForCommon.js?1"></script>
<div class="page animation-fade page-forms">
	<div class="page-content container-fluid">
		<div class="row">
			<div class="panel">
				<div class="panel-body container-fluid">
					<div class="example-wrap">
						<!--<div class="steps steps-sm row" data-plugin="matchHeight"
							data-by-row="true" role="tablist">
							 <div class="step col-md-6 current" data-target="#exampleAccount"
								role="tab">
								<span class="step-number">1</span>
								<div class="step-desc">
									<span class="step-title">专家基本信息</span>
								</div>
							</div>
							<div class="step col-md-6" data-target="#exampleBilling"
								role="tab">
								<span class="step-number">2</span>
								<div class="step-desc">
									<span class="step-title">专家履历</span>
								</div>
							</div> 
						</div>-->
<div class="wizard-content">
							<div class="wizard-pane active"id="activityAccount" role="tabpanel">
						<div >
							<div id="activityAccount" role="tabpanel">
								<form id="activityAccountForm" class="form-horizontal" autocomplete="off">
								 <input type="hidden" name="id" value="${activity.id}">
									<div class="tab-content padding-top-20">
										<div class="row row-lg">
											<div class="col-md-9">
												<div class="example-wrap margin-md-0">
													<div class="example">
														<div class="form-group">
															<label class="col-sm-3 control-label">活动标题：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="title" id="name"
																	placeholder="活动标题" data-fv-notempty="true"
																	data-fv-notempty-message="必填项" value="${activity.title}" maxlength="30">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">活动分类：</label>
															<div class="col-sm-3">
																<select name="activityClassId" id="activityClassId"  data-value="${activity.activityClassId}"  class="form-control">
															 </select>
															</div>
															<label class="col-sm-3 control-label">活动类型：</label>
															<div class="col-sm-3">
															 <select name="activityTypeId" id="activityTypeId" data-value="${activity.activityTypeId}" class="form-control">
															 </select>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">开始时间：</label>
															<div class="col-sm-3">
																<input type="text" class="form-control" name="beginTime" id="beginTime" value="<fmt:formatDate value="${activity.beginTime }" type="both"/>"
																	data-fv-notempty="true" data-fv-notempty-message="必填项" onclick="WdatePicker({ dateFmt: 'yyyy-MM-dd HH:00:00' })">
															</div>
															<label class="col-sm-3 control-label">结束时间：</label>
															<div class="col-sm-3">
																<input type="text" class="form-control" name="endTime"  id="endTime" value="<fmt:formatDate value="${activity.endTime }" type="both"/>"
																	data-fv-notempty="true" data-fv-notempty-message="必填项"  onclick="WdatePicker({ dateFmt: 'yyyy-MM-dd HH:00:00' })">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">举办城市：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="city" id="city"
																	data-fv-notempty="true" data-fv-notempty-message="必填项" value="${activity.city}">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">举办地点：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control"
																	name="venue"  value="${activity.venue}"  id="venue">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">详细地址：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="address" id="address" value="${activity.address}">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">组织机构：</label>
															   <div class="col-sm-3">
															 <select data-type="organizationType"  data-value="${activity.organizationType}" class="form-control"> 
															    <option value="1" selected>主办方</option>
															    <option value="2">承办方</option>
															 </select>
															</div>
															<div class="col-sm-5">
															 	<input type="text" class="form-control"  data-name="organizationName"  value="${activity.organizationName}">
															</div>
															<button type="button" class="btn btn-pure btn-default btn-sm icon fa-trash-o pull-right delete-submenu" onclick="deleteSelect(this)"></button>
														</div>
															<div class="form-group">
															<label class="col-sm-3 control-label"></label>
															   <div class="col-sm-3">
															 <select data-type="organizationType"  data-value="${activity.organizationType}" class="form-control"> 
															    <option value="1" >主办方</option>
															    <option value="2" selected>承办方</option>
															 </select>
															</div>
															<div class="col-sm-5">
															 	<input type="text" class="form-control" data-name="organizationName" value="${activity.organizationName}">
															</div>
															<button type="button" class="btn btn-pure btn-default btn-sm icon fa-trash-o pull-right delete-submenu" onclick="deleteSelect(this)"></button>
														</div>
															<div class="form-group" id="insertIndex">
															<label class="col-sm-3 control-label"></label>
															<div class="col-sm-9">
															
																	<div class="btn-group">
			  			                                     	<a class="btn btn-default btn-outline buttons-html5"  href="javascript:addSelect()"> <span>添加组织机构</span>
			  				                                   </a>
															</div>
			  		                                  	</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">活动封面（300k以内）：</label>
															<div class="col-sm-9">
															     <button type="button" name="upload" class=" btn btn-primary" style="margin-top: 15px">点击上传活动图片</button>
<!-- 																 <input type="file"  class="file" contentEditable="false" id="stationImg" accept="image/png,image/gif" onchange="readFile()">
 -->                                                            <input type="hidden" name="activityImage"  id="stationImgStr"  >
															</div>
														</div>
														<c:choose>
														  <c:when test="${activity!=null and activity.activityImage !=''}">
														     <div class="form-group">
															<label class="col-sm-3 control-label"></label>
															<div class="col-sm-9">
																<img src="${activity.activityImage}" alt="Photo" id="image" />
															</div>
														</div>
														  </c:when>
														  <c:otherwise>
														    <div class="form-group">
															<label class="col-sm-3 control-label"></label>
															<div class="col-sm-9">
																<img  alt="Photo" id="image" style="display:none"/>
															</div>
														</div>
														  </c:otherwise>
														</c:choose> 
													 <div class="form-group">
															<label class="col-sm-3 control-label">活动简介：</label>
															<div class="col-sm-9">
																<div id="summernote" data-plugin="summernote" data-lang="zh-CN" style="height: 200px"></div>
															</div>
														</div>
													</div>
												</div>
												<input type="hidden" name="remarks" id="remarks">
									<input type="hidden" name="organizationType" id="organizationType">
									<input type="hidden" name="organizationName" id="organizationName">
											</div>
							</div>
										</div>
										</form>
									</div>
									
									 <div class="btn-actions text-right">
                                        <div class="btn-actions text-center">
											<button type="button" class="btn btn-success"
												onclick="save(event)">保存</button>
										</div>
                                  </div>
									
								
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${ctx}/public/themes/classic/global/js/components.js"></script>
<script src="${ctx}/public/vendor/formvalidation/formValidation.min.js"
	data-name="formValidation"></script>
<script
	src="${ctx}/public/vendor/formvalidation/framework/bootstrap.min.js"
	data-deps="formValidation"></script>
<script src="${ctx}/public/vendor/matchheight/jquery.matchHeight.min.js"></script>

<script src="${ctx}/public/vendor/summernote/summernote.min.js"
	data-name="summernote"></script>
<script
	src="${ctx}/public/vendor/summernote/lang/summernote-zh-CN.min.js"
	data-deps="summernote"></script>
	<script src="${ctx }/public/js/calendar/WdatePicker.js"></script>
<script src="${ctx}/public/js/examples/forms/editor-summernote.js"></script>
<script src="${ctx}/public/vendor/alertify-js/alertify.min.js"></script>
<script src="${ctx}/public/vendor/jquery-wizard/jquery-wizard.min.js"
	data-name="wizard"></script>

<!--这段不是js代码,注意了,这个是ArtTemplate专用的模板代码,就是前面加载的template.js这个插件-->
<script id="portraitUpload" type="text/html">
    <div style="padding: 10px 20px">
        <form role="form" enctype="multipart/form-data" method="post">
            <div class="embed-responsive embed-responsive-16by9">
                <div class="embed-responsive-item pre-scrollable">
                   <img alt=""  src="${ctx}/public/uploadFile/img/showings.jpg" id="cut-img"
                         class="img-responsive img-thumbnail"/>
                </div>
            </div>
            <div class="white-divider-md"></div>
            <input type="file" name="imgFile" id="fileUpload"/>
            <div class="white-divider-md"></div>
            <div id="alert" class="alert alert-danger hidden" role="alert"></div>
            <input type="hidden" id="x" name="x"/>
            <input type="hidden" id="y" name="y"/>
            <input type="hidden" id="w" name="w"/>
            <input type="hidden" id="h" name="h"/>
        </form>
    </div>
</script>
<!-- 文件上传新增 -->

<script src="${ctx}/public/js/activity/edit.js?4"></script>
<script type="text/javascript">
var remarks='${activity.remarks}';
var id='${activity.id}';
</script>
