<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../includes/taglib.jsp"%>

<jsp:include page="../common/listUtil.jsp"></jsp:include>

<title>活动列表</title>

<link rel="stylesheet" href="${ctx}/public/css/examples/pages/general/user.css">
<link rel="stylesheet" href="${ctx}/public/css/examples/forms/layouts.css">
<link rel="stylesheet" href="${ctx}/public/vendor/select2/select2.css">
<link rel="stylesheet" href="${ctx}/public/vendor/alertify-js/alertify.css">
<script src="${ctx}/public/vendor/alertify-js/alertify.min.js"></script>
<script src="${ctx}/public/vendor/select2/select2.min.js" data-name="select2"></script>
<script src="${ctx}/public/vendor/select2/i18n/zh-CN.min.js" data-deps="select2"></script>
<style>
<!--

-->
.list-group-item:hover {
	background: #e6e6e6;
}
</style>
<div class="page animation-fade page-user">
    <div class="page-header padding-10">
    	<h6 class="page-title">活动列表</h6>
        <div class="page-header-actions">
            <button type="button" class="btn btn-icon btn-info collapsed" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
                <i class="icon fa-filter"></i>
            </button>
        </div>
    </div>
    
    <div class="page-content">
    	<div class="collapse" id="collapseFilter" aria-expanded="true">
            <div class="panel margin-bottom-10">
                <div class="panel-body padding-10">
                    <form class="form-inline" id="searchForm" action="" autocomplete='off' >
                    	<input type="hidden" name="bean" value="activity">
						<input type="hidden" name="activityClassId" value="${activityClassId}">
						<input type="hidden" name="method" value="page">
						<input type="hidden" name="pageSize" id="pageSize" value="10">
						<input type="hidden" name="pageNum" id="pageNum" value="1">
							
	                	<div class="form-group col-sm-5">
	                    	<input id="activityTitleValue" name="title" type="text" class="form-control empty" placeholder="活动标题">
	                	</div>
	                	<div class="form-group col-sm-5">
	                    	<select name="activityClassId" id="activityClassId"  class="form-control">
							</select>
	                	</div>
	                	<div class="form-group col-sm-5">
	                    	<input name="beginTime" type="text" class="form-control empty" placeholder="活动开始时间"  onclick="WdatePicker({ dateFmt: 'yyyy-MM-dd HH:00:00' })">
	                	</div>
	                	<div class="form-group col-sm-5">
	                    	<input name="endTime" type="text" class="form-control empty" placeholder="活动结束时间"  onclick="WdatePicker({ dateFmt: 'yyyy-MM-dd HH:00:00' })">
	                	</div>
                        <div class="form-group">
                            <button type="button" class="btn" onclick="tj_query()"><i class="icon fa-search"></i> 查找</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    	
    	<div class="panel margin-bottom-10">
            <div class="panel-body padding-10">
				<div class="row">
				  	<div class="col-sm-6">
			  			<div class="btn-group">
			  				<a class="btn btn-default btn-outline buttons-html5"
			  					data-pjax href="editUI" target="_blank">
			  					<span>发布活动</span>
			  				</a>
			  			</div>
				  	</div>
				  	<div class="col-sm-6">
				  		<div class="text-right">
				  			<div class="form-inline margin-bottom-0" >
			                	<div class="form-group">
   					  				<input id="activityTitleInput" type="search" class="form-control" placeholder="请输入搜索的活动名称" aria-controls="DataTables_Table_0">
			                	</div>
		                        <div class="form-group">
		                            <button type="button" class="btn" onclick="tj_querySimple()"><i class="icon fa-search"></i>搜索</button>
		                        </div>
  		                    </div>
				  		</div>
				  	</div>
				 </div>
    		</div>
        </div>
    	
        <div class="panel">
            <div class="panel-body padding-10">
                <div class="nav-tabs-horizontal">
                    <div class="tab-content">
                        <div class="tab-pane animation-fade active" id="all_contacts" role="tabpanel">
                            <ul class="list-group" id="activitylist">
                    	        
                            </ul>
                           <%@ include file="../includes/pagination.jsp"%>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
	<script src="${ctx }/public/js/calendar/WdatePicker.js"></script>
	<script src="${ctx}/public/js/activity/activity.js?223ddd5"></script>
