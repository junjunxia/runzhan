<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../includes/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<jsp:include page="../common/listUtil.jsp"></jsp:include>

<title>活动分类</title>
<link rel="stylesheet" href="${ctx}/public/vendor/alertify-js/alertify.css">
<script src="${ctx}/public/vendor/alertify-js/alertify.min.js"></script>
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-responsive/dataTables.responsive.css">
<link rel="stylesheet" href="${ctx}/public/vendor/highlight/default.css">
<link rel="stylesheet" href="${ctx}/public/vendor/highlight/github-gist.css">
<link rel="stylesheet" href="${ctx}/public/vendor/highlight/highlight.css">
<link rel="stylesheet" href="${ctx}/public/css/examples/tables/data-tables/examples.css">

<%-- <script src="${ctx}/public/vendor/datatables/jquery.dataTables.min.js" data-name="dataTables"></script> --%>
<%-- <script src="${ctx}/public/vendor/datatables-bootstrap/dataTables.bootstrap.min.js" data-deps="dataTables"></script>
<script src="${ctx}/public/vendor/datatables-responsive/dataTables.responsive.min.js" data-deps="dataTables"></script> --%>

<script type="text/javascript">
/* $(function () {
	var srchForm = $("#searchForm");
	$('#activityClassTable').DataTable($.po('dataTable', {
	    "processing": true,
	    "serverSide": true,
	    "ajax": {
		    "url": '${pageContext.request.contextPath}' + "/process",
		    "type": "POST",
			"data" : sy.serialieObject(srchForm)
		},
		"columns": [
		    {"data": "id"},
		    {"data": "name"},
		    {"data": "createId"},
		    {"data": "createName"},
		    {"data": "createTime"},
		    {"data": "updateName"}
		]
	}));
}); */
</script>

<div class="page page-full animation-fade page-data-tables">
    <div class="page-main">
        <div class="page-content" id="DTConent">

			<div class="panel">
				<div class="panel-heading">
			        <h3 class="panel-title">活动分类</h3>
			    </div>
			    
			    <div class="panel-body">
			    	
			    	<div class="panel-body padding-10">
						<div class="row">
						  	<div class="col-sm-6">
					  			<div class="btn-group btn-action">
					  				<a class="btn btn-default btn-outline buttons-html5"
				  					data-pjax href="#" target="_blank" id="add-emailTemplate" onclick="edit_emailTemplate(-1,-1)">
				  					<span>新增活动分类</span>
				  				</a>
					  			</div>
						  	</div>
						  	<div class="col-sm-6">
						  		<div class="text-right">
						  			<form class="form-inline margin-bottom-0" id="seniorSearchForm" action="${ctx }/activityClass/list" autocomplete='off' method="post">
					                	<!-- div class="form-group">
			  					  				<input name="name" type="search" class="form-control" placeholder="请输入搜索的活动名称" aria-controls="DataTables_Table_0">
					                	</div>
				                        <div class="form-group">
				                            <button type="submit" class="btn" ><i class="icon fa-search"></i>搜索</button>
				                        </div> -->
			 		                    </form>
						  		</div>
						  	</div>
						 </div>
			   		</div>
			    	
			        <table class="table table-bordered table-hover dataTable table-striped width-full text-nowrap" id="activityClassTable">
			            <thead>
			            <tr>
			                <th>序号</th>
			                <th>分类名称</th>
			                <th>活动数量</th>
			                <th>创建人</th>
			                <th>创建时间</th>
			                <th>操作</th>
			            </tr>
			            </thead>
			            <tfoot>
			            <c:forEach items="${activityClass}" var="temp"  varStatus="status">
			                 <tr id="${temp.id}">
			                <th>${status.index+1 }</th>
			                <th>${temp.name}</th>
			                <th><a data-pjax href="${ctx}/activity/list?activityClassId=${temp.id}" target="_blank">${temp.counts>0?temp.counts:0}</a></th>
			                <th>${temp.createName }</th>
			                <th><fmt:formatDate value="${temp.createTime }" pattern="yyyy-MM-dd HH:mm:ss"/></th>
			                <th><div class="item-actions">
                                <span class="btn btn-pure btn-icon btn-edit" data-toggle="modal" data-target="#roleForm" onclick="edit_emailTemplate(${temp.id},'${temp.name}')"><i class="icon wb-edit" aria-hidden="true"></i></span>
                                <span class="btn btn-pure btn-icon" data-tag="list-delete" onclick="deleteActivity(${temp.id})"><i class="icon wb-close" aria-hidden="true"></i></span>
                            </div>
							</th>
			            </tr>
			            </c:forEach>
			            </tfoot>
			        </table>
			    </div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="addLabelForm" aria-hidden="true" aria-labelledby="addLabelForm" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                <h4 class="modal-title">添加活动分类</h4>
            </div>
            <div class="modal-body">
                <form id="activityClassForm">
                     <input type="hidden" name="id" id="id" >
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" id="name" placeholder="活动分类名称">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button" onclick="save()">保存</button>
                <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:;" onclick="clear()">取消</a>
            </div>
        </div>
    </div>
</div>
<script src="${ctx}/public/themes/classic/global/js/plugins/action-btn.js"></script>
<%-- <script src="${ctx}/public/js/examples/tables/data-tables/server-side/post.js"></script> --%>
<script src="${ctx}/public/vendor/highlight/highlight.pack.min.js"></script>
<%-- <script src="${ctx}/public/themes/classic/base/js/app.js" data-name="app"></script> --%>
<%-- <script src="${ctx}/public/js/examples/tables/data-tables/common.js" data-deps="app"></script> --%>

<script src="${ctx}/public/js/activity/activityClass.js?5"></script>