<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../includes/taglib.jsp"%>
<jsp:include page="../common/listUtil.jsp"></jsp:include>
<title>新增邮件模板</title>

<%-- <link rel="stylesheet"
	href="${ctx}/public/vendor/jquery-wizard/jquery-wizard.css"> --%>
<link rel="stylesheet"
	href="${ctx}/public/vendor/formvalidation/formValidation.css">
<link rel="stylesheet"
	href="${ctx}/public/css/examples/forms/validation.css">
<link rel="stylesheet"
	href="${ctx}/public/vendor/summernote/summernote.css">
<link rel="stylesheet"
	href="${ctx}/public/vendor/alertify-js/alertify.css"> 
<div class="page animation-fade page-forms">
	<div class="page-content container-fluid">
		<div class="row">
			<div class="panel">
				<div class="panel-body container-fluid">
					<div class="example-wrap">
						<div class="wizard-content">
							<div class="wizard-pane active" role="tabpanel">
								<form class="form-horizontal" autocomplete="off" id="activityAccountForm">
									<div class="tab-content padding-top-20">
										<div class="form-group">
											<label class="col-sm-2 control-label">模板名称：</label>
											<div class="col-sm-9 text-left">
												<input type="text" class="form-control" name="name" id="name"
													data-fv-notempty="true" data-fv-notempty-message="必填项" value="${emailTemplate.name}">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">目标对象：</label>
											<div class="col-sm-9 text-left">
												<select name="userType" class="form-control" id="userType">
			   					  					<option value="0">选择用户类型</option>
			   					  					<option value="1"<c:if test="${emailTemplate.userType==1}">selected</c:if>>专家</option>
			   					  					<option value="2" <c:if test="${emailTemplate.userType==2}">selected</c:if>>企业</option>
			   					  					<option value="3" <c:if test="${emailTemplate.userType==3}">selected</c:if>>媒体</option>
			   					  					<option value="4" <c:if test="${emailTemplate.userType==4}">selected</c:if>>观众</option>
	   					  				        </select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">字段说明：</label>
											<div class="col-sm-9 text-left">
												<div>
													<table class="table table-bordered">
													 <c:forEach var="item" items="${objectDictList}">
														<tr>
														<th width="20%">${item.name}</th>
														<th width="30%">${item.value}</th>
														</tr>
													</c:forEach>
													</table>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">邮件标题：</label>
											<div class="col-sm-9 text-left">
												<input type="text" class="form-control" name="title" id="title"
													data-fv-notempty="true" data-fv-notempty-message="必填项"value="${emailTemplate.title}">
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">邮件内容：</label>
											<div class="col-sm-9">
												<div id="summernote" data-plugin="summernote"
													data-lang="zh-CN"></div>
											</div>
										</div>
                                        <input type="hidden" name="content" id="content" value="${emailTemplate.content}">
                                        <input type="hidden" name="id" id="id" value="${emailTemplate.id}">
										<div class="btn-actions text-center">
											<button type="button" class="btn btn-success"
												onclick="save(event)">保存</button>
						                	<a class="btn btn-cancel" data-dismiss="modal" href="emailTemplate-list">取消</a>
										</div>
									</div>
									
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="${ctx}/public/themes/classic/global/js/components.js"></script>
<script src="${ctx}/public/vendor/formvalidation/formValidation.min.js"
	data-name="formValidation"></script>
<script
	src="${ctx}/public/vendor/formvalidation/framework/bootstrap.min.js"
	data-deps="formValidation"></script>
<!-- <script -->
<%-- 	src="${ctx}/public/vendor/matchheight/jquery.matchHeight.min.js"></script> --%>

<script src="${ctx}/public/vendor/summernote/summernote.min.js"
	data-name="summernote"></script>
<script
	src="${ctx}/public/vendor/summernote/lang/summernote-zh-CN.min.js"
	data-deps="summernote"></script>
<script src="${ctx}/public/js/examples/forms/editor-summernote.js"></script>
 <script src="${ctx}/public/vendor/alertify-js/alertify.min.js"></script>
<script src="${ctx}/public/vendor/jquery-wizard/jquery-wizard.min.js"
	data-name="wizard"></script>
<script src="${ctx}/public/js/email/editTemplate.js?dd9"></script>
<script>
var contents='${emailTemplate.content}';

</script>