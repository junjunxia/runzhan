<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript">
var sy = $.extend({},sy);/*定义全局对象*/

sy.serialieObject = function(form) {/* 将from表单元素的值序列化成对象*/
	var o = {};
	$.each(form.serializeArray(), function(index) {
		if (o[this['name']]) {
			o[this['name']] = o[this['name']] + "," + $.trim(this['value']);
		} else {
			o[this['name']] = $.trim(this['value']);
		}
	});
	return o;
};

sy.serialieString = function(form) {/* 将from表单元素的值反序列化成参数字符串*/
	var s = "?";
	var obj = sy.serialieObject(form);
	for(var prop in obj){  
		s+=prop+"="+obj[prop]+"&";
    }
	s = s.substring(0, s.length-1);
	return s;
};


Date.prototype.format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}



</script>
