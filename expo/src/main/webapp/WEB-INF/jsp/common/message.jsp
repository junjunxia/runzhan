<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<div class="modal fade" id="emailDialog" aria-hidden="true" aria-labelledby="emailDialog" role="dialog" tabindex="-1">
   <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                <h4 class="modal-title">发送邮件</h4>
            </div>
            <div class="modal-body">
            
            <div class="row">
			<div class="panel">
				<div class="panel-body container-fluid">
					<div class="example-wrap">
						<div class="wizard-content">
							<div class="wizard-pane active" role="tabpanel">
								<form class="form-horizontal" autocomplete="off" id="emailForm">
									<div class="tab-content padding-top-20">
										<div class="form-group">
											<label class="col-sm-2 control-label">收件人<font color="red">*</font>：</label>
											<div class="col-sm-9 text-left">
												<input type="text" class="form-control" id="emailRecivers"
													data-fv-notempty="true" data-fv-notempty-message="必填项">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">选择模板<font color="red">*</font>：</label>
											<div class="col-sm-9">
												<select class="form-control" id="emailTemplatesList"> 
												</select>
											</div>
										</div>
                                      <div class="form-group">
											<label class="col-sm-2 control-label">邮件标题<font color="red">*</font>：</label>
											<div class="col-sm-9 text-left">
												<input type="text" class="form-control" name="title" id="title"
													data-fv-notempty="true" data-fv-notempty-message="必填项">
											</div>
										</div>
                                     <div class="form-group">
															<label class="col-sm-2 control-label"><font color="red">*</font>邮件内容：</label>
															<div class="col-sm-9">
																<textarea rows="10" name="content" id="emailContent" class="form-control"></textarea>
															</div>
										</div>
										<div class="btn-actions text-center">
											<button type="button" class="btn btn-success"
												onclick="sendEmail(event)">发送</button>
						                	<a class="btn btn-cancel" data-dismiss="modal" href="javascript:;">取消</a>
										</div>
										<input type="hidden" name="expertIds" id="emailExpertIds">
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
            
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="smsDialog" aria-hidden="true" aria-labelledby="smsDialog" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                <h4 class="modal-title">发送短信</h4>
            </div>
            <div class="modal-body">
            
            <div class="row">
			<div class="panel">
				<div class="panel-body container-fluid">
					<div class="example-wrap">
						<div class="wizard-content">
							<div class="wizard-pane active" role="tabpanel">
								<form class="form-horizontal" autocomplete="off" id="smsForm">
									<div class="tab-content padding-top-20">
										<div class="form-group">
											<label class="col-sm-2 control-label">收件人<font color="red">*</font>：</label>
											<div class="col-sm-9 text-left">
												<input type="text" class="form-control" id="smsRecivers"
													data-fv-notempty="true" data-fv-notempty-message="必填项">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">选择模板：</label>
											<div class="col-sm-9">
												<select class="form-control" id="smsTemplatesList"> 
												</select>
											</div>
										</div>

                                     <div class="form-group">
															<label class="col-sm-2 control-label">短信内容：</label>
															<div class="col-sm-9">
																<textarea rows="10" cols="" name="content" id="smsContent" class="form-control"></textarea>
															</div>
										</div>
										<div class="btn-actions text-center">
											<button type="button" class="btn btn-success"
												onclick="sendSms(event)">发送</button>
						                	<a class="btn btn-cancel" data-dismiss="modal" href="javascript:;">取消</a>
										</div>
										<input type="hidden" name="expertIds" id="smsExpertIds">
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
            
            </div>
        </div>
    </div>
</div>
