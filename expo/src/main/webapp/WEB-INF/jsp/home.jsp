<%--
  Created by IntelliJ IDEA.
  User: lijianjun
  Date: 2017/6/26
  Time: 下午7:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<html class="no-js css-menubar" lang="zh-cn">
<head>
    <title>主页</title>
    <%@ include file="includes/head.jsp"%>
    <!-- 图标 CSS-->
    <link rel="stylesheet"
          href="${ctx}/public/fonts/font-awesome/font-awesome.css">
    <link rel="stylesheet"
          href="${ctx}/public/fonts/web-icons/web-icons.css">
    <!-- 插件 CSS -->
    <link rel="stylesheet"
          href="${ctx}/public/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="${ctx}/public/vendor/toastr/toastr.css">

    <script src="${ctx}/public/vendor/jquery/jquery.min.js"></script>
    <script src="${ctx}/public/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="${ctx}/public/vendor/bootstrap/bootstrap.min.js"></script>
    <!-- 插件 -->
    <script src="${ctx}/public/vendor/modernizr/modernizr.min.js"></script>
    <script src="${ctx}/public/vendor/breakpoints/breakpoints.min.js"></script>
    <script src="${ctx}/public/vendor/artTemplate/template.min.js"></script>
    <script src="${ctx}/public/vendor/toastr/toastr.min.js"></script>
    <!-- 核心  -->
    <script src="${ctx}/public/themes/classic/global/js/core.js"></script>
    <script src="${ctx}/public/themes/classic/base/js/site.js?11"></script>
    <script
            src="${ctx}/public/themes/classic/global/js/configs/site-configs.js"></script>

    <script>
        Breakpoints();
        var ctx='${ctx}';
    </script>
</head>
<body class="site-contabs-open site-menubar-unfold">

<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-inverse " role="navigation">
    <div style="font-size: 20px;color:#fff;padding-top:10px;text-align:center">
        选择您的角色，开始您的使用
    </div>
</nav>

<main class="site-page" style="margin-left:0px;margin-top:0px;overflow-y: auto;height: calc(100% - 40px);">
    <div class="page-container" id="admui-pageContent">
        <link rel="stylesheet" href="${ctx}/public/vendor/bootstrap-select/bootstrap-select.css">
        <link rel="stylesheet" href="${ctx}/public/css/examples/pages/team/documents.css">

        <div class="page animation-fade page-documents">
            <div class="page-content" style="padding-top:0px;padding-bottom:0px">
                <div class="documents-wrap categories" data-plugin="animateList" data-child="li">
                    <ul class="blocks blocks-100 blocks-xlg-4 blocks-md-3 blocks-xs-2" data-plugin="matchHeight">
                        <li class="animation-scale-up" style="animation-fill-mode: backwards; animation-duration: 250ms; animation-delay: 0ms; height: 221px;">
                            <div class="category">
                                <div class="icon-wrap">
                                    <i class="icon fa-archive" aria-hidden="true"></i>
                                </div>
                                <h4 class="title">
                                    <a href="${ctx}/expert/list">专家数据库</a>
                                </h4>
                            </div>
                        </li>
                        <li class="animation-scale-up" style="animation-fill-mode: backwards; animation-duration: 250ms; animation-delay: 50ms; height: 221px;">
                            <div class="category">
                                <div class="icon-wrap">
                                    <i class="icon fa-check-circle-o" aria-hidden="true"></i>
                                </div>
                                <h4 class="title">
                                    <a href="${ctx}/activity/list">活动数据库</a>
                                </h4>
                            </div>
                        </li>
                        <li class="animation-scale-up" style="animation-fill-mode: backwards; animation-duration: 250ms; animation-delay: 100ms; height: 221px;">
                            <div class="category">
                                <div class="icon-wrap">
                                    <i class="icon fa-edit" aria-hidden="true"></i>
                                </div>
                                <h4 class="title">
                                    <a style="color:#9a9393">展会数据库</a>
                                </h4>

                            </div>
                        </li>
                        <li class="animation-scale-up" style="animation-fill-mode: backwards; animation-duration: 250ms; animation-delay: 150ms; height: 221px;">
                            <div class="category">
                                <div class="icon-wrap">
                                    <i class="icon fa-shield" aria-hidden="true"></i>
                                </div>
                                <h4 class="title">
                                    <a style="color:#9a9393">供应商数据库</a>
                                </h4>

                            </div>
                        </li>
                        <li class="animation-scale-up" style="animation-fill-mode: backwards; animation-duration: 250ms; animation-delay: 200ms; height: 221px;">
                            <div class="category">
                                <div class="icon-wrap">
                                    <i class="icon fa-ticket" aria-hidden="true"></i>
                                </div>
                                <h4 class="title">
                                    <a style="color:#9a9393">观众数据库</a>
                                </h4>

                            </div>
                        </li>
                        <li class="animation-scale-up" style="animation-fill-mode: backwards; animation-duration: 250ms; animation-delay: 250ms; height: 221px;">
                            <div class="category">
                                <div class="icon-wrap">
                                    <i class="icon fa-frown-o" aria-hidden="true"></i>
                                </div>
                                <h4 class="title">
                                    <a style="color:#9a9393">媒体数据库</a>
                                </h4>

                            </div>
                        </li>
                        <li class="animation-scale-up" style="animation-fill-mode: backwards; animation-duration: 250ms; animation-delay: 300ms; height: 221px;">
                            <div class="category">
                                <div class="icon-wrap">
                                    <i class="icon fa-magnet" aria-hidden="true"></i>
                                </div>
                                <h4 class="title">
                                    <a style="color:#9a9393">企业数据库</a>
                                </h4>
                            </div>
                        </li>
                        <li class="animation-scale-up" style="animation-fill-mode: backwards; animation-duration: 250ms; animation-delay: 350ms; height: 221px;">
                            <div class="category">
                                <div class="icon-wrap">
                                    <i class="icon fa-group" aria-hidden="true"></i>
                                </div>
                                <h4 class="title">
                                    <a  style="color:#9a9393">企业家数据库</a>
                                </h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>





    </div>
    <div class="page-loading vertical-align text-center">
        <div class="page-loader loader-default loader vertical-align-middle" data-type="default"></div>
    </div>
</main>

<footer class="site-footer"  style="margin-left:0px;margin-top:0px;">
    <div class="site-footer-legal">南京慕冉 ©
        <a href="http://www.mransoft.com/" target="_blank">www.mransoft.com</a>
    </div>
    <div class="site-footer-right">
        当前版本：v1.0.1
        <a class="margin-left-5" data-toggle="tooltip" title="" href="http://www.mransoft.com/" target="_blank" data-original-title="升级">
            <i class="icon fa-cloud-upload"></i>
        </a>
    </div>
</footer>

<!-- 布局 -->
<script src="${ctx}/public/themes/classic/base/js/sections/menu.js"></script>
<script
        src="${ctx}/public/themes/classic/base/js/sections/media-menu.js"></script>
<script
        src="${ctx}/public/themes/classic/base/js/sections/content-tabs.js"></script>

<!-- 插件 -->
<script src="${ctx}/public/vendor/jquery-pjax/jquery.pjax.min.js"></script>
<script
        src="${ctx}/public/themes/classic/global/js/plugins/responsive-tabs.js"></script>
<script
        src="${ctx}/public/vendor/ashoverscroll/jquery-asHoverScroll.min.js"></script>
<script src="${ctx}/public/vendor/slimscroll/jquery.slimscroll.min.js"></script>
<script src="${ctx}/public/vendor/screenfull/screenfull.min.js"></script>
</body>
</html>
