<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../includes/taglib.jsp"%>

<title>首页</title>

<link rel="stylesheet" href="${ctx}/public/fonts/themify/themify.css">
<link rel="stylesheet" href="${ctx}/public/css/home.css">

<div class="page animation-fade page-index">
    <div class="page-content">
        <div class="media account-info">
            <div class="media-left">
                <div class="avatar avatar-online">
                    <img src="${ctx}/public/images/avatar.svg" alt="${loginUser.loginName}">
                    <i class="avatar avatar-busy"></i>
                </div>
            </div>
            <div class="media-body">
                <h4 class="media-heading">
                    欢迎您，${loginUser.loginName}
                    <small>
                    <c:forEach items="${loginUser.roles}" var="role" varStatus="status">
                    ${role.roleName }
                    </c:forEach>
					</small>
                </h4>
                <p>
                    <i class="icon icon-color wb-bell" aria-hidden="true"></i> 这是您第 ${loginUser.loginCount} 次登录，上次登录日期：<fmt:formatDate type="both" value="${loginUser.lastLoginTime}" />，详细信息请查看
                    <a data-pjax href="${ctx}/account/log" target="_blank">日志</a>
                    ，如果不是您本人登录，请及时
                    <a data-pjax href="${ctx}/account/password" target="_blank">修改密码</a>
                    。
                </p>
            </div>
            <div class="media-right">
                <a href="${ctx}/system/account" data-pjax target="_blank" class="btn btn-outline btn-success btn-outline btn-sm">账户管理</a>
            </div>
        </div>
    </div>
</div>

<script src="${ctx}/public/vendor/matchheight/jquery.matchHeight.js"></script>
