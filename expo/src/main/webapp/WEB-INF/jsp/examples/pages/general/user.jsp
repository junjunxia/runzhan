<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../includes/taglib.jsp"%>

<title>专家列表</title>

<link rel="stylesheet" href="${ctx}/public/css/examples/pages/general/user.css">
<link rel="stylesheet" href="${ctx}/public/css/examples/components/advanced/rating.css">
<link rel="stylesheet" href="${ctx}/public/css/examples/forms/layouts.css">

<link rel="stylesheet" href="${ctx}/public/vendor/select2/select2.css">

<script src="${ctx}/public/vendor/select2/select2.min.js" data-name="select2"></script>
<script src="${ctx}/public/vendor/select2/i18n/zh-CN.min.js" data-deps="select2"></script>

<div class="page animation-fade page-user">
    <div class="page-header padding-10">
        <h6 class="page-title">专家查找</h6>
        <div class="page-header-actions">
            <button type="button" class="btn btn-icon btn-info collapsed" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
                <i class="icon fa-filter"></i>
            </button>
        </div>
    </div>
    
    <div class="page-content">
    	<div class="collapse" id="collapseFilter" aria-expanded="true">
            <div class="panel margin-bottom-10">
                <div class="panel-body padding-10">
                    <form class="form-inline" id="logForm">
	                	<div class="form-group col-sm-5">
	                    	<input type="text" class="form-control empty" placeholder="专家姓名">
	                	</div>
	                	<div class="form-group col-sm-5">
	                    	<input type="text" class="form-control empty" placeholder="公司名称">
	                	</div>
	                	<div class="form-group col-sm-5">
	                    	<input type="text" class="form-control empty" placeholder="行业领域">
	                	</div>
	                	<div class="form-group col-sm-5">
	                    	<input type="text" class="form-control empty" placeholder="社交圈子">
	                	</div>
<!-- 	                	<div class="form-group col-sm-5"> -->
<!-- 	                    	<button type="button" class="btn btn-success margin-bottom-10" >搜索</button> -->
<!-- 	                	</div> -->
                        <div class="form-group">
                            <button type="button" class="btn btn-success"><i class="icon fa-search"></i> 查找</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    	
    	<div class="panel margin-bottom-10">
            <div class="panel-body padding-10">
				<div class="row">
				  	<div class="col-sm-6">
			  			<div class="btn-group">
			  				<a class="btn btn-default btn-outline buttons-html5"
			  					data-pjax href="${ctx}/pages/general/adduser-wizard" target="_blank">
			  					<span>添加专家</span>
			  				</a>
			  				<a class="btn btn-default btn-outline buttons-html5"
			  					href="#">
			  					<span>邮&nbsp;&nbsp;&nbsp;&nbsp;件</span>
			  				</a>
			  				<a class="btn btn-default btn-outline buttons-html5"
			  					href="#">
			  					<span>短&nbsp;&nbsp;&nbsp;&nbsp;信</span>
			  				</a>
			  				<a class="btn btn-default btn-outline buttons-html5" href="#">
			  					<span>导&nbsp;&nbsp;&nbsp;&nbsp;出 </span>
			  				</a>
			  			</div>
				  	</div>
				  	<div class="col-sm-6">
				  		<div class="text-right">
				  			<label>
				  				<input type="search" class="form-control" placeholder="快速查找" aria-controls="DataTables_Table_0">
				  			</label>
				  		</div>
				  	</div>
				 </div>
    		</div>
        </div>
    	
        <div class="panel">
            <div class="panel-body padding-10">
                <div class="nav-tabs-horizontal">
                    <div class="tab-content">
                        <div class="tab-pane animation-fade active" id="all_contacts" role="tabpanel">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-left">
                                            <div width="20" data-order="false">
						                        <span class="checkbox-custom checkbox-primary user-select-all">
						                            <input type="checkbox" class="user-checkbox selectable-all">
						                            <label></label>
						                        </span>
						                    </div>
                                            <div class="avatar avatar-online">
                                            	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                                <img src="${ctx}/public/images/portraits/13.jpg" alt="...">
	                                                <i class="avatar avatar-busy"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                        	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                            <h4 class="media-heading">
	                                            <font size="5">杜重治&nbsp;&nbsp;</font>
	                                                董事总经理&nbsp;&nbsp;13976476543
	                                            </h4>
	                                        </a>
                                            <h5>南京润展国际展览有限公司</h5>
			                                <div class="rating" data-score="3" data-plugin="rating"></div>
                                            <div>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-envelope" aria-hidden="true"></i></a>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-mobile" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="media-right">
                                        	<div style="width: 300px;">
                                        		<h5>行业领域：互联网金融</h5>
                                        		<h5>社交圈子：南京农业大学</h5>
                                        	</div>
                                        	<div class="btn-group" role="group">
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-pencil" aria-hidden="true"></i>编辑
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-trash" aria-hidden="true"></i>删除
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-user-add" aria-hidden="true"></i>添加联系人
			                                    </button>
			                                </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-left">
                                            <div width="20" data-order="false">
						                        <span class="checkbox-custom checkbox-primary user-select-all">
						                            <input type="checkbox" class="user-checkbox selectable-all">
						                            <label></label>
						                        </span>
						                    </div>
                                            <div class="avatar avatar-online">
                                            	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                                <img src="${ctx}/public/images/portraits/13.jpg" alt="...">
	                                                <i class="avatar avatar-busy"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                        	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                            <h4 class="media-heading">
	                                            <font size="5">杜重治&nbsp;&nbsp;</font>
	                                                董事总经理&nbsp;&nbsp;13976476543
	                                            </h4>
	                                        </a>
                                            <h5>南京润展国际展览有限公司</h5>
			                                <div class="rating" data-score="3" data-plugin="rating"></div>
                                            <div>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-envelope" aria-hidden="true"></i></a>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-mobile" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="media-right">
                                        	<div style="width: 300px;">
                                        		<h5>行业领域：互联网金融</h5>
                                        		<h5>社交圈子：南京农业大学</h5>
                                        	</div>
                                        	<div class="btn-group" role="group">
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-pencil" aria-hidden="true"></i>编辑
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-trash" aria-hidden="true"></i>删除
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-user-add" aria-hidden="true"></i>添加联系人
			                                    </button>
			                                </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-left">
                                            <div width="20" data-order="false">
						                        <span class="checkbox-custom checkbox-primary user-select-all">
						                            <input type="checkbox" class="user-checkbox selectable-all">
						                            <label></label>
						                        </span>
						                    </div>
                                            <div class="avatar avatar-online">
                                            	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                                <img src="${ctx}/public/images/portraits/13.jpg" alt="...">
	                                                <i class="avatar avatar-busy"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                        	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                            <h4 class="media-heading">
	                                            <font size="5">杜重治&nbsp;&nbsp;</font>
	                                                董事总经理&nbsp;&nbsp;13976476543
	                                            </h4>
	                                        </a>
                                            <h5>南京润展国际展览有限公司</h5>
			                                <div class="rating" data-score="3" data-plugin="rating"></div>
                                            <div>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-envelope" aria-hidden="true"></i></a>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-mobile" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="media-right">
                                        	<div style="width: 300px;">
                                        		<h5>行业领域：互联网金融</h5>
                                        		<h5>社交圈子：南京农业大学</h5>
                                        	</div>
                                        	<div class="btn-group" role="group">
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-pencil" aria-hidden="true"></i>编辑
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-trash" aria-hidden="true"></i>删除
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-user-add" aria-hidden="true"></i>添加联系人
			                                    </button>
			                                </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-left">
                                            <div width="20" data-order="false">
						                        <span class="checkbox-custom checkbox-primary user-select-all">
						                            <input type="checkbox" class="user-checkbox selectable-all">
						                            <label></label>
						                        </span>
						                    </div>
                                            <div class="avatar avatar-online">
                                            	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                                <img src="${ctx}/public/images/portraits/13.jpg" alt="...">
	                                                <i class="avatar avatar-busy"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                        	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                            <h4 class="media-heading">
	                                            <font size="5">杜重治&nbsp;&nbsp;</font>
	                                                董事总经理&nbsp;&nbsp;13976476543
	                                            </h4>
	                                        </a>
                                            <h5>南京润展国际展览有限公司</h5>
			                                <div class="rating" data-score="3" data-plugin="rating"></div>
                                            <div>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-envelope" aria-hidden="true"></i></a>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-mobile" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="media-right">
                                        	<div style="width: 300px;">
                                        		<h5>行业领域：互联网金融</h5>
                                        		<h5>社交圈子：南京农业大学</h5>
                                        	</div>
                                        	<div class="btn-group" role="group">
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-pencil" aria-hidden="true"></i>编辑
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-trash" aria-hidden="true"></i>删除
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-user-add" aria-hidden="true"></i>添加联系人
			                                    </button>
			                                </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-left">
                                            <div width="20" data-order="false">
						                        <span class="checkbox-custom checkbox-primary user-select-all">
						                            <input type="checkbox" class="user-checkbox selectable-all">
						                            <label></label>
						                        </span>
						                    </div>
                                            <div class="avatar avatar-online">
                                            	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                                <img src="${ctx}/public/images/portraits/13.jpg" alt="...">
	                                                <i class="avatar avatar-busy"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                        	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                            <h4 class="media-heading">
	                                            <font size="5">杜重治&nbsp;&nbsp;</font>
	                                                董事总经理&nbsp;&nbsp;13976476543
	                                            </h4>
	                                        </a>
                                            <h5>南京润展国际展览有限公司</h5>
			                                <div class="rating" data-score="3" data-plugin="rating"></div>
                                            <div>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-envelope" aria-hidden="true"></i></a>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-mobile" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="media-right">
                                        	<div style="width: 300px;">
                                        		<h5>行业领域：互联网金融</h5>
                                        		<h5>社交圈子：南京农业大学</h5>
                                        	</div>
                                        	<div class="btn-group" role="group">
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-pencil" aria-hidden="true"></i>编辑
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-trash" aria-hidden="true"></i>删除
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-user-add" aria-hidden="true"></i>添加联系人
			                                    </button>
			                                </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-left">
                                            <div width="20" data-order="false">
						                        <span class="checkbox-custom checkbox-primary user-select-all">
						                            <input type="checkbox" class="user-checkbox selectable-all">
						                            <label></label>
						                        </span>
						                    </div>
                                            <div class="avatar avatar-online">
                                            	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                                <img src="${ctx}/public/images/portraits/13.jpg" alt="...">
	                                                <i class="avatar avatar-busy"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                        	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                            <h4 class="media-heading">
	                                            <font size="5">杜重治&nbsp;&nbsp;</font>
	                                                董事总经理&nbsp;&nbsp;13976476543
	                                            </h4>
	                                        </a>
                                            <h5>南京润展国际展览有限公司</h5>
			                                <div class="rating" data-score="3" data-plugin="rating"></div>
                                            <div>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-envelope" aria-hidden="true"></i></a>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-mobile" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="media-right">
                                        	<div style="width: 300px;">
                                        		<h5>行业领域：互联网金融</h5>
                                        		<h5>社交圈子：南京农业大学</h5>
                                        	</div>
                                        	<div class="btn-group" role="group">
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-pencil" aria-hidden="true"></i>编辑
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-trash" aria-hidden="true"></i>删除
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-user-add" aria-hidden="true"></i>添加联系人
			                                    </button>
			                                </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-left">
                                            <div width="20" data-order="false">
						                        <span class="checkbox-custom checkbox-primary user-select-all">
						                            <input type="checkbox" class="user-checkbox selectable-all">
						                            <label></label>
						                        </span>
						                    </div>
                                            <div class="avatar avatar-online">
                                            	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                                <img src="${ctx}/public/images/portraits/13.jpg" alt="...">
	                                                <i class="avatar avatar-busy"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                        	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                            <h4 class="media-heading">
	                                            <font size="5">杜重治&nbsp;&nbsp;</font>
	                                                董事总经理&nbsp;&nbsp;13976476543
	                                            </h4>
	                                        </a>
                                            <h5>南京润展国际展览有限公司</h5>
			                                <div class="rating" data-score="3" data-plugin="rating"></div>
                                            <div>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-envelope" aria-hidden="true"></i></a>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-mobile" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="media-right">
                                        	<div style="width: 300px;">
                                        		<h5>行业领域：互联网金融</h5>
                                        		<h5>社交圈子：南京农业大学</h5>
                                        	</div>
                                        	<div class="btn-group" role="group">
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-pencil" aria-hidden="true"></i>编辑
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-trash" aria-hidden="true"></i>删除
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-user-add" aria-hidden="true"></i>添加联系人
			                                    </button>
			                                </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-left">
                                            <div width="20" data-order="false">
						                        <span class="checkbox-custom checkbox-primary user-select-all">
						                            <input type="checkbox" class="user-checkbox selectable-all">
						                            <label></label>
						                        </span>
						                    </div>
                                            <div class="avatar avatar-online">
                                            	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                                <img src="${ctx}/public/images/portraits/13.jpg" alt="...">
	                                                <i class="avatar avatar-busy"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                        	<a data-pjax href="${ctx}/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                            <h4 class="media-heading">
	                                            <font size="5">杜重治&nbsp;&nbsp;</font>
	                                                董事总经理&nbsp;&nbsp;13976476543
	                                            </h4>
	                                        </a>
                                            <h5>南京润展国际展览有限公司</h5>
			                                <div class="rating" data-score="3" data-plugin="rating"></div>
                                            <div>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-envelope" aria-hidden="true"></i></a>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-mobile" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="media-right">
                                        	<div style="width: 300px;">
                                        		<h5>行业领域：互联网金融</h5>
                                        		<h5>社交圈子：南京农业大学</h5>
                                        	</div>
                                        	<div class="btn-group" role="group">
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-pencil" aria-hidden="true"></i>编辑
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-trash" aria-hidden="true"></i>删除
			                                    </button>
			                                    <button type="button" class="btn btn-outline btn-default">
			                                        <i class="icon wb-user-add" aria-hidden="true"></i>添加联系人
			                                    </button>
			                                </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <nav>
                                <ul data-plugin="twbsPagination" data-total-pages="50" data-pagination-class="pagination pagination-no-border"></ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div id="add-user-dialog" title="Basic dialog"> -->
<!--   <p>这是一个动画显示的对话框，用于显示信息。对话框窗口可以移动，调整尺寸，默认可通过 'x' 图标关闭。</p> -->
<!-- </div> -->

<script src="${ctx}/public/vendor/twbs-pagination/jquery.twbsPagination.min.js"></script>
<%-- <script src="${ctx}/public/js/examples/pages/general/add-user-dialog.js"></script> --%>
<script src="${ctx}/public/vendor/raty/jquery.raty.min.js"></script>

