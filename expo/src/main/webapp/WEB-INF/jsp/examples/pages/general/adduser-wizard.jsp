<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../../includes/taglib.jsp"%>

<title>添加专家</title>

<link rel="stylesheet"
	href="${ctx}/public/vendor/jquery-wizard/jquery-wizard.css">
<link rel="stylesheet"
	href="${ctx}/public/vendor/formvalidation/formValidation.css">
<link rel="stylesheet"
	href="${ctx}/public/css/examples/forms/validation.css">
<link rel="stylesheet"
	href="${ctx}/public/vendor/summernote/summernote.css">
	
<div class="page animation-fade page-forms">
	<div class="page-content container-fluid">
		<div class="row">
			<div class="panel" id="exampleWizardForm">
				<div class="panel-body container-fluid">
					<div class="example-wrap">
						<div class="steps steps-sm row" data-plugin="matchHeight"
							data-by-row="true" role="tablist">
							<div class="step col-md-4 current" data-target="#exampleAccount"
								role="tab">
								<span class="step-number">1</span>
								<div class="step-desc">
									<span class="step-title">专家基本信息</span>
								</div>
							</div>
							<div class="step col-md-4" data-target="#exampleBilling"
								role="tab">
								<span class="step-number">2</span>
								<div class="step-desc">
									<span class="step-title">专家履历</span>
								</div>
							</div>
							<div class="step col-md-4" data-target="#exampleGetting"
								role="tab">
								<span class="step-number">3</span>
								<div class="step-desc">
									<span class="step-title">专家联系人</span>
								</div>
							</div>
						</div>

						<div class="wizard-content">
							<div class="wizard-pane active" id="exampleAccount" role="tabpanel">
								<form id="exampleAccountForm" class="form-horizontal" autocomplete="off">
									<div class="tab-content padding-top-20">
										<div class="row row-lg">
											<div class="col-md-6">
												<div class="example-wrap margin-md-0">
													<div class="example">
														<div class="form-group">
															<label class="col-sm-3 control-label">姓名：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="userName"
																	placeholder="专家姓名" data-fv-notempty="true"
																	data-fv-notempty-message="必填项">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">单位名称：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="Name"
																	data-fv-notempty="true" data-fv-notempty-message="必填项">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">职务：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="name"
																	data-fv-notempty="true" data-fv-notempty-message="必填项">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">手机号码：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control"
																	name="type_phone" placeholder="13012345678">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">传真：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="fax">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">单位地址：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="address">
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="example-wrap">
													<div class="example">
														<div class="form-group">
															<label class="col-sm-3 control-label">所属领域：</label>
															<div class="col-sm-9">
																<select class="form-control" name="area"
																	data-fv-notempty="true">
																	<option value="">请选择</option>
																	<option value="a">互联网</option>
																	<option value="b">服务业</option>
																	<option value="c">机械</option>
																	<option value="d">食品</option>
																</select>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">部门：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control"
																	name="department">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">社会头衔：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control"
																	name="type_numberic">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">联系电话：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="tel">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">邮箱:</label>
															<div class="col-sm-9">
																<input type="text" class="form-control"
																	name="type_email" placeholder="email@email.com">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">社交圈子：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="sss">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="wizard-pane" id="exampleBilling" role="tabpanel">
								<form id="exampleBillingForm">
									<div id="summernote" data-plugin="summernote" data-lang="zh-CN"></div>
								</form>
							</div>
							<div class="wizard-pane" id="exampleGetting" role="tabpanel">
								<form id="exampleContactForm" class="form-horizontal" autocomplete="off">
									<div class="tab-content padding-top-20">
										<div class="row row-lg">
											<div class="col-md-6">
												<div class="example-wrap margin-md-0">
													<div class="example">
														<div class="form-group">
															<label class="col-sm-3 control-label">姓名：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="userName"
																	placeholder="联系人姓名" data-fv-notempty="true"
																	data-fv-notempty-message="必填项">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">单位名称：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="Name"
																	data-fv-notempty="true" data-fv-notempty-message="必填项">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">手机号码：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control"
																	name="type_phone" placeholder="13012345678">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">传真：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="fax">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">单位地址：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="address">
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="example-wrap">
													<div class="example">
														<div class="form-group">
															<label class="col-sm-3 control-label">职务：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="name"
																	data-fv-notempty="true" data-fv-notempty-message="必填项">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">部门：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control"
																	name="department">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">联系电话：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="tel">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">邮箱:</label>
															<div class="col-sm-9">
																<input type="text" class="form-control"
																	name="type_email" placeholder="email@email.com">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="${ctx}/public/vendor/formvalidation/formValidation.min.js"
	data-name="formValidation"></script>
<script
	src="${ctx}/public/vendor/formvalidation/framework/bootstrap.min.js"
	data-deps="formValidation"></script>
<script src="${ctx}/public/vendor/matchheight/jquery.matchHeight.min.js"></script>
<script src="${ctx}/public/vendor/jquery-wizard/jquery-wizard.min.js"
	data-name="wizard"></script>
<script src="${ctx}/public/js/examples/forms/wizard.js"
	data-deps="wizard"></script>

<script src="${ctx}/public/vendor/summernote/summernote.min.js"
	data-name="summernote"></script>
<script
	src="${ctx}/public/vendor/summernote/lang/summernote-zh-CN.min.js"
	data-deps="summernote"></script>
<script src="${ctx}/public/js/examples/forms/editor-summernote.js"></script>
