<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../includes/taglib.jsp"%>

<title>专家详情</title>

<div class="page animation-fade">
    <div class="page-header">
        <h1 class="page-title">专家详情</h1>
        <div class="page-header-actions">
            <button type="button" class="btn btn-icon btn-inverse btn-round">
                <i class="icon wb-pencil" aria-hidden="true"></i>
            </button>
            <button type="button" class="btn btn-icon btn-inverse btn-round">
                <i class="icon wb-refresh" aria-hidden="true"></i>
            </button>
            <button type="button" class="btn btn-icon btn-inverse btn-round">
                <i class="icon wb-settings" aria-hidden="true"></i>
            </button>
        </div>
    </div>
    <div class="page-content container-fluid">
        <div class="panel">
<!--             <div class="panel-heading"> -->
<!--                 <h3 class="panel-title">选项卡</h3> -->
<!--             </div> -->
            <div class="panel-body container-fluid">
                 <div class="example-wrap">
                     <div class="nav-tabs-horizontal" data-approve="nav-tabs">
                         <ul class="nav nav-tabs" role="tablist">
                             <li class="active" role="presentation">
                                 <a data-toggle="tab" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab">
                                     联系人
                                 </a>
                             </li>
                             <li role="presentation">
                                 <a data-toggle="tab" href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab">
                                    参加活动
                                 </a>
                             </li>
                             <li role="presentation">
                                 <a data-toggle="tab" href="#exampleTabsThree" aria-controls="exampleTabsThree" role="tab">
                                    参加展会
                                 </a>
                             </li>
                         </ul>
                         <div class="tab-content padding-top-20">
                             <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                                 近日沃尔沃宣布将联合瑞典汽车安全公司奥托立夫(AUTOLIV)成立一个独立子公司，该公司将专攻瞄准未来车型自动驾驶技
                                 术相关的研发。奥托立夫公司(AUTOLIV)是在瑞典设立的一家国际跨国公司，公司多年来研发汽车电子安全系统、电子控制单元，汽车方向盘系统以及夜视
                                 和雷达传感系统，新的合作能够让新公司吸取Autoliv多年来在汽车驾驶安全配件制造方面的经验，研发未来用于沃尔沃或其它厂商的无人驾驶软件系统。
                             </div>
                             <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">
                                 在试运营期间，Jio将向全印度人免费提供服务，直到今年年底。在免费期过后，其数据流量月资费也低至每月149卢比（约合15元人民币）。安巴尼上周在公司年度全体大会上对投资者说：“任何、所有能实现数字化的东西都将快速走向数字化，生活将走向数字化。”目前，只有五分之一的印度成年人口能够上网。在印度，公共WiFi热点极少。城市贫困区缺乏高速宽带所需的基础设备，更不用说乡村地区了。
                             </div>
                             <div class="tab-pane" id="exampleTabsThree" role="tabpanel">
                                 车轮查违章与百度地图推出《中国车主违章地图大数据》报告，江苏高居“违章停车”榜首，在全国范围内，江苏、湖北、山东、浙江以及北京五个省市中的车主违章比例最高，其中江苏省以高达11.52%的比例遥遥领先。另外，江苏、湖北、山东三省的“马路杀手”已经超过了全国人口最多的广东省。有62.38%是不按规定行驶，26.18%超速、7.13%违章停车、2.82%无视尾号限行、1.49%不系安全带和不带头盔。
                             </div>
                         </div>
                     </div>
                 </div>
            </div>
        </div>
    </div>
</div>

<script src="${ctx}/public/vendor/matchheight/jquery.matchHeight.min.js"></script>
