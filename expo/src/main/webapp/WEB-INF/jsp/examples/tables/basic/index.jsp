<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../includes/taglib.jsp"%>

<title>专家详情</title>
<div class="page animation-fade page-tables">
<!--     <div class="page-header"> -->
<!--         <h1 class="page-title">专家基本信息</h1> -->
<!--     </div> -->
    <div class="page-content">
        <div class="panel">
            <div class="panel-body container-fluid">
                 <div class="example-wrap">
                 	 <h1 class="page-title">专家基本信息</h1>
                     <div class="example table-responsive">
                         <table class="table table-bordered">
                             <thead>
                             <tr>
                                 <th>专家姓名</th>
                                 <th>单位名称</th>
                                 <th>部门</th>
                                 <th>职务</th>
                                 <th>手机号码</th>
                                 <th>联系电话</th>
                                 <th>传真</th>
                                 <th>单位地址</th>
                             </tr>
                             </thead>
                             <tbody>
                             <tr>
                                 <td>杜重治</td>
                                 <td>苏宁云商</td>
                                 <td>董事会</td>
                                 <td>董事总经理</td>
                                 <td>13976476543</td>
                                 <td>0550-6476543</td>
                                 <td>0550-6476543</td>
                                 <td>南京市雨花台区</td>
                             </tr>
                             </tbody>
                         </table>
                     </div>
                 </div>
                 
                 <div class="example-wrap">
                     <div class="nav-tabs-horizontal" data-approve="nav-tabs">
                         <ul class="nav nav-tabs" role="tablist">
                             <li class="active" role="presentation">
                                 <a data-toggle="tab" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab">
                                     联系人
                                 </a>
                             </li>
                             <li role="presentation">
                                 <a data-toggle="tab" href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab">
                                    参加活动
                                 </a>
                             </li>
                             <li role="presentation">
                                 <a data-toggle="tab" href="#exampleTabsThree" aria-controls="exampleTabsThree" role="tab">
                                    参加展会
                                 </a>
                             </li>
                         </ul>
                         <div class="tab-content padding-top-20">
                             <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
		                         <table class="table table-bordered">
		                             <thead>
		                             <tr>
		                                 <th>联系人</th>
		                                 <th>单位名称</th>
		                                 <th>部门</th>
		                                 <th>职务</th>
		                                 <th>手机号码</th>
		                                 <th>联系电话</th>
		                                 <th>传真</th>
		                                 <th>单位地址</th>
		                                 <th>邮编</th>
		                             </tr>
		                             </thead>
		                             <tbody>
		                             <tr>
		                                 <td>李浩</td>
		                                 <td>苏宁云商</td>
		                                 <td>董事会</td>
		                                 <td>董事总经理</td>
		                                 <td>13976476543</td>
		                                 <td>0550-6476543</td>
		                                 <td>0550-6476543</td>
		                                 <td>南京市雨花台区</td>
		                                 <td>0476543</td>
		                             </tr>
		                             </tbody>
		                         </table>
                             </div>
                             <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">
                                 <table class="table table-bordered">
		                             <thead>
		                             <tr>
		                                 <th>活动名称</th>
		                                 <th>主办单位</th>
		                                 <th>承办单位</th>
		                                 <th>邀请人</th>
		                                 <th>演讲题目</th>
		                                 <th>时间</th>
		                                 <th>地点</th>
		                                 <th>备注</th>
		                             </tr>
		                             </thead>
		                             <tbody>
		                             <tr>
		                                 <td>李浩</td>
		                                 <td>苏宁云商</td>
		                                 <td>中兴集团</td>
		                                 <td>李响</td>
		                                 <td>区块链原理</td>
		                                 <td>2017-05-08</td>
		                                 <td>南京</td>
		                                 <td></td>
		                             </tr>
		                             </tbody>
		                         </table>
                             </div>
                             <div class="tab-pane" id="exampleTabsThree" role="tabpanel">
                                 <table class="table table-bordered">
		                             <thead>
		                             <tr>
		                                 <th>展会名称</th>
		                                 <th>主办单位</th>
		                                 <th>承办单位</th>
		                                 <th>邀请人</th>
		                                 <th>演讲题目</th>
		                                 <th>时间</th>
                               			 <th>地点</th>
		                                 <th>备注</th>
		                             </tr>
		                             </thead>
		                             <tbody>
		                             <tr>
		                                 <td>杜重治</td>
		                                 <td>苏宁云商</td>
		                                 <td>中兴集团</td>
		                                 <td>李响</td>
		                                 <td>区块链原理</td>
		                                 <td>2017-05-08</td>
		                                 <td>南京</td>
		                                 <td></td>
		                             </tr>
		                             </tbody>
		                         </table>
                             </div>
                         </div>
                     </div>
                 </div>
            
            </div>
        </div>
    </div>
</div>

<script src="${ctx}/public/vendor/peity/jquery.peity.min.js"></script>
<script src="${ctx}/public/themes/classic/global/js/plugins/selectable.js"></script>