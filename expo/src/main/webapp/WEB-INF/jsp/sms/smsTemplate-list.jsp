<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../includes/taglib.jsp"%>
<jsp:include page="../common/listUtil.jsp"></jsp:include>

<title>短信模板</title>

<link rel="stylesheet" href="${ctx}/public/vendor/select2/select2.css">
<link rel="stylesheet" href="${ctx}/public/vendor/bootstrap-markdown/bootstrap-markdown.css">
<link rel="stylesheet" href="${ctx}/public/vendor/bootstrap-select/bootstrap-select.css">
<link rel="stylesheet" href="${ctx}/public/vendor/slidepanel/slidePanel.css">
<link rel="stylesheet" href="${ctx}/public/css/examples/pages/team/mailbox.css">

<div class="page page-full bg-white animsition page-mailbox">

    <div class="page-aside">
        <div class="page-aside-switch">
            <i class="icon wb-chevron-left" aria-hidden="true"></i>
            <i class="icon wb-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner height-full" data-plugin="slimScroll">
            <div class="page-aside-section">
                <div class="list-group">
                    <a class="list-group-item" href="javascript:;">
                        <i class="icon wb-envelope" aria-hidden="true"></i>已发送列表
                    </a>
                    <a class="list-group-item" href="javascript:;">
                        <i class="icon wb-envelope" aria-hidden="true"></i>失败列表
                    </a>
                    <a class="list-group-item" href="javascript:;">
                        <i class="icon wb-star" aria-hidden="true"></i>短信模板
                    </a>
                </div>
            </div>
            <div class="page-aside-section">
                <h5 class="page-aside-title">类型</h5>
                <div class="list-group has-actions">
                    <div class="list-group-item">
                        <div class="list-content" onclick="setUserType(1)">
                            <i class="item-right wb-medium-point red-600" aria-hidden="true"></i>
                            <span class="list-text">专家</span>
                        </div>
                    </div>
                    <div class="list-group-item">
                        <div class="list-content"onclick="setUserType(2)">
                            <i class="item-right wb-medium-point orange-600" aria-hidden="true"></i>
                            <span class="list-text">企业</span>
                        </div>
                    </div>
                    <div class="list-group-item">
                        <div class="list-content"onclick="setUserType(3)">
                            <i class="item-right wb-medium-point cyan-600" aria-hidden="true"></i>
                            <span class="list-text">媒体</span>
                        </div>
                    </div>
                    <div class="list-group-item">
                        <div class="list-content"onclick="setUserType(4)">
                            <i class="item-right wb-medium-point green-600" aria-hidden="true"></i>
                            <span class="list-text">观众</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-main">

        <div class="page-header">
            <h1 class="page-title">短信</h1>
<!--             <div class="page-header-actions"> -->
<!--                 <form> -->
<!--                     <div class="input-search input-search-dark"> -->
<!--                         <i class="input-search-icon wb-search" aria-hidden="true"></i> -->
<!--                         <input type="text" class="form-control" name="" placeholder="查找..."> -->
<!--                     </div> -->
<!--                 </form> -->
<!--             </div> -->
        </div>

        <div class="page-content page-content-table" data-selectable="selectable">

            <div class="page-content-actions">
                <div class="pull-right filter">
                </div>
                <div class="actions-main">
                    <div class="row">
					  	<div class="col-sm-6">
			  				<span class="checkbox-custom checkbox-primary checkbox-lg inline-block vertical-align-bottom">
		                        <input type="checkbox" class="mailbox-checkbox selectable-all" onclick="select_all(this)">
		                    	<label for="select_all"></label>
		                    </span>
				  			<div class="btn-group btn-action">
				  				<a class="btn btn-default btn-outline buttons-html5"
				  					data-pjax href="editSmsTemplate" target="_blank" >
<!-- 				  					id="add-emailTemplate" onclick="add_emailTemplate()" -->
				  					<span>添&nbsp;&nbsp;加</span>
				  				</a>
				  				<!-- <a class="btn btn-default btn-outline buttons-html5"
				  					href="#">
				  					<span>修&nbsp;&nbsp;改</span>
				  				</a> -->
				  				<a class="btn btn-default btn-outline buttons-html5 delete-btn-action"
				  					href="javascript:deleteSms()">
				  					<span>删&nbsp;&nbsp;除</span>
				  				</a>
				  			</div>
					  	</div>
					  	<div class="col-sm-6">
					  		<div class="text-right">
					  			<form class="form-inline margin-bottom-0" id="srchForm" action="" autocomplete='off'>
				                	<input type="hidden" name="bean" value="smsTemplate">
									<input type="hidden" name="method" value="page">
				                	<input type="hidden" name="pageSize" id="pageSize" value="10">
									<input type="hidden" name="pageNum" id="pageNum" value="1">
				                	
				                	<div class="form-group">
	   					  				<input name="name" id="name" type="search" class="form-control" placeholder="短信模板名称" aria-controls="DataTables_Table_0">
				                	</div>
				                	<div class="form-group">
	   					  				<select name="userType" class="form-control" id="userType">
	   					  					<option value="">用户类型</option>
	   					  					<option value="1">专家</option>
	   					  					<option value="2">企业</option>
	   					  					<option value="3">媒体</option>
	   					  					<option value="4">观众</option>
	   					  				</select>
				                	</div>
			                        <div class="form-group">
			                            <button type="button" class="btn" onclick="tj_query()"><i class="icon fa-search"></i>搜索</button>
			                        </div>
	  		                    </form>
					  		</div>
					  	</div>
					 </div>
                </div>
            </div>
	<form class="form-inline margin-bottom-0" id="deleteForm" action="" autocomplete='off'>
            <table id="mailboxTable" class="table" data-plugin="animateList" data-animate="fade" data-child="tr">
                <tbody></tbody>
            </table>
 </form>
<!--             <ul data-plugin="twbsPagination" data-total-pages="50" data-pagination-class="pagination pagination-gap"></ul> -->
        </div>
    </div>
</div>

<div class="modal fade" id="addMailForm" aria-hidden="true" aria-labelledby="addMailForm" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                <h4 class="modal-title">写邮件</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <select id="topicTo" class="form-control" data-plugin="select2" multiple="multiple" data-placeholder="收件人：">
                            <optgroup label="">
                                <option value="AK">梅小燕 - meixiaoyan@qq.com</option>
                                <option value="HI">唐雪琴 - xueqin@163.com</option>
                            </optgroup>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="主题：">
                    </div>
                    <div class="form-group">
                        <textarea name="content" class="markdown-edit" rows="10"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer text-left">
                <button class="btn btn-primary" data-dismiss="modal" type="submit">发送</button>
                <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:;">取消</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addLabelForm" aria-hidden="true" aria-labelledby="addLabelForm" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                <h4 class="modal-title">添加新标签</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" name="lablename" placeholder="标签名称">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="submit">保存</button>
                <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:;">取消</a>
            </div>
        </div>
    </div>
</div>

<script src="${ctx}/public/vendor/bootstrap-markdown/bootstrap-markdown.min.js" data-name="markdown"></script>
<script src="${ctx}/public/vendor/bootstrap-markdown/locale/bootstrap-markdown.zh.min.js" data-deps="markdown"></script>
<script src="${ctx}/public/vendor/select2/select2.min.js"></script>
<script src="${ctx}/public/vendor/marked/marked.min.js"></script>
<script src="${ctx}/public/vendor/to-markdown/to-markdown.min.js"></script>
<script src="${ctx}/public/vendor/twbs-pagination/jquery.twbsPagination.min.js"></script>
<script src="${ctx}/public/vendor/bootbox/bootbox.min.js"></script>
<script src="${ctx}/public/vendor/slidepanel/jquery-slidePanel.min.js"></script>
<script src="${ctx}/public/themes/classic/global/js/plugins/action-btn.js"></script>
<script src="${ctx}/public/themes/classic/global/js/plugins/selectable.js"></script>
<script src="${ctx}/public/themes/classic/base/js/app.js" data-name="app"></script>
<script src="${ctx}/public/js/sms/sms-list.js?ddddssdd3" data-deps="app"></script>
