<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>



<div class="row">
  	<div class="col-sm-6">
  		<div class="hidden-xs">
  			<div class="btn-group btn-group-sm">
  				<a class="btn btn-default btn-outline buttons-copy buttons-html5"
  					tabindex="0" aria-controls="DataTables_Table_0" href="#">
  					<span>拷贝</span>
  				</a>
  				<a class="btn btn-default btn-outline buttons-excel buttons-html5"
  					tabindex="0" aria-controls="DataTables_Table_0" href="#">
  					<span>导出 Excel</span>
  				</a>
  				<a class="btn btn-default btn-outline buttons-csv buttons-html5"
  					tabindex="0" aria-controls="DataTables_Table_0" href="#">
  					<span>导出 CSV</span>
  				</a>
  				<a class="btn btn-default btn-outline buttons-print buttons-html5"
  					tabindex="0" aria-controls="DataTables_Table_0" href="#">
  					<span>打印</span>
  				</a>
  			</div>
  		</div>
  	</div>
  	
  	<div class="col-sm-6">
  		<div id="DataTables_Table_0_filter" class="dataTables_filter">
  			<label>
  				<input type="search" class="form-control input-sm" placeholder="快速查找" aria-controls="DataTables_Table_0">
  			</label>
  		</div>
  	</div>
 </div>



</body>
</html>