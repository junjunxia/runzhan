<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../includes/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<jsp:include page="../common/listUtil.jsp"></jsp:include>
<jsp:include page="../common/message.jsp"></jsp:include>
<title>活动专家管理</title>
<link rel="stylesheet" href="${ctx}/public/vendor/alertify-js/alertify.css">
<script src="${ctx}/public/vendor/alertify-js/alertify.min.js"></script>
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-responsive/dataTables.responsive.css">
<link rel="stylesheet" href="${ctx}/public/vendor/highlight/default.css">
<link rel="stylesheet" href="${ctx}/public/vendor/highlight/github-gist.css">
<link rel="stylesheet" href="${ctx}/public/vendor/highlight/highlight.css">
<link rel="stylesheet" href="${ctx}/public/css/examples/tables/data-tables/examples.css">


<div class="page page-full animation-fade page-data-tables">
    <div class="page-main">
        <div class="page-content" id="DTConent">

			<div class="panel">
				<div class="panel-heading">
			        <h3 class="panel-title">活动专家管理</h3>
			    </div>
			    
			    <div class="panel-body">
			    	<div>
			    	  
			  			<i class="icon wb-order" aria-hidden="true"></i>&nbsp;${activity.title} <input type="hidden" id="activitytitle" value="${activity.title}" ><span></span>
			  			</br><i class="icon wb-time" aria-hidden="true"></i>&nbsp;报名时间：<fmt:formatDate value="${activity.beginTime }" type="both"/>-<fmt:formatDate value="${activity.endTime }" type="both"/> 
			  			<span></span>&nbsp;&nbsp;<i class="icon wb-small-point" aria-hidden="true"></i>发布者:${activity.createName}
				  		<span></span>&nbsp;&nbsp;<i class="icon wb-small-point" aria-hidden="true"></i>参会人数:${activity.expertCount }
				  	</div> 
			    	<div class="panel-body padding-10">
						<div class="row">
						  	<div class="col-sm-6">
					  			<div class="btn-group btn-action">
					  				<a class="btn btn-default btn-outline buttons-html5"
				  					data-pjax href="toAddExpert?activityId=${activity.id}&activityTitle=${activity.title}" target="_blank" id="add-emailTemplate">
				  					<span>添加专家</span>
				  				</a>
				  				<a class="btn btn-default btn-outline buttons-html5"
			  					href="#" onclick="showSendEmailDialog()">
			  					<span>发送邮件</span>
			  				</a>
			  				<a class="btn btn-default btn-outline buttons-html5"
			  					href="#" onclick="showSendSmsDialog()">
			  					<span>发送短信</span>
			  				</a>
					  			</div>
						  	</div>
						  	<div class="col-sm-6">
						  		<div class="text-right">
						  			<form class="form-inline margin-bottom-0" id="seniorSearchForm"  autocomplete='off' method="post">
					                	<div class="form-group">
			  					  				<input name="expertName"   type="search" class="form-control" placeholder="请输入搜索的专家名称" aria-controls="DataTables_Table_0">
					                	</div>
					                	<input type="hidden" name="activityId" value="${activityId}" >
				                        <div class="form-group">
				                            <a class="btn btn-primary" href="javascript:showActivityExpertList()" ><i class="icon fa-search" ></i>搜索</a>
				                        </div>
			 		                    </form>
						  		</div>
						  	</div> 
						 </div>
			   		</div>
			    	
			        <table class="table table-bordered table-hover dataTable table-striped width-full text-nowrap" id="activityClassTable">
			            <thead>
			            <tr>
			                <th><span class="checkbox-custom checkbox-primary checkbox-lg inline-block vertical-align-bottom">
		                        <input type="checkbox" class="mailbox-checkbox selectable-all" onclick="select_all(this)">
		                    	<label for="select_all"></label>
		                    </span></th>
			                <th>姓名</th>
			                <th>公司</th>
			                <th>部门</th>
			                <!--th>活动名称</th-->
			                <th>演讲主题</th>
			                <th>邀请状态</th>
			                <th>操作</th>
			            </tr>
			            </thead>
			            <tfoot id="content">
			            <%--  <c:forEach items="${expertManageList}" var="temp"  varStatus="status">
			                 <tr id="${temp.id}">
			                <th><span class="checkbox-custom checkbox-primary checkbox-lg inline-block vertical-align-bottom" >
		                        <input type="checkbox" class="mailbox-checkbox selectable-all" name="expertIds" value="${temp.id}_${temp.name}">
		                    	<label for="select_all"></label>
		                    </span></th>
			                <th>${temp.name}</th>
			                <th>${temp.unitname}</th>
			                <th>${temp.department}</th>
			                <th>${activity.title}</th>
			                <th>${temp.topic}</th>
			                <th>
			                    <c:if test="${temp.inviteStatus==0}">未邀请</c:if>
			                     <c:if test="${temp.inviteStatus==1}">已邀请</c:if>
			                      <c:if test="${temp.inviteStatus==2}">已拒绝</c:if>
			                </th>
			                <th><div class="item-actions">
                                <span class="btn btn-pure btn-icon btn-edit" data-toggle="modal" data-target="#roleForm" onclick="edit_emailTemplate(${temp.id},'${temp.inviteStatus}','${temp.topic}')"><i class="icon wb-edit" aria-hidden="true"></i></span>
                            </div>
							</th>
			            </tr>
			            </c:forEach>  --%>
			            </tfoot>
			        </table>
			    </div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="addLabelForm" aria-hidden="true" aria-labelledby="addLabelForm" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                <h4 class="modal-title">编辑专家状态</h4>
            </div>
            <div class="modal-body">
                <form id="activityClassForm">
                     <input type="hidden" name="id" id="id" >
                    <label class="col-sm-3 control-label"><span style="margin-right: 10px">邀请状态：</span></label>
					<div class="col-sm-9">
						<input type="radio" name="inviteStatus" value="1" >接受 <input type="radio" name="inviteStatus" value="2" >拒绝
					</div>
					</br>
					</br>
					 <label class="col-sm-3 control-label">演讲主题：</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="topic"  id="topic"
							placeholder="演讲主题" data-fv-notempty="true"
							data-fv-notempty-message="必填项" >
					</div>
					</br>
					</br>
                </form>
            </div>
            <div class="btn-actions text-center">
											<button type="button" class="btn btn-success"
												onclick="save()">保存</button>
						                	<a class="btn btn-cancel" data-dismiss="modal" href="javascript:;" onclick="clear()">取消</a>
										</div>
           <!--  <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" type="button" onclick="save()">保存</button>
                <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:;" onclick="clear()">取消</a>
            </div> -->
        </div>
    </div>
</div>

<script src="${ctx}/public/themes/classic/global/js/plugins/action-btn.js"></script>
<script src="${ctx}/public/vendor/highlight/highlight.pack.min.js"></script>
<script src="${ctx}/public/vendor/raty/jquery.raty.min.js"></script>
<script src="${ctx}/public/js/activity/expertManage.js?12"></script>
<script src="${ctx}/public/js/message/message.js"></script>