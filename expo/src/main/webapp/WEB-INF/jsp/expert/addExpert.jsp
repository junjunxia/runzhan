<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../includes/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<jsp:include page="../common/listUtil.jsp"></jsp:include>

<title>添加专家</title>
<link rel="stylesheet" href="${ctx}/public/vendor/alertify-js/alertify.css">
<script src="${ctx}/public/vendor/alertify-js/alertify.min.js"></script>
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-responsive/dataTables.responsive.css">
<link rel="stylesheet" href="${ctx}/public/vendor/highlight/default.css">
<link rel="stylesheet" href="${ctx}/public/vendor/highlight/github-gist.css">
<link rel="stylesheet" href="${ctx}/public/vendor/highlight/highlight.css">
<link rel="stylesheet" href="${ctx}/public/css/examples/tables/data-tables/examples.css">


<div class="page page-full animation-fade page-data-tables">
    <div class="page-main">
        <div class="page-content" id="DTConent">

			<div class="panel">
				<div class="panel-heading">
			        <h3 class="panel-title">添加专家</h3>
			    </div>
			    
			    <div class="panel-body">
			    	<%-- <div>
					  			${activity.title}<span></span>
					  			</br>报名时间：<fmt:formatDate value="${activity.beginTime }" type="both"/>-<fmt:formatDate value="${activity.endTime }" type="both"/> <span></span> 发布者:${activity.createName}
						  	<span></span> 参会人数:0
						  	</div>  --%>
			    	<div class="panel-body padding-10">
						<div class="row">
						  	<div class="col-sm-6">
						  	<!-- <span class="checkbox-custom checkbox-primary user-select-all">
                             <input type="checkbox" class="user-checkbox selectable-all">
                              <label></label>
                             </span> -->
					  			<div class="btn-group btn-action">
					  				<a class="btn btn-default btn-outline buttons-html5"
				  					data-pjax href="#" target="_blank" id="add-emailTemplate" onclick="invite()">
				  					<span>邀请</span>
				  				</a>
					  			</div>
						  	</div>
						  	<div class="col-sm-6">
						  		<div class="text-right">
						  			<form class="form-inline margin-bottom-0" id="seniorSearchForm" autocomplete='off' method="post">
					                	<div class="form-group">
			  					  				<input name="expertName" value="${activityInfo.expertName}"  type="search" class="form-control" placeholder="请输入搜索的专家名称" aria-controls="DataTables_Table_0">
					                	</div>
					                	 <input type="hidden" name="activityTitle" value="${activityInfo.activityTitle}" >
					                	<div class="form-group">
			  					  				<input name="domainName" id="domainName" value="${activityInfo.domainName }"  placeholder="请输入专家领域">
					                	</div>
					                	<input type="hidden" name="activityId" value="${activityInfo.activityId}" >
				                        <div class="form-group">
				                            <a class="btn btn-primary" href="javascript:showToInviteExpertList()" ><i class="icon fa-search" ></i>搜索</a>
				                        </div>
			 		                    </form>
						  		</div>
						  	</div> 
						 </div>
			   		</div>
			    	 <form class="form-horizontal" autocomplete="off" id="activityForm">
			        <table class="table table-bordered table-hover dataTable table-striped width-full text-nowrap" id="activityClassTable">
			            <thead>
			            <tr>
			                <th><span class="checkbox-custom checkbox-primary user-select-all">
                             <input type="checkbox" class="user-checkbox selectable-all" onclick="checkAll(this)">
                              <label></label>
                             </span></th>
			                <th>姓名</th>
			                <th>公司</th>
			                <th>部门</th>
			                <th>职务</th>
			                <th>所属领域</th>
			                <!--th>社交圈子</th> -->
			                <th>亲密度</th>
			            </tr>
			            </thead>
			            <tfoot id="content">
			            <%--  <c:forEach items="${expertList}" var="temp"  varStatus="status">
			                 <tr id="${temp.id}">
			                <th><span class="checkbox-custom checkbox-primary user-select-all">
                             <input type="checkbox" class="user-checkbox selectable-all" name="expertIdList" value="${temp.id}">
                              <label></label>
                             </span></th>
			                <th>${temp.name}</th>
			                <th>${temp.unitname}</th>
			                <th>${temp.department}</th>
			                <th>${temp.title}</th>
			                <th>${temp.domainName}</th>
			                  <th>${temp.socialCircle}</th>
			                  <th><div class="rating" data-score="${temp.score}" data-plugin="rating" ></div></th>
			            </tr>
			            </c:forEach>  --%>
			            </tfoot>
			        </table>
			        
			        
								<input type="hidden" name="activityId" value="${activityInfo.activityId}">
                                <input type="hidden" name="activityTitle" value="${activityInfo.activityTitle}" >
                   </form>
			    </div>
			</div>
		</div>
	</div>
</div>
<script src="${ctx}/public/themes/classic/global/js/plugins/action-btn.js"></script>
<script src="${ctx}/public/vendor/highlight/highlight.pack.min.js"></script>
<script src="${ctx}/public/vendor/raty/jquery.raty.min.js"></script>
<script src="${ctx}/public/js/expert/addExpert.js?123"></script>
