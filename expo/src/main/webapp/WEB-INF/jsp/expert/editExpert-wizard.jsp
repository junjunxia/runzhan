<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../includes/taglib.jsp"%>
<jsp:include page="../common/listUtil.jsp"></jsp:include>
<title>编辑专家</title>

<link rel="stylesheet"
	href="${ctx}/public/vendor/jquery-wizard/jquery-wizard.css">
<link rel="stylesheet"
	href="${ctx}/public/vendor/formvalidation/formValidation.css">
<link rel="stylesheet"
	href="${ctx}/public/css/examples/forms/validation.css">
<link rel="stylesheet"
	href="${ctx}/public/vendor/summernote/summernote.css">
	<link rel="stylesheet" href="${ctx}/public/vendor/alertify-js/alertify.css">
		<!-- 文件上传新增 -->
<%-- 	 <link rel="stylesheet" href="${ctx}/public/vendor/bootstrap/bootstrap.css"> --%>
    <link rel="stylesheet" href="${ctx}/public/uploadFile/css/fileinput/file-input.min.css">
    <link rel="stylesheet" href="${ctx}/public/uploadFile/css/jcrop/jquery.Jcrop.min.css">
    <link rel="stylesheet" href="${ctx}/public/uploadFile/css/sco/scojs.css">
    <link rel="stylesheet" href="${ctx}/public/uploadFile/css/sco/sco.message.css">
    <link rel="stylesheet" href="${ctx}/public/vendor/bootstrap-select/bootstrap-select.css">
    <!-- 文件上传新增结束 -->
    <!-- 文件上传新增 -->
<!-- <script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap/bootstrap.min.js"></script> -->
<!--图片裁剪插件-->
<script type="text/javascript" src="${ctx}/public/uploadFile/js/jcrop/jquery.Jcrop.min.js"></script>
<!--文件上传插件-->
<script type="text/javascript" src="${ctx}/public/uploadFile/js/fileinput/file-input.min.js"></script>
<!--fileinput中文化-->
<script type="text/javascript" src="${ctx}/public/uploadFile/js/fileinput/zh.js"></script>
<!--模态窗口优化插件,sco还有很多其他优化组件-->
<script type="text/javascript" src="${ctx}/public/uploadFile/js/sco/sco.modal.js"></script>
<!--腾讯的模板插件,使用起来类似JSTL和EL表达式-->
<!-- <script type="text/javascript" src="../js/template/template.js"></script> -->
<script type="text/javascript" src="${ctx}/public/uploadFile/js/portraitForCommon.js?1"></script>
<div class="page animation-fade page-forms">
	<div class="page-content container-fluid">
		<div class="row">
			<div class="panel" id="exampleWizardForm">
				<div class="panel-body container-fluid">
					<div class="example-wrap">
						<div class="steps steps-sm row" data-plugin="matchHeight"
							data-by-row="true" role="tablist">
							<div class="step col-md-6 current" data-target="#exampleAccount"
								role="tab">
								<span class="step-number">1</span>
								<div class="step-desc">
									<span class="step-title">专家基本信息</span>
								</div>
							</div>
							<div class="step col-md-6" data-target="#exampleBilling"
								role="tab">
								<span class="step-number">2</span>
								<div class="step-desc">
									<span class="step-title">专家履历</span>
								</div>
							</div>
						</div>

						<div class="wizard-content">
							<div class="wizard-pane active" id="exampleAccount" role="tabpanel">
								<form id="exampleAccountForm" class="form-horizontal" autocomplete="off">
								 <input type="hidden" name="id" value="${expert.id}">
									<div class="tab-content padding-top-20">
										<div class="row row-lg">
											<div class="col-md-6">
												<div class="example-wrap margin-md-0">
													<div class="example">
														<div class="form-group">
															<label class="col-sm-3 control-label">姓名：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="name"
																	placeholder="专家姓名" data-fv-notempty="true"
																	data-fv-notempty-message="必填项" value="${expert.name}">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">所属领域：</label>
															<div class="col-sm-9" id="domainIdDiv">
																<select name="domainId" data-plugin="selectpicker" data-live-search="true" id="domainId"  data-value="${expert.domainId}" class="form-control">
															 </select>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">行业领域：</label>
															<div class="col-sm-9">
															<input type="text" class="form-control" name="domainName"
																	placeholder="行业领域" data-fv-notempty="true"
																	data-fv-notempty-message="必填项" value="${expert.domainName}">
																
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">单位名称：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="unitname" value="${expert.unitname}"
																	data-fv-notempty="true" data-fv-notempty-message="必填项">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">职务：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="title"
																	data-fv-notempty="true" data-fv-notempty-message="必填项" value="${expert.title}">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">手机号码：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control"
																	name="mobile" placeholder="13012345678" maxlength="11" value="${expert.mobile}">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">部门：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control"
																	name="department" value="${expert.department}">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">社会头衔：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control"
																	name="socialTitle" value="${expert.socialTitle}">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">联系电话：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" maxlength="12" name="phone" value="${expert.phone}">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">邮箱:</label>
															<div class="col-sm-9">
																<input type="text" class="form-control"
																	name="email" placeholder="email@email.com"  value="${expert.email}">
															</div>
														</div>
														<!-- div class="form-group">
															<label class="col-sm-3 control-label">社交圈子：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="socialCircle"  value="${expert.socialCircle}">
															</div>
														</div> -->
														<div class="form-group">
															<label class="col-sm-3 control-label">传真：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="fax" value="${expert.fax}">
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">单位地址：</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="unitAddr" value="${expert.unitAddr}">
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="example-wrap">
													<div class="example">
														
														<div class="form-group">
															<label class="col-sm-3 control-label">亲密度：</label>
															<div class="col-sm-9">
															  <span class="rating" data-plugin="rating" data-score="${expert.score}" ></span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label">头像(300k内)：</label>
															<div class="col-sm-9">
															 <input type="hidden" name="image"  id="stationImgStr"  value="${expert.image}">
                                                               <!--  <input type="file"  class="file" contentEditable="false" id="stationImg" accept="image/png,image/gif" onchange="readFile()">
                                                                <input type="hidden" name="image"  id="stationImgStr"  > -->
                                                              <button type="button" name="upload" class=" btn btn-primary" style="margin-top: 15px">点击上传头像</button>
															</div>
														</div>
														<c:if test="${expert.image !=''}"> 
														<div class="form-group">
															<label class="col-sm-3 control-label"></label>
															<div class="col-sm-9">
																<img src="${expert.image}" alt="Photo" id="image"/>
															</div>
														</div>
													 </c:if>
													</div>
												</div>
											</div>
										</div>
									</div>
									<input type="hidden" name="resume" id="resume" value='${expert.resume}'></input>
								</form>
							</div>
							<div class="wizard-pane" id="exampleBilling" role="tabpanel">
								<form id="exampleBillingForm">
									<div id="summernote" data-plugin="summernote" data-lang="zh-CN"></div>
								</form>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${ctx}/public/themes/classic/global/js/components.js"></script>
<script src="${ctx}/public/vendor/formvalidation/formValidation.min.js"
	data-name="formValidation"></script>
<script
	src="${ctx}/public/vendor/formvalidation/framework/bootstrap.min.js"
	data-deps="formValidation"></script>
<script src="${ctx}/public/vendor/matchheight/jquery.matchHeight.min.js"></script>
<script src="${ctx}/public/vendor/jquery-wizard/jquery-wizard.min.js"
	data-name="wizard"></script>
<script src="${ctx}/public/js/examples/forms/wizard.js"
	data-deps="wizard"></script>

<script src="${ctx}/public/vendor/summernote/summernote.min.js"
	data-name="summernote"></script>
<script
	src="${ctx}/public/vendor/summernote/lang/summernote-zh-CN.min.js"
	data-deps="summernote"></script>
	
<script src="${ctx}/public/js/examples/forms/editor-summernote.js"></script>
<script src="${ctx}/public/vendor/alertify-js/alertify.min.js"></script>

<!-- 文件上传新增 -->
<script src="${ctx}/public/js/expert/expert.js?1313"></script>
<script src="${ctx}/public/js/expert/edit.js?331"></script>
<script src="${ctx}/public/vendor/raty/jquery.raty.min.js"></script>
<script src="${ctx}/public/vendor/bootstrap-select/bootstrap-select.min.js"></script>

<!--这段不是js代码,注意了,这个是ArtTemplate专用的模板代码,就是前面加载的template.js这个插件-->
<script id="portraitUpload" type="text/html">
    <div style="padding: 10px 20px">
        <form role="form" enctype="multipart/form-data" method="post">
            <div class="embed-responsive embed-responsive-16by9">
                <div class="embed-responsive-item pre-scrollable">
                   <img alt=""  src="${ctx}/public/uploadFile/img/showings.jpg" id="cut-img"
                         class="img-responsive img-thumbnail"/>
                </div>
            </div>
            <div class="white-divider-md"></div>
            <input type="file" name="imgFile" id="fileUpload"/>
            <div class="white-divider-md"></div>
            <div id="alert" class="alert alert-danger hidden" role="alert"></div>
            <input type="hidden" id="x" name="x"/>
            <input type="hidden" id="y" name="y"/>
            <input type="hidden" id="w" name="w"/>
            <input type="hidden" id="h" name="h"/>
        </form>
    </div>
</script>

<script type="text/javascript">
showModExpertDomain();
document.onkeydown = function () {
    if (window.event && window.event.keyCode == 13) {
        window.event.returnValue = false;
    }
}
</script>

