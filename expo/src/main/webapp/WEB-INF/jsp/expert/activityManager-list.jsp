<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../includes/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<jsp:include page="../common/listUtil.jsp"></jsp:include>
<title>活动管理</title>
<link rel="stylesheet" href="${ctx}/public/vendor/alertify-js/alertify.css">
<script src="${ctx}/public/vendor/alertify-js/alertify.min.js"></script>
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-responsive/dataTables.responsive.css">
<link rel="stylesheet" href="${ctx}/public/vendor/highlight/default.css">
<link rel="stylesheet" href="${ctx}/public/vendor/highlight/github-gist.css">
<link rel="stylesheet" href="${ctx}/public/vendor/highlight/highlight.css">
<link rel="stylesheet" href="${ctx}/public/css/examples/tables/data-tables/examples.css">


<div class="page page-full animation-fade page-data-tables">
    <div class="page-main">
        <div class="page-content" id="DTConent">

			<div class="panel">
				<div class="panel-heading">
			        <h3 class="panel-title">活动管理</h3>
			    </div>
			    
			    <div class="panel-body">
			    	<div>
		  				<font size="4">${expert.name}</font><span></span> ${expert.title} <span></span>
		  				&nbsp;<div class="rating" data-score="3" data-plugin="rating"></div>
			  			<br><i class="icon wb-home" aria-hidden="true"></i>&nbsp;&nbsp;${expert.unitname}
				  	</div>
			    	<div class="panel-body padding-10">
						<div class="row">
						  	<div class="col-sm-6">
					  			<div class="btn-group btn-action">
					  				<a class="btn btn-default btn-outline buttons-html5"
				  					data-pjax href="#" target="_blank" id="add-emailTemplate" onclick="add_emailTemplate(${expert.id})">
				  					<span>添加活动</span>
				  				</a>
					  			</div>
						  	</div>
						  	<div class="col-sm-6">
						  		<div class="text-right">
						  			<form id="activityFormquery"  class="form-inline margin-bottom-0" id="seniorSearchForm"  autocomplete='off'>
					                	<div class="form-group">
			  					  				<input name="activityName" type="search" class="form-control" placeholder="请输入搜索的活动名称" aria-controls="DataTables_Table_0">
			  					  				<input name="sponsor"  type="search" class="form-control" placeholder="主办单位" aria-controls="DataTables_Table_0">
					                	</div>
					                	<input type="hidden" name="id" value="${expert.id}" id="expertIds">
				                        <div class="form-group">
				                            <a class="btn btn-primary" href="javascript:showActivityList()" ><i class="icon fa-search" ></i>搜索</a>
				                        </div>
			 		                    </form>
						  		</div>
						  	</div>
						 </div>
			   		</div>
			    	
			        <table class="table-bordered table-hover dataTable table-striped width-full text-nowrap" id="activityClassTable">
			            <thead>
			            <tr>
			                <th>序列</th>
			                <th>活动名称</th>
			                <th>主办单位</th>
			                <th>承办单位</th>
			                <th>邀请人</th>
			                <th>演讲题目</th>
			                <th>时间</th>
			                <th>地点</th>
			              <!--   <th>备注</th> -->
			                <th>操作</th>
			            </tr>
			            </thead>
			            <tfoot id="content">
			             <c:forEach items="${activityManageList}" var="temp"  varStatus="status">
			                 <tr id="${temp.id}">
			                <th>${status.index+1}</th> 
			                 <th>
			                  <c:if test="${fn:length(temp.title)>20}">
${fn:substring(temp.title, 0, 20)}
</c:if>
<c:if test="${fn:length(temp.title)<=20}">
${temp.title}
</c:if>
			                 
			                 </th>
			                <th>${temp.sponsor}</th>
			                <th>${temp.organizer}</th>
			                <th>
			                  <c:choose>
<c:when test="${temp.inviter != null}">
   ${temp.inviter}
</c:when>                                               
<c:otherwise>                                                          
</c:otherwise></c:choose>
			                </th>
			               <th>${temp.topic}</th>
			                <th><fmt:formatDate value="${temp.beginTime}" pattern="yyyy-MM-dd"/></th>
			                <th>${temp.venue}</th>
			                <th>${temp.remarks}</th>
			               <th><div class="item-actions">
                                <span class="btn btn-pure btn-icon btn-edit" data-toggle="modal" data-target="#roleForm" onclick="edit_emailTemplate(${temp.id},'${temp.title}','${temp.inviter}','${temp.topic}','${temp.beginTime}','${temp.endTime}','${temp.venue}','${temp.remarks}','${expert.id}')"><i class="icon wb-edit" aria-hidden="true"></i></span>
                                <span class="btn btn-pure btn-icon" data-tag="list-delete" onclick="deleteActivity(${temp.id},'${temp.type}')"><i class="icon wb-close" aria-hidden="true"></i></span>
                            </div> 
							</th>
			            </tr>
			            </c:forEach>
			            </tfoot>
			        </table>
			    </div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="addLabelForm" aria-hidden="true" aria-labelledby="addLabelForm" role="dialog" tabindex="-1">
    <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                <h4 class="modal-title">添加活动</h4>
            </div>
            <div class="modal-body">
            
            <div class="row">
			<div class="panel">
				<div class="panel-body container-fluid">
					<div class="example-wrap">
						<div class="wizard-content">
							<div class="wizard-pane active" role="tabpanel">
								<form class="form-horizontal" autocomplete="off" id="activityForm">
								<input type="hidden" name="id" id="id" >
                                <input type="hidden" name="expertId" id="expertId" >
									<div class="tab-content padding-top-20">
										<div class="form-group">
											<label class="col-sm-2 control-label">活动名称<font color="red">*</font>：</label>
											<div class="col-sm-9 text-left">
												<input type="text" class="form-control" name="title" id="title"
													data-fv-notempty="true" data-fv-notempty-message="必填项">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">组织机构：</label>
											<div class="col-sm-3">
												<select data-type="organizationType" class="form-control"> 
													<option value="1" selected>主办方</option>
													<option value="2">承办方</option>
												</select>
											</div>
											<div class="col-sm-5">
												<input type="text" class="form-control"  data-name="organizationName" >
											</div>
											<button type="button" class="btn btn-pure btn-default btn-sm icon fa-trash-o pull-right delete-submenu" onclick="deleteSelect(this)"></button>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label"></label>
											   <div class="col-sm-3">
											 <select data-type="organizationType" class="form-control"> 
											    <option value="1" >主办方</option>
											    <option value="2" selected>承办方</option>
											 </select>
											</div>
											<div class="col-sm-5">
											 	<input type="text" class="form-control" data-name="organizationName">
											</div>
											<button type="button" class="btn btn-pure btn-default btn-sm icon fa-trash-o pull-right delete-submenu" onclick="deleteSelect(this)"></button>
										</div>
										<!-- div class="form-group" id="insertIndex">
											<label class="col-sm-2 control-label"></label>
											<div class="col-sm-9">
											
													<div class="btn-group">
			  			                             	<a class="btn btn-default btn-outline buttons-html5"  href="javascript:addSelect()"> <span>添加组织机构</span>
			  				                           </a>
											</div>
			  		                          	</div>
										</div> -->
										<div class="form-group">
											<label class="col-sm-2 control-label">邀请人：</label>
											<div class="col-sm-9 text-left">
												<input type="text" class="form-control" name="inviter" id="inviter"
													data-fv-notempty="true" data-fv-notempty-message="必填项">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">演讲题目：</label>
											<div class="col-sm-9 text-left">
												<input type="text" class="form-control" name="topic" id="topic"
													data-fv-notempty="true" data-fv-notempty-message="必填项">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">活动地点：</label>
											<div class="col-sm-9 text-left">
												<input type="text" class="form-control" name="venue" id="venue"
													data-fv-notempty="true" data-fv-notempty-message="必填项">
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">活动时间：</label>
											<div class="col-sm-4">
												<input type="text" class="form-control" name="beginTime"
													data-fv-notempty="true" data-fv-notempty-message="必填项" id="beginTime" placeholder="开始时间" onclick="WdatePicker({ dateFmt: 'yyyy-MM-dd HH:00:00' })">
											</div>
											<div class="col-sm-1">
											</div>
											<div class="col-sm-4">
												<input type="text" class="form-control" name="endTime"
													data-fv-notempty="true" data-fv-notempty-message="必填项"  id="endTime" placeholder="结束时间" onclick="WdatePicker({ dateFmt: 'yyyy-MM-dd HH:00:00' })">
											</div>
										</div>

                                     <div class="form-group">
															<label class="col-sm-2 control-label">活动简介：</label>
															<div class="col-sm-9">
																<textarea rows="" cols="" name="remarks" id="remarks" class="form-control"></textarea>
															</div>
										</div>
										<div class="btn-actions text-center">
											<button type="button" class="btn btn-success"
												onclick="save(${expert.id})">保存</button>
						                	<a class="btn btn-cancel" data-dismiss="modal" href="javascript:;">取消</a>
										</div>
										<input type="hidden" name="organizationType" id="organizationType">
									<input type="hidden" name="organizationName" id="organizationName">
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
            
            </div>
        </div>
    </div>
</div>

	<script src="${ctx }/public/js/calendar/WdatePicker.js"></script>
<script src="${ctx}/public/themes/classic/global/js/plugins/action-btn.js"></script>
<script src="${ctx}/public/vendor/highlight/highlight.pack.min.js"></script>
<script src="${ctx}/public/vendor/raty/jquery.raty.min.js"></script>
<script src="${ctx}/public/js/expert/expertActivity.js?12"></script>
<script>
$(document).ready(function(){  
$.fn.raty.defaults.path = '${ctx}/public/vendor/raty/images';
$(".rating").raty({score:'${expert.score}'});

})

</script>
