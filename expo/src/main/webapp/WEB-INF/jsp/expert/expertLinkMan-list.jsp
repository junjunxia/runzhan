<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../includes/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<jsp:include page="../common/listUtil.jsp"></jsp:include>
<title>联系人管理</title>
<link rel="stylesheet" href="${ctx}/public/vendor/alertify-js/alertify.css">
<script src="${ctx}/public/vendor/alertify-js/alertify.min.js"></script>
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-responsive/dataTables.responsive.css">
<link rel="stylesheet" href="${ctx}/public/vendor/highlight/default.css">
<link rel="stylesheet" href="${ctx}/public/vendor/highlight/github-gist.css">
<link rel="stylesheet" href="${ctx}/public/vendor/highlight/highlight.css">
<link rel="stylesheet" href="${ctx}/public/css/examples/tables/data-tables/examples.css">


<div class="page page-full animation-fade page-data-tables">
    <div class="page-main">
        <div class="page-content" id="DTConent">

			<div class="panel">
				<div class="panel-heading">
			        <h3 class="panel-title">联系人管理</h3>
			    </div>
			    
			    <div class="panel-body">
			    	<div>
			  			<font size="4">${expert.name}</font>&nbsp;&nbsp;${expert.title}
			  			<div class="rating" data-score="${expert.score}" data-plugin="rating" ></div>
			  			<div><i class="icon wb-home" aria-hidden="true"></i>&nbsp;&nbsp;${expert.unitname}</div>
				  	</div>
			    	<div class="panel-body padding-10">
						<div class="row">
						  	<div class="col-sm-6">
					  			<div class="btn-group btn-action">
					  				<a class="btn btn-default btn-outline buttons-html5"
				  					data-pjax href="#" target="_blank" id="add-emailTemplate" onclick="add_emailTemplate(${expert.id})">
				  					<span>添加联系人</span>
				  				</a>
					  			</div>
						  	</div>
						  	<div class="col-sm-6">
						  		<div class="text-right">
						  			<form class="form-inline margin-bottom-0" id="seniorSearchForm"   autocomplete='off' method="post">
					                	<div class="form-group">
			  					  				<input name="name" type="search" class="form-control" placeholder="请输入搜索的联系人名称" aria-controls="DataTables_Table_0">
					                	</div>
					                	<input type="hidden" name="id" value="${expert.id}" id="expertIds">
				                        <div class="form-group">
				                             <a class="btn btn-primary" href="javascript:showExpertLinkManList()" ><i class="icon fa-search" ></i>搜索</a>
				                        </div>
			 		                    </form>
						  		</div>
						  	</div>
						 </div>
			   		</div>
			    	
			        <table class="table table-bordered table-hover dataTable table-striped width-full text-nowrap" id="activityClassTable">
			            <thead>
			            <tr>
			                <th>联系人</th>
			                <th>单位名称</th>
			                <th>部门</th>
			                <th>职务</th>
			                <th>手机号码</th>
			                <th>联系电话</th>
			                <th>传真</th>
			                <th>单位地址</th>
			                <th>邮箱</th>
			                <th>操作</th>
			            </tr>
			            </thead>
			            <tfoot id="content">
			           <%--  <c:forEach items="${expertLinkManList}" var="temp"  varStatus="status">
			                 <tr id="${temp.id}">
			                <th>${temp.name}</th>
			                <th>${temp.unitname}</th>
			                <th>${temp.department}</th>
			                <th>${temp.title}</th>
			                <th>${temp.mobile}</th>
			                <th>${temp.phone}</th>
			                <th>${temp.fax}</th>
			                <th>${temp.unitAddr}</th>
			                 <th>${temp.email}</th>
			                <th><div class="item-actions">
                                <span class="btn btn-pure btn-icon btn-edit" data-toggle="modal" data-target="#roleForm" onclick="edit_emailTemplate(${temp.id},'${temp.name}','${temp.unitname}','${temp.department}','${temp.title}','${temp.mobile}','${temp.phone}','${temp.fax}','${temp.unitAddr}','${temp.email}','${temp.expertId}')"><i class="icon wb-edit" aria-hidden="true"></i></span>
                                <span class="btn btn-pure btn-icon" data-tag="list-delete" onclick="deleteActivity(${temp.id})"><i class="icon wb-close" aria-hidden="true"></i></span>
                            </div>
							</th>
			            </tr>
			            </c:forEach> --%>
			            </tfoot>
			        </table>
			    </div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="addLabelForm" aria-hidden="true" aria-labelledby="addLabelForm" role="dialog" tabindex="-1">
    <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                <h4 class="modal-title">添加联系人</h4>
            </div>
            <div class="modal-body">
                <form id="activityClassForm">
                     <input type="hidden" name="id" id="id" >
                      <input type="hidden" name="expertId" id="expertId" >
                    <!-- <div class="form-group">
                        <input type="text" class="form-control" name="name" id="name" placeholder="活动分类名称">
                        <input type="text" class="form-control" name="name" id="name" placeholder="活动分类名称">
                    </div> -->
                    <label class="col-sm-2 control-label"><span style="margin-right: 10px">姓名：</span></label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="name" id="name"
							placeholder="联系人姓名" data-fv-notempty="true"
							data-fv-notempty-message="必填项" >
					</div>
					 <label class="col-sm-2 control-label">职务：</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="title"  id="title"
							placeholder="职务" data-fv-notempty="true"
							data-fv-notempty-message="必填项" >
					</div>
					</br>
					</br>
					 <label class="col-sm-2 control-label">单位名称：</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="unitname"  id="unitname"
							placeholder="单位名称" data-fv-notempty="true"
							data-fv-notempty-message="必填项" >
					</div>
					 <label class="col-sm-2 control-label">部门：</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="department"  id="department"
							placeholder="部门" data-fv-notempty="true"
							data-fv-notempty-message="必填项" >
					</div>
					</br>
					</br>
					 <label class="col-sm-2 control-label">手机号码：</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="mobile"  id="mobile"
							placeholder="手机号码" maxlength="11" data-fv-notempty="true"
							data-fv-notempty-message="必填项" >
					</div>
					 <label class="col-sm-2 control-label">联系电话：</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="phone"  id="phone"
							placeholder="联系电话" maxlength="12" data-fv-notempty="true"
							data-fv-notempty-message="必填项" >
					</div>
					</br>
					</br>
					 <label class="col-sm-2 control-label">传真：</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="fax"  id="fax"
							placeholder="传真" data-fv-notempty="true"
							data-fv-notempty-message="必填项" >
					</div>
					 <label class="col-sm-2 control-label">邮箱：</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="email"  id="email"
							placeholder="邮箱" data-fv-notempty="true"
							data-fv-notempty-message="必填项" >
					</div>
					</br>
					</br>
					 <label class="col-sm-2 control-label">单位地址：</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="unitAddr" id="unitAddr"
							placeholder="单位地址" data-fv-notempty="true"
							data-fv-notempty-message="必填项" >
					</div>
					<label class="col-sm-2 control-label"></label>
					<div class="col-sm-4">
					</div>
					
					</br>
					</br>
					<div class="btn-actions text-center">
							<button type="button" class="btn btn-success"
								onclick="save(${expert.id})">保存</button>
						    <a class="btn btn-cancel" data-dismiss="modal" href="javascript:;" onclick="clear()">取消</a>
					</div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="${ctx}/public/themes/classic/global/js/plugins/action-btn.js"></script>
<script src="${ctx}/public/vendor/highlight/highlight.pack.min.js"></script>
<script src="${ctx}/public/vendor/raty/jquery.raty.min.js"></script>
<script src="${ctx}/public/js/expert/expertLinkman.js?4"></script>
<script>
$(document).ready(function(){  
$.fn.raty.defaults.path = '${ctx}/public/vendor/raty/images';
$(".rating").raty({score:'${expert.score}'});

})
</script>