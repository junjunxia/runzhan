<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../includes/taglib.jsp"%>
<jsp:include page="../common/listUtil.jsp"></jsp:include>
<title>专家详情</title>


<link rel="stylesheet"
	href="${ctx}/public/vendor/jquery-wizard/jquery-wizard.css">
<link rel="stylesheet"
	href="${ctx}/public/vendor/formvalidation/formValidation.css">
<link rel="stylesheet"
	href="${ctx}/public/css/examples/forms/validation.css">
<link rel="stylesheet"
	href="${ctx}/public/vendor/summernote/summernote.css">
	<link rel="stylesheet" href="${ctx}/public/css/baidu.css">
<div class="page animation-fade page-forms">
	<div class="page-content container-fluid">
		<div  style="margin-left: 12px">
			<div class="panel" id="exampleWizardForm">
				<div class="btn-group btn-group-sm" style="float: right;margin: 20px 20px;z-index: 999">
					<a class="btn btn-default btn-outline buttons-print" id="printId" onclick="myPrint()">
						<span>打&nbsp;&nbsp;&nbsp;&nbsp;印</span>
					</a>
				</div>
				<div class="panel-body container-fluid" id="myPrintArea">
					
					<div class="para-title level-2" label-module="para-title">
                       <h2 class="title-text"><span class="title-prefix"></span>基本信息</h2>
                    </div>
				  <div>
					<table class="table table-bordered">
					<tr>
					<th width="20%">姓名</th>
					<th width="30%">${expert.name}</th>
					<th width="20%">亲密度</th>
					<th width="30%"><span class="rating" data-score="${expert.score}" data-plugin="rating" style="cursor:pointer"></span></th>
					</tr>
					<tr>
					<th width="20%">单位名称</th>
					<th width="30%">${expert.unitname}</th>
					<th width="20%">社会头衔</th>
					<th width="30%">${expert.socialTitle}</th>					
					</tr>
					
					<tr>
					<th width="20%">所属行业</th>
					<th width="30%">${expert.domainIdName}</th>				
					<th width="20%">行业领域</th>
					<th width="30%">${expert.domainName}</th>
					</tr>					
					
					<tr>
					<th width="20%">部门</th>
					<th width="30%">${expert.department}</th>
					<th width="20%">职务</th>
					<th width="30%">${expert.title}</th>
					</tr>
					<tr id="mobileinfo">
					<th width="20%">手机号码</th>
					<th width="30%">${expert.mobile}</th>
					<th width="20%">联系电话</th>
					<th width="30%">${expert.phone}</th>
					</tr>
					<tr id="faxinfo">
					<th width="20%">传真</th>
					<th width="30%">${expert.fax}</th>
					<th width="20%">邮箱</th>
					<th width="30%">${expert.email}</th>
					</tr>
					
					<tr>

					<th width="20%">单位地址</th>
					<th width="30%">${expert.unitAddr}</th>
					<th width="20%"></th>
					<th width="30%"></th>					
					</tr>
					<!-- tr>
					<th width="20%">社交圈子</th>
					<th width="30%">${expert.socialCircle}</th>
					<th width="20%"></th>
					<th width="30%"></th>
					</tr> -->
					</table>
					</div>
					
					
					
					<div class="para-title level-2" label-module="para-title" id="linkinfo">
                       <h2 class="title-text"><span class="title-prefix"></span>联系人信息</h2>
                    </div>
				  <div id="link">
				 <c:if test="${not empty expertLinkManList}">   
				    <c:forEach var="linkMan" items="${expertLinkManList}">
					<table class="table table-bordered">
					<tr>
					<th width="20%">姓名</th>
					<th width="30%">${linkMan.name }</th>
					<th width="20%">职务</th>
					<th width="30%">${linkMan.title }</th>
					</tr>
					<tr>
					<th width="20%">单位名称</th>
					<th width="30%">${linkMan.unitname }</th>
					<th width="20%">部门</th>
					<th width="30%">${linkMan.department }</th>
					</tr>
					<tr>
					<th width="20%">手机号码</th>
					<th width="30%">${linkMan.mobile }</th>
					<th width="20%">联系电话</th>
					<th width="30%">${linkMan.phone }</th>
					</tr>
					<tr>
					<th width="20%">传真</th>
					<th width="30%">${linkMan.fax }</th>
					<th width="20%">邮箱</th>
					<th width="30%">${linkMan.email }</th>
					</tr>
					<tr>
					<th width="20%">单位地址</th>
					<th width="30%">${linkMan.unitAddr }</th>
					<th width="20%"></th>
					<th width="30%"></th>
					</tr>
					</table>
					</c:forEach>
					</c:if>
					</div>
					
					
					
					<div class="para-title level-2" label-module="para-title">
                       <h2 class="title-text"><span class="title-prefix"></span>专家履历</h2>
                    </div>
				  <div>
					  <span><h1>${expert.resume }</h1></span>
					</div>
					
					
					
					
						
			   <div class="para-title level-2" label-module="para-title">
                       <h2 class="title-text"><span class="title-prefix"></span>活动历史</h2>
                    </div>
				  <div>
				  <c:if test="${not empty activityHistoryList}">   
				    <c:forEach var="activity" items="${activityHistoryList}">
					  <div>
					  <span><c:if test="${not empty activity.activityDate}"> <fmt:formatDate value="${activity.activityDate }" pattern="yyyy-MM-dd"/>, </c:if><c:if test="${not empty activity.venue}"> 在${activity.venue}，</c:if>${expert.name} <c:if test="${not empty activity.inviter}">受${activity.inviter}邀请，</c:if>参加${activity.title },<c:if test="${not empty activity.topic}">发表${activity.topic }的主题演讲。</c:if><c:if test="${not empty activity.sponsor}">主办单位${activity.sponsor}；<c:if test="${not empty activity.organizer}"></c:if>承办单位${activity.organizer}</c:if>。<c:if test="${not empty activity.remarks}">(备注：${activity.remarks})</c:if></span>
					  </div>
					   </c:forEach>
					</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${ctx}/public/themes/classic/global/js/components.js"></script>
<script src="${ctx}/public/vendor/formvalidation/formValidation.min.js"
	data-name="formValidation"></script>
<script
	src="${ctx}/public/vendor/formvalidation/framework/bootstrap.min.js"
	data-deps="formValidation"></script>
<script src="${ctx}/public/vendor/matchheight/jquery.matchHeight.min.js"></script>

<script src="${ctx}/public/vendor/summernote/summernote.min.js"
	data-name="summernote"></script>
<script
	src="${ctx}/public/vendor/summernote/lang/summernote-zh-CN.min.js"
	data-deps="summernote"></script>
	
<script src="${ctx}/public/js/examples/forms/editor-summernote.js"></script>

<script src="${ctx}/public/vendor/raty/jquery.raty.min.js"></script>
<script src="${ctx}/public/vendor/alertify-js/alertify.min.js"></script>
<!-- <script src="/expo/public/vendor/jquery/jquery.PrintArea.js"></script> -->
<script src="${ctx}/public/vendor/jquery/jQuery.print.js"></script>

<script>
$(document).ready(function(){  
$.fn.raty.defaults.path = '${ctx}/public/vendor/raty/images';	
});

var hkey_root,hkey_path,hkey_key;  
hkey_root="HKEY_CURRENT_USER";  
hkey_path="\\Software\\Microsoft\\Internet Explorer\\PageSetup\\"; 
var roleType ="<%=session.getAttribute("roleType")%>"; 
if (roleType != 1 )
{
	document.getElementById("mobileinfo").style.display="none";
	document.getElementById("faxinfo").style.display="none";
	document.getElementById("linkinfo").style.display="none";
	document.getElementById("link").style.display="none"
}

function myPrint()
{
	PageSetup_Null();
	$("#myPrintArea").print({
	    globalStyles:true,//是否包含父文档的样式，默认为true
	    mediaPrint:false,//是否包含media='print'的链接标签。会被globalStyles选项覆盖，默认为false
	    stylesheet:null,//外部样式表的URL地址，默认为null

	    noPrintSelector:".no-print",//不想打印的元素的jQuery选择器，默认为".no-print"
	    iframe:true,//是否使用一个iframe来替代打印表单的弹出窗口，true为在本页面进行打印，false就是说新开一个页面打印，默认为true
	    append:null,//将内容添加到打印内容的后面
	    prepend:null,//将内容添加到打印内容的前面，可以用来作为要打印内容
	    deferred:
	 $.Deferred()//回调函数
	});
}

function PageSetup_Null()  
{  
	try{  
	 var RegWsh = new ActiveXObject("WScript.Shell") ;  
	 hkey_key="header" ;  
	 RegWsh.RegWrite(hkey_root+hkey_path+hkey_key,"") ;  
	 hkey_key="footer" ;  
	 RegWsh.RegWrite(hkey_root+hkey_path+hkey_key,"") ;  
	 }  
	catch(e){}  
}

</script>