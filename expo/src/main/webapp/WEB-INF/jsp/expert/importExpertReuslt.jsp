<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<head>
<title>专家</title>

<!-- 图标 CSS-->
<link rel="stylesheet"
	href="${ctx}/public/fonts/font-awesome/font-awesome.css">
<link rel="stylesheet"
	href="${ctx}/public/fonts/web-icons/web-icons.css">
<!-- 插件 CSS -->
<link rel="stylesheet"
	href="${ctx}/public/vendor/animsition/animsition.css">
<link rel="stylesheet" href="${ctx}/public/vendor/toastr/toastr.css">

<script src="${ctx}/public/vendor/jquery/jquery.min.js"></script>
<script src="${ctx}/public/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="${ctx}/public/vendor/bootstrap/bootstrap.min.js"></script>
<html>
<meta name="decorator" content="default" />
<script type="text/javascript">
$(function(){
	window.opener.location.reload();
})
</script>
</head>
<style>
html{background:#FFF}
</style>
<body   style="background:#FFF !important;min-width:755px;">
	<div class="header">
		<h1 class="page-title">导入结果</h1>
	</div>
	<div style="padding:0 20px">
<style>
		#contentTable th{ padding:10px 0;text-align: center;}
			#contentTable td{ padding:10px 5px;text-align: center;vertical-align:middle}
			#contentTable td .btn{margin-right:6px; margin-bottom:5px;}
		</style>

	<table id="contentTable"
		class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>信息</th>
			</tr>
		<c:choose>
			<c:when test="${text!=null && text!='' }">
				<tr >
					<th>${text}</th>
				</tr>
			</c:when>	
			<c:otherwise>
			<tr>
				<th>导入成功记录数:${fn:length(successData)}</th>
			</tr>
			<tr>
				<th>导入失败记录数:${fn:length(excelErrorMsgs)+fn:length(failData)}</th>
			</tr>
		</c:otherwise>
		</c:choose>			
		</thead>
		<tbody>
				<c:forEach items="${excelErrorMsgs}" var="msg" varStatus="varStatus">
					<tr >
						<td>${msg}</td>
					</tr>
				</c:forEach>
				<c:forEach items="${failData}" var="data" varStatus="varStatus">
					<tr >
						<td>${data.expert.name}:${data.errorMsg}</td>
					</tr>
				</c:forEach>
			
		</tbody>
	</table>
</div>
</body>
</html>