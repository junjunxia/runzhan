<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<title>发送邮件</title>

<div class="modal fade" id="addMailForm" aria-hidden="true" aria-labelledby="addMailForm" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                <h4 class="modal-title">发送邮件</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
<!--                         <select id="topicTo" class="form-control" data-plugin="select2" multiple="multiple" data-placeholder="收件人："> -->
<!--                             <optgroup label=""> -->
<!--                                 <option value="AK">梅小燕 - meixiaoyan@qq.com</option> -->
<!--                                 <option value="HI">唐雪琴 - xueqin@163.com</option> -->
<!--                             </optgroup> -->
<!--                         </select> -->

						<label class="col-sm-1 control-label">收件人：</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="name"
								placeholder="专家姓名" data-fv-notempty="true"
								data-fv-notempty-message="必填项" readonly="readonly">
						</div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">邮件标题：</label>
						<div class="col-sm-9">
							<select class="form-control" name="domainId"
								data-fv-notempty="true">
								<option value="">请选择</option>
								<option value="1">互联网</option>
								<option value="2">服务业</option>
								<option value="3">机械</option>
								<option value="4">食品</option>
							</select>
						</div>
                    </div>
                    <div class="form-group">
<!--                         <textarea name="content" class="markdown-edit" rows="10"></textarea> -->
                        <label class="col-sm-3 control-label">活动简介：</label>
						<div class="col-sm-9">
							<div id="summernote" data-plugin="summernote" data-lang="zh-CN"></div>
						</div>
                    </div>
                </form>
            </div>
            <div class="modal-footer text-center">
                <button class="btn btn-primary" data-dismiss="modal" type="submit">发送</button>
                <button class="btn btn-sm" data-dismiss="modal">取消</button>
                
<!--                 <a class="btn btn-sm" data-dismiss="modal" href="javascript:;">取消</a> -->
<!--                 btn-white -->
            </div>
        </div>
    </div>
</div>
