<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../includes/taglib.jsp"%>

<title>添加专家</title>

<div class="page animation-fade">
    <div class="page-content container-fluid">
        <div class="panel">
            <div class="panel-body container-fluid">
                 <div class="example-wrap">
                     <div class="nav-tabs-horizontal" data-approve="nav-tabs">
                         <ul class="nav nav-tabs" role="tablist">
                             <li class="active" role="presentation">
                                 <a data-toggle="tab" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab">
                                     专家基本信息
                                 </a>
                             </li>
                             <li role="presentation">
                                 <a data-toggle="tab" href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab">
                                    专家履历
                                 </a>
                             </li>
                             <li role="presentation">
                                 <a data-toggle="tab" href="#exampleTabsThree" aria-controls="exampleTabsThree" role="tab">
                                    专家联系人
                                 </a>
                             </li>
                         </ul>
                         <div class="tab-content padding-top-20">
<!--                              <form class="form-horizontal" id="exampleConstraintsForm" autocomplete="off"> -->
                             <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
								<div class="row row-lg">
				                    <div class="col-md-6">
				                        <div class="example-wrap margin-md-0">
				                            <div class="example">
			                                    <div class="form-group">
			                                        <label class="col-sm-3 control-label">姓名：</label>
			                                        <div class="col-sm-9">
			                                            <input type="text" class="form-control" name="userName" placeholder="专家姓名" data-fv-notempty="true" data-fv-notempty-message="必填项">
			                                        </div>
			                                    </div>
			                                    <div class="form-group">
			                                        <label class="col-sm-3 control-label">单位名称：</label>
			                                        <div class="col-sm-9">
			                                            <input type="text" class="form-control" name="Name" data-fv-notempty="true" data-fv-notempty-message="必填项">
			                                        </div>
			                                    </div>
			                                    <div class="form-group">
			                                        <label class="col-sm-3 control-label">职务：</label>
			                                        <div class="col-sm-9">
			                                            <input type="text" class="form-control" name="name" data-fv-notempty="true" data-fv-notempty-message="必填项">
			                                        </div>
			                                    </div>
			                                    <div class="form-group">
			                                        <label class="col-sm-3 control-label">手机号码：</label>
			                                        <div class="col-sm-9">
			                                            <input type="text" class="form-control" name="type_phone" placeholder="13012345678">
			                                        </div>
			                                    </div>
			                                    <div class="form-group">
			                                        <label class="col-sm-3 control-label">传真：</label>
			                                        <div class="col-sm-9">
			                                            <input type="text" class="form-control" name="fax">
			                                        </div>
			                                    </div>
			                                    <div class="form-group">
			                                        <label class="col-sm-3 control-label">单位地址：</label>
			                                        <div class="col-sm-9">
			                                            <input type="text" class="form-control" name="address">
			                                        </div>
			                                    </div>
				                            </div>
				                        </div>
				                    </div>
				                    <div class="col-md-6">
				                        <div class="example-wrap">
				                            <div class="example">
			                                    <div class="form-group">
			                                        <label class="col-sm-3 control-label">所属领域：</label>
			                                        <div class="col-sm-9">
			                                            <select class="form-control" name="area" data-fv-notempty="true">
			                                                <option value="">请选择</option>
			                                                <option value="a">互联网</option>
			                                                <option value="b">服务业</option>
			                                                <option value="c">机械</option>
			                                                <option value="d">食品</option>
			                                            </select>
			                                        </div>
			                                    </div>
			                                    <div class="form-group">
			                                        <label class="col-sm-3 control-label">部门：</label>
			                                        <div class="col-sm-9">
			                                            <input type="text" class="form-control" name="department">
			                                        </div>
			                                    </div>
			                                    <div class="form-group">
			                                        <label class="col-sm-3 control-label">社会头衔：</label>
			                                        <div class="col-sm-9">
			                                            <input type="text" class="form-control" name="type_numberic">
			                                        </div>
			                                    </div>
			                                    <div class="form-group">
			                                        <label class="col-sm-3 control-label">联系电话：</label>
			                                        <div class="col-sm-9">
			                                            <input type="text" class="form-control" name="tel" >
			                                        </div>
			                                    </div>
			                                    <div class="form-group">
			                                        <label class="col-sm-3 control-label">邮箱:</label>
			                                        <div class="col-sm-9">
			                                            <input type="text" class="form-control" name="type_email" placeholder="email@email.com">
			                                        </div>
			                                    </div>
			                                    <div class="form-group">
			                                        <label class="col-sm-3 control-label">社交圈子：</label>
			                                        <div class="col-sm-9">
			                                            <input type="text" class="form-control" name="sss">
			                                        </div>
			                                    </div>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				                <div class="row row-lg">
			                        <div class="col-lg-12 text-center margin-top-15">
			                            <button type="submit" class="btn btn-primary" id="validateButton1">下一步</button>
			                        </div>
			                    </div>
                             </div>
                             <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">

                             </div>
                             <div class="tab-pane" id="exampleTabsThree" role="tabpanel">

                             </div>
<!--                              </form> -->
                         </div>
                     </div>
                 </div>
            </div>
        </div>
    </div>
</div>

<script src="${ctx}/public/vendor/matchheight/jquery.matchHeight.min.js"></script>
