<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../includes/taglib.jsp"%>

<title>专家列表</title>
<%-- <link rel="stylesheet" href="${ctx}/public/vendor/bootstrap/bootstrap.css">
<link rel="stylesheet" href="${ctx}/public/vendor/datatables-responsive/dataTables.responsive.css">  --%>

<link rel="stylesheet" href="${ctx}/public/css/examples/pages/general/user.css">
<link rel="stylesheet" href="${ctx}/public/css/examples/components/advanced/rating.css">
<link rel="stylesheet" href="${ctx}/public/css/examples/forms/layouts.css">
<link rel="stylesheet" href="${ctx}/public/vendor/alertify-js/alertify.css">
<link rel="stylesheet" href="${ctx}/public/vendor/select2/select2.css">
    <link rel="stylesheet" href="${ctx}/public/vendor/bootstrap-select/bootstrap-select.css">
<script src="${ctx}/public/vendor/raty/jquery.raty.min.js"></script>
<script src="${ctx}/public/vendor/alertify-js/alertify.min.js"></script>
<script src="${ctx}/public/vendor/select2/select2.min.js" data-name="select2"></script>
<script src="${ctx}/public/vendor/select2/i18n/zh-CN.min.js" data-deps="select2"></script>
<jsp:include page="../common/listUtil.jsp"></jsp:include>
<jsp:include page="../common/message.jsp"></jsp:include>
<script src="${ctx}/public/vendor/twbs-pagination/jquery.twbsPagination.js"></script>
<script src="${ctx}/public/js/message/message.js?22dddddddddd"></script>
<style type="text/css">
.list-group-item:hover {
	background: #e6e6e6;
}
    body {
        margin-left: 0px;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
    }
 
    .auto_hidden {
        width: 204px;
        border-top: 1px solid #333;
        border-bottom: 1px solid #333;
        border-left: 1px solid #333;
        border-right: 1px solid #333;
        position: absolute;
        display: none;
    }
 
    .auto_show {
        width: 204px;
        border-top: 1px solid #333;
        border-bottom: 1px solid #333;
        border-left: 1px solid #333;
        border-right: 1px solid #333;
        position: absolute;
        z-index: 9999; /* 设置对象的层叠顺序 */
        display: block;
    }
 
    .auto_onmouseover {
        color: #ffffff;
        background-color: highlight;
        width: 100%;
    }
 
    .auto_onmouseout {
        color: #000000;
        width: 100%;
        background-color: #ffffff;
    }
</style>

<div class="page animation-fade page-user">
    <div class="page-header padding-10">
        <h6 class="page-title">专家列表</h6>
        <div class="page-header-actions">
            <button type="button" class="btn btn-icon btn-info collapsed" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
                <i class="icon fa-filter"></i>
            </button>
        </div>
    </div>
    
    <div class="page-content">
    	<div class="collapse" id="collapseFilter" aria-expanded="true">
            <div class="panel margin-bottom-10">
                <div class="panel-body padding-20">
                    <form class="form-inline" id="searchForm" autocomplete='off'  style="margin-bottom:1px">
                    	<input type="hidden" name="bean" value="expert">
						<input type="hidden" name="method" value="page">
						<input type="hidden" name="pageSize" id="pageSize" value="10">
						<input type="hidden" name="pageNum" id="pageNum" value="1">
						<input type="hidden" name="ids" id="ids">
						
						
	                	<div class="form-group">
	                    	<input type="text" class="form-control empty" placeholder="专家姓名" name="name">
	                    	<input type="text" class="form-control empty" placeholder="公司名称" name="unitname">
	                    	<input type="text" class="form-control empty" placeholder="行业领域" name="domainName">
	                    	
							<select name="domainId" data-plugin="selectpicker" data-live-search="true" id="domainId"  class="form-control col-sm-2"></select>
	                	</div>
	                	<!--<div class="form-group col-sm-5">
	                    	<input type="text" class="form-control empty" placeholder="公司名称" name="unitname">
	                	</div>-->
	                	<!-- <div class="form-group col-sm-5">
<!-- 	                	   <input type="text" class="form-control empty" placeholder="请输入所属领域匹配" id="domainName" name="domainName" onkeyup="autoComplete.start(event)">
 -->	                	  <!-- <input type="text" class="form-control empty" placeholder="行业领域" name="domainName"> -->
					        <!-- <div class="auto_hidden" id="auto">自动完成 DIV</div> -->
	                    	<!-- <select class="form-control" name="domainId" id="domainId"
																	data-fv-notempty="true">
																	<option value="">请选择</option>
																</select> -->
	                	<!--</div>-->
	                	<!-- div class="form-group col-sm-5">
	                    	<input type="text" class="form-control empty" placeholder="社交圈子"  name="socialCircle">
	                	</div> -->
<!-- 	                	<div class="form-group col-sm-5"> -->
<!-- 	                    	<button type="button" class="btn btn-success margin-bottom-10" >搜索</button> -->
<!-- 	                	</div> -->
                        <div class="form-group" style="float:right">
                            <button type="button" class="btn btn-success" onclick="tj_query()"><i class="icon fa-search"></i> 查找</button>
                        </div>
						<input type="hidden" name="quickey" id="quickey">
                    </form>
                </div>
            </div>
        </div>
    	
    	<div class="panel margin-bottom-10">
            <div class="panel-body padding-10">
				<div class="row">
				  	<div class="col-sm-6">
			  			    <span class="checkbox-custom checkbox-primary checkbox-lg inline-block vertical-align-bottom">
		                        <input type="checkbox" class="mailbox-checkbox selectable-all" onclick="select_all(this)">
		                    	<label for="select_all"></label>
								</span>
								 <div class="btn-group">
								<a class="btn btn-default btn-outline buttons-html5"
									data-pjax href="add" target="_blank">
									<span>添加专家</span>
								</a>
								<a class="btn btn-default btn-outline buttons-html5"
									href="#" onclick="showSendEmailDialog()">
									<span>邮&nbsp;&nbsp;&nbsp;&nbsp;件</span>
								</a>
								<a class="btn btn-default btn-outline buttons-html5"
									href="#" onclick="showSendSmsDialog()">
									<span>短&nbsp;&nbsp;&nbsp;&nbsp;信</span>
								</a>
								<a class="btn btn-default btn-outline buttons-html5" href="#" onclick="importExcel()">
									<span>导&nbsp;&nbsp;&nbsp;&nbsp;入</span>
								</a>
								 <a class="btn btn-default btn-outline buttons-html5" href="#" onclick="exportExcel()">
									 <span>导&nbsp;&nbsp;&nbsp;&nbsp;出</span>
								 </a>
								 <a class="btn btn-default btn-outline buttons-html5" id="deleteAllExpert" href="#" onclick="deleteAllExpert()">
									 <span>删&nbsp;&nbsp;&nbsp;&nbsp;除</span>
								 </a>
			  			</div>
				  	</div>
				  	<div class="col-sm-6">
				  		<div class="text-right" style="margin-top:10px">
				  			<span id="datatotal"></span>
				  			<label>
				  				<input type="search" class="form-control" placeholder="快速查找" aria-controls="DataTables_Table_0" id="search" onchange="quickQuery()">
				  				<%--<div class="input-search"> --%>
                                    <%--<button type="button" class="input-search-btn"> --%>
                                        <%--<i class="icon wb-search" aria-hidden="true"></i></button> --%>
                                    <%--<input type="text" class="form-control input-sm" name="jstree_search" placeholder="快速查找..."> --%>
                                <%--</div> --%>
				  				
				  			</label>
				  		</div>
				  	</div>
				 </div>
    		</div>
        </div>
    	
        <div class="panel">
            <div class="panel-body padding-10">
                <div class="nav-tabs-horizontal">
                    <div class="tab-content">
                        <div class="tab-pane animation-fade active" id="all_contacts" role="tabpanel">
                            <ul class="list-group" id="expertlist">
                            
                               <%--  <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-left">
                                            <div width="20" data-order="false">
						                        <span class="checkbox-custom checkbox-primary user-select-all">
						                            <input type="checkbox" class="user-checkbox selectable-all">
						                            <label></label>
						                        </span>
						                    </div>
                                            <div class="avatar avatar-online">
                                            	<a data-pjax href="/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                                <img src="${ctx}/public/images/portraits/13.jpg" alt="...">
	                                                <i class="avatar avatar-busy"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                        	<a data-pjax href="/tables/basic/index" target="_blank" style='color: black;text-decoration:none;'>
	                                            <h4 class="media-heading">
	                                            <font size="5">杜重治&nbsp;&nbsp;</font>
	                                                董事总经理&nbsp;&nbsp; <div class="rating" data-score="3" data-plugin="rating"></div>
	                                            </h4>
	                                        </a>
                                            <h5>南京润展国际展览有限公司</h5>
                                            <div >受邀次数:10  参会次数:3</div>
                                            <div>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-envelope" aria-hidden="true"></i>发送邮件</a>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon icon-color wb-mobile" aria-hidden="true"></i>发送短信</a>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon wb-pencil" aria-hidden="true"></i>编辑信息</a>
                                                      <a class="text-action" href="javascript:;">
                                                   <i class="icon topmenu-icon wb-table" aria-hidden="true"></i>活动管理</a>
                                                <a class="text-action" href="javascript:;">
                                                    <i class="icon wb-user-add" aria-hidden="true"></i>联系人管理</a>
                                                <a class="text-action" href="javascript:;">
                                                   <i class="icon wb-trash" aria-hidden="true"></i>删除</a>
                                            </div>
                                        </div>
                                        <div class="media-right">
                                        	<div style="width: 300px;">
                                        		<h5>行业领域：互联网金融</h5>
                                        		<h5>社交圈子：南京农业大学</h5>
                                        	</div>
                                        </div>
                                    </div>
                                </li> --%>
                            </ul>
                        <!--   <div class="row form-inline"> -->
                            	<!--  <div class="col-xs-6">
                            		<div class="hidden-xs">
                            			<div class="dataTables_length" id="dataTableExample_length">
                            				<label>每页显示
                            					<select id="pageSizeSelect" name="dataTableExample_length"  aria-controls="dataTableExample" class="form-control input-sm" onchange="change_pageSize();">
                            						<option value="2" selected="selected">2</option>
                            						<option value="3">3</option>
                            						<option value="50">50</option>
                            						<option value="100">100</option>
                            					</select>
                            				</label>
                            			</div>
                            			
                            		</div>
                            	</div>  -->
                            	 <%@ include file="../includes/pagination.jsp"%>
                           
                           <!--  </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
<div class="modal fade" id="emailDialog" aria-hidden="true" aria-labelledby="emailDialog" role="dialog" tabindex="-1">
   <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                <h4 class="modal-title">发送邮件</h4>
            </div>
            <div class="modal-body">
            
            <div class="row">
			<div class="panel">
				<div class="panel-body container-fluid">
					<div class="example-wrap">
						<div class="wizard-content">
							<div class="wizard-pane active" role="tabpanel">
								<form class="form-horizontal" autocomplete="off" id="emailForm">
									<div class="tab-content padding-top-20">
										<div class="form-group">
											<label class="col-sm-2 control-label">收件人<font color="red">*</font>：</label>
											<div class="col-sm-9 text-left">
												<input type="text" class="form-control" id="emailRecivers"
													data-fv-notempty="true" data-fv-notempty-message="必填项">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">选择模板<font color="red">*</font>：</label>
											<div class="col-sm-9">
												<select class="form-control" id="emailTemplatesList"> 
												</select>
											</div>
										</div>
                                      <div class="form-group">
											<label class="col-sm-2 control-label">邮件标题<font color="red">*</font>：</label>
											<div class="col-sm-9 text-left">
												<input type="text" class="form-control" name="title" id="title"
													data-fv-notempty="true" data-fv-notempty-message="必填项">
											</div>
										</div>
                                     <div class="form-group">
															<label class="col-sm-2 control-label"><font color="red">*</font>邮件内容：</label>
															<div class="col-sm-9">
																<textarea rows="10" name="content" id="emailContent" class="form-control"></textarea>
															</div>
										</div>
										<div class="btn-actions text-center">
											<button type="button" class="btn btn-success"
												onclick="sendEmail(event)">发送</button>
						                	<a class="btn btn-cancel" data-dismiss="modal" href="javascript:;">取消</a>
										</div>
										<input type="hidden" name="expertIds" id="emailExpertIds">
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
            
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="smsDialog" aria-hidden="true" aria-labelledby="smsDialog" role="dialog" tabindex="-1">
    <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                <h4 class="modal-title">发送短信</h4>
            </div>
            <div class="modal-body">
            
            <div class="row">
			<div class="panel">
				<div class="panel-body container-fluid">
					<div class="example-wrap">
						<div class="wizard-content">
							<div class="wizard-pane active" role="tabpanel">
								<form class="form-horizontal" autocomplete="off" id="smsForm">
									<div class="tab-content padding-top-20">
										<div class="form-group">
											<label class="col-sm-2 control-label">收件人<font color="red">*</font>：</label>
											<div class="col-sm-9 text-left">
												<input type="text" class="form-control" id="smsRecivers"
													data-fv-notempty="true" data-fv-notempty-message="必填项">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">选择模板：</label>
											<div class="col-sm-9">
												<select class="form-control" id="smsTemplatesList"> 
												</select>
											</div>
										</div>

                                     <div class="form-group">
															<label class="col-sm-2 control-label">短信内容：</label>
															<div class="col-sm-9">
																<textarea rows="10" cols="" name="content" id="smsContent" class="form-control"></textarea>
															</div>
										</div>
										<div class="btn-actions text-center">
											<button type="button" class="btn btn-success"
												onclick="sendSms(event)">发送</button>
						                	<a class="btn btn-cancel" data-dismiss="modal" href="javascript:;">取消</a>
										</div>
										<input type="hidden" name="expertIds" id="smsExpertIds">
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
            
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="importDialog" aria-hidden="true" aria-labelledby="importDialog" role="dialog" tabindex="-1">
    <div class="modal-dialog" style="width:70%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                <h4 class="modal-title">上传excel</h4>
            </div>
            <div class="modal-body">
            
            <div class="row">
			<div class="panel">
				<div class="panel-body container-fluid">
					<div class="example-wrap">
						<div class="wizard-content">
							<div class="wizard-pane active" role="tabpanel">
								<form class="form-horizontal" autocomplete="off" id="importForm" action="${ctx}/expert/importExpert" enctype="multipart/form-data"
               target="_blank" method="post">
									<div class="tab-content padding-top-20">
										<div class="form-group">
											<label class="col-sm-4 control-label">上传excel（.xls/.xlsx）<font color="red">*</font>：</label>
											<!-- <div class="col-sm-9 text-left">
												<input type="file" id="upFile" name="upFile" ac/>
											</div> -->
											<div class=" col-sm-7">
                                              <input type="file" name="upFile" class="form-control" id="input-file-now" data-plugin="dropify" data-default-file=""  accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel">
                                            </div>
										</div>

                                           <div class="col-lg-4 col-sm-6">


                    </div>
										<div class="btn-actions text-center">
											<button type="button" class="btn btn-success"
												onclick="return impInfo(event);">上传</button>
						                	<a class="btn btn-cancel" data-dismiss="modal" href="javascript:;">取消</a>
										</div>
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
            
            </div>
        </div>
    </div>
</div>
<script src="${ctx}/public/js/expert/expert.js?9"></script>
<script type="text/javascript">
showExpertDomain();
	var roleType ="<%=session.getAttribute("roleType")%>"; 
	if (roleType != 1 )
	{
		document.getElementById("deleteAllExpert").style.display="none";
	}
</script>

<script src="${ctx}/public/js/expert/autoComplete.js?5"></script>
<script src="${ctx}/public/js/expert/import.js?8"></script>
<script src="${ctx}/public/js/expert/deleteAllExpert.js?887"></script>
<script src="${ctx}/public/js/expert/export.js"></script>
<script src="${ctx}/public/vendor/bootstrap-select/bootstrap-select.min.js"></script>

