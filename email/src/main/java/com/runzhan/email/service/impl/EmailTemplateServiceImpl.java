package com.runzhan.email.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.base.service.impl.BaseServiceImpl;
import com.runzhan.base.util.JacksonUtils;
import com.runzhan.email.dao.EmailTemplateDao;
import com.runzhan.email.po.EmailTemplate;
import com.runzhan.email.po.TemplateObjectDict;
import com.runzhan.email.service.EmailTemplateService;

@Service("emailTemplateService")
public class EmailTemplateServiceImpl extends BaseServiceImpl<EmailTemplate> implements EmailTemplateService {

	@Autowired
	private EmailTemplateDao emailTemplateDao;
	

	protected BaseDao<EmailTemplate> getMapper() {
		return emailTemplateDao;
	}
	
	public String page(Map<String, Object> params) {
        List<EmailTemplate> list = emailTemplateDao.page(params);
        int total = emailTemplateDao.total(params);
        
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("total", total);
        map.put("rows", list);
        int pageSize = Integer.valueOf((String) params.get("pageSize"));
        map.put("pages", total%pageSize==0?total/pageSize:total/pageSize+1);
        map.put("pageSize", pageSize);
        map.put("pageNum", params.get("pageNum"));
        return JacksonUtils.object2json(map);
	}


	public List<TemplateObjectDict> getTemplateObjectDict() {
		return emailTemplateDao.getTemplateObjectDict();
	}


	public void updateEmailTemplate(EmailTemplate params) {
		emailTemplateDao.updateEmailTemplate(params);
	}


	public void batchDelete(List<Long> list) {
		emailTemplateDao.batchDelete(list);
	}


	public EmailTemplate getById(Long id) {
		return emailTemplateDao.getById(id);
	}


	public List<EmailTemplate> getEmailTemplateList() {
		return emailTemplateDao.queryAll();
	}

}
