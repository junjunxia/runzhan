package com.runzhan.email.service;

import java.util.List;

import com.runzhan.base.service.BaseService;
import com.runzhan.email.po.EmailTemplate;
import com.runzhan.email.po.TemplateObjectDict;

public interface EmailTemplateService extends BaseService<EmailTemplate> {

	List<TemplateObjectDict> getTemplateObjectDict();

	void updateEmailTemplate(EmailTemplate params);

	void batchDelete(List<Long> list);

	EmailTemplate getById(Long id);

	List<EmailTemplate> getEmailTemplateList();

}
