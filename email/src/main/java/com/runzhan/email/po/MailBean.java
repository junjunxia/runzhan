package com.runzhan.email.po;

import java.io.Serializable;
import java.util.Map;

public class MailBean implements Serializable {
	private static final long serialVersionUID = 5441719265326676871L;

	private String from; // 发送方
	private String[] toEmails;// 接收方

	private String subject; // 邮件主题

	private Map<String, Object> data; // 邮件数据
	private String template; // 邮件模板

	private Map<String, String> richContents; // 富文本地址

	private Map<String, String> attachments; // 附件地址

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String[] getToEmails() {
		return toEmails;
	}

	public void setToEmails(String[] toEmails) {
		this.toEmails = toEmails;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public Map<String, String> getRichContents() {
		return richContents;
	}

	public void setRichContents(Map<String, String> richContents) {
		this.richContents = richContents;
	}

	public Map<String, String> getAttachments() {
		return attachments;
	}

	public void setAttachments(Map<String, String> attachments) {
		this.attachments = attachments;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
