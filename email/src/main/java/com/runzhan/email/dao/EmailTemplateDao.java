package com.runzhan.email.dao;

import java.util.List;

import com.runzhan.base.dao.BaseDao;
import com.runzhan.email.po.EmailTemplate;
import com.runzhan.email.po.TemplateObjectDict;

public interface EmailTemplateDao extends BaseDao<EmailTemplate>{
    int deleteByPrimaryKey(Long id);

    int insert(EmailTemplate record);

    EmailTemplate selectByPrimaryKey(Long id);

	void updateEmailTemplate(EmailTemplate params);

	List<TemplateObjectDict> getTemplateObjectDict();

	void batchDelete(List<Long> list);

	EmailTemplate getById(Long id);

}