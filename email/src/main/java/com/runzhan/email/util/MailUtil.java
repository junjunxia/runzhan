package com.runzhan.email.util;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.Map.Entry;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.runzhan.email.po.MailBean;

public class MailUtil {
	
	Logger log = Logger.getLogger(MailUtil.class);  
    
    private JavaMailSenderImpl javaMailSender;
    @Autowired
    private VelocityEngine velocityEngine;          //模板解析  
      
    /** 
     * @param mailBean 
     * @return 
     * @throws MessagingException 
     */  
    public boolean send(MailBean mailBean) {  
  
    	try {
    		MimeMessage msg = createMimeMessage(mailBean);  
    		if(msg == null){  
    			return false;  
    		}  
    		
    		this.sendMail(msg, mailBean);  
		} catch (MessagingException e) {
			log.error(" MessagingException : " + mailBean + "\n" + e);  
		}
          
        return true;  
    }  
      
    private void sendMail(MimeMessage msg, MailBean mailBean){  
                javaMailSender.send(msg);  
                log.info("$$$ Send mail Subject:" +  mailBean.getSubject()   
                        + ", TO:" + arrayToStr(mailBean.getToEmails()) );  
  
    }  
    
    /*  
     * 根据 mailBean 创建 MimeMessage 
     */  
    private MimeMessage createMimeMessage(MailBean mailBean) throws MessagingException {  
        if (!checkMailBean(mailBean)) {  
            return null;  
        }  
        String text = getMessage(mailBean);  
        if(text == null){  
            log.warn("@@@ warn mail text is null (Thread name="   
                    + Thread.currentThread().getName() + ") @@@ " +  mailBean.getSubject());  
            return null;  
        }  
        MimeMessage msg = javaMailSender.createMimeMessage();  
        MimeMessageHelper messageHelper = new MimeMessageHelper(msg, true, "UTF-8"); 
        
        String from = mailBean.getFrom();
        if (StringUtils.isEmpty(from)) {
        	from = javaMailSender.getUsername();
        }
        messageHelper.setFrom(from);  
        messageHelper.setSubject(mailBean.getSubject());  
        messageHelper.setTo(mailBean.getToEmails());  
        messageHelper.setText(text, true); // html: true  
        
        // 添加富文本
        addRichContents(messageHelper, mailBean.getRichContents());
        
        // 添加附件
        addAttachmentUrls(messageHelper, mailBean.getAttachments());
        
        return msg;
    }  
    
    // 添加附件
    private void addAttachmentUrls(MimeMessageHelper messageHelper, Map<String, String> attachments) throws MessagingException {
    	if (!CollectionUtils.isEmpty(attachments)) {
			for (Entry<String, String> entry: attachments.entrySet()) {
			    FileSystemResource attachmentUrl = new FileSystemResource(new File(entry.getValue()));
			    messageHelper.addAttachment(entry.getKey(), attachmentUrl);
			}
    	}
	}

    // 添加富文本
	private void addRichContents(MimeMessageHelper messageHelper, Map<String, String> richContents) throws MessagingException {
    	if (!CollectionUtils.isEmpty(richContents)) {
			for (Entry<String, String> entry: richContents.entrySet()) {
			    FileSystemResource richContentUrl = new FileSystemResource(new File(entry.getValue()));
			    messageHelper.addInline(entry.getKey(), richContentUrl); 
			}
    	}
	}

	/* 
     * 模板解析 
     * @param mailBean 
     * @return 
     */  
    private String getMessage(MailBean mailBean) {  
        StringWriter writer = null;  
        try {  
  
            writer = new StringWriter();  
            VelocityContext context = new VelocityContext(mailBean.getData());  
  
            velocityEngine.evaluate(context, writer, "", mailBean.getTemplate());  
  
            return writer.toString();  
        } catch (VelocityException e) {  
            log.error(" VelocityException : " + mailBean.getSubject() + "\n" + e);  
        } finally {  
            if (writer != null) {  
                try {  
                    writer.close();  
                } catch (IOException e) {  
                    log.error("###StringWriter close error ... ");  
                }  
            }  
        }  
        return null;  
    }  
      
    /* 
     * check 邮件 
     */  
    private boolean checkMailBean(MailBean mailBean){  
        if (mailBean == null) {  
            log.warn("@@@ warn mailBean is null (Thread name="   
                    + Thread.currentThread().getName() + ") ");  
            return false;  
        }  
        if (mailBean.getSubject() == null) {  
            log.warn("@@@ warn mailBean.getSubject() is null (Thread name="   
                    + Thread.currentThread().getName() + ") ");  
            return false;  
        }  
        if (mailBean.getToEmails() == null) {  
            log.warn("@@@ warn mailBean.getToEmails() is null (Thread name="   
                    + Thread.currentThread().getName() + ") ");  
            return false;  
        }  
        if (mailBean.getTemplate() == null) {  
            log.warn("@@@ warn mailBean.getTemplate() is null (Thread name="   
                    + Thread.currentThread().getName() + ") ");  
            return false;  
        }  
        return true;  
    }  
    
    /* 
     * 记日记用的 
     */  
    private String arrayToStr(String[] array){  
        if(array == null || array.length == 0){  
            return null;  
        }  
        StringBuffer sb = new StringBuffer();  
        for(String str : array){  
            sb.append(str+" , ") ;  
        }  
        return sb.toString();  
    }  
  
      
    /*===================== setter & getter =======================*/  
    public void setJavaMailSender(JavaMailSenderImpl javaMailSender) {  
        this.javaMailSender = javaMailSender;  
    }  
  
    public void setVelocityEngine(VelocityEngine velocityEngine) {  
        this.velocityEngine = velocityEngine;  
    }  

}
